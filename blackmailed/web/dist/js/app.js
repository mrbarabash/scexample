(function () {
    function Carousel(options) {
        var element = document.getElementById(options.elem);

        if (!element) {
            return;
        }

        var interval = options.interval || 3000,

            crslClass = 'slider',
            crslArrowPrevClass = 'slider__control--prev',
            crslArrowNextClass = 'slider__control--next',

            count = element.querySelectorAll('li').length,
            current = 0,
            cycle = null;

        /**
         * Render the carousel if more than one slide and body's width more or equal than 600px.
         * Otherwise just show the single item.
         */
        if ((count > 1) && document.body.offsetWidth >= '600') {
            arrows();
            play();
        }

        /**
         * Helper for moving items - last to be first or first to be the last. Needed
         * for infinite rotation of the carousel.
         */
        function moveItem(i, marginLeft, position) {
            var itemToMove = element.querySelectorAll('.' + crslClass + ' > ul li')[i];
            itemToMove.style.marginLeft = marginLeft;

            element.querySelector('.' + crslClass + ' > ul')
                .removeChild(itemToMove);

            element.querySelector('.' + crslClass + ' > ul')
                .insertAdjacentHTML(position, itemToMove.outerHTML);
        }

        /**
         * Find the navigation arrows (prev/next) and add the event listener.
         */
        function arrows() {
            var buttonPrev = document.querySelectorAll('.' + crslArrowPrevClass)[0];
            var buttonNext = document.querySelectorAll('.' + crslArrowNextClass)[0];

            buttonPrev.addEventListener('click', showPrev);
            buttonNext.addEventListener('click', showNext);
        }

        /**
         * Animate the carousel to go back 1 slide.
         */
        function animatePrev(item) {
            item.style.marginLeft = '';
        }

        /**
         * Animate the carousel to go forward 1 slide.
         */
        function animateNext(item) {
            item.style.marginLeft = -element.offsetWidth + 'px';
        }

        /**
         * Move the carousel back.
         */
        function showPrev() {
            animatePrev(document.querySelectorAll('.' + crslClass + ' > ul li')[0]);
            moveItem(count - 1, -element.offsetWidth + 'px', 'afterBegin');

            clearInterval(cycle);
            cycle = setInterval(showNext.bind(this), interval);

            adjustCurrent(-1);
        }

        /**
         * Move the carousel forward.
         */
        function showNext() {
            animateNext(document.querySelectorAll('.' + crslClass + ' > ul li')[1]);
            moveItem(0, '', 'beforeEnd');

            clearInterval(cycle);
            cycle = setInterval(showNext.bind(this), interval);

            adjustCurrent(1);
        }

        /**
         * Stop the carousel.
         */
        function stop() {
            clearInterval(cycle);
        }

        /**
         * Adjust current.
         */
        function adjustCurrent(val) {
            current += val;
        }

        /**
         * Start the auto play.
         * If already playing do nothing.
         */
        function play() {
            if (cycle) {
                return;
            }
            cycle = setInterval(showNext.bind(this), interval);
        }

        return {
            'prev': showPrev,
            'next': showNext,
            'play': play,
            'stop': stop
        };
    }

    (function () {
        var carouselElement = 'carousel';

        if (document.getElementById(carouselElement)) {
            new Carousel({
                elem:     'carousel',    // id of the carousel container
                interval: 8000       // interval between slide changes
            });
        }
    }());
})();

(function(){
	/*
	* Find all needed elements.
	*
	*/
	var changePswdForm = document.getElementById('changePassword'),

		changePswdBtn = document.getElementById('changePswdBtn'),
		cancelChangePswdBtn = document.getElementById('cancelChangePassword'),
		savePswdBtn = document.getElementById('savePassword'),

		changePasswordWrapperList = document.getElementsByClassName('change-password'),
		changePasswordWrapper;

	if (changePswdBtn && savePswdBtn && cancelChangePswdBtn) {
		changePswdBtn.addEventListener('click', function(){toggleForm(this)});
		savePswdBtn.addEventListener('click', function(){toggleForm(this)});
		cancelChangePswdBtn.addEventListener('click', cancelChanges);
	}

	/*
	* Toggle form.
	*
	*/
	function toggleForm(that) {

		changePasswordWrapper = findAncestor(that, 'change-password');

		for (var i = 0; i < changePasswordWrapperList.length; i++) {
			changePasswordWrapperList[i].style.display = 'block';
		}
		changePasswordWrapper.style.display = 'none';
	}

	/*
	* Clear all inputs and close form.
	*
	*/
	function cancelChanges() {
		changePswdForm.reset();
		var that = this;
		toggleForm(that);
	}

	/*
	* Find the closest elem with class.
	*
	*/
	function findAncestor(el, cls) {
		while ((el = el.parentElement) && !el.classList.contains(cls));
		return el;
	}

})();
(function () {
    var secondsRemaining,
        intervalHandle,
        countdown = document.getElementsByClassName('countdown')[0];

    if (!countdown) {
        return;
    }

    function tick() {
        /**
         * Get the elements.
         */
        var hourDisplay = document.getElementsByClassName('countdown__item--hours')[0],
            minDisplay = document.getElementsByClassName('countdown__item--mins')[0],
            secDisplay = document.getElementsByClassName('countdown__item--secs')[0];

        /**
         * Computing values.
         */
        var hour = Math.floor(secondsRemaining / 3600),
            min = Math.floor(secondsRemaining / 60) - (hour * 60),
            sec = secondsRemaining - (hour * 3600) - (min * 60);

        /**
         * Display values.
         */
        hour = addLeadingZero(hour);
        min = addLeadingZero(min);
        sec = addLeadingZero(sec);


        /**
         * Add a leading zero for correct displaying.
         */
        hourDisplay.innerHTML = hour;
        minDisplay.innerHTML = min;
        secDisplay.innerHTML = sec;

        /**
         * Stop countdown.
         */
        if (secondsRemaining === 0) {
            clearInterval(intervalHandle);
        }

        /**
         * Subtract from seconds remaining.
         */
        secondsRemaining--;

    }

    function startCountdown() {
        /**
         * Get content from countdown's data-attr.
         */
        secondsRemaining = document.getElementsByClassName('countdown')[0].getAttribute('data-countdown');

        /**
         * Every second, call the 'tick' function.
         */
        intervalHandle = setInterval(tick, 1000);

    }

    /**
     * Add a leading zero (as a string value) if hour, min or sec less than 10.
     */
    function addLeadingZero(val) {
        if (val < 10) {
            val = '0' + val;
        }
        return val;
    }

    /**
     * Start countdown.
     */
    startCountdown();


}());

(function () {
    /*
    * Function to toggle input type.
    *
    */
    function togglePasswordFieldClicked() {

        var passwordField = document.querySelectorAll('input.form__input--password');

        for (var i = 0; i < passwordField.length; i++) {
            if (passwordField[i].type == 'password') {
                passwordField[i].type = 'text';
            } else {
                passwordField[i].type = 'password';
            }
        }
    }

    /*
    * Add an event listener on the elements.
    *
    */
    function handleEvent() {
        var toggleLink = document.getElementById('showPassword');

        if (toggleLink) {
            toggleLink.addEventListener('change', togglePasswordFieldClicked);
        }
    }

    /*
    * Init.
    *
    */
    handleEvent();

})();
(function () {
    'use strict';

    var closeButton = document.querySelector('.promo-block__link--close');
    if (!closeButton) {
        return;
    }

    closeButton.addEventListener('click', function (e) {
        e.preventDefault();

        createCookie('hide_signup_banner', 1, 1000);

        var promoBlock = document.querySelector('.promo-block');
        promoBlock.parentNode.removeChild(promoBlock);
    });

    function createCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }
}());

(function () {
    /*
    * Get the elements.
    *
    */
    var toggleMenu = document.getElementsByClassName('toggle-menu')[0],
        mobileMenu = document.getElementsByClassName('navigation')[0],
        infoTitle = document.getElementsByClassName('infoblock__title');

    /*
    * Add an event listener on the elements.
    *
    */
    toggleMenu.addEventListener('click', function () {
        var toggleMenuAnimateEl = this.getElementsByClassName('sandwich')[0];
        toggleSlide(toggleMenuAnimateEl, mobileMenu);
    });

    for (var i = 0; i < infoTitle.length; i++) {
        infoTitle[i].addEventListener('click', infoSlide);
    }

    /*
    * Expand the block.
    *
    */
    function slideDown(element) {
        element.className += ' animate';
        element.classList.remove('slideup');
        element.classList.toggle('slidedown');
    }

    /*
    * Disappear the block.
    *
    */
    function slideUp(element) {
        element.className += ' animate';
        element.classList.remove('slidedown');
        element.classList.toggle('slideup');
    }

    /*
    * Check the block's status.
    * If the block is displayed - hide it;
    * if the block is hidden - unfold it.
    */
    function toggleSlide(current, element) {
        if (hasClass(current, 'active')) {
            current.classList.remove('active');
            slideUp(element);
        } else {
            current.classList.add('active');
            slideDown(element);
        }
    }

    function infoSlide() {
        var ancestor = findAncestor(this, 'infoblock'),
            target = ancestor.getElementsByClassName('infoblock__list')[0];
        toggleSlide(this, target);
    }

    function findAncestor(el, cls) {
        while ((el = el.parentElement) && !el.classList.contains(cls)) {
        }
        return el;
    }

    function hasClass(element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }
}());

//# sourceMappingURL=app.js.map
