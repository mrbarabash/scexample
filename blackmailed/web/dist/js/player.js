(function (config, playerContainer, flowplayer, Cookies) {
    'use strict';

    if (!config) {
        return;
    }

    if (!playerContainer) {
        return;
    }

    var flowplayerConfig = {
        "aspectRatio": "16:9",
        "autoplay":    false,
        "key":         "$789717647597974",
        "analytics":   config.gaId,
        "share":       false,
        "hlsjs":       {
            "startLevel": "auto"
        },
        "clip":        {
            "quality":    true,
            "sources":    config.sources,
            "title":      config.title,
            "thumbnails": {
                "height":      99,
                "interval":    5,
                "time_format": function (t) {
                    var padding = "0000";
                    return padding.substring(0, padding.length - t.toString().length) + t;
                },
                "template":    config.thumbnailsTemplate,
                "preload":     false
            }
        }
    };

    var api = flowplayer(playerContainer, flowplayerConfig);

    if (config.limitPromoViews) {
        var MAX_PROMO_VIEWS = 5;
        var currentViews = +(Cookies.get('bm_limit_promo_views')) || 0;

        if (currentViews >= MAX_PROMO_VIEWS) {
            api.disable(true);
// display overlay
            return false;
        }

        api.on('ready', function onPlayerLoad(e) {
            currentViews++;

            Cookies.set('bm_limit_promo_views', currentViews, {expires: 365});
        });
    }

}(playerConfig, document.querySelector('.j-flowplayer'), flowplayer, Cookies));

//# sourceMappingURL=player.js.map
