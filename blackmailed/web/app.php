<?php

use Symfony\Component\HttpFoundation\Request;

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__ . '/../app/autoload.php';
include_once __DIR__ . '/../var/bootstrap.php.cache';

$kernel = new AppKernel('prod', false);

$request = Request::createFromGlobals();
Request::setTrustedProxies(
    ['127.0.0.1', $request->server->get('REMOTE_ADDR')], // trust *all* requests
    Request::HEADER_X_FORWARDED_AWS_ELB // if you're using ELB, otherwise use a constant from above
);

$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
