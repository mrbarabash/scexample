#!/bin/sh

setfacl -dR -m u:www-data:rwX -m u:$(whoami):rwX var
setfacl -R -m u:www-data:rwX -m u:$(whoami):rwX var
php-fpm &

/var/awslogs/bin/awslogs-agent-launcher.sh &

SYMFONY_ENV=prod /var/www/bin/console app:consume-sqs-queue sync_content_permissions_bm &
SYMFONY_ENV=prod /var/www/bin/console app:consume-sqs-queue sync_customer_bm &

wait
