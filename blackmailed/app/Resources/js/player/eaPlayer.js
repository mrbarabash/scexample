(function (appConfig, playerContainer, flowplayer, Cookies) {
    'use strict';

    if (!appConfig || !playerContainer || !flowplayer || !Cookies) {
        return;
    }

    var eaPlayer = {
        MAX_PROMO_VIEWS:         5,
        PROMO_VIEWS_COOKIE_NAME: 'bm_limit_promo_views',

        _appConfig:       null,
        _playerContainer: null,
        _api:             null,
        _lastVolumeValue: null,

        init: function init(appConfig, playerContainer) {
            this._appConfig = appConfig;
            this._playerContainer = playerContainer;
            this._api = flowplayer(playerContainer, this._getFlowplayerConfig());

            this._limitPromoViews();
            this._addFullScreenButton();
            this._addVolumeControl();
            this._styleQualityButton();
            this._initTrailerSwitch();
            this._initStreamSwitch();

        },

        _initTrailerSwitch: function initTrailerSwitch() {
            var trailerButton = document.querySelector('.j-downloads__btn--trailer');
            if (!trailerButton) {
                return;
            }

            trailerButton.addEventListener('click', function (e) {
                e.preventDefault();

                var streamButton = document.querySelector('.j-downloads__btn--stream');
                streamButton.classList.remove('downloads__btn--hidden');

                trailerButton.classList.add('downloads__btn--hidden');
                this._api.load(this._getClip(false));

                this._scrollToTop();
            }.bind(this));
        },

        _initStreamSwitch: function initStreamSwitch() {
            var streamButton = document.querySelector('.j-downloads__btn--stream');
            if (!streamButton) {
                return;
            }

            streamButton.addEventListener('click', function (e) {
                e.preventDefault();

                var trailerButton = document.querySelector('.j-downloads__btn--trailer');
                trailerButton.classList.remove('downloads__btn--hidden');

                streamButton.classList.add('downloads__btn--hidden');
                this._api.load(this._getClip(true));

                this._scrollToTop();
            }.bind(this));
        },

        _scrollToTop: function scrollToTop() {
            scrollTo(document.documentElement, 0, 300);

            function scrollTo(element, to, duration) {
                if (duration <= 0) return;
                var difference = to - element.scrollTop;
                var perTick = difference / duration * 10;

                setTimeout(function() {
                    element.scrollTop = element.scrollTop + perTick;
                    if (element.scrollTop === to) return;
                    scrollTo(element, to, duration - 10);
                }, 10);
            }
        },

        _styleQualityButton: function styleQualityButton() {
            this._api.on('ready', function styleQualityButtonCb() {
                var qualitySelector = this._playerContainer.querySelector('.fp-qsel');
                if (qualitySelector) {
                    qualitySelector.innerHTML = '';
                }
            }.bind(this));
        },

        _limitPromoViews: function limitPromoViews() {
            if (this._appConfig.limitPromoViews) {
                var currentViews = +(Cookies.get(this.PROMO_VIEWS_COOKIE_NAME)) || 0;

                if (currentViews >= this.MAX_PROMO_VIEWS) {
                    this._api.disable(true);
                    this._playerContainer.querySelector('.overlay-sign-up').classList.add('overlay--visible');

                    return;
                }

                this._api.on('ready', function limitPromoViewsCb() {
                    currentViews++;

                    Cookies.set(this.PROMO_VIEWS_COOKIE_NAME, currentViews, {expires: 365});
                }.bind(this));
            }
        },

        _addVolumeControl: function addVolumeControl() {
            var volumeControl = this._playerContainer.querySelector('.fp-volume');
            volumeControl.parentNode.removeChild(volumeControl);

            volumeControl = document.createElement('input');
            volumeControl.min = 0;
            volumeControl.max = 1;
            volumeControl.step = 0.1;
            volumeControl.type = 'range';
            volumeControl.className += 'fp-volume fp-icon';

            var volumeIcon = document.createElement('span');
            volumeIcon.classList.add('fp-volume-icon', 'fp-icon');

            var volumeControlWrapper = document.createElement('div');
            volumeControlWrapper.classList.add('fp-volume-wrapper');
            volumeControlWrapper.appendChild(volumeIcon);
            volumeControlWrapper.appendChild(volumeControl);

            this._playerContainer.querySelector('.fp-controls').insertBefore(
                volumeControlWrapper,
                this._playerContainer.querySelector('.fp-elapsed')
            );

            this._setVolumeBarValue(this._api.volumeLevel);

            volumeControl.addEventListener('change', function (e) {
                this._api.volume(e.target.value);
            }.bind(this));

            volumeIcon.addEventListener('click', function (e) {
                var newVolume;
                if (this._api.volumeLevel > 0) {
                    newVolume = 0;
                } else {
                    newVolume = +this._lastVolumeValue || 1;
                }

                this._api.volume(newVolume);
            }.bind(this));

            this._api.on('mute', function (e, api) {
                this._setVolumeBarValue(0);
            }.bind(this));

            this._api.on('volume', function (e, api, volume) {
                this._setVolumeBarValue(volume);
            }.bind(this));
        },

        _setVolumeBarValue: function setVolumeBarValue(volume) {
            volume = +volume;
            var volumeControlWrapper = this._playerContainer.querySelector('.fp-volume-wrapper');

            if (volume) {
                volumeControlWrapper.classList.remove('mute');
                this._lastVolumeValue = volume;
            } else {
                volumeControlWrapper.classList.add('mute');
            }

            this._playerContainer.querySelector('.fp-volume').value = volume;
        },

        _addFullScreenButton: function addFullScreenButton() {
            var fullscreenButton = this._playerContainer.querySelector('.fp-fullscreen');
            fullscreenButton.parentNode.removeChild(fullscreenButton);

            fullscreenButton = document.createElement('a');
            fullscreenButton.className += 'fp-fullscreen fp-icon';
            playerContainer.querySelector('.fp-controls').appendChild(fullscreenButton);
        },

        _getFlowplayerConfig: function getFlowplayerConfig() {
            var isStreamSource = this._appConfig.streamSources.length > 0;

            return {
                "aspectRatio": "16:9",
                "autoplay":    false,
                "key":         "$789717647597974",
                "analytics":   this._appConfig.gaId,
                "share":       false,
                "hlsjs":       {
                    "startLevel": "auto"
                },
                "clip":        this._getClip(isStreamSource)
            };
        },

        _getClip: function getClip(isStream) {
            return {
                "quality":    true,
                "sources":    isStream ? this._appConfig.streamSources : this._appConfig.trailerSources,
                "thumbnails": {
                    "height":      99,
                    "interval":    5,
                    "time_format": function (t) {
                        var padding = "0000";
                        return padding.substring(0, padding.length - t.toString().length) + t;
                    },
                    "template":    isStream ? this._appConfig.streamThumbnailsTemplate : this._appConfig.trailerThumbnailsTemplate,
                    "preload":     false
                }
            };
        }
    };

    eaPlayer.init(appConfig, playerContainer);

}(playerConfig, document.querySelector('.j-flowplayer'), flowplayer, Cookies));
