(function () {
    /*
    * Function to toggle input type.
    *
    */
    function togglePasswordFieldClicked() {

        var passwordField = document.querySelectorAll('input.form__input--password');

        for (var i = 0; i < passwordField.length; i++) {
            if (passwordField[i].type == 'password') {
                passwordField[i].type = 'text';
            } else {
                passwordField[i].type = 'password';
            }
        }
    }

    /*
    * Add an event listener on the elements.
    *
    */
    function handleEvent() {
        var toggleLink = document.getElementById('showPassword');

        if (toggleLink) {
            toggleLink.addEventListener('change', togglePasswordFieldClicked);
        }
    }

    /*
    * Init.
    *
    */
    handleEvent();

})();