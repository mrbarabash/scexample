(function(){
	/*
	* Find all needed elements.
	*
	*/
	var changePswdForm = document.getElementById('changePassword'),

		changePswdBtn = document.getElementById('changePswdBtn'),
		cancelChangePswdBtn = document.getElementById('cancelChangePassword'),
		savePswdBtn = document.getElementById('savePassword'),

		changePasswordWrapperList = document.getElementsByClassName('change-password'),
		changePasswordWrapper;

	if (changePswdBtn && savePswdBtn && cancelChangePswdBtn) {
		changePswdBtn.addEventListener('click', function(){toggleForm(this)});
		// savePswdBtn.addEventListener('click', function(){toggleForm(this)});
		cancelChangePswdBtn.addEventListener('click', cancelChanges);
	}

	/*
	* Toggle form.
	*
	*/
	function toggleForm(that) {

		changePasswordWrapper = findAncestor(that, 'change-password');

		for (var i = 0; i < changePasswordWrapperList.length; i++) {
			changePasswordWrapperList[i].style.display = 'block';
		}
		changePasswordWrapper.style.display = 'none';
	}

	/*
	* Clear all inputs and close form.
	*
	*/
	function cancelChanges() {
		changePswdForm.reset();
		var that = this;
		toggleForm(that);
	}

	/*
	* Find the closest elem with class.
	*
	*/
	function findAncestor(el, cls) {
		while ((el = el.parentElement) && !el.classList.contains(cls));
		return el;
	}

})();