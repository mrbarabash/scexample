(function () {
    /*
    * Get the elements.
    *
    */
    var toggleMenu = document.getElementsByClassName('toggle-menu')[0],
        mobileMenu = document.getElementsByClassName('navigation')[0],
        infoTitle = document.getElementsByClassName('infoblock__title');

    /*
    * Add an event listener on the elements.
    *
    */
    toggleMenu.addEventListener('click', function () {
        var toggleMenuAnimateEl = this.getElementsByClassName('sandwich')[0];
        toggleSlide(toggleMenuAnimateEl, mobileMenu);
    });

    for (var i = 0; i < infoTitle.length; i++) {
        infoTitle[i].addEventListener('click', infoSlide);
    }

    /*
    * Expand the block.
    *
    */
    function slideDown(element) {
        element.className += ' animate';
        element.classList.remove('slideup');
        element.classList.toggle('slidedown');
    }

    /*
    * Disappear the block.
    *
    */
    function slideUp(element) {
        element.className += ' animate';
        element.classList.remove('slidedown');
        element.classList.toggle('slideup');
    }

    /*
    * Check the block's status.
    * If the block is displayed - hide it;
    * if the block is hidden - unfold it.
    */
    function toggleSlide(current, element) {
        if (hasClass(current, 'active')) {
            current.classList.remove('active');
            slideUp(element);
        } else {
            current.classList.add('active');
            slideDown(element);
        }
    }

    function infoSlide() {
        var ancestor = findAncestor(this, 'infoblock'),
            target = ancestor.getElementsByClassName('infoblock__list')[0];
        toggleSlide(this, target);
    }

    function findAncestor(el, cls) {
        while ((el = el.parentElement) && !el.classList.contains(cls)) {
        }
        return el;
    }

    function hasClass(element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }
}());
