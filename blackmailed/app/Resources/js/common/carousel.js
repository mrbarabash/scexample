(function () {
    function Carousel(options) {
        var element = document.getElementById(options.elem);

        if (!element) {
            return;
        }

        var interval = options.interval || 3000,

            crslClass = 'slider',
            crslArrowPrevClass = 'slider__control--prev',
            crslArrowNextClass = 'slider__control--next',

            count = element.querySelectorAll('li').length,
            current = 0,
            cycle = null;

        /**
         * Render the carousel if more than one slide and body's width more or equal than 600px.
         * Otherwise just show the single item.
         */
        if ((count > 1) && document.body.offsetWidth >= '600') {
            arrows();
            play();
        }

        /**
         * Helper for moving items - last to be first or first to be the last. Needed
         * for infinite rotation of the carousel.
         */
        function moveItem(i, marginLeft, position) {
            var itemToMove = element.querySelectorAll('.' + crslClass + ' > ul li')[i];
            itemToMove.style.marginLeft = marginLeft;

            element.querySelector('.' + crslClass + ' > ul')
                .removeChild(itemToMove);

            element.querySelector('.' + crslClass + ' > ul')
                .insertAdjacentHTML(position, itemToMove.outerHTML);
        }

        /**
         * Find the navigation arrows (prev/next) and add the event listener.
         */
        function arrows() {
            var buttonPrev = document.querySelectorAll('.' + crslArrowPrevClass)[0];
            var buttonNext = document.querySelectorAll('.' + crslArrowNextClass)[0];

            buttonPrev.addEventListener('click', showPrev);
            buttonNext.addEventListener('click', showNext);
        }

        /**
         * Animate the carousel to go back 1 slide.
         */
        function animatePrev(item) {
            item.style.marginLeft = '';
        }

        /**
         * Animate the carousel to go forward 1 slide.
         */
        function animateNext(item) {
            item.style.marginLeft = -element.offsetWidth + 'px';
        }

        /**
         * Move the carousel back.
         */
        function showPrev() {
            animatePrev(document.querySelectorAll('.' + crslClass + ' > ul li')[0]);
            moveItem(count - 1, -element.offsetWidth + 'px', 'afterBegin');

            clearInterval(cycle);
            cycle = setInterval(showNext.bind(this), interval);

            adjustCurrent(-1);
        }

        /**
         * Move the carousel forward.
         */
        function showNext() {
            animateNext(document.querySelectorAll('.' + crslClass + ' > ul li')[1]);
            moveItem(0, '', 'beforeEnd');

            clearInterval(cycle);
            cycle = setInterval(showNext.bind(this), interval);

            adjustCurrent(1);
        }

        /**
         * Stop the carousel.
         */
        function stop() {
            clearInterval(cycle);
        }

        /**
         * Adjust current.
         */
        function adjustCurrent(val) {
            current += val;
        }

        /**
         * Start the auto play.
         * If already playing do nothing.
         */
        function play() {
            if (cycle) {
                return;
            }
            cycle = setInterval(showNext.bind(this), interval);
        }

        return {
            'prev': showPrev,
            'next': showNext,
            'play': play,
            'stop': stop
        };
    }

    (function () {
        var carouselElement = 'carousel';

        if (document.getElementById(carouselElement)) {
            new Carousel({
                elem:     'carousel',    // id of the carousel container
                interval: 8000       // interval between slide changes
            });
        }
    }());
})();
