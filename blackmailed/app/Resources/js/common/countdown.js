(function () {
    var secondsRemaining,
        intervalHandle,
        countdown = document.getElementsByClassName('countdown')[0];

    if (!countdown) {
        return;
    }

    function tick() {
        /**
         * Get the elements.
         */
        var hourDisplay = document.getElementsByClassName('countdown__item--hours')[0],
            minDisplay = document.getElementsByClassName('countdown__item--mins')[0],
            secDisplay = document.getElementsByClassName('countdown__item--secs')[0];

        /**
         * Computing values.
         */
        var hour = Math.floor(secondsRemaining / 3600),
            min = Math.floor(secondsRemaining / 60) - (hour * 60),
            sec = secondsRemaining - (hour * 3600) - (min * 60);

        /**
         * Display values.
         */
        hour = addLeadingZero(hour);
        min = addLeadingZero(min);
        sec = addLeadingZero(sec);


        /**
         * Add a leading zero for correct displaying.
         */
        hourDisplay.innerHTML = hour;
        minDisplay.innerHTML = min;
        secDisplay.innerHTML = sec;

        /**
         * Stop countdown.
         */
        if (secondsRemaining === 0) {
            clearInterval(intervalHandle);
        }

        /**
         * Subtract from seconds remaining.
         */
        secondsRemaining--;

    }

    function startCountdown() {
        /**
         * Get content from countdown's data-attr.
         */
        secondsRemaining = document.getElementsByClassName('countdown')[0].getAttribute('data-countdown');

        /**
         * Every second, call the 'tick' function.
         */
        intervalHandle = setInterval(tick, 1000);

    }

    /**
     * Add a leading zero (as a string value) if hour, min or sec less than 10.
     */
    function addLeadingZero(val) {
        if (val < 10) {
            val = '0' + val;
        }
        return val;
    }

    /**
     * Start countdown.
     */
    startCountdown();


}());
