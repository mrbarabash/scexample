(function () {
    'use strict';

    var closeButton = document.querySelector('.promo-block__link--close');
    if (!closeButton) {
        return;
    }

    closeButton.addEventListener('click', function (e) {
        e.preventDefault();

        createCookie('hide_signup_banner', 1, 1000);

        var promoBlock = document.querySelector('.promo-block');
        promoBlock.parentNode.removeChild(promoBlock);
    });

    function createCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }
}());
