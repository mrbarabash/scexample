(function () {
    'use strict';

    var episodes = document.querySelectorAll('.episodes__item .item__image-wrap');
    if (episodes.length === 0) {
        return;
    }

    Array.prototype.forEach.call(episodes, function (episode) {
        var currentThumbIndex = 0;

        episode.addEventListener('mousemove', function (e) {
            var episodeOffsetX = getElementOffsetX(episode);
            var episodeWidth = episode.offsetWidth;

            var cursorPageOffsetX = e.pageX;
            var cursorPositionWithinEpisodePercentage = (cursorPageOffsetX - episodeOffsetX) / episodeWidth;

            var newThumbIndex = Math.trunc(cursorPositionWithinEpisodePercentage * 5);
            if (newThumbIndex > 4) {
                newThumbIndex = 4;
            }

            if (currentThumbIndex === newThumbIndex) {
                return;
            }

            var newImage = getEpisodeImages(episode)[newThumbIndex];

            if (isImageLoaded(newImage)) {
                updateImagesVisibility(episode, newThumbIndex);
            } else {
                newImage.setAttribute('src', newImage.getAttribute('data-src'));
                newImage.setAttribute('srcset', newImage.getAttribute('data-srcset'));
                newImage.onload = function () {
                    if (newThumbIndex !== currentThumbIndex) {
                        // if cursor already moved out of section which we were initially loading
                        return;
                    }
                    updateImagesVisibility(episode, newThumbIndex);
                };
            }

            currentThumbIndex = newThumbIndex;
        });
    });

    function getEpisodeImages(episode) {
        return episode.querySelectorAll('.item__image');
    }

    function updateImagesVisibility(episode, visibleIndex) {
        Array.prototype.forEach.call(getEpisodeImages(episode), function (episodeImage, index) {
            if (index === visibleIndex) {
                episodeImage.style.display = 'inline';
            } else {
                episodeImage.style.display = 'none';
            }
        });
        episode.querySelector('.item__image-position-marker').style.left = visibleIndex * 20 + '%';
    }

    function isImageLoaded(img) {
        return img.complete && img.naturalHeight !== 0;
    }

    function getElementOffsetX(element) {
        var elementOffsetX = 0;

        if (element.offsetParent) {
            do {
                elementOffsetX += element.offsetLeft;
                element = element.offsetParent;
            } while (element);
        }

        return elementOffsetX;
    }
}());
