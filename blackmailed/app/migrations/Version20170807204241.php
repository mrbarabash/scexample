<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170807204241 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE content_tags (id INT NOT NULL, name VARCHAR(255) NOT NULL, weight DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE eye_colors (id INT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hair_colors (id INT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE performers (id INT NOT NULL, name VARCHAR(255) NOT NULL, bio TEXT DEFAULT NULL, gender VARCHAR(2) DEFAULT NULL, birth_date DATE DEFAULT NULL, nationality VARCHAR(2) DEFAULT NULL, height SMALLINT DEFAULT NULL, chest SMALLINT DEFAULT NULL, waist SMALLINT DEFAULT NULL, hips SMALLINT DEFAULT NULL, bust VARCHAR(10) DEFAULT NULL, weight SMALLINT DEFAULT NULL, rating DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE performer_eye_color (performer_id INT NOT NULL, eye_color_id INT NOT NULL, INDEX IDX_50AE35A46C6B33F3 (performer_id), INDEX IDX_50AE35A4CB96304E (eye_color_id), PRIMARY KEY(performer_id, eye_color_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE performer_hair_color (performer_id INT NOT NULL, hair_color_id INT NOT NULL, INDEX IDX_FF1647EF6C6B33F3 (performer_id), INDEX IDX_FF1647EF8345DCB5 (hair_color_id), PRIMARY KEY(performer_id, hair_color_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE scenes (id INT NOT NULL, original_scene_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, runtime INT DEFAULT NULL, synopsis TEXT DEFAULT NULL, summary_255 VARCHAR(255) DEFAULT NULL, live_date_start DATETIME NOT NULL, live_date_end DATETIME NOT NULL, is_bts TINYINT(1) NOT NULL, pictures_count SMALLINT NOT NULL, quality INT NOT NULL, rating DOUBLE PRECISION DEFAULT NULL, views BIGINT NOT NULL, INDEX IDX_7DD18D2E353FB4A5 (original_scene_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE scene_performer (scene_id INT NOT NULL, performer_id INT NOT NULL, INDEX IDX_7A453761166053B4 (scene_id), INDEX IDX_7A4537616C6B33F3 (performer_id), PRIMARY KEY(scene_id, performer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE scene_content_tag (scene_id INT NOT NULL, content_tag_id INT NOT NULL, INDEX IDX_58618054166053B4 (scene_id), INDEX IDX_58618054D2E413BD (content_tag_id), PRIMARY KEY(scene_id, content_tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE performer_eye_color ADD CONSTRAINT FK_50AE35A46C6B33F3 FOREIGN KEY (performer_id) REFERENCES performers (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE performer_eye_color ADD CONSTRAINT FK_50AE35A4CB96304E FOREIGN KEY (eye_color_id) REFERENCES eye_colors (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE performer_hair_color ADD CONSTRAINT FK_FF1647EF6C6B33F3 FOREIGN KEY (performer_id) REFERENCES performers (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE performer_hair_color ADD CONSTRAINT FK_FF1647EF8345DCB5 FOREIGN KEY (hair_color_id) REFERENCES hair_colors (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE scenes ADD CONSTRAINT FK_7DD18D2E353FB4A5 FOREIGN KEY (original_scene_id) REFERENCES scenes (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE scene_performer ADD CONSTRAINT FK_7A453761166053B4 FOREIGN KEY (scene_id) REFERENCES scenes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE scene_performer ADD CONSTRAINT FK_7A4537616C6B33F3 FOREIGN KEY (performer_id) REFERENCES performers (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE scene_content_tag ADD CONSTRAINT FK_58618054166053B4 FOREIGN KEY (scene_id) REFERENCES scenes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE scene_content_tag ADD CONSTRAINT FK_58618054D2E413BD FOREIGN KEY (content_tag_id) REFERENCES content_tags (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE scene_content_tag DROP FOREIGN KEY FK_58618054D2E413BD');
        $this->addSql('ALTER TABLE performer_eye_color DROP FOREIGN KEY FK_50AE35A4CB96304E');
        $this->addSql('ALTER TABLE performer_hair_color DROP FOREIGN KEY FK_FF1647EF8345DCB5');
        $this->addSql('ALTER TABLE performer_eye_color DROP FOREIGN KEY FK_50AE35A46C6B33F3');
        $this->addSql('ALTER TABLE performer_hair_color DROP FOREIGN KEY FK_FF1647EF6C6B33F3');
        $this->addSql('ALTER TABLE scene_performer DROP FOREIGN KEY FK_7A4537616C6B33F3');
        $this->addSql('ALTER TABLE scenes DROP FOREIGN KEY FK_7DD18D2E353FB4A5');
        $this->addSql('ALTER TABLE scene_performer DROP FOREIGN KEY FK_7A453761166053B4');
        $this->addSql('ALTER TABLE scene_content_tag DROP FOREIGN KEY FK_58618054166053B4');
        $this->addSql('DROP TABLE content_tags');
        $this->addSql('DROP TABLE eye_colors');
        $this->addSql('DROP TABLE hair_colors');
        $this->addSql('DROP TABLE performers');
        $this->addSql('DROP TABLE performer_eye_color');
        $this->addSql('DROP TABLE performer_hair_color');
        $this->addSql('DROP TABLE scenes');
        $this->addSql('DROP TABLE scene_performer');
        $this->addSql('DROP TABLE scene_content_tag');
    }
}
