<?php

namespace Tests\AppBundle;

use AppBundle\Entity\ContentPermission;
use AppBundle\Entity\ContentTag;
use AppBundle\Entity\PasswordResetRequest;
use AppBundle\Entity\Performer;
use AppBundle\Entity\PurchaseToken;
use AppBundle\Entity\Scene;
use AppBundle\Entity\User;
use EaPaysites\Entity\AbstractEntity;
use EaPaysites\Entity\ContentPermission as BaseContentPermission;

class EntityFactory
{
    /**
     * @param array $data
     *
     * @return User
     */
    public function getUser(array $data = []): User
    {
        $data = array_merge(
            [
                'id'               => 1,
                'email'            => 'MaX@EvilAngel.com',
                'emailCanonical'   => 'max@evilangel.com',
                'name'             => 'Max',
                'password'         => '$2y$10$F1GavDg/FYn7VPt/0JQbrOHs06BAJoQQNfiCTXWPelT2Agwv7LIIy',
                // encoded 12345678
                'creationDateTime' => new \DateTime('2017-05-20 14:40:00'),
                'emailConfirmed'   => true,
            ],
            $data
        );

        /** @var User $entity */
        $entity = $this->createEntity(User::class, $data);

        return $entity;
    }

    /**
     * @param array $data
     *
     * @return PasswordResetRequest
     */
    public function getPasswordResetRequest(array $data = []): PasswordResetRequest
    {
        $data = array_merge(
            [
                'id'               => 1,
                'tokenHash'        => '7c222fb2927d828af22f592134e8932480637c0d', // hash of 12345678
                'creationDateTime' => new \DateTime('2017-05-20 14:40:00')
            ],
            $data
        );

        /** @var PasswordResetRequest $entity */
        $entity = $this->createEntity(PasswordResetRequest::class, $data);

        return $entity;
    }

    /**
     * @param array $data
     *
     * @return Scene
     */
    public function getScene(array $data = []): Scene
    {
        $data = array_merge(
            [
                'id'            => 1,
                'title'         => 'Scene Title',
                'slug'          => 'scene-title',
                'liveDateStart' => new \DateTime('today -1 day'),
                'liveDateEnd'   => new \DateTime('today +1 day'),
                'isBts'         => false,
                'picturesCount' => 5,
                'quality'       => Scene::QUALITY_HD
            ],
            $data
        );

        /** @var Scene $entity */
        $entity = $this->createEntity(Scene::class, $data);

        return $entity;
    }

    /**
     * @param array $data
     *
     * @return ContentTag
     */
    public function getContentTag(array $data = []): ContentTag
    {
        $data = array_merge(
            [
                'id'     => 1,
                'name'   => 'Content Tag',
                'slug'   => 'content-tag',
                'weight' => 0.8
            ],
            $data
        );

        /** @var ContentTag $entity */
        $entity = $this->createEntity(ContentTag::class, $data);

        return $entity;
    }

    /**
     * @param array $data
     *
     * @return Performer
     */
    public function getPerformer(array $data = []): Performer
    {
        $data = array_merge(
            [
                'id'     => 1,
                'name'   => 'Performer Name',
                'gender' => 'F',
                'slug'   => 'performer-name',
            ],
            $data
        );

        /** @var Performer $entity */
        $entity = $this->createEntity(Performer::class, $data);

        return $entity;
    }

    /**
     * @param array $data
     *
     * @return PurchaseToken
     */
    public function getPurchaseToken(array $data = []): PurchaseToken
    {
        $data = array_merge(
            [
                'id'        => 1,
                'kind'      => Scene::staticGetOwnClassName(),
                'createdAt' => new \DateTime(),
                'user'      => $this->getUser(),
            ],
            $data
        );

        /** @var PurchaseToken $entity */
        $entity = $this->createEntity(PurchaseToken::class, $data);

        return $entity;
    }

    /**
     * @param array $data
     *
     * @return ContentPermission
     */
    public function getContentPermission(array $data = []): ContentPermission
    {
        $data = array_merge(
            [
                'id'             => 1,
                'user'           => $this->getUser(),
                'contentId'      => 1,
                'contentType'    => Scene::staticGetOwnClassName(),
                'grantedAt'      => new \DateTime(),
                'permission'     => BaseContentPermission::PERMISSION_SCENE_VIEW_FULL,
                'grantedVia'     => 1,
                'grantedViaType' => PurchaseToken::staticGetOwnClassName(),
            ],
            $data
        );

        /** @var ContentPermission $entity */
        $entity = $this->createEntity(ContentPermission::class, $data);

        return $entity;
    }

    /**
     * @param string $class
     * @param array  $data
     *
     * @return AbstractEntity
     */
    protected function createEntity(string $class, array $data): AbstractEntity
    {
        /** @var AbstractEntity $entity */
        $entity = new $class();
        $entity->populate($data);

        if (isset($data['id'])) {
            $ref  = new \ReflectionObject($entity);
            $prop = $ref->getProperty('id');
            $prop->setAccessible(true);
            $prop->setValue($entity, $data['id']);
        }

        return $entity;
    }
}
