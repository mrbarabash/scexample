<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Scene;
use AppBundle\Entity\Scene\CdnTokens;
use AppBundle\Entity\Website;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\AppBundle\ControllerTestCase;

class SceneControllerTest extends ControllerTestCase
{
    public function testSceneViewPageRendersData()
    {
        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);

        $performer = $this->entityFactory->getPerformer();
        $scene->setPerformers(new ArrayCollection([$performer]));
        $this->em->persist($performer);

        $contentTag = $this->entityFactory->getContentTag();
        $scene->setContentTags(new ArrayCollection([$contentTag]));
        $this->em->persist($contentTag);

        $this->em->flush();

        $this->client->request('GET', '/episodes/scene-title');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Scene Title', $this->client->getResponse()->getContent());
        $this->assertContains('Performer Name', $this->client->getResponse()->getContent());
        $this->assertContains('Content Tag', $this->client->getResponse()->getContent());
    }

    public function testSceneThumbContainsCdnToken()
    {
        $token = 'alskgjsalkgj';

        $cdnTokens       = new CdnTokens();
        $cdnTokens->free = $token;

        $scene = $this->entityFactory->getScene(['cdnTokens' => $cdnTokens]);
        $this->em->persist($scene);
        $this->em->flush();

        $this->client->request('GET', '/episodes/newest');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains($token, $this->client->getResponse()->getContent());
    }

    public function testEpisodesPageRendersTitle()
    {
        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);
        $this->em->flush();

        $this->client->request('GET', '/episodes/newest');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('/episodes/scene-title', $this->client->getResponse()->getContent());
    }

    public function testBtsPageRendersTitle()
    {
        $scene = $this->entityFactory->getScene(['isBts' => true]);
        $this->em->persist($scene);
        $this->em->flush();

        $this->client->request('GET', '/behind-the-scenes/newest');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('/episodes/scene-title', $this->client->getResponse()->getContent());
    }

    public function testGuestUserDoesNotSeeProtectedContent()
    {
        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);
        $this->em->flush();

        $crawler  = $this->client->request('GET', '/episodes/scene-title');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertContains($this->getAssetUrl($scene, 'trailer'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'streaming'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-480'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-720'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-1080'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-uhd'), $response->getContent());

        $url = '/signup';
        $this->assertEquals(
            1,
            $crawler->filter('a[href="' . $url . '"] > .downloads__btn--head:contains("Image Gallery")')->count()
        );
    }

    public function testFreeUserDoesNotSeeProtectedContent()
    {
        $user = $this->entityFactory->getUser(['emailConfirmed' => true]);

        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);
        $this->em->flush();

        $this->logIn($user);
        $crawler  = $this->client->request('GET', '/episodes/scene-title');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains($this->getAssetUrl($scene, 'trailer'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'streaming'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-480'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-720'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-1080'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-uhd'), $response->getContent());

        $url = '/upgrade';
        $this->assertEquals(
            1,
            $crawler->filter('a[href="' . $url . '"] > .downloads__btn--head:contains("Image Gallery")')->count()
        );
    }

    public function testFreeUserWithUnlockedSceneSeesSomeProtectedContent()
    {
        $user              = $this->entityFactory->getUser(['emailConfirmed' => true]);
        $scene             = $this->entityFactory->getScene();
        $token             = $this->entityFactory->getPurchaseToken(
            [
                'user'    => $user,
                'usedAt'  => new \DateTime(),
                'usedFor' => $scene->getId(),
            ]
        );
        $contentPermission = $this->entityFactory->getContentPermission(
            [
                'user'       => $user,
                'contentId'  => $scene->getId(),
                'grantedVia' => $token->getId(),
            ]
        );
        $user->addPurchaseToken($token);
        $user->addContentPermission($contentPermission);

        $this->em->persist($scene);
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);

        $crawler  = $this->client->request('GET', '/episodes/scene-title');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains($this->getAssetUrl($scene, 'trailer'), $response->getContent());
        $this->assertContains($this->getAssetUrl($scene, 'streaming'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-480'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-720'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-1080'), $response->getContent());
        $this->assertNotContains($this->getAssetUrl($scene, 'download-uhd'), $response->getContent());

        $url = '/upgrade';
        $this->assertEquals(
            1,
            $crawler->filter('a[href="' . $url . '"] > .downloads__btn--head:contains("Image Gallery")')->count()
        );

    }

    public function testPaidUserSeesProtectedContent()
    {
        $user              = $this->entityFactory->getUser(['emailConfirmed' => true]);
        $scene             = $this->entityFactory->getScene(['quality' => Scene::QUALITY_4K]);
        $contentPermission = $this->entityFactory->getContentPermission(
            [
                'user'        => $user,
                'contentId'   => Website::BLACKMAILED,
                'contentType' => Website::staticGetOwnClassName(),
                'permission'  => 'website_access'
            ]
        );
        $user->addContentPermission($contentPermission);

        $this->em->persist($scene);
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);

        $crawler  = $this->client->request('GET', '/episodes/scene-title');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains($this->getAssetUrl($scene, 'trailer'), $response->getContent());
        $this->assertContains($this->getAssetUrl($scene, 'streaming'), $response->getContent());
        $this->assertContains($this->getAssetUrl($scene, 'download-480'), $response->getContent());
        $this->assertContains($this->getAssetUrl($scene, 'download-720'), $response->getContent());
        $this->assertContains($this->getAssetUrl($scene, 'download-1080'), $response->getContent());
        $this->assertContains($this->getAssetUrl($scene, 'download-uhd'), $response->getContent());

        $this->assertEquals(
            1,
            $crawler->filter(
                sprintf(
                    'a[href="%s"] > .downloads__btn--head:contains("Image Gallery")',
                    $this->getAssetUrl($scene, 'gallery')
                )
            )->count()
        );
    }

    /**
     * @param Scene  $scene
     * @param string $type
     *
     * @return string
     */
    private function getAssetUrl(Scene $scene, string $type): string
    {
        switch ($type) {
            case 'trailer':
                return "https://videocdn.evilangel.com/paysites/scenes/{$scene->getId()}/free/trailer/mp4/{$scene->getId()}-trailer-720.mp4?";
            case 'streaming':
                return "https://videocdn.evilangel.com/paysites/scenes/{$scene->getId()}/streaming/video/mp4/{$scene->getId()}-scene-720.mp4?";
            case 'gallery':
                return "https://imagecdn.evilangel.com/paysites/scenes/{$scene->getId()}/streaming/photo-gallery/{$scene->getId()}-gallery.zip?";
            case 'download-480':
                return "https://videocdn.evilangel.com/paysites/scenes/{$scene->getId()}/download-basic/video/mp4/{$scene->getId()}-scene-480.mp4?";
            case 'download-720':
                return "https://videocdn.evilangel.com/paysites/scenes/{$scene->getId()}/download-basic/video/mp4/{$scene->getId()}-scene-720.mp4?";
            case 'download-1080':
                return "https://videocdn.evilangel.com/paysites/scenes/{$scene->getId()}/download-basic/video/mp4/{$scene->getId()}-scene-1080.mp4?";
            case 'download-uhd':
                return "https://videocdn.evilangel.com/paysites/scenes/{$scene->getId()}/download-uhd/video/mp4/{$scene->getId()}-scene-uhd.mp4?";
        }

        throw new \InvalidArgumentException();
    }
}
