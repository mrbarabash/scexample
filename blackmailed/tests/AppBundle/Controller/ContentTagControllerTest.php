<?php

namespace Tests\AppBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Tests\AppBundle\ControllerTestCase;

class ContentTagControllerTest extends ControllerTestCase
{
    public function testTagsPageContainsNames()
    {
        $tag = $this->entityFactory->getContentTag(
            ['id' => 1, 'name' => 'Content Tag', 'slug' => 'content-tag']
        );
        $this->em->persist($tag);
        $this->em->flush();

        $this->client->request('GET', '/tags/popular');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains('/tags/content-tag/scenes', $this->client->getResponse()->getContent());
    }

    public function testTagScenesPageListsScene()
    {
        $tag = $this->entityFactory->getContentTag(
            ['id' => 1, 'name' => 'Content Tag', 'slug' => 'content-tag']
        );
        $this->em->persist($tag);

        $scene = $this->entityFactory->getScene();
        $scene->setContentTags(new ArrayCollection([$tag]));
        $this->em->persist($scene);

        $this->em->flush();

        $this->client->request('GET', '/tags/content-tag/scenes');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains('Scene Title', $this->client->getResponse()->getContent());
        $this->assertContains('/episodes/scene-title', $this->client->getResponse()->getContent());
    }
}
