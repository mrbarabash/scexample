<?php

namespace Tests\AppBundle\Controller;

use EaPaysites\Service\Email\Email;
use EaPaysites\Sqs\Client\SqsClientInterface;
use Symfony\Component\DomCrawler\Crawler;
use Tests\AppBundle\ControllerTestCase;

class IndexControllerTest extends ControllerTestCase
{
    public function testSupportPageRenders()
    {
        $crawler = $this->client->request('GET', '/support');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(
            1,
            $crawler->selectButton('Submit')
        );
    }

    public function testSupportFormShowsErrorOnWrongEmail()
    {
        $this->client->request(
            'POST',
            '/support',
            [
                'contact_us' => [
                    'email'  => 'not@a.a',
                    '_token' => $this->getCsrfToken('contact_us')
                ]
            ]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('This value is not a valid email address', $this->client->getResponse()->getContent());
        $this->assertContains('This value should not be blank', $this->client->getResponse()->getContent());
    }

    public function testSupportFormCreatesEmail()
    {
        $sqsClientMock = \Mockery::mock(SqsClientInterface::class);
        $sqsClientMock
            ->shouldReceive('sendMessage')
            ->once()
            ->withArgs(
                function (string $queueId, Email $email) {
                    return $queueId == 'email_transactional'
                        && strpos($email->subject, 'New support request') === 0;
                }
            );
        $this->getContainer()->stub(SqsClientInterface::class, $sqsClientMock);

        $this->client->request(
            'POST',
            '/support',
            [
                'contact_us' => [
                    'email'       => 'some@email.com',
                    'subject'     => 'some subj',
                    'description' => 'some descr',
                    '_token'      => $this->getCsrfToken('contact_us')
                ]
            ]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertTrue($response->isRedirect('/'));
    }

    public function testNewSceneIsShownInCarouselAndList()
    {
        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);
        $this->em->flush();

        $crawler = $this->client->request('GET', '/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(
            1,
            $crawler->filter('.slider .slider__item a.item__image-wrap[href="/episodes/scene-title"]')
        );
        $this->assertSceneIdInEpisodesListCount($crawler, 'Newest episodes', $scene->getSlug(), 1);
        $this->assertSceneIdInEpisodesListCount($crawler, 'Top rated episodes', $scene->getSlug(), 0);
    }

    public function testUpcomingSceneRendered()
    {
        $scene = $this->entityFactory->getScene(
            ['liveDateStart' => new \DateTime('tomorrow'), 'liveDateEnd' => new \DateTime('tomorrow')]
        );

        $this->em->persist($scene);
        $this->em->flush();

        $crawler = $this->client->request('GET', '/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(
            0,
            $crawler->filter('.slider .slider__item a.item__image-wrap[href="/episodes/scene-title"]')
        );
        $this->assertCount(
            1,
            $crawler->filter('.upcoming .upcoming__photos > .upcoming__photo--popup[href="/episodes/scene-title"]')
        );
        $this->assertSceneIdInEpisodesListCount($crawler, 'Newest episodes', $scene->getSlug(), 0);
        $this->assertSceneIdInEpisodesListCount($crawler, 'Top rated episodes', $scene->getSlug(), 0);
    }

    /**
     * @param Crawler $crawler
     * @param string  $listName
     * @param string  $sceneSlug
     * @param int     $count
     */
    private function assertSceneIdInEpisodesListCount(Crawler $crawler, string $listName, string $sceneSlug, int $count)
    {
        $this->assertCount(
            $count,
            $crawler->filter('.episodes-section')->reduce(
                function (Crawler $node) use ($listName) {
                    return trim($node->filter('.episodes-section__title')->text()) == $listName;
                }
            )->filter('.episodes__item .item__image-wrap[href="/episodes/' . $sceneSlug . '"]')
        );
    }
}
