<?php

namespace Tests\AppBundle\Controller;

use EaPaysites\Entity\Website;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Request\RpcRequestInterface;
use Tests\AppBundle\ControllerTestCase;

class MembershipControllerTest extends ControllerTestCase
{
    public function testLoggedInUserCanSeeProductList()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);

        $this->mockProductGetListRequest();

        $this->client->request('GET', '/upgrade');

        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $this->assertContains('1 Month', $this->client->getResponse()->getContent());
        $this->assertContains('3 Month', $this->client->getResponse()->getContent());
    }

    public function testDoPurchaseReturn404IfUserIsNotAuthenticated()
    {
        $this->client->request('GET', '/purchase');

        $this->assertTrue(
            $this->client->getResponse()->isNotFound(),
            'User was found'
        );
    }

    public function testBugProductListDescriptionsOnUpgradePage()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);

        $this->mockProductGetListRequest();

        $this->client->request('GET', '/upgrade');
        $content = $this->client->getResponse()->getContent();

        $this->assertContains('Non-recurrent Billing Description', $content);
        $this->assertContains('$10.99', $content);
        $this->assertContains('€10.00/month', $content);
        $this->assertContains('€30.00', $content);
    }

    private function mockProductGetListRequest()
    {
        $response = new RpcTask\Product\GetList\Response();
        $response->products = [
            [
                'id' => 1,
                'title' => '1 Month',
                'description' => '1 Month Purchase',
                'billingDescription' => 'Non-recurrent Billing Description',
                'recurrent' => false,
                'recurrentCount' => null,
                'recurrentFrequency' => null,
                'kind' => Website::staticGetOwnClassName(),
                'price' => 10.99,
                'priceMonthly' => 10.99,
                'currency' => 'USD',
            ],
            [
                'id' => 1,
                'title' => '3 Month',
                'description' => '3 Month Purchase',
                'billingDescription' => 'Recurrent Billing Description',
                'recurrent' => true,
                'recurrentCount' => 1000000,
                'recurrentFrequency' => 3,
                'kind' => Website::staticGetOwnClassName(),
                'price' => 30.00,
                'priceMonthly' => 10.00,
                'currency' => 'EUR',
            ],
        ];

        $callback = function (RpcRequestInterface $rpcRequest) {
            /** @var RpcTask\Product\GetList\Request $request */
            $request = $rpcRequest->getServiceRequest();

            return $rpcRequest->getTaskName() == 'Product\\GetList'
                && $request->website == Website::BLACKMAILED
                && $request->ip == '127.0.0.1';
        };

        $this->mockRpcClient($callback, $response);
    }
}
