<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\PurchaseToken;
use AppBundle\Entity\Scene;
use EaPaysites\Entity\ContentPermission;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequestInterface;
use Mockery\Mock;
use Tests\AppBundle\ControllerTestCase;

class PurchaseTokenControllerTest extends ControllerTestCase
{
    public function testUserCanRedeemSceneToken()
    {
        $user  = $this->entityFactory->getUser(['emailConfirmed' => true]);
        $token = $this->entityFactory->getPurchaseToken(['user' => $user]);
        $scene = $this->entityFactory->getScene();
        $user->addPurchaseToken($token);

        $this->em->persist($scene);
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);

        $response                    = new RpcTask\PurchaseToken\Redeem\Response();
        $response->token             = ['usedFor' => $scene->getId(), 'usedAt' => new \DateTime()];
        $response->contentPermission = [
            'id'             => time(),
            'contentId'      => $scene->getId(),
            'contentType'    => Scene::staticGetOwnClassName(),
            'grantedAt'      => new \DateTime(),
            'expiresAt'      => null,
            'permission'     => ContentPermission::PERMISSION_SCENE_VIEW_FULL,
            'grantedVia'     => $token->getId(),
            'grantedViaType' => PurchaseToken::staticGetOwnClassName(),
        ];

        /** @var Mock $rpcClientMock */
        $rpcClientMock = \Mockery::mock(RpcClientInterface::class);
        $rpcClientMock
            ->shouldReceive('send')
            ->once()
            ->withArgs(
                function (RpcRequestInterface $rpcRequest) use ($token, $scene) {
                    /** @var RpcTask\PurchaseToken\Redeem\Request $request */
                    $request = $rpcRequest->getServiceRequest();

                    return $rpcRequest->getTaskName() == 'PurchaseToken\\Redeem'
                        && $request->id == $token->getId()
                        && $request->usedFor == $scene->getId();
                }
            )
            ->andReturn($response);
        $this->getContainer()->stub(RpcClientInterface::class, $rpcClientMock);

        $this->client->request(
            'GET',
            sprintf('/purchase-token/use/%s/%s', Scene::staticGetOwnClassName(), $scene->getId())
        );

        $this->em->refresh($user);

        $this->assertCount(1, $user->getContentPermissions());
        $token = $user->getPurchaseTokens()->first();
        $this->assertNotNull($token->getUsedAt());
    }
}
