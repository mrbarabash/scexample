<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\User;
use EaPaysites\RpcTask;
use Tests\AppBundle\ControllerTestCase;

class LoginSharingProtectionTest extends ControllerTestCase
{
    public function testGuestUserNotTracked()
    {
        $this->client->request('GET', "/");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(0, count($this->redis->keys('*')));
    }

    public function testLoggedInUserNotTrackedOnSkippedRoutes()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $this->em->flush();

        $this->logIn($user);
        $this->client->request('GET', "/forgot-password");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(0, count($this->redis->keys('*')));
    }

    public function testLoggedInUserTracked()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $this->em->flush();

        $this->logIn($user);
        $this->client->request('GET', "/");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(1, count($this->redis->keys('bm:login_sharing:1')));
    }

    public function testFailedRedisConnectionDoesNotCrashPageRender()
    {
        $this->redis->close();

        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $this->em->flush();

        $this->logIn($user);
        $this->client->request('GET', "/");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLoggedInUserBlockedWhenIpsLimitExceeded()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $this->em->flush();

        $this->redis->zAdd('bm:login_sharing:1', strtotime('now'), '127.0.0.2', strtotime('now'), '127.0.0.3');

        $this->logIn($user);
        $this->client->request('GET', "/");
        $response = $this->client->getResponse();
        $this->em->refresh($user);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertTrue($response->isRedirect('/account-blocked'));
        $this->assertEquals(3, $this->redis->zCount('bm:login_sharing:1', '-inf', '+inf'));
        $this->assertEquals(User::BLOCK_TYPE_LOGIN_SHARING, $user->getBlockType());
        $this->assertNotNull($user->getBlockedUntil());
    }

    public function testLoggedInUserNotBlockedWhenIpsLimitNotExceeded()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $this->em->flush();

        $this->redis->zAdd('bm:login_sharing:1', strtotime('now'), '127.0.0.2');

        $this->logIn($user);
        $this->client->request('GET', "/");
        $response = $this->client->getResponse();
        $this->em->refresh($user);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(2, $this->redis->zCount('bm:login_sharing:1', '-inf', '+inf'));
        $this->assertNull($user->getBlockType());
        $this->assertNull($user->getBlockedUntil());
    }

    public function testOldEntriesRemovedFromZsetAndExpireSet()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $this->em->flush();

        $this->redis->zAdd('bm:login_sharing:1', strtotime('today -1 month'), '127.0.0.2');

        $this->logIn($user);
        $this->client->request('GET', "/");
        $response = $this->client->getResponse();
        $this->em->refresh($user);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(1, $this->redis->zCount('bm:login_sharing:1', '-inf', '+inf'));
        $expireTtl = 6 * 60 * 60 - 5; // 6 hours (max rule -5 seconds for test run safety)
        $this->assertGreaterThan($expireTtl, $this->redis->ttl('bm:login_sharing:1'));
        $this->assertNull($user->getBlockType());
        $this->assertNull($user->getBlockedUntil());
    }

    public function testResetPasswordSubmitRemovesLoginSharingBlock()
    {
        $user = $this->entityFactory->getUser(
            ['blockType' => User::BLOCK_TYPE_LOGIN_SHARING, 'blockedUntil' => new \DateTime('+1 month')]
        );
        $this->em->persist($user);

        $passwordResetRequest = $this->entityFactory->getPasswordResetRequest(['user' => $user]);
        $passwordResetRequest->setUser($user);
        $this->em->persist($passwordResetRequest);

        $this->em->flush();

        $stanResponse               = new RpcTask\Customer\ChangePassword\Response();
        $stanResponse->passwordHash = 'new-password-hash';
        $this->mockRpcClient(
            function () {
                return true;
            },
            $stanResponse
        );

        $this->client->request(
            'POST',
            '/reset-password?t=12345678',
            [
                'reset_password' => [
                    'password' => ['password' => 'new-password', 'confirm' => 'new-password'],
                    '_token'   => $this->getCsrfToken('reset_password')
                ]
            ]
        );

        $this->em->refresh($user);
        $this->assertNull($user->getBlockType());
        $this->assertNull($user->getBlockedUntil());
        $this->assertEquals(0, count($this->redis->keys('*')));
    }
}
