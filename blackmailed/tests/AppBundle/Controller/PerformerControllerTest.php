<?php

namespace Tests\AppBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Tests\AppBundle\ControllerTestCase;

class PerformerControllerTest extends ControllerTestCase
{
    public function testGirlsPageContainsNames()
    {
        $performer1 = $this->entityFactory->getPerformer(
            ['id' => 1, 'name' => 'Performer 1', 'slug' => 'performer-1', 'rating' => 0.9]
        );
        $performer2 = $this->entityFactory->getPerformer(
            ['id' => 2, 'name' => 'Performer 2', 'slug' => 'performer-2', 'rating' => 0.6]
        );
        $this->em->persist($performer1);
        $this->em->persist($performer2);
        $this->em->flush();

        $this->client->request('GET', '/girls/newest');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains('/girls/performer-1', $this->client->getResponse()->getContent());
        $this->assertContains('90%', $this->client->getResponse()->getContent());

        $this->assertContains('/girls/performer-2', $this->client->getResponse()->getContent());
        $this->assertNotContains('60%', $this->client->getResponse()->getContent());
    }

    public function testGirlViewPageContainsData()
    {
        $performer = $this->entityFactory->getPerformer(
            [
                'name' => 'Performer Name',
                'slug' => 'performer-name',
            ]
        );

        $scene = $this->entityFactory->getScene();
        $scene->setPerformers(new ArrayCollection([$performer]));

        $tag = $this->entityFactory->getContentTag();
        $scene->setContentTags(new ArrayCollection([$tag]));

        $this->em->persist($performer);
        $this->em->persist($tag);
        $this->em->persist($scene);

        $this->em->flush();

        $this->client->request('GET', '/girls/performer-name');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Scene Title', $this->client->getResponse()->getContent());
        $this->assertContains('Content Tag', $this->client->getResponse()->getContent());
    }
}
