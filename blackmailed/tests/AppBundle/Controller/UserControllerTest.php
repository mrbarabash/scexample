<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\PasswordResetRequest;
use AppBundle\Entity\Product;
use AppBundle\Entity\Scene;
use AppBundle\Entity\User;
use AppBundle\Service\Product\GetList\ProductGetListRequest;
use AppBundle\Service\Product\GetList\ProductGetListResponse;
use AppBundle\Service\Product\ProductService;
use EaPaysites\Entity\Website;
use EaPaysites\RpcTask;
use EaPaysites\Service\Email\Email;
use EaPaysites\Service\Rpc\Request\RpcRequestInterface;
use EaPaysites\Sqs\Client\SqsClientInterface;
use Tests\AppBundle\ControllerTestCase;

class UserControllerTest extends ControllerTestCase
{
    public function testMyAccountPageClosedForGuestUser()
    {
        $this->client->request('GET', '/my-account');
        $response = $this->client->getResponse();

        $this->assertTrue($response->isRedirect('http://localhost/login'));
    }

    public function testMyAccountPageRendersForUser()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);
        $crawler  = $this->client->request('GET', '/my-account');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(
            1,
            $crawler->selectButton('Change password')
        );
    }

    public function testChangePasswordSubmitChangesPassword()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $stanResponse               = new RpcTask\Customer\ChangePassword\Response();
        $stanResponse->passwordHash = 'new-password-hash';

        $this->mockRpcClient(
            function (RpcRequestInterface $rpcRequest) {
                /** @var RpcTask\Customer\ChangePassword\Request $stanRequest */
                $stanRequest = $rpcRequest->getServiceRequest();

                return $stanRequest->customerId = 1
                    && $stanRequest->password = 'new-password';
            },
            $stanResponse
        );

        $csrfToken = $this->getCsrfToken('change_password');
        $this->logIn($user);
        $this->client->request(
            'POST',
            '/my-account',
            [
                'change_password' => [
                    'currentPassword' => '12345678',
                    'password'        => ['password' => 'new-password', 'confirm' => 'new-password'],
                    '_token'          => $csrfToken
                ]
            ]
        );

        $response = $this->client->getResponse();

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertTrue($response->isRedirect('/'));

        $this->em->refresh($user);
        $this->assertEquals('new-password-hash', $user->getPassword());
    }

    public function testChangePasswordSubmitFailsOnWrongCurrentPassword()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $csrfToken = $this->getCsrfToken('change_password');
        $this->logIn($user);
        $this->client->request(
            'POST',
            '/my-account',
            [
                'change_password' => [
                    'currentPassword' => '1234567',
                    'password'        => ['password' => 'new-password', 'confirm' => 'new-password'],
                    '_token'          => $csrfToken
                ]
            ]
        );

        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('Wrong password', $response->getContent());
    }


    public function testResetPasswordPageFailsForInvalidToken()
    {
        $this->client->request('GET', '/reset-password?t=not-existing-token');

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testResetPasswordPageRendersForValidToken()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $passwordResetRequest = $this->entityFactory->getPasswordResetRequest(['user' => $user]);
        $passwordResetRequest->setUser($user);
        $this->em->persist($passwordResetRequest);

        $this->em->flush();

        $crawler = $this->client->request('GET', '/reset-password?t=12345678');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(
            1,
            $crawler->selectButton('Change password')
        );
    }

    public function testResetPasswordSubmitChangesPassword()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $passwordResetRequest = $this->entityFactory->getPasswordResetRequest(['user' => $user]);
        $passwordResetRequest->setUser($user);
        $this->em->persist($passwordResetRequest);

        $this->em->flush();

        $stanResponse               = new RpcTask\Customer\ChangePassword\Response();
        $stanResponse->passwordHash = 'new-password-hash';

        $this->mockRpcClient(
            function (RpcRequestInterface $rpcRequest) {
                /** @var RpcTask\Customer\ChangePassword\Request $stanRequest */
                $stanRequest = $rpcRequest->getServiceRequest();

                return $stanRequest->customerId = 1
                    && $stanRequest->password = 'new-password';
            },
            $stanResponse
        );

        $this->client->request(
            'POST',
            '/reset-password?t=12345678',
            [
                'reset_password' => [
                    'password' => ['password' => 'new-password', 'confirm' => 'new-password'],
                    '_token'   => $this->getCsrfToken('reset_password')
                ]
            ]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertTrue($response->isRedirect('/'));

        $passwordResetRequest = $this->em->getRepository(PasswordResetRequest::class)->findOneBy([]);
        $this->assertNull($passwordResetRequest);

        $this->em->refresh($user);
        $this->assertEquals('new-password-hash', $user->getPassword());
    }

    public function testForgotPasswordPageRenders()
    {
        $crawler = $this->client->request('GET', '/forgot-password');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(
            1,
            $crawler->selectButton('Send Reset Instructions')
        );
    }

    public function testForgotPasswordFormShowsErrorOnNonExistentEmail()
    {
        $this->client->request(
            'POST',
            '/forgot-password',
            [
                'forgot_password' => [
                    'email'  => 'not@existing.com',
                    '_token' => $this->getCsrfToken('forgot_password')
                ]
            ]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('User not found', $this->client->getResponse()->getContent());
    }

    public function testForgotPasswordFormCreatesEntityAndEmail()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $sqsClientMock = \Mockery::mock(SqsClientInterface::class);
        $sqsClientMock
            ->shouldReceive('sendMessage')
            ->once()
            ->withArgs(
                function (string $queueId, Email $email) {
                    return $queueId == 'email_transactional'
                        && strpos($email->subject, 'Reset your password') === 0;
                }
            );
        $this->getContainer()->stub(SqsClientInterface::class, $sqsClientMock);

        $this->client->request(
            'POST',
            '/forgot-password',
            [
                'forgot_password' => [
                    'email'  => 'max@evilangel.com',
                    '_token' => $this->getCsrfToken('forgot_password')
                ]
            ]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertTrue($response->isRedirect('/'));

        $passwordResetRequest = $this->em->getRepository(PasswordResetRequest::class)->findOneBy(['user' => $user]);
        $this->assertNotNull($passwordResetRequest);
        $this->assertEquals($user, $passwordResetRequest->getUser());
    }

    public function testLoginPageRendersSuccessfully()
    {
        $crawler = $this->client->request('GET', '/login');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(
            1,
            $crawler->selectButton('Login')
        );
    }

    public function testSignupButtonIsShownToGuests()
    {
        $this->client->request('GET', '/');
        $this->assertContains('/signup', $this->client->getResponse()->getContent());
    }

    public function testSignupPageRendersSignupButton()
    {
        $this->mockProductService();

        $crawler = $this->client->request('GET', '/signup');

        $this->assertCount(
            1,
            $crawler->filter('button:contains("Get access")'),
            'There is no link to send sign up form.'
        );
    }

    public function testSignUp()
    {
        $name  = 'User';
        $email = 'user@example.com';

        $response                = new RpcTask\Customer\Signup\Response();
        $response->customer      = [
            'id'               => time(),
            'name'             => $name,
            'email'            => $email,
            'password'         => 'abc',
            'emailCanonical'   => $email,
            'isEmailConfirmed' => false,
        ];
        $response->purchaseToken = [
            'id'        => time(),
            'kind'      => Scene::staticGetOwnClassName(),
            'usedFor'   => null,
            'createdAt' => date('Y-m-d'),
            'usedAt'    => null,
        ];

        $this->mockProductService();

        $this->mockRpcClient(
            function (RpcRequestInterface $rpcRequest) use ($name, $email) {
                /** @var RpcTask\Customer\Signup\Request $request */
                $request = $rpcRequest->getServiceRequest();

                return $rpcRequest->getTaskName() == 'Customer\\Signup'
                    && $request->name == $name
                    && $request->email == $email;
            },
            $response
        );

        $csrfToken = $this->getCsrfToken('user_signup');
        $this->client->request(
            'POST',
            '/signup',
            [
                'user_signup' => [
                    'name'     => $name,
                    'email'    => $email,
                    'password' => [
                        'first'  => 'secure_password',
                        'second' => 'secure_password',
                    ],
                    'birthday' => [
                        'day'   => '01',
                        'month' => '01',
                        'year'  => '1960',
                    ],
                    'product'  => 0,
                    '_token'   => $csrfToken,
                ]
            ]
        );

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        $this->assertNotNull($user);
        $this->assertCount(1, $user->getPurchaseTokens());

        $this->client->followRedirect();
        $this->assertContains(
            'Nice one. Verify your email and you’re all set!',
            $this->client->getResponse()->getContent()
        );
    }

    private function mockProductService()
    {
        $productListResponse = new ProductGetListResponse();
        $product             = new Product();
        $product->setId(0);
        $product->setTitle('Free');
        $product->setBillingDescription('Test');
        $product->setPrice(0.0);
        $productListResponse->products = [$product];

        $productServiceMock = \Mockery::mock(ProductService::class);
        $productServiceMock
            ->shouldReceive('getProductList')
            ->once()
            ->withArgs(
                function (ProductGetListRequest $request) {
                    return $request->website == Website::BLACKMAILED
                        && $request->ip == '127.0.0.1'
                        && $request->withFreeAccessProduct == true;
                }
            )
            ->andReturn($productListResponse);
        $this->getContainer()->stub(ProductService::class, $productServiceMock);
    }
}
