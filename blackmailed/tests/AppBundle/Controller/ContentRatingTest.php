<?php

namespace Tests\AppBundle\Controller;

use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequestInterface;
use Mockery\Mock;
use Tests\AppBundle\ControllerTestCase;

class ContentRatingTest extends ControllerTestCase
{
    public function testGuestUserDoesNotSeeVoteControls()
    {
        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);
        $this->em->flush();

        $this->client->request('GET', "/episodes/scene-title");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotContains(
            "/content-rating/vote?contentType=Scene&contentId={$scene->getId()}&value=1",
            $response->getContent()
        );
    }

    public function testLoggedInUserSeesVoteControlsOnScenePage()
    {
        $scene = $this->entityFactory->getScene();
        $user  = $this->entityFactory->getUser();

        $this->em->persist($scene);
        $this->em->persist($user);

        $this->em->flush();

        $this->logIn($user);
        $this->client->request('GET', "/episodes/scene-title");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains(
            "/content-rating/vote?contentType=Scene&contentId={$scene->getId()}&value=1",
            $response->getContent()
        );
    }

    public function testLoggedInUserSeesVoteControlsOnPerformerPage()
    {
        $performer = $this->entityFactory->getPerformer();
        $user      = $this->entityFactory->getUser();

        $this->em->persist($performer);
        $this->em->persist($user);

        $this->em->flush();

        $this->logIn($user);
        $this->client->request('GET', "/girls/performer-name");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains(
            "/content-rating/vote?contentType=Performer&contentId={$performer->getId()}&value=1",
            $response->getContent()
        );
    }

    public function testGuestCannotExecuteVoteAction()
    {
        $this->client->request('POST', "/content-rating/vote", ['contentType' => 'Scene', 'contentId' => '123']);
        $response = $this->client->getResponse();

        $this->assertEquals(302, $response->getStatusCode());
    }

    public function testVoteActionFailsOnEmptyContentType()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);
        $this->client->request(
            'POST',
            "/content-rating/vote",
            ['contentType' => '', 'contentId' => '111', 'value' => 1]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testVoteActionFailsOnEmptyContentId()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);
        $this->client->request(
            'POST',
            "/content-rating/vote",
            ['contentType' => 'Scene', 'contentId' => '', 'value' => 1]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testVoteActionFailsOnUnknownContentType()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);
        $this->client->request(
            'POST',
            "/content-rating/vote",
            ['contentType' => 'Not-Scene', 'contentId' => '123', 'value' => 1]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testVoteActionFailsOnUnknownVoteValue()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);
        $this->client->request(
            'POST',
            "/content-rating/vote",
            ['contentType' => 'Scene', 'contentId' => '123', 'value' => '2']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testVoteActionFailsOnNonExistingEntity()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);
        $this->em->flush();

        $this->logIn($user);
        $this->client->request(
            'POST',
            "/content-rating/vote",
            ['contentType' => 'Scene', 'contentId' => '111', 'value' => 1]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testVoteActionCallsStan()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);

        $this->em->flush();

        $stanResponse = new RpcTask\ContentRating\RateContent\Response();

        /** @var Mock $rpcClientMock */
        $rpcClientMock = \Mockery::mock(RpcClientInterface::class);
        $rpcClientMock
            ->shouldReceive('send')
            ->once()
            ->withArgs(
                function (RpcRequestInterface $rpcRequest) use ($scene) {
                    /** @var RpcTask\ContentRating\RateContent\Request $stanRequest */
                    $stanRequest = $rpcRequest->getServiceRequest();

                    return $rpcRequest->getTaskName() == 'ContentRating\\RateContent'
                        && $stanRequest->contentId == $scene->getId()
                        && $stanRequest->contentType == 'Scene'
                        && $stanRequest->value == '1';
                }
            )
            ->andReturn($stanResponse);
        $this->getContainer()->stub(RpcClientInterface::class, $rpcClientMock);

        $this->logIn($user);
        $this->client->request(
            'POST',
            "/content-rating/vote",
            ['contentType' => 'Scene', 'contentId' => $scene->getId(), 'value' => '1']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(true, $response->isRedirect("/episodes/scene-title"));
    }

    public function testSceneViewPageRendersBothVoteLinksIfNoPreviousVote()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);

        $this->em->flush();

        $stanResponse        = new RpcTask\ContentRating\GetRatingByUser\Response();
        $stanResponse->value = null;

        /** @var Mock $rpcClientMock */
        $rpcClientMock = \Mockery::mock(RpcClientInterface::class);
        $rpcClientMock
            ->shouldReceive('send')
            ->once()
            ->withArgs(
                function (RpcRequestInterface $rpcRequest) use ($scene, $user) {
                    /** @var RpcTask\ContentRating\GetRatingByUser\Request $stanRequest */
                    $stanRequest = $rpcRequest->getServiceRequest();

                    return $rpcRequest->getTaskName() == 'ContentRating\\GetRatingByUser'
                        && $stanRequest->contentId == $scene->getId()
                        && $stanRequest->contentType == 'Scene'
                        && $stanRequest->userId == $user->getId();
                }
            )
            ->andReturn($stanResponse);
        $this->getContainer()->stub(RpcClientInterface::class, $rpcClientMock);

        $this->logIn($user);
        $this->client->request(
            'GET',
            "/episodes/scene-title"
        );
        $response = $this->client->getResponse();

        $this->assertContains(
            "/content-rating/vote?contentType=Scene&contentId={$scene->getId()}&value=1",
            $response->getContent()
        );
        $this->assertContains(
            "/content-rating/vote?contentType=Scene&contentId={$scene->getId()}&value=-1",
            $response->getContent()
        );
    }

    public function testSceneViewPageRendersJustOneVoteLinkIfAlreadyVoted()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);

        $this->em->flush();

        $stanResponse        = new RpcTask\ContentRating\GetRatingByUser\Response();
        $stanResponse->value = 1;

        /** @var Mock $rpcClientMock */
        $rpcClientMock = \Mockery::mock(RpcClientInterface::class);
        $rpcClientMock
            ->shouldReceive('send')
            ->once()
            ->withArgs(
                function (RpcRequestInterface $rpcRequest) use ($scene, $user) {
                    /** @var RpcTask\ContentRating\GetRatingByUser\Request $stanRequest */
                    $stanRequest = $rpcRequest->getServiceRequest();

                    return $rpcRequest->getTaskName() == 'ContentRating\\GetRatingByUser'
                        && $stanRequest->contentId == $scene->getId()
                        && $stanRequest->contentType == 'Scene'
                        && $stanRequest->userId == $user->getId();
                }
            )
            ->andReturn($stanResponse);
        $this->getContainer()->stub(RpcClientInterface::class, $rpcClientMock);

        $this->logIn($user);
        $this->client->request(
            'GET',
            '/episodes/scene-title'
        );
        $response = $this->client->getResponse();

        $this->assertContains(
            "/content-rating/vote?contentType=Scene&contentId={$scene->getId()}&value=-1",
            $response->getContent()
        );
        $this->assertNotContains(
            "/content-rating/vote?contentType=Scene&contentId={$scene->getId()}&value=1",
            $response->getContent()
        );
    }

    public function testPerformerViewPageRendersJustOneVoteLinkIfAlreadyVoted()
    {
        $user = $this->entityFactory->getUser();
        $this->em->persist($user);

        $performer = $this->entityFactory->getPerformer();
        $this->em->persist($performer);

        $this->em->flush();

        $stanResponse        = new RpcTask\ContentRating\GetRatingByUser\Response();
        $stanResponse->value = 1;

        /** @var Mock $rpcClientMock */
        $rpcClientMock = \Mockery::mock(RpcClientInterface::class);
        $rpcClientMock
            ->shouldReceive('send')
            ->once()
            ->withArgs(
                function (RpcRequestInterface $rpcRequest) use ($performer, $user) {
                    /** @var RpcTask\ContentRating\GetRatingByUser\Request $stanRequest */
                    $stanRequest = $rpcRequest->getServiceRequest();

                    return $rpcRequest->getTaskName() == 'ContentRating\\GetRatingByUser'
                        && $stanRequest->contentId == $performer->getId()
                        && $stanRequest->contentType == 'Performer'
                        && $stanRequest->userId == $user->getId();
                }
            )
            ->andReturn($stanResponse);
        $this->getContainer()->stub(RpcClientInterface::class, $rpcClientMock);

        $this->logIn($user);
        $this->client->request(
            'GET',
            "/girls/performer-name"
        );
        $response = $this->client->getResponse();

        $this->assertContains(
            "/content-rating/vote?contentType=Performer&contentId={$performer->getId()}&value=-1",
            $response->getContent()
        );
        $this->assertNotContains(
            "/content-rating/vote?contentType=Performer&contentId={$performer->getId()}&value=1",
            $response->getContent()
        );
    }

    public function testHighRatingIsVisibleOnScenePage()
    {
        $scene = $this->entityFactory->getScene(['rating' => 0.8]);
        $this->em->persist($scene);
        $this->em->flush();

        $crawler  = $this->client->request('GET', "/episodes/scene-title");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $crawler->filter('.item__info .rating__value:contains("80%")'));
    }

    public function testHighRatingIsVisibleOnPerformerPage()
    {
        $performer = $this->entityFactory->getPerformer(['rating' => 0.8]);
        $this->em->persist($performer);
        $this->em->flush();

        $crawler  = $this->client->request('GET', "/girls/performer-name");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $crawler->filter('.item__info .rating__value:contains("80%")'));
    }

    public function testLowRatingNotVisibleOnScenePage()
    {
        $scene = $this->entityFactory->getScene(['rating' => 0.6]);
        $this->em->persist($scene);
        $this->em->flush();

        $crawler  = $this->client->request('GET', "/episodes/scene-title");
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(0, $crawler->filter('.item__info .rating__value'));
    }
}
