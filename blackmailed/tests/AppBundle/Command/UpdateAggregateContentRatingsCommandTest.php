<?php

namespace Tests\AppBundle\Command;

use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Request\RpcRequestInterface;
use Tests\AppBundle\CommandTestCase;

class UpdateAggregateContentRatingsCommandTest extends CommandTestCase
{
    public function testSceneAggregateRatingUpdated()
    {
        $scene = $this->entityFactory->getScene(['title' => 'Scene Title 1', 'slug' => 'scene-title-1']);
        $this->em->persist($scene);

        $scene2 = $this->entityFactory->getScene(['id' => 2, 'title' => 'Scene Title 2', 'slug' => 'scene-title-2']);
        $this->em->persist($scene2);

        $this->em->flush();

        $stanResponse                      = new RpcTask\ContentRating\GetAggregateRating\Response();
        $stanResponse->contentIdsToRatings = [$scene->getId() => 7.8, $scene2->getId() => 9];

        $this->mockRpcClient(
            function (RpcRequestInterface $rpcRequest) use ($scene, $scene2) {
                /** @var RpcTask\ContentRating\GetAggregateRating\Request $stanRequest */
                $stanRequest = $rpcRequest->getServiceRequest();

                return $rpcRequest->getTaskName() == 'ContentRating\\GetAggregateRating'
                    && $stanRequest->contentIds == [$scene->getId(), $scene2->getId()]
                    && $stanRequest->contentType == 'Scene';
            },
            $stanResponse
        );

        $input = [
            'app:update-aggregate-content-ratings',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('Aggregate Content Rating Updater', $output);
        $this->assertContains('Successfully finished', $output);

        $this->em->refresh($scene);
        $this->em->refresh($scene2);

        $this->assertEquals(7.8, $scene->getRating());
        $this->assertEquals(9, $scene2->getRating());
    }

    public function testPerformerAggregateRatingUpdated()
    {
        $performer = $this->entityFactory->getPerformer();
        $this->em->persist($performer);

        $this->em->flush();

        $stanResponse                      = new RpcTask\ContentRating\GetAggregateRating\Response();
        $stanResponse->contentIdsToRatings = [$performer->getId() => 7.8];

        $this->mockRpcClient(
            function (RpcRequestInterface $rpcRequest) use ($performer) {
                /** @var RpcTask\ContentRating\GetAggregateRating\Request $stanRequest */
                $stanRequest = $rpcRequest->getServiceRequest();

                return $rpcRequest->getTaskName() == 'ContentRating\\GetAggregateRating'
                    && $stanRequest->contentIds == [$performer->getId()]
                    && $stanRequest->contentType == 'Performer';
            },
            $stanResponse
        );

        $input = [
            'app:update-aggregate-content-ratings',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('Aggregate Content Rating Updater', $output);
        $this->assertContains('Successfully finished', $output);

        $this->em->refresh($performer);

        $this->assertEquals(7.8, $performer->getRating());
    }
}
