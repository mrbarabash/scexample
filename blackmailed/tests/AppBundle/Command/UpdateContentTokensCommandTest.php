<?php

namespace Tests\AppBundle\Command;

use Tests\AppBundle\CommandTestCase;

class UpdateContentTokensCommandTest extends CommandTestCase
{
    public function testExecute()
    {
        $scene = $this->entityFactory->getScene();
        $this->em->persist($scene);
        $this->em->flush();

        $input = [
            'app:update-content-tokens',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();
        $this->assertContains('CDN Tokens Updater', $output);
        $this->assertContains('Successfully finished', $output);

        $this->em->refresh($scene);

        $contentCdnEncryptionKey = static::$kernel->getContainer()->getParameter('content_cdn_encryption_key');

        /** @noinspection PhpUndefinedFunctionInspection */
        $params = ectoken_decrypt_token($contentCdnEncryptionKey, $scene->getCdnTokens()->free);

        $this->assertRegExp(
            "/^ec_expire=\d+&ec_url_allow=\/paysites\/scenes\/1\/free$/",
            $params
        );
    }
}
