<?php

namespace Tests\AppBundle\Command;

use AppBundle\Entity\ContentTag;
use AppBundle\Entity\Performer;
use AppBundle\Entity\Scene;
use EaPaysites\RpcTask;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Tests\AppBundle\CommandTestCase;

class SyncShedContentCommandTest extends CommandTestCase
{
    public function testScenePersistedToLocalDb()
    {
        $data = [
            [
                'id'              => 1,
                'title'           => 'Some Scene Title',
                'liveDateStart'   => '2017-08-01T18:00:00+00:00',
                'liveDateEnd'     => '2021-01-01T07:59:00+00:00',
                'isBts'           => false,
                'quality'         => 2,
                'picturesCount'   => 1,
                'performers'      => [],
                'contentTags'     => [],
                'originalSceneId' => null,
            ]
        ];

        $shedResponse = new Response(200, [], \GuzzleHttp\json_encode($data));

        $guzzleMock = \Mockery::mock(ClientInterface::class);
        $guzzleMock
            ->shouldReceive('get')
            ->once()
            ->withArgs(
                function ($url) {
                    return $url == 'paysites/1/scenes';
                }
            )
            ->andReturn($shedResponse);

        $this->getContainer()->stub('guzzle.client.shed', $guzzleMock);

        $input = [
            'app:sync-shed-content',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('SHED Content Importer', $output);
        $this->assertContains('Successfully finished', $output);

        $scene = $this->em->find(Scene::class, 1);

        $this->assertEquals('Some Scene Title', $scene->getTitle());
        $this->assertEquals('some-scene-title', $scene->getSlug());
        $this->assertEquals('2017-08-01T18:00:00+00:00', $scene->getLiveDateStart()->format('c'));
        $this->assertEquals(2, $scene->getQuality());
        $this->assertNotEmpty($scene->getCdnTokens()->free);
    }

    public function testMalePerformerNotSavedInLocalDb()
    {
        $data = [
            [
                'id'              => 1,
                'title'           => 'Some Scene Title',
                'liveDateStart'   => '2017-08-01T18:00:00+00:00',
                'liveDateEnd'     => '2021-01-01T07:59:00+00:00',
                'isBts'           => false,
                'quality'         => 2,
                'picturesCount'   => 1,
                'performers'      => [
                    [
                        'id'         => 1,
                        'name'       => 'Some Performer Name',
                        'gender'     => Performer::GENDER_MALE,
                        'hairColors' => [],
                        'eyeColors'  => [],
                    ]
                ],
                'contentTags'     => [],
                'originalSceneId' => null,
            ]
        ];

        $shedResponse = new Response(200, [], \GuzzleHttp\json_encode($data));

        $guzzleMock = \Mockery::mock(ClientInterface::class);
        $guzzleMock
            ->shouldReceive('get')
            ->once()
            ->withArgs(
                function ($url) {
                    return $url == 'paysites/1/scenes';
                }
            )
            ->andReturn($shedResponse);

        $this->getContainer()->stub('guzzle.client.shed', $guzzleMock);

        $input = [
            'app:sync-shed-content',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('SHED Content Importer', $output);
        $this->assertContains('Successfully finished', $output);

        $performer = $this->em->find(Performer::class, 1);
        $this->assertEquals(null, $performer, 'Male performer must not be there but it is');
    }

    /**
     * @group asd
     */
    public function testNonSexPerformerNotSavedInLocalDb()
    {
        $data = [
            [
                'id'              => 1,
                'title'           => 'Some Scene Title',
                'liveDateStart'   => '2017-08-01T18:00:00+00:00',
                'liveDateEnd'     => '2021-01-01T07:59:00+00:00',
                'isBts'           => false,
                'quality'         => 2,
                'picturesCount'   => 1,
                'performers'      => [
                    [
                        'id'         => 1,
                        'name'       => 'Some Performer Name',
                        'gender'     => Performer::GENDER_FEMALE,
                        'hairColors' => [],
                        'eyeColors'  => [],
                        'nonSex'     => true
                    ]
                ],
                'contentTags'     => [],
                'originalSceneId' => null,
            ]
        ];

        $shedResponse = new Response(200, [], \GuzzleHttp\json_encode($data));

        $guzzleMock = \Mockery::mock(ClientInterface::class);
        $guzzleMock
            ->shouldReceive('get')
            ->once()
            ->withArgs(
                function ($url) {
                    return $url == 'paysites/1/scenes';
                }
            )
            ->andReturn($shedResponse);

        $this->getContainer()->stub('guzzle.client.shed', $guzzleMock);

        $input = [
            'app:sync-shed-content',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('SHED Content Importer', $output);
        $this->assertContains('Successfully finished', $output);

        $performer = $this->em->find(Performer::class, 1);
        $this->assertEquals(null, $performer, 'Non-sex performer must not be there but it is');
    }

    public function testPerformerPersistedToLocalDb()
    {
        $data = [
            [
                'id'              => 1,
                'title'           => 'Some Scene Title',
                'liveDateStart'   => '2017-08-01T18:00:00+00:00',
                'liveDateEnd'     => '2021-01-01T07:59:00+00:00',
                'isBts'           => false,
                'quality'         => 2,
                'picturesCount'   => 1,
                'performers'      => [
                    [
                        'id'         => 1,
                        'name'       => 'Some Performer Name',
                        'gender'     => Performer::GENDER_FEMALE,
                        'hairColors' => [],
                        'eyeColors'  => [],
                    ]
                ],
                'contentTags'     => [],
                'originalSceneId' => null,
            ]
        ];

        $shedResponse = new Response(200, [], \GuzzleHttp\json_encode($data));

        $guzzleMock = \Mockery::mock(ClientInterface::class);
        $guzzleMock
            ->shouldReceive('get')
            ->once()
            ->withArgs(
                function ($url) {
                    return $url == 'paysites/1/scenes';
                }
            )
            ->andReturn($shedResponse);

        $this->getContainer()->stub('guzzle.client.shed', $guzzleMock);

        $input = [
            'app:sync-shed-content',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('SHED Content Importer', $output);
        $this->assertContains('Successfully finished', $output);

        $performer = $this->em->find(Performer::class, 1);

        $this->assertEquals('Some Performer Name', $performer->getName());
        $this->assertEquals('some-performer-name', $performer->getSlug());
    }

    public function testContentTagPersistedToLocalDb()
    {
        $data = [
            [
                'id'              => 1,
                'title'           => 'Some Scene Title',
                'liveDateStart'   => '2017-08-01T18:00:00+00:00',
                'liveDateEnd'     => '2021-01-01T07:59:00+00:00',
                'isBts'           => false,
                'quality'         => 2,
                'picturesCount'   => 1,
                'performers'      => [],
                'contentTags'     => [
                    [
                        'id'     => 1,
                        'name'   => 'Some Content Tag',
                        'weight' => 0.8,
                    ]
                ],
                'originalSceneId' => null,
            ]
        ];

        $shedResponse = new Response(200, [], \GuzzleHttp\json_encode($data));

        $guzzleMock = \Mockery::mock(ClientInterface::class);
        $guzzleMock
            ->shouldReceive('get')
            ->once()
            ->withArgs(
                function ($url) {
                    return $url == 'paysites/1/scenes';
                }
            )
            ->andReturn($shedResponse);

        $this->getContainer()->stub('guzzle.client.shed', $guzzleMock);

        $input = [
            'app:sync-shed-content',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('SHED Content Importer', $output);
        $this->assertContains('Successfully finished', $output);

        $contentTag = $this->em->find(ContentTag::class, 1);

        $this->assertEquals('Some Content Tag', $contentTag->getName());
        $this->assertEquals('some-content-tag', $contentTag->getSlug());
    }

    public function testPerformerDuplicateSlugSuffixed()
    {
        $performer = $this->entityFactory->getPerformer(
            ['id' => 2, 'name' => 'Some Performer Name', 'slug' => 'some-performer-name']
        );
        $this->em->persist($performer);
        $this->em->flush();

        $data = [
            [
                'id'              => 1,
                'title'           => 'Some Scene Title',
                'liveDateStart'   => '2017-08-01T18:00:00+00:00',
                'liveDateEnd'     => '2021-01-01T07:59:00+00:00',
                'isBts'           => false,
                'quality'         => 2,
                'picturesCount'   => 1,
                'performers'      => [
                    [
                        'id'         => 1,
                        'name'       => 'Some Performer Name',
                        'hairColors' => [],
                        'eyeColors'  => [],
                        'gender'     => Performer::GENDER_FEMALE
                    ]
                ],
                'contentTags'     => [],
                'originalSceneId' => null,
            ]
        ];

        $shedResponse = new Response(200, [], \GuzzleHttp\json_encode($data));

        $guzzleMock = \Mockery::mock(ClientInterface::class);
        $guzzleMock
            ->shouldReceive('get')
            ->once()
            ->withArgs(
                function ($url) {
                    return $url == 'paysites/1/scenes';
                }
            )
            ->andReturn($shedResponse);

        $this->getContainer()->stub('guzzle.client.shed', $guzzleMock);

        $input = [
            'app:sync-shed-content',
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('SHED Content Importer', $output);
        $this->assertContains('Successfully finished', $output);

        $performer = $this->em->find(Performer::class, 1);

        $this->assertEquals('Some Performer Name', $performer->getName());
        $this->assertEquals('some-performer-name-1', $performer->getSlug());
    }
}
