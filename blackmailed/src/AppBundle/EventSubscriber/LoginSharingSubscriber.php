<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use EaPaysites\Service\Customer\LoginSharing\LoginSharingProtector;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LoginSharingSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoginSharingProtector
     */
    private $loginSharingProtector;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var array
     */
    private $skipRoutes = [
        'logout',
        'userResetPassword',
        'userForgotPassword',
        'userAccountBlocked',
    ];

    /**
     * LoginSharingSubscriber constructor.
     *
     * @param LoginSharingProtector $loginSharingProtector
     * @param UserRepository        $userRepo
     * @param TokenStorageInterface $tokenStorage
     * @param RouterInterface       $router
     */
    public function __construct(
        LoginSharingProtector $loginSharingProtector,
        UserRepository $userRepo,
        TokenStorageInterface $tokenStorage,
        RouterInterface $router
    ) {
        $this->loginSharingProtector = $loginSharingProtector;
        $this->userRepo              = $userRepo;
        $this->tokenStorage          = $tokenStorage;
        $this->router                = $router;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => [
                ['counter', 0],
                ['redirector', -1]
            ]
        ];
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function counter(FilterControllerEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $user = $this->getUser();
        if (!$user) {
            return;
        }

        $route = $event->getRequest()->attributes->get('_route');
        if (in_array($route, $this->skipRoutes)) {
            return;
        }

        $ip = $event->getRequest()->getClientIp();

        $this->loginSharingProtector->trackVisit($user->getId(), $ip);

        if (!$this->loginSharingProtector->isAllowed($user->getId())) {
            $user->block(User::BLOCK_TYPE_LOGIN_SHARING, new \DateTime('today +1 month'));
            $this->userRepo->store($user, true);
        }
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function redirector(FilterControllerEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $user = $this->getUser();
        if (!$user) {
            return;
        }

        if (in_array($this->getRouteName($event->getRequest()), $this->skipRoutes)) {
            return;
        }

        if ($user->isBlocked()) {
            $event->setController(
                function () {
                    return new RedirectResponse($this->router->generate('userAccountBlocked'));
                }
            );
        }
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    private function getRouteName(Request $request): string
    {
        return $request->attributes->get('_route');
    }

    /**
     * @return User|null
     */
    private function getUser(): ?User
    {
        if (!$this->tokenStorage->getToken()) {
            return null;
        }

        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user instanceof User) {
            return null;
        }

        return $user;
    }
}
