<?php

namespace AppBundle\Twig;

use AppBundle\Entity\User;
use AppBundle\Service\ContentCdn\AssetsUrlBuilder;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class Extension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var AssetsUrlBuilder
     */
    private $assetsUrlBuilder;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * Extension constructor.
     *
     * @param AssetsUrlBuilder      $assetsUrlBuilder
     * @param AuthorizationChecker  $authorizationChecker
     * @param TokenStorageInterface $tokenStorage
     * @param RequestStack          $requestStack
     */
    public function __construct(
        AssetsUrlBuilder $assetsUrlBuilder,
        AuthorizationChecker $authorizationChecker,
        TokenStorageInterface $tokenStorage,
        RequestStack $requestStack
    ) {
        $this->assetsUrlBuilder     = $assetsUrlBuilder;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage         = $tokenStorage;
        $this->requestStack         = $requestStack;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new \Twig_SimpleFilter('contentRating', [$this, 'contentRating']),
        ];
    }

    /**
     * @param float|null $rating
     *
     * @return string
     */
    public function contentRating(float $rating = null, $positive = true): string
    {
        if (!$rating) {
            return '';
        }

        if (!$positive) {
            $rating = 1 - $rating;
        }
        $rating = (int)round($rating * 100);

        return $rating . '%';
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
        ];
    }

    /**
     * @return array
     */
    public function getGlobals(): array
    {
        return [
            'assetsUrlBuilder'     => $this->assetsUrlBuilder,
            'authChecker'          => $this->authorizationChecker,
            'needShowSignupBanner' => $this->needShowSignupBanner()
        ];
    }

    /**
     * @return bool
     */
    private function needShowSignupBanner(): bool
    {
        $isLoggedIn          = $this->tokenStorage->getToken()
            && $this->tokenStorage->getToken()->getUser() instanceof User;
        $hideBannerCookieSet = $this->requestStack->getCurrentRequest()->cookies->get('hide_signup_banner');

        return !$isLoggedIn && !$hideBannerCookieSet;
    }
}
