<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EntityInterface\VotableInterface;
use AppBundle\Entity\Performer;
use AppBundle\Entity\Scene;
use AppBundle\Security\ContentRatingVoter;
use AppBundle\Service\ContentRating;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ContentRatingController extends AppController
{
    /**
     * @Route("/content-rating/vote", name="contentRatingVote")
     * @Method({"GET", "POST"})
     * @param Request                   $request
     * @param ContentRating\VoteService $voteService
     *
     * @return Response
     */
    public function voteAction(Request $request, ContentRating\VoteService $voteService)
    {
        $this->denyAccessUnlessGranted(ContentRatingVoter::ATTRIBUTE_VOTE, ContentRatingVoter::SUBJECT);

        $serviceRequest              = new ContentRating\Vote\Request();
        $serviceRequest->contentType = $request->get('contentType');
        $serviceRequest->contentId   = $request->get('contentId');
        $serviceRequest->value       = $request->get('value');

        $serviceResponse = $voteService->vote($serviceRequest);
        if ($serviceResponse->isError()) {
            throw new BadRequestHttpException($serviceResponse->getError()->getMessage());
        }

        return $this->getRedirectResponse($request);
    }

    /**
     * @param Request $request
     *
     * @return VotableInterface
     */
    private function getVotableEntity(Request $request): VotableInterface
    {
        $entityClass = "AppBundle\\Entity\\" . $request->get('contentType');

        /** @var VotableInterface $entity */
        $entity = $this->getDoctrine()->getRepository($entityClass)->find($request->get('contentId'));

        return $entity;
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    private function getRedirectResponse(Request $request): RedirectResponse
    {
        /** @var Scene|Performer $entity */
        $entity = $this->getVotableEntity($request);

        switch ($request->get('contentType')) {
            case Performer::staticGetOwnClassName():
                return $this->redirectToRoute('performerView', ['slug' => $entity->getSlug()]);
            case Scene::staticGetOwnClassName():
                return $this->redirectToRoute('sceneView', ['slug' => $entity->getSlug()]);
            default:
                throw new \RuntimeException('Unknown contentType');
        }
    }
}
