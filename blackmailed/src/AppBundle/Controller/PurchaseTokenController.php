<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PurchaseToken;
use AppBundle\Entity\Repository\SceneRepository;
use AppBundle\Entity\Scene;
use AppBundle\Security\PurchaseTokenVoter;
use AppBundle\Service\PurchaseToken\RedeemPurchaseTokenRequest;
use AppBundle\Service\PurchaseToken\RedeemService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/purchase-token")
 */
class PurchaseTokenController extends AppController
{
    /**
     * @Route("/use/{kind}/{for}", name="purchaseTokenUse", requirements={"kind": "[a-zA-Z]+", "for": "\d+"})
     * @Method({"GET"})
     * @param RedeemService   $redeemService
     * @param string          $kind
     * @param int             $for
     * @param SceneRepository $sceneRepo
     *
     * @return RedirectResponse
     */
    public function useAction(RedeemService $redeemService, string $kind, int $for, SceneRepository $sceneRepo)
    {
        $this->denyAccessUnlessGranted(PurchaseTokenVoter::USE, PurchaseToken::staticGetOwnClassName());

        $request       = new RedeemPurchaseTokenRequest();
        $request->kind = $kind;
        $request->for  = $for;

        $response = $redeemService->redeem($request);
        if ($response->isError()) {
            die($response->getError()->getMessage());
        }

        /** @var Scene $scene */
        $scene = $sceneRepo->find($for);

        // TODO Universal solution
        return $this->redirectToRoute('sceneView', ['slug' => $scene->getSlug()]);
    }
}
