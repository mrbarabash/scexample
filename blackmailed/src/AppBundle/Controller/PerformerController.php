<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Performer;
use AppBundle\Entity\Repository\ContentTagRepository;
use AppBundle\Entity\Repository\PerformerRepository;
use AppBundle\Entity\Repository\SceneRepository;
use AppBundle\Entity\User;
use AppBundle\Service\ContentRating\VoteService;
use EaPaysites\Service\SortField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PerformerController extends AppController
{
    const SORT_NEWEST = 'newest';

    const SORT_TOP_RATED = 'top-rated';

    const SORT_A_Z = 'a-z';

    const SORT_Z_A = 'z-a';

    /**
     * @Route("/girls/{sort}", name="performerGirls", requirements={"sort": "(newest|top-rated|a-z|z-a)"})
     * @Method({"GET"})
     * @param Request             $request
     * @param PerformerRepository $performerRepo
     *
     * @return Response
     */
    public function girlsAction(Request $request, PerformerRepository $performerRepo)
    {
        $sort        = $request->get('sort', self::SORT_NEWEST);
        $sortField   = $this->getSortFieldForIndex($sort);
        $sortOptions = [
            self::SORT_NEWEST    => 'Newest',
            self::SORT_TOP_RATED => 'Top rated',
            self::SORT_A_Z       => 'A-Z',
            self::SORT_Z_A       => 'Z-A',
        ];

        $performers = $performerRepo->getPerformers($sortField);

        return $this->render(
            'performer/girls.html.twig',
            [
                'performers'  => $performers,
                'sort'        => $sort,
                'sortOptions' => $sortOptions,
                'pageTitle'   => "$sortOptions[$sort] girls"
            ]
        );
    }

    /**
     * @Route("/girls/{slug}", name="performerView")
     * @Method({"GET"})
     * @param Performer            $performer
     * @param VoteService          $voteService
     * @param SceneRepository      $sceneRepo
     * @param ContentTagRepository $contentTagRepo
     *
     * @return Response
     * @internal param Request $request
     */
    public function viewAction(
        Performer $performer,
        VoteService $voteService,
        SceneRepository $sceneRepo,
        ContentTagRepository $contentTagRepo
    ) {
        /** @var User|null $user */
        $user        = $this->getUser();
        $currentVote = $user ? $voteService->getCurrentVote($user, $performer) : null;

        $scenes      = $sceneRepo->getPerformerScenes($performer->getId());
        $contentTags = $contentTagRepo->getPerformerContentTags($performer->getId());

        return $this->render(
            'performer/view.html.twig',
            [
                'performer'   => $performer,
                'currentVote' => $currentVote,
                'scenes'      => $scenes,
                'contentTags' => $contentTags
            ]
        );
    }

    /**
     * @param string $sort
     *
     * @return SortField
     */
    private function getSortFieldForIndex(string $sort): SortField
    {
        switch ($sort) {
            case self::SORT_A_Z:
                return new SortField('name', SortField::SORT_DIRECTION_ASC);
            case self::SORT_Z_A:
                return new SortField('name', SortField::SORT_DIRECTION_DESC);
            case self::SORT_TOP_RATED:
                return new SortField('rating', SortField::SORT_DIRECTION_DESC);
            case self::SORT_NEWEST:
            default:
                return new SortField('id', SortField::SORT_DIRECTION_DESC);
        }
    }
}
