<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use EaPaysites\Service\ServiceResponseError;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class AppController extends Controller
{
    /**
     * @param        $entityName
     * @param        $value
     * @param string $key
     *
     * @return object
     */
    public function findRequiredEntity($entityName, $value, $key = 'id')
    {
        $repo = $this->getDoctrine()->getManager()->getRepository($entityName);

        $entity = $repo->findOneBy([$key => $value]);
        if (!$entity) {
            throw $this->createNotFoundException("$entityName not found by $key");
        }

        return $entity;
    }

    /**
     * @param ServiceResponseError $error
     * @param Form                 $form
     */
    protected function mapServiceResponseErrorToForm(ServiceResponseError $error, Form $form)
    {
        switch ($error->getType()) {
            case ServiceResponseError::TYPE_BAD_REQUEST:
                foreach ($error->getData() as $field => $errors) {
                    foreach ($errors as $errorCode => $errorMessage) {
                        $form->get($field)->addError(new FormError($errorMessage));
                    }
                }
                break;
            case ServiceResponseError::TYPE_SERVER_ERROR:
            default:
                $message = sprintf(
                    'Process failed. Request ID: %s. Please try again in a few minutes.',
                    $error->getRequestId()
                );
                $form->addError(new FormError($message));
        }
    }

    /**
     * @param Request $request
     * @param User    $user
     */
    protected function manuallyAuthenticateUser(Request $request, User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $this->get('event_dispatcher')->dispatch(
            SecurityEvents::INTERACTIVE_LOGIN,
            new InteractiveLoginEvent($request, $token)
        );
    }
}
