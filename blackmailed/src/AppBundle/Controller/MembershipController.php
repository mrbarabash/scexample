<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Repository\SceneRepository;
use AppBundle\Entity\User;
use AppBundle\Service\PaymentGateway\GetPurchaseUrl\GetPurchaseUrlRequest;
use AppBundle\Service\PaymentGateway\PaymentGatewayService;
use AppBundle\Service\Product\GetList\ProductGetListRequest;
use AppBundle\Service\Product\ProductService;
use EaPaysites\Entity\Website;
use EaPaysites\Util\Arrays;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MembershipController extends AppController
{
    /**
     * @Route("/upgrade", name="membershipUpgrade")
     * @Method("GET")
     * @param Request        $request
     * @param ProductService $productService
     *
     * @return Response
     */
    public function upgradeAction(Request $request, ProductService $productService, SceneRepository $sceneRepo)
    {
        /** @var User $user */
        if (!$user = $this->getUser()) {
            throw $this->createAccessDeniedException();
        }

        $serviceRequest          = new ProductGetListRequest();
        $serviceRequest->website = Website::BLACKMAILED;
        $serviceRequest->ip      = $request->getClientIp();

        $serviceResponse = $productService->getProductList($serviceRequest);
        if ($serviceResponse->isError()) {
            die('Cannot receive product list.');
        }

        $scenesForImages = $sceneRepo->getScenesByIds([15369, 15934, 16405, 16406]);
        $scenesForImages = Arrays::getKeysToEntitiesArray($scenesForImages, 'id');

        return $this->render(
            'membership/upgrade.html.twig',
            [
                'products' => $serviceResponse->products,
                'scenesForImages' => $scenesForImages
            ]
        );
    }

    /**
     * @Route("/purchase", name="membershipPurchase")
     * @Method("GET")
     * @param Request               $request
     * @param PaymentGatewayService $paymentGatewayService
     *
     * @return Response
     */
    public function purchaseAction(Request $request, PaymentGatewayService $paymentGatewayService)
    {
        /** @var User $user */
        if (!$user = $this->getUser()) {
            throw $this->createNotFoundException();
        }

        $product = $request->query->get('product');
        if (!$product) {
            throw new BadRequestHttpException();
        }

        $serviceRequest            = new GetPurchaseUrlRequest();
        $serviceRequest->userId    = $user->getId();
        $serviceRequest->productId = $product;
        $serviceRequest->ip        = $request->getClientIp();
        $serviceRequest->locale    = $request->getLocale();

        $serviceResponse = $paymentGatewayService->getPurchaseUrl($serviceRequest);
        if ($serviceResponse->isError()) {
            die('Error during purchase url receiving');
        }

        return $this->render(
            'membership/purchase.html.twig',
            [
                'url' => $serviceResponse->url,
            ]
        );
    }

    /**
     * @Route("/purchase/{gateway}/success", name="membershipPurchaseSuccess")
     * @Method("GET")
     *
     * @return Response
     */
    public function purchaseSuccessAction(string $gateway)
    {
        return $this->render('membership/purchase_success.html.twig');
    }

    /**
     * @Route("/purchase/{gateway}/failure", name="membershipPurchaseFailure")
     * @Method("GET")
     *
     * @return Response
     */
    public function purchaseFailureAction(string $gateway)
    {
        return $this->render('membership/purchase_failure.html.twig');
    }
}
