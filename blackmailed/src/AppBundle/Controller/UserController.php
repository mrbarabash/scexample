<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Entity\Repository\SceneRepository;
use AppBundle\Entity\User;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\ForgotPasswordType;
use AppBundle\Form\ResetPasswordType;
use AppBundle\Form\UserSignupType;
use AppBundle\Service\Product\GetList\ProductGetListRequest;
use AppBundle\Service\Product\ProductService;
use AppBundle\Service\User\EmailConfirmationService;
use AppBundle\Service\User\Password\CreatePasswordResetRequest;
use AppBundle\Service\User\Password\ResetPasswordRequest;
use AppBundle\Service\User\PasswordService;
use AppBundle\Service\User\Signup\ConfirmEmailRequest;
use AppBundle\Service\User\Signup\SignupRequest;
use AppBundle\Service\User\SignupService;
use EaPaysites\Entity\Website;
use EaPaysites\Util\Arrays;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AppController
{
    /**
     * @Route("/signup", name="userSignup")
     * @Method({"GET", "POST"})
     * @param Request        $request
     * @param SignupService  $signupService
     * @param ProductService $productService
     *
     * @return Response
     */
    public function signupAction(
        Request $request,
        SignupService $signupService,
        ProductService $productService,
        SceneRepository $sceneRepo
    ) {
        $productListRequest                        = new ProductGetListRequest();
        $productListRequest->website               = Website::BLACKMAILED;
        $productListRequest->ip                    = $request->getClientIp();
        $productListRequest->withFreeAccessProduct = true;

        $productListResponse = $productService->getProductList($productListRequest);
        if ($productListResponse->isError()) {
            $form = $this->createForm(UserSignupType::class);
            $form->addError(
                new FormError(
                    'There was a system error and signup is not available right now. Please try again in a few minutes.'
                )
            );

            return $this->getSignupRenderResponse($form, $sceneRepo);
        }

        $form = $this->createForm(UserSignupType::class, null, ['products' => $productListResponse->products]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $serviceRequest           = new SignupRequest();
            $serviceRequest->email    = $data['email'];
            $serviceRequest->password = $data['password'];
            $serviceRequest->name     = $data['name'];
            $serviceRequest->website  = Website::BLACKMAILED;

            $serviceResponse = $signupService->signup($serviceRequest);

            if ($serviceResponse->isError()) {
                $this->mapServiceResponseErrorToForm($serviceResponse->getError(), $form);
            } else {
                $redirectUrl = $this->generateUrl('userSignupSuccess');

                /** @var Product $product */
                $product = $data['product'];
                if (!$product->isFree()) {
                    $redirectUrl = $this->generateUrl('membershipPurchase', ['product' => $product->getId()]);
                }
                $this->manuallyAuthenticateUser($request, $serviceResponse->user);

                return $this->redirect($redirectUrl);
            }
        }

        return $this->getSignupRenderResponse($form, $sceneRepo);
    }

    /**
     * @Route("/signup/success", name="userSignupSuccess")
     * @Method("GET")
     * @return Response
     */
    public function signupSuccessAction()
    {
        return $this->render('user/signupSuccess.html.twig');
    }

    /**
     * @Route("/forgot-password", name="userForgotPassword")
     * @Method({"GET", "POST"})
     * @param Request         $request
     * @param PasswordService $passwordService
     *
     * @return Response
     */
    public function forgotPasswordAction(Request $request, PasswordService $passwordService)
    {
        $form = $this->createForm(ForgotPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $serviceRequest        = new CreatePasswordResetRequest();
            $serviceRequest->email = $data['email'];

            $serviceResponse = $passwordService->createPasswordResetRequest($serviceRequest);

            if ($serviceResponse->isError()) {
                $this->mapServiceResponseErrorToForm($serviceResponse->getError(), $form);
            } else {
                return $this->redirectToRoute('indexIndex');
            }
        }

        return $this->render(
            'user/forgotPassword.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/confirm-email", name="userConfirmEmail")
     * @Method({"GET", "POST"})
     * @param Request                  $request
     * @param EmailConfirmationService $emailConfirmationService
     *
     * @return Response
     */
    public function confirmEmailAction(Request $request, EmailConfirmationService $emailConfirmationService)
    {
        $token = $request->get('t');
        if (!$token) {
            throw new BadRequestHttpException('Missing token');
        }

        $emailConfirmation = $emailConfirmationService->getEmailConfirmationByToken($token);
        if (!$emailConfirmation) {
            throw new BadRequestHttpException('Invalid token');
        }

        $serviceRequest                    = new ConfirmEmailRequest();
        $serviceRequest->emailConfirmation = $emailConfirmation;

        $serviceResponse = $emailConfirmationService->confirmEmail($emailConfirmation);

        if ($serviceResponse->isError()) {
            die('error');
        } else {
            die('saved');
        }
    }

    /**
     * @Route("/my-account", name="userMyAccount")
     * @Method({"GET", "POST"})
     * @param Request         $request
     * @param PasswordService $passwordService
     *
     * @return Response|AccessDeniedHttpException
     */
    public function myAccountAction(Request $request, PasswordService $passwordService)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $changePasswordForm = $this->createForm(ChangePasswordType::class);
        $changePasswordForm->handleRequest($request);

        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {
            $data = $changePasswordForm->getData();

            if (!$passwordService->isPasswordValid($this->getUser(), $data['currentPassword'])) {
                $changePasswordForm->get('currentPassword')->addError(new FormError('Wrong password'));
            } else {
                $serviceResponse = $passwordService->changePassword($this->getUser(), $data['password']);

                if ($serviceResponse->isError()) {
                    $this->mapServiceResponseErrorToForm($serviceResponse->getError(), $changePasswordForm);
                } else {
                    return $this->redirectToRoute('indexIndex');
                }
            }
        }

        return $this->render(
            'user/myAccount.html.twig',
            [
                'changePasswordFormSubmitted' => $changePasswordForm->isSubmitted(),
                'changePasswordForm'          => $changePasswordForm->createView()
            ]
        );
    }

    /**
     * @Route("/reset-password", name="userResetPassword")
     * @Method({"GET", "POST"})
     * @param Request         $request
     * @param PasswordService $passwordService
     *
     * @return Response
     */
    public function resetPasswordAction(Request $request, PasswordService $passwordService)
    {
        $token = $request->get('t');
        if (!$token) {
            throw new BadRequestHttpException('Missing token');
        }

        $passwordResetRequest = $passwordService->getPasswordResetRequestByToken($token);
        if (!$passwordResetRequest) {
            throw new BadRequestHttpException('Invalid token');
        }

        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $serviceRequest                       = new ResetPasswordRequest();
            $serviceRequest->passwordResetRequest = $passwordResetRequest;
            $serviceRequest->password             = $data['password'];

            $serviceResponse = $passwordService->resetPassword($serviceRequest);

            if ($serviceResponse->isError()) {
                $this->mapServiceResponseErrorToForm($serviceResponse->getError(), $form);
            } else {
                return $this->redirectToRoute('indexIndex');
            }
        }

        return $this->render(
            'user/resetPassword.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/login", name="userLogin")
     * @Method({"GET", "POST"})
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(AuthenticationUtils $authenticationUtils)
    {
        $error        = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'user/login.html.twig',
            [
                'last_username' => $lastUsername,
                'error'         => $error,
            ]
        );
    }

    /**
     * @Route("/account-blocked", name="userAccountBlocked")
     * @Method({"GET"})
     */
    public function accountBlockedAction()
    {
        $user = $this->getUser();
        if (!$user instanceof User || !$user->isBlocked()) {
            $this->redirectToRoute('indexIndex');
        }

        return $this->render(
            'user/accountBlocked.html.twig',
            [
                'user' => $this->getUser()
            ]
        );
    }

    private function getSignupRenderResponse(Form $form, SceneRepository $sceneRepo)
    {
        $scenesForImages = $sceneRepo->getScenesByIds([15369, 15934, 16369, 16405, 16406]);
        $scenesForImages = Arrays::getKeysToEntitiesArray($scenesForImages, 'id');

        return $this->render(
            'user/signup.html.twig',
            [
                'form'            => $form->createView(),
                'scenesForImages' => $scenesForImages
            ]
        );
    }
}
