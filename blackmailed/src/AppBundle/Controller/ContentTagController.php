<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ContentTag;
use AppBundle\Entity\Repository\ContentTagRepository;
use AppBundle\Entity\Repository\QueryParams\ScenesListQueryParams;
use AppBundle\Entity\Repository\SceneRepository;
use EaPaysites\Service\SortField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContentTagController extends AppController
{
    const SORT_POPULAR = 'popular';

    const SORT_A_Z = 'a-z';

    const SORT_Z_A = 'z-a';

    const TAGS_WITH_PICTURE_LIMIT = 12;

    /**
     * @Route("/tags/{sort}", name="contentTagIndex", requirements={"sort": "(popular|a-z|z-a)"})
     * @Method({"GET"})
     * @param Request              $request
     * @param ContentTagRepository $contentTagRepo
     *
     * @return Response
     */
    public function indexAction(Request $request, ContentTagRepository $contentTagRepo)
    {
        $sort        = $request->get('sort', self::SORT_POPULAR);
        $sortField   = $this->getSortFieldForIndex($sort);
        $sortOptions = [
            self::SORT_POPULAR => 'Popular',
            self::SORT_A_Z     => 'A-Z',
            self::SORT_Z_A     => 'Z-A',
        ];

        $contentTags = $contentTagRepo->getSorted($sortField);

        if ($sort == self::SORT_POPULAR) {
            $imageTags = array_slice($contentTags, 0, self::TAGS_WITH_PICTURE_LIMIT);
            $textTags  = array_slice($contentTags, self::TAGS_WITH_PICTURE_LIMIT);
        } else {
            $imageTags = [];
            $textTags  = $contentTags;
        }

        return $this->render(
            'contentTag/index.html.twig',
            [
                'imageTags'   => $imageTags,
                'textTags'    => $textTags,
                'sort'        => $sort,
                'sortOptions' => $sortOptions,
                'pageTitle'   => "$sortOptions[$sort] tags"
            ]
        );
    }

    /**
     * @Route("/tags/{slug}/scenes", name="contentTagScenes")
     * @Method({"GET"})
     * @param ContentTag      $contentTag
     * @param SceneRepository $sceneRepo
     *
     * @return Response
     */
    public function scenesAction(ContentTag $contentTag, SceneRepository $sceneRepo)
    {
        $scenes = $sceneRepo->getScenesByQueryParams(
            new ScenesListQueryParams(
                ['sort' => 'liveDateStart', 'sortDirection' => 'desc', 'contentTagId' => $contentTag->getId()]
            )
        );

        return $this->render(
            'contentTag/scenes.html.twig',
            [
                'scenes'     => $scenes,
                'contentTag' => $contentTag
            ]
        );
    }

    /**
     * @param string $sort
     *
     * @return SortField
     */
    private function getSortFieldForIndex(string $sort): SortField
    {
        switch ($sort) {
            case self::SORT_A_Z:
                return new SortField('name', SortField::SORT_DIRECTION_ASC);
            case self::SORT_Z_A:
                return new SortField('name', SortField::SORT_DIRECTION_DESC);
            case self::SORT_POPULAR:
            default:
                return new SortField('weight', SortField::SORT_DIRECTION_DESC);
        }
    }
}
