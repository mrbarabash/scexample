<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Repository\QueryParams\ScenesListQueryParams;
use AppBundle\Entity\Repository\SceneRepository;
use AppBundle\Form\ContactUsType;
use AppBundle\Service\Feedback\FeedbackService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AppController
{
    /**
     * @Route("/", name="indexIndex")
     * @Method({"GET"})
     * @param SceneRepository $sceneRepo
     *
     * @return Response
     */
    public function indexAction(SceneRepository $sceneRepo)
    {
        $newestScenes = $sceneRepo->getScenesByQueryParams(
            new ScenesListQueryParams(['sort' => 'liveDateStart', 'sortDirection' => 'desc', 'limit' => 6])
        );

        $topRatedScenes = $sceneRepo->getScenesByQueryParams(
            new ScenesListQueryParams(['sort' => 'rating', 'sortDirection' => 'desc', 'limit' => 6])
        );

        $upcomingScene = $sceneRepo->getClosestUpcomingScene();

        return $this->render(
            'index/index.html.twig',
            [
                'newestScenes'   => $newestScenes,
                'topRatedScenes' => $topRatedScenes,
                'upcomingScene'  => $upcomingScene
            ]
        );
    }

    /**
     * @Route("/terms", name="indexTerms")
     * @Method({"GET"})
     * @return Response
     */
    public function termsAction()
    {
        return $this->render(
            'index/terms.html.twig'
        );
    }

    /**
     * @Route("/privacy", name="indexPrivacy")
     * @Method({"GET"})
     * @return Response
     */
    public function privacyAction()
    {
        return $this->render(
            'index/privacy.html.twig'
        );
    }

    /**
     * @Route("/support", name="indexSupport")
     * @Method({"GET", "POST"})
     * @param Request         $request
     * @param FeedbackService $feedbackService
     *
     * @return Response
     */
    public function supportAction(Request $request, FeedbackService $feedbackService)
    {
        $form = $this->createForm(ContactUsType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $serviceResponse = $feedbackService->sendSupportRequest(
                $data['email'],
                $data['subject'],
                $data['description']
            );

            if ($serviceResponse->isError()) {
                $this->mapServiceResponseErrorToForm($serviceResponse->getError(), $form);
            } else {
                return $this->redirectToRoute('indexIndex');
            }
        }

        return $this->render(
            'index/support.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
