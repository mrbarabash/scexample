<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Repository\QueryParams\ScenesListQueryParams;
use AppBundle\Entity\Repository\SceneRepository;
use AppBundle\Entity\Scene;
use AppBundle\Entity\User;
use AppBundle\Security\SceneVoter;
use AppBundle\Security\WebsiteVoter;
use AppBundle\Service\ContentRating\VoteService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class SceneController extends AppController
{
    const SORT_MOST_VIEWED = 'most-viewed';

    const SORT_TOP_RATED = 'top-rated';

    const SORT_NEWEST = 'newest';

    /**
     * @Route("/episodes/{sort}", name="sceneEpisodes", requirements={"sort": "(newest|top-rated|most-viewed)"})
     * @Method({"GET"})
     * @param Request         $request
     * @param SceneRepository $sceneRepository
     *
     * @return Response
     */
    public function episodesAction(Request $request, SceneRepository $sceneRepository)
    {
        $sort        = $request->get('sort', self::SORT_NEWEST);
        $sortOptions = [
            self::SORT_NEWEST      => 'Newest',
            self::SORT_MOST_VIEWED => 'Most viewed',
            self::SORT_TOP_RATED   => 'Top rated'
        ];

        $queryParams = new ScenesListQueryParams();
        $this->setSortParams($sort, $queryParams);

        $scenes = $sceneRepository->getScenesByQueryParams($queryParams);

        return $this->render(
            'scene/episodes.html.twig',
            [
                'scenes'      => $scenes,
                'sort'        => $sort,
                'sortOptions' => $sortOptions,
                'pageTitle'   => "$sortOptions[$sort] episodes"
            ]
        );
    }

    /**
     * @Route("/behind-the-scenes/{sort}", name="sceneBts", requirements={"sort": "(newest|top-rated|most-viewed)"})
     * @Method({"GET"})
     * @param Request         $request
     * @param SceneRepository $sceneRepository
     *
     * @return Response
     */
    public function btsAction(Request $request, SceneRepository $sceneRepository)
    {
        $sort        = $request->get('sort', self::SORT_NEWEST);
        $sortOptions = [
            self::SORT_NEWEST      => 'Newest',
            self::SORT_MOST_VIEWED => 'Most viewed',
            self::SORT_TOP_RATED   => 'Top rated'
        ];

        $queryParams = new ScenesListQueryParams(['isBts' => true]);
        $this->setSortParams($sort, $queryParams);

        $scenes = $sceneRepository->getScenesByQueryParams($queryParams);

        return $this->render(
            'scene/bts.html.twig',
            [
                'scenes'      => $scenes,
                'sort'        => $sort,
                'sortOptions' => $sortOptions,
                'pageTitle'   => "$sortOptions[$sort] behind-the-scenes episodes"
            ]
        );
    }

    /**
     * @Route("/episodes/{slug}", name="sceneView")
     * @Method({"GET"})
     * @param Scene                $scene
     * @param AuthorizationChecker $authorizationChecker
     * @param VoteService          $voteService
     * @param SceneRepository      $sceneRepo
     *
     * @return Response
     */
    public function viewAction(
        Scene $scene,
        AuthorizationChecker $authorizationChecker,
        VoteService $voteService,
        SceneRepository $sceneRepo
    ) {
        /** @var User|null $user */
        $user = $this->getUser();

        $currentVote = $user ? $voteService->getCurrentVote($user, $scene) : null;

        $newestScenes = $sceneRepo->getScenesByQueryParams(
            new ScenesListQueryParams(['sort' => 'liveDateStart', 'sortDirection' => 'desc', 'limit' => 6])
        );

        $topRatedScenes = $sceneRepo->getScenesByQueryParams(
            new ScenesListQueryParams(['sort' => 'rating', 'sortDirection' => 'desc', 'limit' => 6])
        );

        $upcomingScene = $sceneRepo->getClosestUpcomingScene();

        $fullViewGranted  = $authorizationChecker->isGranted(SceneVoter::PERMISSION_VIEW_FULL, $scene);
        $isGuest          = !$user instanceof User;
        $hasWebsiteAccess = $authorizationChecker->isGranted(WebsiteVoter::ATTRIBUTE_ACCESS, WebsiteVoter::SUBJECT);

        return $this->render(
            'scene/view.html.twig',
            [
                'scene'            => $scene,
                'fullViewGranted'  => $fullViewGranted,
                'isGuest'          => $isGuest,
                'hasWebsiteAccess' => $hasWebsiteAccess,
                'tokenKind'        => Scene::staticGetOwnClassName(),
                'token'            => $user ? $user->getAvailablePurchaseToken(Scene::staticGetOwnClassName()) : null,
                'currentVote'      => $currentVote,
                'newestScenes'     => $newestScenes,
                'topRatedScenes'   => $topRatedScenes,
                'upcomingScene'    => $upcomingScene
            ]
        );
    }

    /**
     * @param string                $sort
     * @param ScenesListQueryParams $queryParams
     */
    private function setSortParams(string $sort, ScenesListQueryParams $queryParams)
    {
        switch ($sort) {
            case self::SORT_MOST_VIEWED:
                $queryParams->sort          = 'views';
                $queryParams->sortDirection = 'desc';
                break;
            case self::SORT_TOP_RATED:
                $queryParams->sort          = 'rating';
                $queryParams->sortDirection = 'desc';
                break;
            case self::SORT_NEWEST:
            default:
                $queryParams->sort          = 'liveDateStart';
                $queryParams->sortDirection = 'desc';
        }
    }
}
