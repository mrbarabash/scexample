<?php

namespace AppBundle\Service\Feedback;

use AppBundle\EmailFactory\ContactUsEmailFactory;
use EaPaysites\Service\EmptyServiceResponse;
use EaPaysites\Service\ServiceResponseError;
use EaPaysites\Sqs\SqsProducer;
use Psr\Log\LoggerInterface;

class FeedbackService
{
    /**
     * @var ContactUsEmailFactory
     */
    private $contactUsEmailFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SqsProducer
     */
    private $sqsProducer;

    /**
     * FeedbackService constructor.
     *
     * @param ContactUsEmailFactory $contactUsEmailFactory
     * @param LoggerInterface       $logger
     * @param SqsProducer           $sqsProducer
     */
    public function __construct(
        ContactUsEmailFactory $contactUsEmailFactory,
        LoggerInterface $logger,
        SqsProducer $sqsProducer
    ) {
        $this->contactUsEmailFactory = $contactUsEmailFactory;
        $this->logger                = $logger;
        $this->sqsProducer           = $sqsProducer;
    }

    /**
     * @param string $fromEmail
     * @param string $subject
     * @param string $description
     *
     * @return EmptyServiceResponse
     */
    public function sendSupportRequest(string $fromEmail, string $subject, string $description): EmptyServiceResponse
    {
        $email = $this->contactUsEmailFactory->factory($fromEmail, $subject, $description);

        $response = new EmptyServiceResponse();

        try {
            $this->sqsProducer->send('email_transactional', $email);
        } catch (\Exception $e) {
            $message = sprintf(
                "%s failed to post email message. Exception: %s. Trace: %s",
                __METHOD__,
                $e,
                $e->getTraceAsString()
            );

            $this->logger->error($message);

            $response->setError(
                new ServiceResponseError(ServiceResponseError::TYPE_SERVER_ERROR, 'Failed to process message')
            );
        }

        return $response;
    }
}
