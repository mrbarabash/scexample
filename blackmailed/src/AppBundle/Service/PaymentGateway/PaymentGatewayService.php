<?php

namespace AppBundle\Service\PaymentGateway;

use AppBundle\Service\PaymentGateway\GetPurchaseUrl\{GetPurchaseUrlRequest, GetPurchaseUrlResponse};
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;

class PaymentGatewayService
{
    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * @param RpcClientInterface $rpcClient
     */
    public function __construct(RpcClientInterface $rpcClient)
    {
        $this->rpcClient = $rpcClient;
    }

    /**
     * @param GetPurchaseUrlRequest $serviceRequest
     *
     * @return GetPurchaseUrlResponse
     */
    public function getPurchaseUrl(GetPurchaseUrlRequest $serviceRequest)
    {
        $rpcRequest = new RpcTask\PaymentGateway\GetPurchaseUrl\Request();
        $rpcRequest->populate($serviceRequest->toArray());

        $rpcRequest = new RpcRequest(
            'PaymentGateway\\GetPurchaseUrl',
            $rpcRequest,
            RpcTask\PaymentGateway\GetPurchaseUrl\Response::class
        );
        $serviceResponse = new GetPurchaseUrlResponse();

        /** @var RpcTask\PaymentGateway\GetPurchaseUrl\Response $rpcResponse */
        $rpcResponse = $this->rpcClient->send($rpcRequest);

        if ($rpcResponse->isError()) {
            $serviceResponse->error = $rpcResponse->error;
        } else {
            $serviceResponse->url = $rpcResponse->url;
        }

        return $serviceResponse;
    }
}
