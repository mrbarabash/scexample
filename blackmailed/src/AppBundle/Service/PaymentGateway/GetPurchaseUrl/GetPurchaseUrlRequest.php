<?php

namespace AppBundle\Service\PaymentGateway\GetPurchaseUrl;

use EaPaysites\Service\AbstractServiceRequest;

class GetPurchaseUrlRequest extends AbstractServiceRequest
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $productId;

    /**
     * @var string
     */
    public $ip;

    /**
     * @var string
     */
    public $locale;
}
