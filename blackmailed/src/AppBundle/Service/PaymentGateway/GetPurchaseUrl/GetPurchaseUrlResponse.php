<?php

namespace AppBundle\Service\PaymentGateway\GetPurchaseUrl;

use EaPaysites\Service\AbstractServiceResponse;

class GetPurchaseUrlResponse extends AbstractServiceResponse
{
    /**
     * @var string
     */
    public $url;
}
