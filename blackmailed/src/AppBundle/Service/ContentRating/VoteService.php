<?php

namespace AppBundle\Service\ContentRating;

use AppBundle\Entity\EntityInterface\VotableInterface;
use AppBundle\Entity\Performer;
use AppBundle\Entity\Scene;
use AppBundle\Entity\User;
use AppBundle\Service\ContentRating;
use Doctrine\ORM\EntityManagerInterface;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;
use EaPaysites\Util\Services;
use EaPaysites\Util\Validators;
use Particle\Validator\ValidationResult;
use Particle\Validator\Validator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class VoteService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var User
     */
    private $currentUser;

    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * VoteService constructor.
     *
     * @param EntityManagerInterface $em
     * @param TokenStorage           $tokenStorage
     * @param RpcClientInterface     $rpcClient
     * @param LoggerInterface        $logger
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorage $tokenStorage,
        RpcClientInterface $rpcClient,
        LoggerInterface $logger
    ) {
        $this->em          = $em;
        $this->currentUser = $tokenStorage->getToken()->getUser();
        $this->rpcClient   = $rpcClient;
        $this->logger      = $logger;
    }

    /**
     * @param Vote\Request $request
     *
     * @return Vote\Response
     */
    public function vote(ContentRating\Vote\Request $request): ContentRating\Vote\Response
    {
        $response = new ContentRating\Vote\Response();

        $validationResult = $this->validate($request);
        if (!$validationResult->isValid()) {
            Services::setValidationError($response, $validationResult);

            return $response;
        }

        $stanRequest              = new RpcTask\ContentRating\RateContent\Request();
        $stanRequest->contentId   = $request->contentId;
        $stanRequest->contentType = $request->contentType;
        $stanRequest->userId      = $this->currentUser->getId();
        $stanRequest->value       = $request->value;

        $rpcRequest = new RpcRequest(
            'ContentRating\\RateContent',
            $stanRequest,
            RpcTask\ContentRating\RateContent\Response::class
        );

        /** @var RpcTask\ContentRating\RateContent\Response $stanResponse */
        $stanResponse = $this->rpcClient->send($rpcRequest);

        if ($stanResponse->isError()) {
            $response->error = $stanResponse->error;

            return $response;
        }

        return $response;
    }

    /**
     * @param User             $user
     * @param VotableInterface $entity
     *
     * @return int|null
     */
    public function getCurrentVote(User $user, VotableInterface $entity)
    {
        $stanRequest              = new RpcTask\ContentRating\GetRatingByUser\Request();
        $stanRequest->contentId   = $entity->getId();
        $stanRequest->contentType = $entity->getOwnClassName();
        $stanRequest->userId      = $user->getId();

        $rpcRequest = new RpcRequest(
            'ContentRating\\GetRatingByUser',
            $stanRequest,
            RpcTask\ContentRating\GetRatingByUser\Response::class
        );

        /** @var RpcTask\ContentRating\GetRatingByUser\Response $stanResponse */
        $stanResponse = $this->rpcClient->send($rpcRequest);

        if (!$stanResponse->isError()) {
            return $stanResponse->value;
        }

        return null;
    }

    /**
     * @param Vote\Request $request
     *
     * @return ValidationResult
     */
    private function validate(ContentRating\Vote\Request $request): ValidationResult
    {
        $v = new Validator();

        $v->required('contentType')
          ->lengthBetween(3, 100)
          ->inArray([Scene::staticGetOwnClassName(), Performer::staticGetOwnClassName()]);

        $v->required('contentId')
          ->greaterThan(0)
          ->digits()
          ->callback(
              Validators::getEntityExistsByDiscriminatorRule($this->em, 'contentType', 'contentId', 'Entity not found')
          );

        $v->required('value')
          ->integer()
          ->inArray([-1, 1], false);

        return $v->validate(get_object_vars($request));
    }
}
