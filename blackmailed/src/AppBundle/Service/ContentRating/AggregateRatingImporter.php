<?php

namespace AppBundle\Service\ContentRating;

use AppBundle\Entity\Performer;
use AppBundle\Entity\Repository\PerformerRepository;
use AppBundle\Entity\Repository\SceneRepository;
use AppBundle\Entity\Scene;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;

class AggregateRatingImporter
{
    /**
     * @var SceneRepository
     */
    private $sceneRepo;

    /**
     * @var PerformerRepository
     */
    private $performerRepo;

    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * AggregateRatingImporter constructor.
     *
     * @param SceneRepository     $sceneRepo
     * @param PerformerRepository $performerRepo
     * @param RpcClientInterface  $rpcClient
     */
    public function __construct(
        SceneRepository $sceneRepo,
        PerformerRepository $performerRepo,
        RpcClientInterface $rpcClient
    ) {
        $this->sceneRepo     = $sceneRepo;
        $this->performerRepo = $performerRepo;
        $this->rpcClient     = $rpcClient;
    }

    public function import()
    {
        $this->importScenesRatings();
        $this->importPerformersRatings();
    }

    private function importScenesRatings()
    {
        $contentIdsToRatings = $this->getContentIdsToRatings(
            Scene::staticGetOwnClassName(),
            $this->sceneRepo->getScenesIds()
        );

        $this->sceneRepo->updateAggregateRating($contentIdsToRatings);
    }

    private function importPerformersRatings()
    {
        $contentIdsToRatings = $this->getContentIdsToRatings(
            Performer::staticGetOwnClassName(),
            $this->performerRepo->getIds()
        );

        $this->performerRepo->updateAggregateRating($contentIdsToRatings);
    }

    /**
     * @param string $contentType
     * @param array  $contentIds
     *
     * @return array
     */
    private function getContentIdsToRatings(string $contentType, array $contentIds): array
    {
        if (empty($contentIds)) {
            return [];
        }

        $stanRequest              = new RpcTask\ContentRating\GetAggregateRating\Request();
        $stanRequest->contentType = $contentType;
        $stanRequest->contentIds  = $contentIds;

        $rpcRequest = new RpcRequest(
            'ContentRating\\GetAggregateRating',
            $stanRequest,
            RpcTask\ContentRating\GetAggregateRating\Response::class
        );

        /** @var RpcTask\ContentRating\GetAggregateRating\Response $stanResponse */
        $stanResponse = $this->rpcClient->send($rpcRequest);

        if ($stanResponse->isError()) {
            throw new \RuntimeException($stanResponse->getError()->getMessage());
        }

        return $stanResponse->contentIdsToRatings;
    }
}
