<?php

namespace AppBundle\Service\ContentRating\Vote;

use EaPaysites\Service\AbstractServiceRequest;

class Request extends AbstractServiceRequest
{
    /**
     * @var string
     */
    public $contentType;

    /**
     * @var int
     */
    public $contentId;

    /**
     * @var int
     */
    public $value;
}
