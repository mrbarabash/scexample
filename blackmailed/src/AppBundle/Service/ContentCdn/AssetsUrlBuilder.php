<?php

namespace AppBundle\Service\ContentCdn;

use AppBundle\Entity\ContentTag;
use AppBundle\Entity\Performer;
use AppBundle\Entity\Scene;

class AssetsUrlBuilder
{
    /**
     * @var string
     */
    private $videoCdnBaseUrl;

    /**
     * @var string
     */
    private $imageCdnBaseUrl;

    /**
     * AssetsUrlBuilder constructor.
     *
     * @param string $videoCdnBaseUrl
     * @param string $imageCdnBaseUrl
     */
    public function __construct(string $videoCdnBaseUrl, string $imageCdnBaseUrl)
    {
        $this->videoCdnBaseUrl = $videoCdnBaseUrl;
        $this->imageCdnBaseUrl = $imageCdnBaseUrl;
    }

    /**
     * @param Scene $scene
     * @param int   $width
     * @param int   $number
     *
     * @return string
     */
    public function sceneThumb(Scene $scene, int $width, int $number): string
    {
        return $this->replaceParamsInPattern(
            '{baseUrl}/scenes/{sceneId}/free/images/{width}/{sceneId}-preview-{width}-{number}.jpg?{cdnToken}',
            [
                'baseUrl'  => $this->imageCdnBaseUrl,
                'width'    => $width,
                'sceneId'  => $scene->getId(),
                'number'   => sprintf('%04d', $number),
                'cdnToken' => $scene->getCdnTokens()->free
            ]
        );
    }

    /**
     * @param Scene $scene
     *
     * @return string
     */
    public function sceneGalleryArchive(Scene $scene): string
    {
        return $this->replaceParamsInPattern(
            '{baseUrl}/scenes/{sceneId}/streaming/photo-gallery/{sceneId}-gallery.zip?{cdnToken}',
            [
                'baseUrl'  => $this->imageCdnBaseUrl,
                'sceneId'  => $scene->getId(),
                'cdnToken' => $scene->getCdnTokens()->free
            ]
        );
    }

    /**
     * @param Scene $scene
     *
     * @return string
     */
    public function sceneDownload(Scene $scene, string $quality): string
    {
        if ($quality == 'uhd') {
            $pattern = '{baseUrl}/scenes/{sceneId}/download-uhd/video/mp4/{sceneId}-scene-uhd.mp4?{uhdCdnToken}';
        } else {
            $pattern = '{baseUrl}/scenes/{sceneId}/download-basic/video/mp4/{sceneId}-scene-{quality}.mp4?{basicCdnToken}';
        }

        return $this->replaceParamsInPattern(
            $pattern,
            [
                'baseUrl'       => $this->videoCdnBaseUrl,
                'sceneId'       => $scene->getId(),
                'uhdCdnToken'   => $scene->getCdnTokens()->downloadUhd,
                'basicCdnToken' => $scene->getCdnTokens()->downloadBasic,
                'quality'       => $quality
            ]
        );
    }

    /**
     * @param Scene  $scene
     * @param string $format
     *
     * @return string
     */
    public function sceneTrailer(Scene $scene, string $format): string
    {
        switch ($format) {
            case 'dash':
                $internalPath = 'adaptive/{sceneId}-trailer-stream.mpd';
                break;
            case 'hls':
                $internalPath = 'adaptive/{sceneId}-trailer-stream.m3u8';
                break;
            case 'mp4':
                $internalPath = 'mp4/{sceneId}-trailer-720.mp4';
                break;
            case 'webm':
                $internalPath = 'webm/{sceneId}-trailer-720.webm';
                break;
            default:
                throw new \InvalidArgumentException("Unknown format: $format");
        }

        return $this->replaceParamsInPattern(
            '{baseUrl}/scenes/{sceneId}/free/trailer/' . $internalPath . '?{cdnToken}',
            [
                'baseUrl'  => $this->videoCdnBaseUrl,
                'sceneId'  => $scene->getId(),
                'cdnToken' => $scene->getCdnTokens()->free
            ]
        );
    }

    /**
     * @param Scene $scene
     *
     * @return string
     */
    public function sceneTrailerScreengrabsPattern(Scene $scene): string
    {
        return $this->replaceParamsInPattern(
            '{baseUrl}/scenes/{sceneId}/free/trailer/screengrabs/{sceneId}-trailer-screengrabs-{time}.jpg?{cdnToken}',
            [
                'baseUrl'  => $this->imageCdnBaseUrl,
                'sceneId'  => $scene->getId(),
                'cdnToken' => $scene->getCdnTokens()->free
            ]
        );
    }

    /**
     * @param Scene  $scene
     * @param string $format
     *
     * @return string
     */
    public function sceneStreaming(Scene $scene, string $format): string
    {
        switch ($format) {
            case 'dash':
                $internalPath = 'adaptive/{sceneId}-scene-stream.mpd';
                break;
            case 'hls':
                $internalPath = 'adaptive/{sceneId}-scene-stream.m3u8';
                break;
            case 'mp4':
                $internalPath = 'mp4/{sceneId}-scene-720.mp4';
                break;
            case 'webm':
                $internalPath = 'webm/{sceneId}-scene-720.webm';
                break;
            default:
                throw new \InvalidArgumentException("Unknown format: $format");
        }

        return $this->replaceParamsInPattern(
            '{baseUrl}/scenes/{sceneId}/streaming/video/' . $internalPath . '?{cdnToken}',
            [
                'baseUrl'  => $this->videoCdnBaseUrl,
                'sceneId'  => $scene->getId(),
                'cdnToken' => $scene->getCdnTokens()->streaming
            ]
        );
    }

    /**
     * @param Scene $scene
     *
     * @return string
     */
    public function sceneScreengrabsPattern(Scene $scene): string
    {
        return $this->replaceParamsInPattern(
            '{baseUrl}/scenes/{sceneId}/streaming/video/screengrabs/{sceneId}-scene-screengrabs-{time}.jpg?{cdnToken}',
            [
                'baseUrl'  => $this->imageCdnBaseUrl,
                'sceneId'  => $scene->getId(),
                'cdnToken' => $scene->getCdnTokens()->streaming
            ]
        );
    }

    /**
     * @param ContentTag $contentTag
     * @param int        $width
     *
     * @return string
     */
    public function contentTagThumb(ContentTag $contentTag, int $width): string
    {
        return $this->replaceParamsInPattern(
            '{baseUrl}/tags/{width}/{tagId}-tag-{width}.jpg',
            [
                'baseUrl' => $this->imageCdnBaseUrl,
                'width'   => $width,
                'tagId'   => $contentTag->getId(),
            ]
        );
    }

    /**
     * @param Performer $performer
     * @param int       $width
     * @param int       $number
     *
     * @return string
     */
    public function performerThumb(Performer $performer, int $width, int $number): string
    {
        return $this->replaceParamsInPattern(
            '{baseUrl}/performers/blackmailed.com/{width}/{performerId}-performer-{width}-{number}.jpg',
            [
                'baseUrl'     => $this->imageCdnBaseUrl,
                'width'       => $width,
                'number'      => sprintf('%04d', $number),
                'performerId' => $performer->getId()
            ]
        );
    }

    /**
     * @param string $pattern
     * @param array  $params
     *
     * @return string
     */
    private function replaceParamsInPattern(string $pattern, array $params): string
    {
        foreach ($params as $key => $value) {
            $pattern = str_replace('{' . $key . '}', $value, $pattern);
        }

        return $pattern;
    }
}
