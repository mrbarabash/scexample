<?php

namespace AppBundle\Service\ContentCdn\ProtectedFolder;

use AppBundle\Entity\Scene;

abstract class AbstractSceneProtectedFolder
{
    const TOKEN_MAX_LENGTH = 512;

    /**
     * @var ProtectedFolderParameters
     */
    private $parameters;

    /**
     * AbstractSceneProtectedFolder constructor.
     *
     * @param ProtectedFolderParameters $parameters
     */
    public function __construct(ProtectedFolderParameters $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param Scene $scene
     *
     * @return string
     */
    public function generateToken(Scene $scene): string
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        $token = ectoken_encrypt_token(
            $this->parameters->getContentCdnEncryptionKey(),
            $this->getEncryptionParams($scene)
        );

        if (strlen($token) > self::TOKEN_MAX_LENGTH) {
            throw new \RuntimeException("Token max length exceeded");
        }

        return $token;
    }

    /**
     * @return int
     */
    abstract protected function getTokenLifetime(): int;

    /**
     * @return string
     */
    abstract protected function getCdnPathPattern(): string;

    /**
     * @param Scene $scene
     *
     * @return string
     */
    private function getEncryptionParams(Scene $scene): string
    {
        $params = [
            'ec_expire=' . $this->getExpirationTimestamp(),
            'ec_url_allow=' . str_replace('{sceneId}', $scene->getId(), $this->getCdnPathPattern())
        ];

        return join('&', $params);
    }

    /**
     * @return int
     */
    private function getExpirationTimestamp(): int
    {
        return time() + $this->getTokenLifetime();
    }
}
