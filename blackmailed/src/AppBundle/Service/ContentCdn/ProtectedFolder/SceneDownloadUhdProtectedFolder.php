<?php

namespace AppBundle\Service\ContentCdn\ProtectedFolder;

class SceneDownloadUhdProtectedFolder extends AbstractSceneProtectedFolder
{
    const TOKEN_LIFETIME_SECONDS = 3600;

    const CDN_PATH = '/paysites/scenes/{sceneId}/download-uhd';

    /**
     * @return int
     */
    protected function getTokenLifetime(): int
    {
        return self::TOKEN_LIFETIME_SECONDS;
    }

    /**
     * @return string
     */
    protected function getCdnPathPattern(): string
    {
        return self::CDN_PATH;
    }
}
