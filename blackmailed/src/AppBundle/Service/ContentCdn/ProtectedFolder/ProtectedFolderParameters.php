<?php

namespace AppBundle\Service\ContentCdn\ProtectedFolder;

class ProtectedFolderParameters
{
    /**
     * @var string
     */
    private $contentCdnEncryptionKey;

    /**
     * @var string
     */
    private $domain;

    /**
     * ContentTokenService constructor.
     *
     * @param string $contentCdnEncryptionKey
     * @param string $domain
     */
    public function __construct(string $contentCdnEncryptionKey, string $domain)
    {
        $this->contentCdnEncryptionKey = $contentCdnEncryptionKey;
        $this->domain                  = $domain;
    }

    /**
     * @return string
     */
    public function getContentCdnEncryptionKey(): string
    {
        return $this->contentCdnEncryptionKey;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }
}
