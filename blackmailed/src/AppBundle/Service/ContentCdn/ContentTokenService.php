<?php

namespace AppBundle\Service\ContentCdn;

use AppBundle\Entity\Scene;
use AppBundle\Entity\Scene\CdnTokens;
use AppBundle\Service\ContentCdn\ProtectedFolder\SceneDownloadBasicProtectedFolder;
use AppBundle\Service\ContentCdn\ProtectedFolder\SceneDownloadUhdProtectedFolder;
use AppBundle\Service\ContentCdn\ProtectedFolder\SceneFreeProtectedFolder;
use AppBundle\Service\ContentCdn\ProtectedFolder\SceneStreamingProtectedFolder;
use Doctrine\ORM\EntityManagerInterface;

class ContentTokenService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SceneFreeProtectedFolder
     */
    private $sceneFreeProtectedFolder;

    /**
     * @var SceneStreamingProtectedFolder
     */
    private $sceneStreamingProtectedFolder;

    /**
     * @var SceneDownloadBasicProtectedFolder
     */
    private $sceneDownloadBasicProtectedFolder;

    /**
     * @var SceneDownloadUhdProtectedFolder
     */
    private $sceneDownloadUhdProtectedFolder;

    /**
     * ContentTokenService constructor.
     *
     * @param EntityManagerInterface            $em
     * @param SceneFreeProtectedFolder          $sceneFreeProtectedFolder
     * @param SceneStreamingProtectedFolder     $sceneStreamingProtectedFolder
     * @param SceneDownloadBasicProtectedFolder $sceneDownloadBasicProtectedFolder
     * @param SceneDownloadUhdProtectedFolder   $sceneDownloadUhdProtectedFolder
     */
    public function __construct(
        EntityManagerInterface $em,
        SceneFreeProtectedFolder $sceneFreeProtectedFolder,
        SceneStreamingProtectedFolder $sceneStreamingProtectedFolder,
        SceneDownloadBasicProtectedFolder $sceneDownloadBasicProtectedFolder,
        SceneDownloadUhdProtectedFolder $sceneDownloadUhdProtectedFolder
    ) {
        $this->em                                = $em;
        $this->sceneFreeProtectedFolder          = $sceneFreeProtectedFolder;
        $this->sceneStreamingProtectedFolder     = $sceneStreamingProtectedFolder;
        $this->sceneDownloadBasicProtectedFolder = $sceneDownloadBasicProtectedFolder;
        $this->sceneDownloadUhdProtectedFolder   = $sceneDownloadUhdProtectedFolder;
    }

    /**
     * @param Scene $scene
     *
     * @return CdnTokens
     */
    public function generateCdnTokensForScene(Scene $scene): CdnTokens
    {
        $cdnTokens                = new CdnTokens();
        $cdnTokens->free          = $this->sceneFreeProtectedFolder->generateToken($scene);
        $cdnTokens->streaming     = $this->sceneStreamingProtectedFolder->generateToken($scene);
        $cdnTokens->downloadBasic = $this->sceneDownloadBasicProtectedFolder->generateToken($scene);
        $cdnTokens->downloadUhd   = $this->sceneDownloadUhdProtectedFolder->generateToken($scene);

        return $cdnTokens;
    }

    public function updateContentTokens()
    {
        $scenes = $this->em->getRepository(Scene::class)->findAll();

        foreach ($scenes as $scene) {
            $scene->setCdnTokens($this->generateCdnTokensForScene($scene));
        }

        $this->em->flush();
    }
}
