<?php

namespace AppBundle\Service\PurchaseToken;

use EaPaysites\Service\AbstractServiceRequest;

class RedeemPurchaseTokenRequest extends AbstractServiceRequest
{
    /**
     * @var string
     */
    public $kind;

    /**
     * @var int
     */
    public $for;
}
