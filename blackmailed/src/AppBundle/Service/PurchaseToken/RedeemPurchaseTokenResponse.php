<?php

namespace AppBundle\Service\PurchaseToken;

use AppBundle\Entity\{ContentPermission, PurchaseToken};
use EaPaysites\Service\AbstractServiceResponse;

class RedeemPurchaseTokenResponse extends AbstractServiceResponse
{
    /**
     * @var PurchaseToken
     */
    public $token;

    /**
     * @var ContentPermission
     */
    public $contentPermission;
}
