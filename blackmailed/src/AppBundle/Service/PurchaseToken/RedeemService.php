<?php

namespace AppBundle\Service\PurchaseToken;

use AppBundle\Entity\PurchaseToken;
use AppBundle\Entity\Repository\PurchaseTokenRepository;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Service\Content\ContentPermissionService;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;
use EaPaysites\Service\ServiceResponseError;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class RedeemService
{
    /**
     * @var PurchaseTokenRepository
     */
    private $purchaseTokenRepo;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var ContentPermissionService
     */
    private $permissionService;

    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var ContentPermissionService
     */
    private $contentPermissionService;

    /**
     * @param PurchaseTokenRepository  $tokenRepository
     * @param UserRepository           $userRepository
     * @param ContentPermissionService $permissionService
     * @param RpcClientInterface       $rpcClient
     * @param LoggerInterface          $logger
     * @param TokenStorage             $storage
     */
    public function __construct(
        PurchaseTokenRepository $tokenRepository,
        UserRepository $userRepository,
        ContentPermissionService $permissionService,
        RpcClientInterface $rpcClient,
        LoggerInterface $logger,
        TokenStorage $storage,
        ContentPermissionService $contentPermissionService
    ) {
        $this->purchaseTokenRepo        = $tokenRepository;
        $this->userRepo                 = $userRepository;
        $this->permissionService        = $permissionService;
        $this->rpcClient                = $rpcClient;
        $this->logger                   = $logger;
        $this->tokenStorage             = $storage;
        $this->contentPermissionService = $contentPermissionService;
    }

    /**
     * @param RedeemPurchaseTokenRequest $request
     *
     * @return RedeemPurchaseTokenResponse
     */
    public function redeem(RedeemPurchaseTokenRequest $request): RedeemPurchaseTokenResponse
    {
        $serviceResponse = new RedeemPurchaseTokenResponse();

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        if (!($token = $user->getAvailablePurchaseToken($request->kind))) {
            $serviceResponse->error = new ServiceResponseError(
                ServiceResponseError::TYPE_BAD_REQUEST,
                sprintf('There is no available PurchaseToken for the current user with kind "%s"', $request->kind)
            );

            return $serviceResponse;
        }

        $rpcServiceRequest          = new RpcTask\PurchaseToken\Redeem\Request();
        $rpcServiceRequest->id      = $token->getId();
        $rpcServiceRequest->usedFor = $request->for;

        $rpcRequest = new RpcRequest(
            'PurchaseToken\\Redeem',
            $rpcServiceRequest,
            RpcTask\PurchaseToken\Redeem\Response::class
        );

        /** @var RpcTask\PurchaseToken\Redeem\Response $rpcServiceResponse */
        $rpcServiceResponse = $this->rpcClient->send($rpcRequest);

        if ($rpcServiceResponse->isError()) {
            $serviceResponse->error = $rpcServiceResponse->error;
        } else {
            $this->updateToken($rpcServiceResponse->token, $token);
            $permission = $this->contentPermissionService
                ->createContentPermission($rpcServiceResponse->contentPermission);

            $permission->setUser($user);
            $user->addContentPermission($permission);

            $this->purchaseTokenRepo->store($token);
            $this->userRepo->store($user, true);

            $serviceResponse->token             = $token;
            $serviceResponse->contentPermission = $permission;
        }

        return $serviceResponse;
    }

    /**
     * @param array         $tokenData
     * @param PurchaseToken $token
     */
    private function updateToken(array $tokenData, PurchaseToken $token)
    {
        $token->setUsedFor($tokenData['usedFor']);
        $token->setUsedAt($tokenData['usedAt']);
    }
}
