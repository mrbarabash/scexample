<?php

namespace AppBundle\Service\User;

use AppBundle\EmailFactory\SignupEmailFactory;
use AppBundle\Entity\EmailConfirmation;
use AppBundle\Entity\Repository\EmailConfirmationRepository;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Service\User\Signup\ConfirmEmailResponse;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;
use EaPaysites\Sqs\SqsProducer;
use EaPaysites\Util\Passwords;
use Psr\Log\LoggerInterface;

class EmailConfirmationService
{
    /**
     * @var EmailConfirmationRepository
     */
    private $emailConfirmationRepo;

    /**
     * @var SignupEmailFactory
     */
    private $signupEmailFactory;

    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SqsProducer
     */
    private $sqsProducer;

    /**
     * SignupService constructor.
     *
     * @param EmailConfirmationRepository $emailConfirmationRepo
     * @param SignupEmailFactory          $signupEmailFactory
     * @param RpcClientInterface          $rpcClient
     * @param UserRepository              $userRepo
     * @param LoggerInterface             $logger
     * @param SqsProducer                 $sqsProducer
     */
    public function __construct(
        EmailConfirmationRepository $emailConfirmationRepo,
        SignupEmailFactory $signupEmailFactory,
        RpcClientInterface $rpcClient,
        UserRepository $userRepo,
        LoggerInterface $logger,
        SqsProducer $sqsProducer
    ) {
        $this->emailConfirmationRepo = $emailConfirmationRepo;
        $this->signupEmailFactory    = $signupEmailFactory;
        $this->rpcClient             = $rpcClient;
        $this->userRepo              = $userRepo;
        $this->logger                = $logger;
        $this->sqsProducer           = $sqsProducer;
    }

    /**
     * @param User $user
     */
    public function createEmailConfirmation(User $user)
    {
        $token = $this->createEmailConfirmationEntity($user);
        $this->sendEmailConfirmation($user, $token);
    }

    /**
     * @param string $token
     *
     * @return EmailConfirmation|null
     */
    public function getEmailConfirmationByToken(string $token): ?EmailConfirmation
    {
        $tokenHash = sha1($token);

        /** @var EmailConfirmation $emailConfirmation */
        $emailConfirmation = $this->emailConfirmationRepo->findOneBy(['tokenHash' => $tokenHash]);

        return $emailConfirmation;
    }

    /**
     * @param EmailConfirmation $emailConfirmation
     *
     * @return ConfirmEmailResponse
     */
    public function confirmEmail(EmailConfirmation $emailConfirmation): ConfirmEmailResponse
    {
        $stanRequest             = new RpcTask\Customer\ConfirmEmail\Request();
        $stanRequest->customerId = $emailConfirmation->getUser()->getId();

        $rpcRequest      = new RpcRequest(
            'Customer\\ConfirmEmail',
            $stanRequest,
            RpcTask\Customer\ConfirmEmail\Response::class
        );
        $serviceResponse = new ConfirmEmailResponse();

        /** @var RpcTask\Customer\ConfirmEmail\Response $stanResponse */
        $stanResponse = $this->rpcClient->send($rpcRequest);

        if ($stanResponse->isError()) {
            $serviceResponse->error = $stanResponse->error;
        } else {
            $user = $emailConfirmation->getUser();

            $user->setEmailConfirmed(true);
            $this->userRepo->store($user, true);

            $this->emailConfirmationRepo->remove($emailConfirmation, true);
        }

        return $serviceResponse;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    private function createEmailConfirmationEntity(User $user): string
    {
        $emailConfirmation = new EmailConfirmation();
        $emailConfirmation->setUser($user);

        $token = Passwords::generateToken();
        $emailConfirmation->setTokenHash(sha1($token));

        $emailConfirmation->setCreationDateTime(new \DateTime());

        $this->emailConfirmationRepo->store($emailConfirmation, true);

        return $token;
    }

    /**
     * @param User   $user
     * @param string $token
     */
    private function sendEmailConfirmation(User $user, string $token)
    {
        $email = $this->signupEmailFactory->factory($user, $token);
        $this->sqsProducer->send('email_transactional', $email);
    }
}
