<?php

namespace AppBundle\Service\User;

use AppBundle\EmailFactory\PasswordResetEmailFactory;
use AppBundle\Entity\PasswordResetRequest;
use AppBundle\Entity\Repository\PasswordResetRequestRepository;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Service\User\Password\CreatePasswordResetRequest;
use AppBundle\Service\User\Password\CreatePasswordResetResponse;
use AppBundle\Service\User\Password\ResetPasswordRequest;
use AppBundle\Service\User\Password\ResetPasswordResponse;
use EaPaysites\RpcTask;
use EaPaysites\Service\Customer\LoginSharing\LoginSharingProtector;
use EaPaysites\Service\EmptyServiceResponse;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;
use EaPaysites\Sqs\SqsProducer;
use EaPaysites\Util\Filters;
use EaPaysites\Util\Passwords;
use EaPaysites\Util\Services;
use EaPaysites\Util\Validators;
use Particle\Validator\ValidationResult;
use Particle\Validator\Validator;
use Psr\Log\LoggerInterface;

class PasswordService
{
    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var PasswordResetRequestRepository
     */
    private $passwordResetRequestRepo;

    /**
     * @var PasswordResetEmailFactory
     */
    private $passwordResetEmailFactory;

    /**
     * @var SqsProducer
     */
    private $sqsProducer;

    /**
     * @var LoginSharingProtector
     */
    private $loginSharingProtector;

    /**
     * PasswordService constructor.
     *
     * @param RpcClientInterface             $rpcClient
     * @param LoggerInterface                $logger
     * @param UserRepository                 $userRepo
     * @param PasswordResetRequestRepository $passwordResetRequestRepo
     * @param PasswordResetEmailFactory      $passwordResetEmailFactory
     * @param SqsProducer                    $sqsProducer
     * @param LoginSharingProtector          $loginSharingProtector
     */
    public function __construct(
        RpcClientInterface $rpcClient,
        LoggerInterface $logger,
        UserRepository $userRepo,
        PasswordResetRequestRepository $passwordResetRequestRepo,
        PasswordResetEmailFactory $passwordResetEmailFactory,
        SqsProducer $sqsProducer,
        LoginSharingProtector $loginSharingProtector
    ) {
        $this->rpcClient                 = $rpcClient;
        $this->logger                    = $logger;
        $this->userRepo                  = $userRepo;
        $this->passwordResetRequestRepo  = $passwordResetRequestRepo;
        $this->passwordResetEmailFactory = $passwordResetEmailFactory;
        $this->sqsProducer               = $sqsProducer;
        $this->loginSharingProtector     = $loginSharingProtector;
    }

    /**
     * @param CreatePasswordResetRequest $request
     *
     * @return CreatePasswordResetResponse
     */
    public function createPasswordResetRequest(CreatePasswordResetRequest $request): CreatePasswordResetResponse
    {
        $request->email = Filters::getCanonicalEmail($request->email);
        $response       = new CreatePasswordResetResponse();

        $validationResult = $this->validatePasswordResetRequest($request);
        if (!$validationResult->isValid()) {
            Services::setValidationError($response, $validationResult);

            return $response;
        }

        /** @var User $user */
        $user  = $this->userRepo->findOneBy(['emailCanonical' => $request->email]);
        $token = $this->createResetPasswordEntity($user);

        $email = $this->passwordResetEmailFactory->factory($user, $token);
        $this->sqsProducer->send('email_transactional', $email);

        return $response;
    }

    /**
     * @param User   $user
     * @param string $password
     *
     * @return bool
     */
    public function isPasswordValid(User $user, string $password): bool
    {
        return Passwords::isPasswordValid($user->getPassword(), $password);
    }

    /**
     * @param ResetPasswordRequest $serviceRequest
     *
     * @return ResetPasswordResponse
     */
    public function resetPassword(ResetPasswordRequest $serviceRequest): ResetPasswordResponse
    {
        $user = $serviceRequest->passwordResetRequest->getUser();

        $stanRequest             = new RpcTask\Customer\ChangePassword\Request();
        $stanRequest->customerId = $user->getId();
        $stanRequest->password   = $serviceRequest->password;

        $rpcRequest      = new RpcRequest(
            'Customer\\ChangePassword', $stanRequest, RpcTask\Customer\ChangePassword\Response::class
        );
        $serviceResponse = new ResetPasswordResponse();

        /** @var RpcTask\Customer\ChangePassword\Response $stanResponse */
        $stanResponse = $this->rpcClient->send($rpcRequest);

        if ($stanResponse->isError()) {
            $serviceResponse->error = $stanResponse->error;
        } else {
            $this->updatePassword(
                $serviceRequest->passwordResetRequest,
                $stanResponse->passwordHash
            );
            $this->passwordResetRequestRepo->remove($serviceRequest->passwordResetRequest, true);

            if ($user->getBlockType() == User::BLOCK_TYPE_LOGIN_SHARING) {
                $user->unblock();
                $this->userRepo->store($user, true);

                $this->loginSharingProtector->resetTracking($user->getId());
            }
        }

        return $serviceResponse;
    }

    /**
     * @param User   $user
     * @param string $password
     *
     * @return EmptyServiceResponse
     */
    public function changePassword(User $user, string $password): EmptyServiceResponse
    {
        $stanRequest             = new RpcTask\Customer\ChangePassword\Request();
        $stanRequest->customerId = $user->getId();
        $stanRequest->password   = $password;

        $rpcRequest      = new RpcRequest(
            'Customer\\ChangePassword', $stanRequest, RpcTask\Customer\ChangePassword\Response::class
        );
        $serviceResponse = new EmptyServiceResponse();

        /** @var RpcTask\Customer\ChangePassword\Response $stanResponse */
        $stanResponse = $this->rpcClient->send($rpcRequest);

        if ($stanResponse->isError()) {
            $serviceResponse->error = $stanResponse->error;
        } else {
            $this->userRepo->updatePassword($user->getId(), $stanResponse->passwordHash);
        }

        return $serviceResponse;
    }

    /**
     * @param string $token
     *
     * @return PasswordResetRequest|null
     */
    public function getPasswordResetRequestByToken(string $token): ?PasswordResetRequest
    {
        $tokenHash = sha1($token);

        /** @var PasswordResetRequest $passwordResetRequest */
        $passwordResetRequest = $this->passwordResetRequestRepo->findOneBy(['tokenHash' => $tokenHash]);

        return $passwordResetRequest;
    }

    /**
     * @param PasswordResetRequest $passwordResetRequest
     * @param string               $passwordHash
     */
    private function updatePassword(PasswordResetRequest $passwordResetRequest, string $passwordHash)
    {
        $this->userRepo->updatePassword($passwordResetRequest->getUser()->getId(), $passwordHash);
    }

    /**
     * @param User $user
     *
     * @return string
     */
    private function createResetPasswordEntity(User $user): string
    {
        $passwordResetRequest = new PasswordResetRequest();
        $passwordResetRequest->setUser($user);

        $token = Passwords::generateToken();
        $passwordResetRequest->setTokenHash(sha1($token));

        $passwordResetRequest->setCreationDateTime(new \DateTime());

        $this->passwordResetRequestRepo->store($passwordResetRequest, true);

        return $token;
    }

    /**
     * @param CreatePasswordResetRequest $serviceRequest
     *
     * @return ValidationResult
     */
    private function validatePasswordResetRequest(CreatePasswordResetRequest $serviceRequest): ValidationResult
    {
        $v = new Validator();

        $v->required('email')
          ->email()
          ->callback(Validators::getEntityExistsRule($this->userRepo, 'emailCanonical', 'User not found'));

        return $v->validate($serviceRequest->toArray());
    }
}
