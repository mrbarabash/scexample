<?php

namespace AppBundle\Service\User\Password;

use AppBundle\Entity\PasswordResetRequest;
use EaPaysites\Service\AbstractServiceRequest;

class ResetPasswordRequest extends AbstractServiceRequest
{
    /**
     * @var PasswordResetRequest
     */
    public $passwordResetRequest;

    /**
     * @var string
     */
    public $password;
}
