<?php

namespace AppBundle\Service\User\Password;

use EaPaysites\Service\AbstractServiceResponse;

class ResetPasswordResponse extends AbstractServiceResponse
{
}
