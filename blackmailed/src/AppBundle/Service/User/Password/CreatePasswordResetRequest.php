<?php

namespace AppBundle\Service\User\Password;

use EaPaysites\Service\AbstractServiceRequest;

class CreatePasswordResetRequest extends AbstractServiceRequest
{
    /**
     * @var string
     */
    public $email;
}
