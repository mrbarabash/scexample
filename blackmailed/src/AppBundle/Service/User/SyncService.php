<?php

namespace AppBundle\Service\User;

use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Service\User\Sync\SyncUserRequest;
use AppBundle\Service\User\Sync\SyncUserResponse;

class SyncService
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * SignupService constructor.
     *
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function sync(SyncUserRequest $serviceRequest): SyncUserResponse
    {
        /** @var User $user */
        $user = $this->userRepo->find($serviceRequest->user['id']);

        if ($user) {
            $user->populate($serviceRequest->user);
            $this->userRepo->store($user, true);
        }

        $serviceResponse       = new SyncUserResponse();
        $serviceResponse->user = $user;

        return $serviceResponse;
    }
}
