<?php

namespace AppBundle\Service\User;

use AppBundle\Entity\PurchaseToken;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Service\User\Signup\SignupRequest;
use AppBundle\Service\User\Signup\SignupResponse;
use EaPaysites\RpcTask;
use EaPaysites\Service\Customer\SignupServiceInterface;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;
use Psr\Log\LoggerInterface;

/**
 * Class SignupService
 * @package AppBundle\Service\User
 */
class SignupService implements SignupServiceInterface
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EmailConfirmationService
     */
    private $emailConfirmationService;

    /**
     * SignupService constructor.
     *
     * @param UserRepository           $userRepo
     * @param RpcClientInterface       $rpcClient
     * @param LoggerInterface          $logger
     * @param EmailConfirmationService $emailConfirmationService
     */
    public function __construct(
        UserRepository $userRepo,
        RpcClientInterface $rpcClient,
        LoggerInterface $logger,
        EmailConfirmationService $emailConfirmationService
    ) {
        $this->userRepo                 = $userRepo;
        $this->rpcClient                = $rpcClient;
        $this->logger                   = $logger;
        $this->emailConfirmationService = $emailConfirmationService;
    }

    /**
     * @param SignupRequest $serviceRequest
     *
     * @return SignupResponse
     */
    public function signup(SignupRequest $serviceRequest): SignupResponse
    {
        $stanRequest = new RpcTask\Customer\Signup\Request();
        $stanRequest->populate($serviceRequest->toArray());

        $rpcRequest      = new RpcRequest('Customer\\Signup', $stanRequest, RpcTask\Customer\Signup\Response::class);
        $serviceResponse = new SignupResponse();

        /** @var RpcTask\Customer\Signup\Response $stanResponse */
        $stanResponse = $this->rpcClient->send($rpcRequest);

        if ($stanResponse->isError()) {
            $serviceResponse->error = $stanResponse->error;
        } else {
            $user          = $this->createUser($stanResponse->customer);
            $purchaseToken = $this->createPurchaseToken($stanResponse->purchaseToken);

            $purchaseToken->setUser($user);
            $user->addPurchaseToken($purchaseToken);

            $this->userRepo->store($user, true);

            $this->emailConfirmationService->createEmailConfirmation($user);

            $serviceResponse->user = $user;
        }

        return $serviceResponse;
    }

    /**
     * @param array $customer
     *
     * @return User
     */
    public function createFromStanCustomer(array $customer): User
    {
        $user = $this->createUser($customer);

        return $user;
    }

    /**
     * @param array $customer
     *
     * @return User
     */
    private function createUser(array $customer): User
    {
        $user = new User();

        $fields = [
            'id',
            'password',
            'name',
            'email',
            'emailCanonical',
            'isEmailConfirmed',
        ];

        $user->populate(array_intersect_key($customer, array_flip($fields)));

        return $user;
    }

    /**
     * @param array $tokenData
     *
     * @return PurchaseToken
     */
    private function createPurchaseToken(array $tokenData): PurchaseToken
    {
        $purchaseToken = new PurchaseToken();

        $fields = [
            'id',
            'kind',
            'usedFor',
            'createdAt',
            'usedAt',
        ];

        $purchaseToken->populate(array_intersect_key($tokenData, array_flip($fields)));

        return $purchaseToken;
    }
}
