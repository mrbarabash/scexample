<?php

namespace AppBundle\Service\User\Signup;

use AppBundle\Entity\EmailConfirmation;
use AppBundle\Entity\User;
use EaPaysites\Service\AbstractServiceResponse;

class ConfirmEmailResponse extends AbstractServiceResponse
{
}
