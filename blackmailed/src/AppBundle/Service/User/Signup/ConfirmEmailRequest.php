<?php

namespace AppBundle\Service\User\Signup;

use AppBundle\Entity\EmailConfirmation;
use EaPaysites\Service\AbstractServiceRequest;

class ConfirmEmailRequest extends AbstractServiceRequest
{
    /**
     * @var EmailConfirmation
     */
    public $emailConfirmation;
}
