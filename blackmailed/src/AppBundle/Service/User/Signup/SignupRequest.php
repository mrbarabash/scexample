<?php

namespace AppBundle\Service\User\Signup;

use EaPaysites\Service\AbstractServiceRequest;

class SignupRequest extends AbstractServiceRequest
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $website;
}
