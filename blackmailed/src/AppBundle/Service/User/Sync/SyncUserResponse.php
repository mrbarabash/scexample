<?php

namespace AppBundle\Service\User\Sync;

use AppBundle\Entity\User;
use EaPaysites\Service\AbstractServiceResponse;

class SyncUserResponse extends AbstractServiceResponse
{
    /**
     * @var User
     */
    public $user;
}
