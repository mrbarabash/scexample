<?php

namespace AppBundle\Service\User\Sync;

use EaPaysites\Service\AbstractServiceRequest;

class SyncUserRequest extends AbstractServiceRequest
{
    /**
     * @var array
     */
    public $user;
}
