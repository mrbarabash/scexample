<?php

namespace AppBundle\Service\Content;

use EaPaysites\Entity\ContentPermissionableInterface;
use AppBundle\Entity\Repository\ContentPermissionRepository;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\ContentPermission;
use Doctrine\Common\Collections\ArrayCollection;

class ContentPermissionService
{
    /**
     * @var ContentPermissionRepository
     */
    private $permissionRepo;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * Field for caching user permissions by id.
     *
     * @var array
     */
    private $cachedPermissions = [];

    /**
     * @param ContentPermissionRepository $permissionRepo
     * @param UserRepository              $userRepo
     */
    public function __construct(ContentPermissionRepository $permissionRepo, UserRepository $userRepo)
    {
        $this->permissionRepo = $permissionRepo;
        $this->userRepo = $userRepo;
    }

    /**
     * @param User                           $user
     * @param ContentPermissionableInterface $content
     * @param string                         $permission
     *
     * @return bool
     */
    public function isAllowed(User $user, ContentPermissionableInterface $content, string $permission): bool
    {
        return !$this->getUserContentPermissions($user)
            ->filter(function (ContentPermission $contentPermission) use ($content) {
                return $contentPermission->getContentId() == $content->getId()
                    && $contentPermission->getContentType() == $content->getOwnClassName();
            })->filter(function (ContentPermission $contentPermission) use ($permission) {
                return !$contentPermission->isExpired()
                    && $contentPermission->isAllowed($permission);
            })->isEmpty();
    }

    /**
     * @param array $data
     * @param bool  $flush
     *
     * @return ContentPermission
     */
    public function createContentPermission(array $data, $flush = false): ContentPermission
    {
        $permission = new ContentPermission();

        $fields = [
            'id',
            'contentId',
            'contentType',
            'grantedAt',
            'expiresAt',
            'permission',
            'grantedVia',
            'grantedViaType',
        ];
        $permission->populate(array_intersect_key($data, array_flip($fields)));

        if (array_key_exists('user', $data) && $user = $data['user']) {
            $permission->setUser($this->userRepo->find($user));
        }

        $this->permissionRepo->store($permission, $flush);

        return $permission;
    }

    /**
     * @param User $user
     *
     * @return ArrayCollection
     */
    private function getUserContentPermissions(User $user): ArrayCollection
    {
        // TODO Add ability to invalidate cache.
        if (!in_array($user->getId(), array_keys($this->cachedPermissions))) {
            $this->cachedPermissions[$user->getId()] = new ArrayCollection($this->permissionRepo->findByUser($user));
        }

        return $this->cachedPermissions[$user->getId()];
    }
}
