<?php

namespace AppBundle\Service\ContentImporter;

use AppBundle\Entity\ContentTag;
use AppBundle\Entity\EntityInterface\SluggableInterface;
use AppBundle\Entity\EyeColor;
use AppBundle\Entity\HairColor;
use AppBundle\Entity\Performer;
use AppBundle\Entity\Scene;
use AppBundle\Service\ContentCdn\ContentTokenService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use EaPaysites\Service\ServiceResponseError;
use EaPaysites\Util\Arrays;
use EaPaysites\Util\Filters;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

class ShedImporter
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Client
     */
    private $guzzle;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ContentTokenService
     */
    private $contentTokenService;

    /**
     * ShedImporter constructor.
     *
     * @param EntityManagerInterface $em
     * @param ClientInterface        $guzzle
     * @param LoggerInterface        $logger
     * @param ContentTokenService    $contentTokenService
     */
    public function __construct(
        EntityManagerInterface $em,
        ClientInterface $guzzle,
        LoggerInterface $logger,
        ContentTokenService $contentTokenService
    ) {
        $this->em                  = $em;
        $this->guzzle              = $guzzle;
        $this->logger              = $logger;
        $this->contentTokenService = $contentTokenService;
    }

    /**
     * @return ImportContentResponse
     */
    public function import()
    {
        $data = $this->downloadData();
        if ($data === false) {
            $serviceResponse        = new ImportContentResponse();
            $serviceResponse->error = new ServiceResponseError(
                ServiceResponseError::TYPE_SERVER_ERROR,
                'Failed to download data from API'
            );

            return $serviceResponse;
        }

        $existingEntities = $this->fetchEntities();
        $newEntitiesData  = $this->extractEntitiesData($data);

        $this->updateEyeColors($existingEntities[EyeColor::class], $newEntitiesData[EyeColor::class]);
        $this->updateHairColors($existingEntities[HairColor::class], $newEntitiesData[HairColor::class]);
        $this->updateContentTags($existingEntities[ContentTag::class], $newEntitiesData[ContentTag::class]);
        $this->updatePerformers($existingEntities[Performer::class], $newEntitiesData[Performer::class]);
        $this->updateScenes($existingEntities[Scene::class], $newEntitiesData[Scene::class]);

        $this->em->flush();

        return new ImportContentResponse();
    }

    /**
     * @param array $existingEntities
     * @param array $newEntitiesData
     */
    private function updateScenes(array $existingEntities, array $newEntitiesData)
    {
        $this->removeDisappearedEntities($existingEntities, $newEntitiesData);
        $slugCheckEntities = $existingEntities;

        foreach ($newEntitiesData as $newEntityData) {
            if (isset($existingEntities[$newEntityData['id']])) {
                $entity = $existingEntities[$newEntityData['id']];
            } else {
                $entity              = new Scene();
                $slugCheckEntities[] = $entity;
            }

            $scalarFields = [
                'id',
                'title',
                'synopsis',
                'runtime',
                'summary255',
                'isBts',
                'quality',
                'picturesCount'
            ];
            $entity->populate(array_intersect_key($newEntityData, array_flip($scalarFields)));

            $entity->setSummary255(strip_tags($entity->getSummary255()));
            $entity->setSynopsis(strip_tags($entity->getSynopsis()));
            $entity->setLiveDateStart(new \DateTime($newEntityData['liveDateStart']));
            $entity->setLiveDateEnd(new \DateTime($newEntityData['liveDateEnd']));
            $entity->setCdnTokens($this->contentTokenService->generateCdnTokensForScene($entity));

            if ($newEntityData['originalSceneId']) {
                $entity->setOriginalScene(
                    $this->em->getReference('AppBundle:Scene', $newEntityData['originalSceneId'])
                );
            }

            $collection = new ArrayCollection();
            foreach ($newEntityData['performers'] as $performer) {
                if ($performer['gender'] != Performer::GENDER_FEMALE) {
                    continue;
                }

                if (!empty($performer['nonSex'])) {
                    continue;
                }

                $collection->add($this->em->getReference('AppBundle:Performer', $performer['id']));
            }
            $entity->setPerformers($collection);

            $collection = new ArrayCollection();
            foreach ($newEntityData['contentTags'] as $contentTag) {
                $collection->add($this->em->getReference('AppBundle:ContentTag', $contentTag['id']));
            }
            $entity->setContentTags($collection);

            $this->pickSlug($entity, $slugCheckEntities);

            $this->em->persist($entity);
        }
    }

    /**
     * @param array $existingEntities
     * @param array $newEntitiesData
     */
    private function updatePerformers(array $existingEntities, array $newEntitiesData)
    {
        $this->removeDisappearedEntities($existingEntities, $newEntitiesData);
        $slugCheckEntities = $existingEntities;

        foreach ($newEntitiesData as $newEntityData) {
            if ($newEntityData['gender'] != Performer::GENDER_FEMALE) {
                continue;
            }

            if (isset($existingEntities[$newEntityData['id']])) {
                $entity = $existingEntities[$newEntityData['id']];
            } else {
                $entity              = new Performer();
                $slugCheckEntities[] = $entity;
            }

            $scalarFields = [
                'id',
                'name',
                'bio',
                'gender',
                'nationality',
                'height',
                'weight',
                'chest',
                'bust',
                'waist',
                'hips'
            ];
            $entity->populate(array_intersect_key($newEntityData, array_flip($scalarFields)));

            $entity->setBirthDate(
                isset($newEntityData['birthDate']) ? new \DateTime($newEntityData['birthDate']) : null
            );

            $collection = new ArrayCollection();
            foreach ($newEntityData['eyeColors'] as $eyeColorData) {
                $collection->add($this->em->getReference('AppBundle:EyeColor', $eyeColorData['id']));
            }
            $entity->setEyeColors($collection);

            $collection = new ArrayCollection();
            foreach ($newEntityData['hairColors'] as $hairColorData) {
                $collection->add($this->em->getReference('AppBundle:HairColor', $hairColorData['id']));
            }
            $entity->setHairColors($collection);

            $this->pickSlug($entity, $slugCheckEntities);

            $this->em->persist($entity);
        }
    }

    /**
     * @param SluggableInterface   $entity
     * @param SluggableInterface[] $existingEntities
     */
    private function pickSlug(SluggableInterface $entity, array $existingEntities)
    {
        $slug = Filters::slugify($entity->getSlugSource());

        $slugSuffix = 0;

        do {
            $slugFound = true;
            foreach ($existingEntities as $existingEntity) {
                if ($existingEntity === $entity) {
                    continue;
                }

                if ($existingEntity->getSlug() == ($slugSuffix ? $slug . '-' . $slugSuffix : $slug)) {
                    $slugFound = false;
                    $slugSuffix++;
                    break;
                }
            }
        } while (!$slugFound);

        if ($slugSuffix) {
            $slug .= "-$slugSuffix";
        }

        $entity->setSlug($slug);
    }

    /**
     * @param array $existingEntities
     * @param array $newEntitiesData
     */
    private function updateContentTags(array $existingEntities, array $newEntitiesData)
    {
        $this->removeDisappearedEntities($existingEntities, $newEntitiesData);
        $slugCheckEntities = $existingEntities;

        foreach ($newEntitiesData as $newEntityData) {
            if (isset($existingEntities[$newEntityData['id']])) {
                $entity = $existingEntities[$newEntityData['id']];
            } else {
                $entity              = new ContentTag();
                $slugCheckEntities[] = $entity;
            }

            $entity->populate($newEntityData);
            $this->pickSlug($entity, $slugCheckEntities);

            $this->em->persist($entity);
        }
    }

    /**
     * @param array $existingEntities
     * @param array $newEntitiesData
     */
    private function updateEyeColors(array $existingEntities, array $newEntitiesData)
    {
        $this->removeDisappearedEntities($existingEntities, $newEntitiesData);

        foreach ($newEntitiesData as $newEntityData) {
            $entity = $existingEntities[$newEntityData['id']] ?? new EyeColor();

            $entity->populate($newEntityData);
            $this->em->persist($entity);
        }
    }

    /**
     * @param array $existingEntities
     * @param array $newEntitiesData
     */
    private function updateHairColors(array $existingEntities, array $newEntitiesData)
    {
        $this->removeDisappearedEntities($existingEntities, $newEntitiesData);

        foreach ($newEntitiesData as $newEntityData) {
            $entity = $existingEntities[$newEntityData['id']] ?? new HairColor();

            $entity->populate($newEntityData);
            $this->em->persist($entity);
        }
    }

    /**
     * @return bool|mixed
     */
    private function downloadData()
    {
        $response = $this->guzzle->get('paysites/1/scenes');
        $body     = (string)$response->getBody();

        switch ($response->getStatusCode()) {
            case 200:
                return \GuzzleHttp\json_decode($body, true);
            default:
                $this->logger->error(
                    "API request failed with {$response->getStatusCode()}.\n
                          Response body. {$body}"
                );

                return false;
        }
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function extractEntitiesData(array $data)
    {
        $entitiesData = [];
        $classes      = [
            HairColor::class,
            EyeColor::class,
            ContentTag::class,
            Performer::class,
            Scene::class
        ];

        foreach ($classes as $class) {
            $entitiesData[$class] = [];
        }

        foreach ($data as $scene) {
            $entitiesData[Scene::class][$scene['id']] = $scene;

            foreach ($scene['performers'] as $performer) {
                if (!empty($performer['nonSex'])) {
                    continue;
                }

                $entitiesData[Performer::class][$performer['id']] = $performer;

                foreach ($performer['hairColors'] as $hairColor) {
                    $entitiesData[HairColor::class][$hairColor['id']] = $hairColor;
                }

                foreach ($performer['eyeColors'] as $eyeColor) {
                    $entitiesData[EyeColor::class][$eyeColor['id']] = $eyeColor;
                }
            }

            foreach ($scene['contentTags'] as $contentTag) {
                $entitiesData[ContentTag::class][$contentTag['id']] = $contentTag;
            }
        }

        return $entitiesData;
    }

    /**
     * @param array $existingEntities
     * @param array $newEntitiesData
     */
    private function removeDisappearedEntities(array $existingEntities, array $newEntitiesData)
    {
        foreach ($existingEntities as $entityId => $existingEntity) {
            if (!isset($newEntitiesData[$entityId])) {
                $this->em->remove($existingEntity);
            }
        }
    }

    /**
     * @return array
     */
    private function fetchEntities()
    {
        $entities = [];

        $entities[HairColor::class]  = $this->em->getRepository('AppBundle:HairColor')->findAll();
        $entities[EyeColor::class]   = $this->em->getRepository('AppBundle:EyeColor')->findAll();
        $entities[ContentTag::class] = $this->em->getRepository('AppBundle:ContentTag')->findAll();
        $entities[Performer::class]  = $this->em->getRepository('AppBundle:Performer')->getWithEyeAndHairColors();
        $entities[Scene::class]      = $this->em->getRepository('AppBundle:Scene')->getWithPerformersAndContentTags();

        foreach ($entities as $class => &$collection) {
            $collection = Arrays::getKeysToEntitiesArray($collection, 'id');
        }

        return $entities;
    }
}
