<?php

namespace AppBundle\Service\Product;

use AppBundle\Entity\Product;
use AppBundle\Service\Product\GetList\{ProductGetListRequest, ProductGetListResponse};
use EaPaysites\Entity\Website;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;

class ProductService
{
    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * @param RpcClientInterface $rpcClient
     */
    public function __construct(RpcClientInterface $rpcClient)
    {
        $this->rpcClient = $rpcClient;
    }

    /**
     * @param ProductGetListRequest $serviceRequest
     *
     * @return ProductGetListResponse
     */
    public function getProductList(ProductGetListRequest $serviceRequest)
    {
        $rpcRequest = new RpcTask\Product\GetList\Request();
        $rpcRequest->populate($serviceRequest->toArray());

        $rpcRequest = new RpcRequest('Product\\GetList', $rpcRequest, RpcTask\Product\GetList\Response::class);
        $serviceResponse = new ProductGetListResponse();

        /** @var RpcTask\Product\GetList\Response $rpcResponse */
        $rpcResponse = $this->rpcClient->send($rpcRequest);

        if ($rpcResponse->isError()) {
            $serviceResponse->error = $rpcResponse->error;
        } else {
            $products = [];
            foreach ($rpcResponse->products as $product) {
                $products[] = $this->createProduct($product);
            }

            // Filter by current website and sort by price.
            $products = array_filter($products, function (Product $product) {
                return $product->getKind() == Website::staticGetOwnClassName();
            });
            uasort($products, function (Product $product1, Product $product2) {
                return $product1->getPrice() - $product2->getPrice();
            });

            if ($serviceRequest->withFreeAccessProduct) {
                array_unshift($products, $this->createFreeAccessProduct());
            }

            $serviceResponse->products = $products;
        }

        return $serviceResponse;
    }

    /**
     * @param array $data
     *
     * @return Product
     */
    private function createProduct(array $data): Product
    {
        $product = new Product();

        $fields = [
            'id',
            'title',
            'description',
            'billingDescription',
            'recurrent',
            'recurrentCount',
            'recurrentFrequency',
            'kind',
            'price',
            'currency',
            'priceMonthly',
        ];
        $product->populate(array_intersect_key($data, array_flip($fields)));

        return $product;
    }

    /**
     * @return Product
     */
    private function createFreeAccessProduct(): Product
    {
        $product = new Product();
        $product->setId(0);
        $product->setTitle('Free membership');
        $product->setDescription('1 free scene + all trailers');
        $product->setBillingDescription('1 free scene + all trailers');
        $product->setKind(Website::staticGetOwnClassName());
        $product->setRecurrent(false);
        $product->setPrice(0.0);

        return $product;
    }
}
