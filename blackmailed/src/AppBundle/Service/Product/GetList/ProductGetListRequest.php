<?php

namespace AppBundle\Service\Product\GetList;

use EaPaysites\Service\AbstractServiceRequest;

class ProductGetListRequest extends AbstractServiceRequest
{
    /**
     * @var int
     */
    public $website;

    /**
     * @var string
     */
    public $ip;

    /**
     * @var bool
     */
    public $withFreeAccessProduct = false;
}
