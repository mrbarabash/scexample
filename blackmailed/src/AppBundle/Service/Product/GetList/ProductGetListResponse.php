<?php

namespace AppBundle\Service\Product\GetList;

use AppBundle\Entity\Product;
use EaPaysites\Service\AbstractServiceResponse;

class ProductGetListResponse extends AbstractServiceResponse
{
    /**
     * @var Product[]
     */
    public $products;
}
