<?php

namespace AppBundle\Service\Scene;

use AppBundle\Entity\Repository\SceneRepository;
use EaPaysites\Service\ContentList;
use EaPaysites\Service\SortField;

class SceneListService
{
    private $sceneRepo;

    public function __construct(SceneRepository $sceneRepo)
    {
        $this->sceneRepo = $sceneRepo;
    }

    public function getList(SortField $sortField, $limit = 6)
    {
        return new ContentList();
    }
}
