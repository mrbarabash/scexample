<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AdultValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        $allowedMinimal = new \DateTime('-18 years');

        /** @var \DateTime $value */
        if ($value->getTimestamp() > $allowedMinimal->getTimestamp()) {
            $this->context->buildViolation($constraint->message)
                ->atPath('birthday')
                ->addViolation();
        }
    }
}
