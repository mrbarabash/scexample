<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Adult extends Constraint
{
    /**
     * @var string
     */
    public $message = 'You must be at least 18 years old';
}
