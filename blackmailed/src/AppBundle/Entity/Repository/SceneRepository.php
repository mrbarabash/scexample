<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Repository\QueryParams\ScenesListQueryParams;
use AppBundle\Entity\Scene;
use EaPaysites\Entity\Repository\AbstractRepository;
use EaPaysites\Util\Arrays;

class SceneRepository extends AbstractRepository
{
    /**
     * @param int[] $ids
     *
     * @return Scene[]
     */
    public function getScenesByIds(array $ids): array
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT s FROM AppBundle:Scene s WHERE s.id IN (:ids)"
        )->setParameter('ids', $ids);

        return $query->getResult();
    }

    /**
     * @return Scene[]
     */
    public function getWithPerformersAndContentTags(): array
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT s, p, ct
            FROM AppBundle:Scene s LEFT JOIN s.performers p LEFT JOIN s.contentTags ct"
        );

        return $query->getResult();
    }

    /**
     * @param int $performerId
     *
     * @return Scene[]
     */
    public function getPerformerScenes(int $performerId): array
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT s, p
            FROM AppBundle:Scene s LEFT JOIN s.performers p
            WHERE s.id IN (
                SELECT s_.id
                FROM AppBundle:Performer p_ JOIN p_.scenes s_
                WHERE p_.id = :performerId
            )"
        )->setParameter('performerId', $performerId);

        return $query->getResult();
    }

    /**
     * @param ScenesListQueryParams $queryParams
     *
     * @return Scene[]
     */
    public function getScenesByQueryParams(ScenesListQueryParams $queryParams): array
    {
        if ($queryParams->sort == 'rating') {
            if ($queryParams->isBts) {
                $queryParams->sortByIds = [
                    16746,
                    16751,
                    16748,
                    16742,
                    16764,
                    16759,
                    16757,
                    16756,
                    16754,
                    16755,
                    16753,
                    16747,
                    16743
                ];
            } else {
                $queryParams->sortByIds = [
                    16171,
                    15934,
                    17423,
                    16580,
                    16241,
                    14595,
                    15369,
                    16242,
                    16369,
                    16407,
                    16406,
                    15307,
                    16405,
                    15758,
                    15719,
                    15370
                ];
            }
        } elseif ($queryParams->sort == 'views') {
            if ($queryParams->isBts) {
                $queryParams->sortByIds = [
                    16742,
                    16751,
                    16746,
                    16748,
                    16764,
                    16759,
                    16757,
                    16756,
                    16754,
                    16755,
                    16753,
                    16747,
                    16743
                ];
            } else {
                $queryParams->sortByIds = [
                    15934,
                    16171,
                    16580,
                    14723,
                    16242,
                    14595,
                    16241,
                    15307,
                    16406,
                    16369,
                    16405,
                    16407,
                    15370,
                    15719,
                    15758,
                    15369
                ];
            }
        }

        $qb = $this->createQueryBuilder('s')
                   ->select('s, p')
                   ->leftJoin('s.performers', 'p')
                   ->orderBy("s.{$queryParams->sort}", $queryParams->sortDirection);

        $qb->andWhere('s.isBts = :isBts')
           ->setParameter('isBts', (bool)$queryParams->isBts);

        if ($queryParams->onlyLive) {
            $qb->andWhere('(:now BETWEEN s.liveDateStart AND s.liveDateEnd)')
               ->setParameter('now', new \DateTime());
        }

        if ($queryParams->sortByIds) {
            $qb->andWhere('s.id IN (:ids)')
               ->setParameter(':ids', $queryParams->sortByIds);
        }

        if ($queryParams->contentTagId) {
            $qb->join('s.contentTags', 'ct')
               ->andWhere('ct.id = :contentTagId')
               ->setParameter('contentTagId', $queryParams->contentTagId);
        }

        $scenes = $qb->getQuery()->getResult();

        if ($queryParams->sortByIds) {
            $scenes = Arrays::sortByValuesArray($scenes, 'id', $queryParams->sortByIds);
        }

        if ($queryParams->limit) {
            $scenes = array_slice($scenes, 0, $queryParams->limit);
        }

        return $scenes;
    }

    /**
     * @return Scene|null
     */
    public function getClosestUpcomingScene(): ?Scene
    {
        $dql = "SELECT s, p
                FROM AppBundle:Scene s LEFT JOIN s.performers p
                WHERE s.liveDateStart >= :now AND s.isBts = false
                ORDER BY s.liveDateStart ASC";

        $query = $this->getEntityManager()
                      ->createQuery($dql)
                      ->setMaxResults(1)
                      ->setParameters(['now' => new \DateTime()]);

        return $query->getOneOrNullResult();
    }

    /**
     * @return int[]
     */
    public function getScenesIds(): array
    {
        return $this->getFieldValues('id');
    }

    /**
     * @param array $contentIdsToRatings
     */
    public function updateAggregateRating(array $contentIdsToRatings)
    {
        if (empty($contentIdsToRatings)) {
            return;
        }

        $conn  = $this->getEntityManager()->getConnection();
        $table = $this->getEntityManager()->getClassMetadata($this->getEntityName())->getTableName();

        $selects = [];
        foreach ($contentIdsToRatings as $contentId => $rating) {
            $selects[] = "SELECT {$conn->quote($contentId)} AS id, {$conn->quote($rating)} AS _rating";
        }

        $sql = "UPDATE {$conn->quoteIdentifier($table)} s
                JOIN (" . implode(' UNION ALL ', $selects) . ") rating_values ON s.id = rating_values.id
                SET s.rating = _rating";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
}
