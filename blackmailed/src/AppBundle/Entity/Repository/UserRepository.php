<?php

namespace AppBundle\Entity\Repository;

use EaPaysites\Entity\Repository\AbstractRepository;
use EaPaysites\Entity\Repository\UserRepositoryInterface;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    /**
     * @param int    $userId
     * @param string $passwordHash
     */
    public function updatePassword(int $userId, string $passwordHash)
    {
        $query = $this->getEntityManager()->createQuery(
            "UPDATE AppBundle:User u
            SET u.password = :password
            WHERE u.id = :id"
        )->setParameters(
            [
                'id'       => $userId,
                'password' => $passwordHash,
            ]
        );

        $query->execute();
    }
}
