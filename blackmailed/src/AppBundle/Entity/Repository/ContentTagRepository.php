<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ContentTag;
use EaPaysites\Entity\Repository\AbstractRepository;
use EaPaysites\Service\SortField;

class ContentTagRepository extends AbstractRepository
{
    /**
     * @param int $performerId
     *
     * @return ContentTag[]
     */
    public function getPerformerContentTags(int $performerId): array
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT ct
            FROM AppBundle:ContentTag ct JOIN ct.scenes s JOIN s.performers p
            WHERE p.id = :performerId"
        )->setParameter('performerId', $performerId);

        return $query->getResult();
    }

    /**
     * @param SortField $sortField
     *
     * @return ContentTag[]
     */
    public function getSorted(SortField $sortField): array
    {
        $dql = "SELECT ct
                FROM AppBundle:ContentTag ct
                ORDER BY ct.{$sortField->field} {$sortField->direction}";

        $query = $this->getEntityManager()
                      ->createQuery($dql);

        $contentTags = $query->getResult();

        return $contentTags;
    }
}
