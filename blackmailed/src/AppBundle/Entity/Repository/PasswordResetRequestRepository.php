<?php

namespace AppBundle\Entity\Repository;

use EaPaysites\Entity\Repository\AbstractRepository;

/**
 * Class PasswordResetRequestRepository
 * @package AppBundle\Entity\Repository
 */
class PasswordResetRequestRepository extends AbstractRepository
{
}
