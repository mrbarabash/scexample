<?php

namespace AppBundle\Entity\Repository\QueryParams;

class ScenesListQueryParams extends AbstractQueryParams
{
    /**
     * @var bool
     */
    public $isBts = false;

    /**
     * @var string
     */
    public $sort = 'id';

    /**
     * @var string
     */
    public $sortDirection = 'desc';

    /**
     * @var int
     */
    public $limit;

    /**
     * @var bool
     */
    public $onlyLive = true;

    /**
     * @var array
     */
    public $sortByIds;

    /**
     * @var int
     */
    public $contentTagId;
}
