<?php

namespace AppBundle\Entity\Repository\QueryParams;

class AbstractQueryParams
{
    /**
     * AbstractQueryParams constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->populate($data);
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function populate(array $data)
    {
        foreach ($data as $name => $value) {
            if (property_exists($this, $name)) {
                $this->$name = $value;
            }
        }

        return $this;
    }
}
