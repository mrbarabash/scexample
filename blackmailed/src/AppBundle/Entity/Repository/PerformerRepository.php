<?php

namespace AppBundle\Entity\Repository;

use EaPaysites\Entity\Repository\AbstractRepository;
use EaPaysites\Service\SortField;
use EaPaysites\Util\Arrays;

class PerformerRepository extends AbstractRepository
{
    /**
     * @return array
     */
    public function getWithEyeAndHairColors()
    {
        $dql = "SELECT p, hc, ec FROM AppBundle:Performer p LEFT JOIN p.hairColors hc LEFT JOIN p.eyeColors ec";

        $query = $this->getEntityManager()->createQuery($dql);

        $performers = $query->getResult();

        return $performers;
    }

    /**
     * @return int[]
     */
    public function getIds(): array
    {
        return $this->getFieldValues('id');
    }

    /**
     * @param SortField $sortField
     *
     * @return array
     */
    public function getPerformers(SortField $sortField): array
    {
        $ids = null;
        if ($sortField->field == 'rating') {
            $ids = [3806, 9711, 8957, 9201, 9141, 8473, 2982, 9098, 5622, 8854, 9688, 8895, 9133, 9543, 9076, 9488, 9472, 9095, 9639, 9556, 9474, 9610];
        }

        $qb = $this->createQueryBuilder('p')
                   ->select('p')
                   ->orderBy("p.{$sortField->field}", $sortField->direction);

        if ($ids) {
            $qb->andWhere('p.id IN (:ids)')
               ->setParameter('ids', $ids);
        }

        $query      = $qb->getQuery();
        $performers = $query->getResult();

        if ($ids) {
            $performers = Arrays::sortByValuesArray($performers, 'id', $ids);
        }

        return $performers;
    }

    /**
     * @param array $contentIdsToRatings
     */
    public function updateAggregateRating(array $contentIdsToRatings)
    {
        if (empty($contentIdsToRatings)) {
            return;
        }

        $conn  = $this->getEntityManager()->getConnection();
        $table = $this->getEntityManager()->getClassMetadata($this->getEntityName())->getTableName();

        $selects = [];
        foreach ($contentIdsToRatings as $contentId => $rating) {
            $selects[] = "SELECT {$conn->quote($contentId)} AS id, {$conn->quote($rating)} AS _rating";
        }

        $sql = "UPDATE {$conn->quoteIdentifier($table)} p
                JOIN (" . implode(' UNION ALL ', $selects) . ") rating_values ON p.id = rating_values.id
                SET p.rating = _rating";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
}
