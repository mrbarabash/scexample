<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityInterface\SluggableInterface;
use AppBundle\Entity\EntityTrait\SluggableTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use EaPaysites\Entity\AbstractEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ContentTag
 * @ORM\Table(
 *      name="content_tags",
 *      uniqueConstraints={
 *          @UniqueConstraint(name="slug", columns={"slug"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ContentTagRepository")
 */
class ContentTag extends AbstractEntity implements SluggableInterface
{
    use SluggableTrait;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(
     *      min = 2,
     *      max = 255
     * )
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\Type(type="float")
     * @Assert\Range(
     *      min = 0,
     *      max = 1,
     * )
     * @ORM\Column(name="weight", type="float", nullable=false)
     */
    private $weight;

    /**
     * @ORM\ManyToMany(targetEntity="Scene", mappedBy="contentTags")
     */
    private $scenes;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getScenes()
    {
        return $this->scenes;
    }

    /**
     * @param mixed $scenes
     */
    public function setScenes($scenes)
    {
        $this->scenes = $scenes;
    }

    /**
     * @return string
     */
    public function getSlugSource(): string
    {
        return $this->name;
    }
}
