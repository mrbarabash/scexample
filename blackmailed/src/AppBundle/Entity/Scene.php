<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityInterface\SluggableInterface;
use AppBundle\Entity\EntityInterface\VotableInterface;
use AppBundle\Entity\EntityTrait\SluggableTrait;
use AppBundle\Entity\Scene\CdnTokens;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\UniqueConstraint;
use EaPaysites\Entity\AbstractEntity;
use EaPaysites\Entity\ContentPermissionableInterface;
use EaPaysites\Util\Arrays;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(
 *      name="scenes",
 *      uniqueConstraints={
 *          @UniqueConstraint(name="slug", columns={"slug"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\SceneRepository")
 */
class Scene extends AbstractEntity implements ContentPermissionableInterface, VotableInterface, SluggableInterface
{
    use SluggableTrait;

    const QUALITY_UNKNOWN = 0;

    const QUALITY_SD = 1;

    const QUALITY_HD = 2;

    const QUALITY_4K = 3;

    const RATING_PUBLIC_VISIBILITY_THRESHOLD = 0.7;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var integer|null
     * @ORM\Column(name="runtime", type="integer", nullable=true)
     */
    private $runtime;

    /**
     * @var string|null
     * @ORM\Column(name="synopsis", type="text", length=65535, nullable=true)
     */
    private $synopsis;

    /**
     * @var string|null
     * @ORM\Column(name="summary_255", type="string", length=255, nullable=true)
     */
    private $summary255;

    /**
     * @var \DateTime
     * @ORM\Column(name="live_date_start", type="datetime", nullable=false)
     * @Assert\NotBlank()
     */
    private $liveDateStart;

    /**
     * @var \DateTime
     * @ORM\Column(name="live_date_end", type="datetime", nullable=false)
     * @Assert\NotBlank()
     */
    private $liveDateEnd;

    /**
     * @var boolean
     * @ORM\Column(name="is_bts", type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $isBts;

    /**
     * @var int
     * @ORM\Column(name="pictures_count", type="smallint", nullable=false)
     * @Assert\NotBlank()
     */
    private $picturesCount;

    /**
     * @var Scene|null
     * @ORM\ManyToOne(targetEntity="Scene", inversedBy="subordinateScenes")
     * @ORM\JoinColumn(name="original_scene_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $originalScene;

    /**
     * @var int
     * @ORM\Column(name="quality", type="integer", nullable=false)
     */
    private $quality;

    /**
     * @var Performer[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Performer", inversedBy="scenes")
     * @ORM\JoinTable(
     *      name="scene_performer",
     *      joinColumns={@ORM\JoinColumn(
     *          name="scene_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE"
     *      )},
     *      inverseJoinColumns={@ORM\JoinColumn(
     *          name="performer_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE"
     *      )}
     * )
     */
    private $performers;

    /**
     * @var ContentTag[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="ContentTag", inversedBy="scenes")
     * @ORM\JoinTable(
     *      name="scene_content_tag",
     *      joinColumns={@JoinColumn(
     *          name="scene_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE"
     *      )},
     *      inverseJoinColumns={@JoinColumn(
     *          name="content_tag_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE"
     *      )}
     * )
     */
    private $contentTags;

    /**
     * @var Scene[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Scene", mappedBy="originalScene")
     */
    private $subordinateScenes;

    /**
     * @var float|null
     * @ORM\Column(name="rating", type="float", nullable=true)
     */
    private $rating;

    /**
     * @var int
     * @ORM\Column(name="views", type="bigint", nullable=false)
     */
    private $views = 0;

    /**
     * @var CdnTokens|null
     * @ORM\Column(type="cdn_tokens", nullable=true, name="cdnTokens")
     */
    private $cdnTokens;

    /**
     * Scene constructor.
     */
    public function __construct()
    {
        $this->performers        = new ArrayCollection();
        $this->contentTags       = new ArrayCollection();
        $this->subordinateScenes = new ArrayCollection();
        $this->cdnTokens         = new CdnTokens();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int|null
     */
    public function getRuntime()
    {
        return $this->runtime;
    }

    /**
     * @param int|null $runtime
     */
    public function setRuntime($runtime)
    {
        $this->runtime = $runtime;
    }

    /**
     * @return null|string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * @param null|string $synopsis
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;
    }

    /**
     * @return null|string
     */
    public function getSummary255()
    {
        return $this->summary255;
    }

    /**
     * @param null|string $summary255
     */
    public function setSummary255($summary255)
    {
        $this->summary255 = $summary255;
    }

    /**
     * @return \DateTime
     */
    public function getLiveDateStart(): \DateTime
    {
        return $this->liveDateStart;
    }

    /**
     * @param \DateTime $liveDateStart
     */
    public function setLiveDateStart(\DateTime $liveDateStart)
    {
        $this->liveDateStart = $liveDateStart;
    }

    /**
     * @return \DateTime
     */
    public function getLiveDateEnd(): \DateTime
    {
        return $this->liveDateEnd;
    }

    /**
     * @param \DateTime $liveDateEnd
     */
    public function setLiveDateEnd(\DateTime $liveDateEnd)
    {
        $this->liveDateEnd = $liveDateEnd;
    }

    /**
     * @return bool
     */
    public function getIsBts(): bool
    {
        return $this->isBts;
    }

    /**
     * @param bool $isBts
     */
    public function setIsBts(bool $isBts)
    {
        $this->isBts = $isBts;
    }

    /**
     * @return int
     */
    public function getPicturesCount(): int
    {
        return $this->picturesCount;
    }

    /**
     * @param int $picturesCount
     */
    public function setPicturesCount(int $picturesCount)
    {
        $this->picturesCount = $picturesCount;
    }

    /**
     * @return mixed
     */
    public function getOriginalScene()
    {
        return $this->originalScene;
    }

    /**
     * @param mixed $originalScene
     */
    public function setOriginalScene($originalScene)
    {
        $this->originalScene = $originalScene;
    }

    /**
     * @return int
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     */
    public function setQuality(int $quality)
    {
        $this->quality = $quality;
    }

    /**
     * @return Performer[]|ArrayCollection
     */
    public function getPerformers()
    {
        return $this->performers;
    }

    /**
     * @param mixed $performers
     */
    public function setPerformers($performers)
    {
        $this->performers = $performers;
    }

    /**
     * @return ContentTag[]
     */
    public function getContentTags(): array
    {
        $contentTags = $this->contentTags->toArray();

        Arrays::sortByKey($contentTags, 'weight');

        return $contentTags;
    }

    /**
     * @param mixed $contentTags
     */
    public function setContentTags($contentTags)
    {
        $this->contentTags = $contentTags;
    }

    /**
     * @return Scene[]|ArrayCollection
     */
    public function getSubordinateScenes()
    {
        return $this->subordinateScenes;
    }

    /**
     * @param mixed $subordinateScenes
     */
    public function setSubordinateScenes($subordinateScenes)
    {
        $this->subordinateScenes = $subordinateScenes;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return array Performer[]
     */
    public function getCast()
    {
        $cast = $this->getPerformers()->toArray();

        Arrays::sortByKey($cast, 'name');

        return $cast;
    }

    /**
     * @return bool
     */
    public function isRatingPublic()
    {
        return $this->rating > self::RATING_PUBLIC_VISIBILITY_THRESHOLD;
    }

    /**
     * @return string[]
     */
    public function getImagesPaths(): array
    {
        $paths = [];
        for ($i = 1; $i <= $this->picturesCount; $i++) {
            $paths[] = '#';
        }

        return $paths;
    }

    /**
     * @return int
     */
    public function getTimeTillLiveDateStart(): int
    {
        $diff = $this->liveDateStart->getTimestamp() - time();

        return $diff < 0 ? 0 : $diff;
    }

    /**
     * @return bool
     */
    public function isLive(): bool
    {
        return $this->liveDateStart->getTimestamp() < time() && $this->liveDateEnd->getTimestamp() > time();
    }

    /**
     * @return string
     */
    public function getRuntimeHumanLong()
    {
        if (!$this->runtime) {
            return '';
        }

        $hours   = intval($this->runtime / 60);
        $minutes = $this->runtime % 60;

        $text = '';

        if ($hours) {
            $text .= "{$hours} hours";
        }

        if ($minutes) {
            $text .= " {$minutes} minutes";
        }
        trim($text);

        return $text;
    }

    /**
     * @return string
     */
    public function getRuntimeHumanShort()
    {
        if (!$this->runtime) {
            return '';
        }

        $hours   = intval($this->runtime / 60);
        $minutes = $this->runtime % 60;

        $text = sprintf("%02d:%02d", $hours, $minutes);

        return $text;
    }

    /**
     * @return string
     */
    public function getRuntimeHumanMinutes()
    {
        if (!$this->runtime) {
            return '';
        }

        return $this->runtime;
    }

    /**
     * @return int
     */
    public function getViews(): int
    {
        return $this->views;
    }

    /**
     * @param int $views
     */
    public function setViews(int $views)
    {
        $this->views = $views;
    }

    /**
     * @return CdnTokens
     */
    public function getCdnTokens(): CdnTokens
    {
        return $this->cdnTokens;
    }

    /**
     * @param CdnTokens $cdnTokens
     */
    public function setCdnTokens(CdnTokens $cdnTokens)
    {
        $this->cdnTokens = $cdnTokens;
    }

    /**
     * @return int|null
     */
    public function getBtsRuntimeMinutes()
    {
        /** @var Scene $bts */
        $bts = $this->getSubordinateScenes()->first();
        if (!$bts) {
            return null;
        }

        return $bts->getRuntime();
    }

    /**
     * @return bool
     */
    public function hasBts(): bool
    {
        return $this->getSubordinateScenes()->count() > 0;
    }

    /**
     * @return Scene|null
     */
    public function getBts(): ?Scene
    {
        if (!$this->hasBts()) {
            return null;
        }

        return $this->getSubordinateScenes()->first();
    }

    /**
     * @return string
     */
    public function getSlugSource(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isQuality4kAvailable(): bool
    {
        return $this->quality >= self::QUALITY_4K;
    }

    /**
     * @return bool
     */
    public function isQualityHdAvailable(): bool
    {
        return $this->quality >= self::QUALITY_HD;
    }
}
