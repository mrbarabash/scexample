<?php

namespace AppBundle\Entity\EntityTrait;

trait SluggableTrait
{
    /**
     * @var string
     * @ORM\Column(length=255, nullable=false)
     */
    private $slug;

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }
}
