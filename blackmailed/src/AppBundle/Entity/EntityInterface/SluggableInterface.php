<?php

namespace AppBundle\Entity\EntityInterface;

interface SluggableInterface
{
    /**
     * @return string
     */
    public function getSlug(): string;

    /**
     * @param string $slug
     *
     * @return void
     */
    public function setSlug(string $slug);

    /**
     * Field(s) value used to generate slug (e.g. Scene::title)
     * @return string
     */
    public function getSlugSource(): string;
}
