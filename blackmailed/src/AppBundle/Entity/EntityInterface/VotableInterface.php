<?php

namespace AppBundle\Entity\EntityInterface;

interface VotableInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getOwnClassName(): string;
}
