<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PurchaseTokenRepository")
 * @ORM\Table(name="purchase_tokens")
 */
class PurchaseToken extends AbstractEntity
{
    const KIND_SCENE = 'scene';
    const KIND_CATEGORY = 'category';

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(length=25)
     */
    private $kind;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $usedFor;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="purchaseTokens")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $usedAt;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     */
    public function setKind(string $kind)
    {
        $this->kind = $kind;
    }

    /**
     * @return int
     */
    public function getUsedFor(): int
    {
        return $this->usedFor;
    }

    /**
     * @param int|null $usedFor
     */
    public function setUsedFor(int $usedFor = null)
    {
        $this->usedFor = $usedFor;
    }

    /**
     * @return bool
     */
    public function isUsed(): bool
    {
        return (bool) $this->usedFor;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|string $dateTime
     */
    public function setCreatedAt($dateTime)
    {
        $dateTime = $dateTime instanceof \DateTime ? $dateTime : new \DateTime($dateTime);

        $this->createdAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getUsedAt(): \DateTime
    {
        return $this->usedAt;
    }

    /**
     * @param \DateTime|string|null $usedAt
     */
    public function setUsedAt($usedAt = null)
    {
        if ($usedAt && !$usedAt instanceof \DateTime) {
            $usedAt = new \DateTime($usedAt);
        }

        $this->usedAt = $usedAt;
    }
}
