<?php

namespace AppBundle\Entity\Scene;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use JMS\Serializer\SerializerBuilder;

class CdnTokensType extends Type
{
    const CDN_TOKENS = 'cdn_tokens';

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'TEXT';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return CdnTokens|array|\JMS\Serializer\|mixed|object
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return new CdnTokens();
        }

        $serializer = SerializerBuilder::create()->build();

        return $serializer->deserialize($value, CdnTokens::class, 'json');
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $serializer = SerializerBuilder::create()->build();

        return $serializer->serialize($value, 'json');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::CDN_TOKENS;
    }
}