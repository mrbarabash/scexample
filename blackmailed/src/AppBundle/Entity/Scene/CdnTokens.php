<?php

namespace AppBundle\Entity\Scene;

use JMS\Serializer\Annotation as Serializer;

class CdnTokens
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $free;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $streaming;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $downloadBasic;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $downloadUhd;
}
