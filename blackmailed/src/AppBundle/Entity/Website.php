<?php

namespace AppBundle\Entity;

use EaPaysites\Entity\Website as StanWebsite;

class Website extends StanWebsite
{
    const NAME = 'Blackmailed';

    const EMAIL_FROM = 'noreply@blackmailed.com';
}
