<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityInterface\SluggableInterface;
use AppBundle\Entity\EntityInterface\VotableInterface;
use AppBundle\Entity\EntityTrait\SluggableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use EaPaysites\Entity\AbstractEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(
 *      name="performers",
 *      uniqueConstraints={
 *          @UniqueConstraint(name="slug", columns={"slug"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PerformerRepository")
 */
class Performer extends AbstractEntity implements VotableInterface, SluggableInterface
{
    use SluggableTrait;

    const GENDER_MALE = 'M';

    const GENDER_FEMALE = 'F';

    const GENDER_TS = 'T';

    const RATING_PUBLIC_VISIBILITY_THRESHOLD = 0.7;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(name="bio", type="text", length=65535, nullable=true)
     */
    private $bio;

    /**
     * Either F or M
     * @var string
     * @ORM\Column(name="gender", type="string", length=2, nullable=true)
     */
    private $gender;

    /**
     * @var \DateTime
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @var string
     * @ORM\Column(name="nationality", type="string", length=2, nullable=true)
     */
    private $nationality;

    /**
     * @var EyeColor[]
     * @ORM\ManyToMany(targetEntity="EyeColor")
     * @ORM\JoinTable(
     *      name="performer_eye_color",
     *      joinColumns={@ORM\JoinColumn(
     *          name="performer_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE"
     *      )},
     *      inverseJoinColumns={@ORM\JoinColumn(
     *          name="eye_color_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE"
     *      )}
     * )
     */
    private $eyeColors;

    /**
     * @var HairColor[]
     * @ORM\ManyToMany(targetEntity="HairColor")
     * @ORM\JoinTable(
     *      name="performer_hair_color",
     *      joinColumns={@ORM\JoinColumn(
     *          name="performer_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE"
     *      )},
     *      inverseJoinColumns={@ORM\JoinColumn(
     *          name="hair_color_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE"
     *      )}
     * )
     */
    private $hairColors;

    /**
     * @var int
     * @ORM\Column(name="height", type="smallint", nullable=true)
     * @Assert\Range(min = 0)
     */
    private $height;

    /**
     * @var int
     * @ORM\Column(name="chest", type="smallint", nullable=true)
     * @Assert\Range(min = 0)
     */
    private $chest;

    /**
     * @var int
     * @ORM\Column(name="waist", type="smallint", nullable=true)
     * @Assert\Range(min = 0)
     */
    private $waist;

    /**
     * @var int
     * @ORM\Column(name="hips", type="smallint", nullable=true)
     * @Assert\Range(min = 0)
     */
    private $hips;

    /**
     * @var string
     * @ORM\Column(name="bust", type="string", length=10, nullable=true)
     */
    private $bust;

    /**
     * @var int
     * @ORM\Column(name="weight", type="smallint", nullable=true)
     * @Assert\Range(min = 0)
     */
    private $weight;

    /**
     * @ORM\ManyToMany(targetEntity="Scene", mappedBy="performers")
     * @ORM\JoinTable(name="scene_performer")
     */
    private $scenes;

    /**
     * @var float|null
     * @ORM\Column(name="rating", type="float", nullable=true)
     */
    private $rating;

    /**
     * Performer constructor.
     */
    public function __construct()
    {
        $this->eyeColors  = new ArrayCollection();
        $this->hairColors = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param null|string $bio
     */
    public function setBio($bio = null)
    {
        $this->bio = $bio;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender = null)
    {
        $this->gender = $gender;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     */
    public function setBirthDate(\DateTime $birthDate = null)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string
     */
    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality(string $nationality = null)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return EyeColor[]|ArrayCollection
     */
    public function getEyeColors()
    {
        return $this->eyeColors;
    }

    /**
     * @param EyeColor[]|ArrayCollection $eyeColors
     */
    public function setEyeColors($eyeColors)
    {
        $this->eyeColors = $eyeColors;
    }

    /**
     * @return HairColor[]|ArrayCollection
     */
    public function getHairColors()
    {
        return $this->hairColors;
    }

    /**
     * @param HairColor[]|ArrayCollection $hairColors
     */
    public function setHairColors($hairColors)
    {
        $this->hairColors = $hairColors;
    }

    /**
     * @return int
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height = null)
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getChest(): ?int
    {
        return $this->chest;
    }

    /**
     * @param int $chest
     */
    public function setChest(int $chest = null)
    {
        $this->chest = $chest;
    }

    /**
     * @return int
     */
    public function getWaist(): ?int
    {
        return $this->waist;
    }

    /**
     * @param int $waist
     */
    public function setWaist(int $waist = null)
    {
        $this->waist = $waist;
    }

    /**
     * @return int
     */
    public function getHips(): ?int
    {
        return $this->hips;
    }

    /**
     * @param int $hips
     */
    public function setHips(int $hips = null)
    {
        $this->hips = $hips;
    }

    /**
     * @return string
     */
    public function getBust(): string
    {
        return $this->bust;
    }

    /**
     * @param string $bust
     */
    public function setBust(string $bust = null)
    {
        $this->bust = $bust;
    }

    /**
     * @return int
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight = null)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getScenes()
    {
        return $this->scenes;
    }

    /**
     * @param mixed $scenes
     */
    public function setScenes($scenes)
    {
        $this->scenes = $scenes;
    }

    /**
     * @return float|null
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float|null $rating
     */
    public function setRating($rating = null)
    {
        $this->rating = $rating;
    }

    /**
     * @return bool
     */
    public function isRatingPublic()
    {
        return $this->rating > self::RATING_PUBLIC_VISIBILITY_THRESHOLD;
    }

    /**
     * @return string
     */
    public function getSlugSource(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getHeightHuman(): string
    {
        if (!$this->height) {
            return '';
        }

        $feet   = (int)($this->height / 12);
        $height = $feet . '\'';

        $inches = $this->height % 12;
        if ($inches) {
            $height .= $inches . '"';
        }

        $height .= ' (' . (int)($this->height * 2.54) . ' cm)';

        return $height;
    }

    /**
     * @return string
     */
    public function getWeightHuman(): string
    {
        if (!$this->weight) {
            return '';
        }

        $weight = $this->weight . ' lbs.';
        $weight .= ' (' . (int)($this->weight * 0.454) . ' kg)';

        return $weight;
    }

    /**
     * @return string
     */
    public function getMeasurements(): string
    {
        if (!$this->chest && !$this->bust && !$this->waist && !$this->hips) {
            return '';
        }

        $measurements = '';
        $measurements .= $this->chest ?: '??';
        $measurements .= $this->bust ?: '';
        $measurements .= '-';
        $measurements .= $this->waist ?: '??';
        $measurements .= '-';
        $measurements .= $this->hips ?: '??';

        return $measurements;
    }
}
