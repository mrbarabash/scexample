<?php

namespace AppBundle\Entity;

use EaPaysites\Entity\AbstractEntity;

/**
 * Do not store product in the db.
 */
class Product extends AbstractEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $billingDescription;

    /**
     * @var bool
     */
    private $recurrent;

    /**
     * @var int|null
     */
    private $recurrentCount;

    /**
     * @var int|null
     */
    private $recurrentFrequency;

    /**
     * @var string
     */
    private $kind;

    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $priceMonthly;

    /**
     * @var string|null
     */
    private $currency;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getBillingDescription(): string
    {
        return $this->billingDescription;
    }

    /**
     * @param string $billingDescription
     */
    public function setBillingDescription(string $billingDescription)
    {
        $this->billingDescription = $billingDescription;
    }

    /**
     * @return bool
     */
    public function isRecurrent(): bool
    {
        return $this->recurrent;
    }

    /**
     * @param bool $recurrent
     */
    public function setRecurrent(bool $recurrent)
    {
        $this->recurrent = $recurrent;
    }

    /**
     * @return int|null
     */
    public function getRecurrentCount(): ?int
    {
        return $this->recurrentCount;
    }

    /**
     * @param int|null $recurrentCount
     */
    public function setRecurrentCount(int $recurrentCount = null)
    {
        $this->recurrentCount = $recurrentCount;
    }

    /**
     * @return int|null
     */
    public function getRecurrentFrequency(): ?int
    {
        return $this->recurrentFrequency;
    }

    /**
     * @param int|null $recurrentFrequency
     */
    public function setRecurrentFrequency(int $recurrentFrequency = null)
    {
        $this->recurrentFrequency = $recurrentFrequency;
    }

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     */
    public function setKind(string $kind)
    {
        $this->kind = $kind;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPriceMonthly(): float
    {
        return $this->priceMonthly;
    }

    /**
     * @param float $priceMonthly
     */
    public function setPriceMonthly(float $priceMonthly)
    {
        $this->priceMonthly = $priceMonthly;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(string $currency = null)
    {
        $this->currency = $currency;
    }

    /**
     * @return bool
     */
    public function isFree(): bool
    {
        return $this->getPrice() < 0.1;
    }
}
