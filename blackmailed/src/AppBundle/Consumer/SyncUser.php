<?php

namespace AppBundle\Consumer;

use AppBundle\Service\User\Sync\SyncUserRequest;
use AppBundle\Service\User\SyncService;
use EaPaysites\RpcTask;
use EaPaysites\Sqs\SqsConsumerInterface;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

class SyncUser implements SqsConsumerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SyncService
     */
    private $syncService;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * SyncUser constructor.
     *
     * @param LoggerInterface     $logger
     * @param SyncService         $syncService
     * @param SerializerInterface $serializer
     */
    public function __construct(LoggerInterface $logger, SyncService $syncService, SerializerInterface $serializer)
    {
        $this->logger      = $logger;
        $this->syncService = $syncService;
        $this->serializer  = $serializer;
    }

    /**
     * @param array $message
     *
     * @return bool
     */
    public function execute(array $message): bool
    {
        try {
            /** @var RpcTask\Customer\Sync\Request $rpcServiceRequest */
            $rpcServiceRequest = $this->serializer->deserialize(
                $message['Body'],
                RpcTask\Customer\Sync\Request::class,
                'json'
            );
        } catch (\Exception $e) {
            $this->logger->error('Failed to deserialize message', ['message' => $message]);

            return false;
        }

        $this->logger->notice('Got sync user request', ['rpcServiceRequest' => $rpcServiceRequest]);

        $serviceRequest       = new SyncUserRequest();
        $serviceRequest->user = $rpcServiceRequest->toArray();

        try {
            $serviceResponse = $this->syncService->sync($serviceRequest);
        } catch (\Exception $e) {
            $this->logger->error(
                'Sync user: unexpected error',
                ['exception' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return false;
        }

        $this->logger->notice('Synced user', ['email' => $rpcServiceRequest->email]);

        return true;
    }
}
