<?php

namespace AppBundle\Consumer;

use AppBundle\Service\Content\ContentPermissionService;
use EaPaysites\RpcTask;
use EaPaysites\Sqs\SqsConsumerInterface;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

class SyncContentPermission implements SqsConsumerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ContentPermissionService
     */
    private $contentPermissionService;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * SyncContentPermission constructor.
     *
     * @param LoggerInterface          $logger
     * @param ContentPermissionService $contentPermissionService
     * @param SerializerInterface      $serializer
     */
    public function __construct(
        LoggerInterface $logger,
        ContentPermissionService $contentPermissionService,
        SerializerInterface $serializer
    ) {
        $this->logger                   = $logger;
        $this->contentPermissionService = $contentPermissionService;
        $this->serializer               = $serializer;
    }

    /**
     * @param array $message
     *
     * @return bool
     */
    public function execute(array $message): bool
    {
        try {
            /** @var RpcTask\ContentPermission\Sync\Request $rpcServiceRequest */
            $rpcServiceRequest = $this->serializer->deserialize(
                $message['Body'],
                RpcTask\ContentPermission\Sync\Request::class,
                'json'
            );
        } catch (\Exception $e) {
            $this->logger->error('Failed to deserialize message', ['message' => $message]);

            return false;
        }

        $this->logger->notice('Got sync permissions request', ['rpcServiceRequest' => $rpcServiceRequest]);

        try {
            $this->contentPermissionService->createContentPermission($rpcServiceRequest->toArray(), true);
        } catch (\Exception $e) {
            $this->logger->error(
                'Sync permissions: unexpected error',
                ['exception' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return false;
        }

        $this->logger->notice('Synced permissions', ['rpcServiceRequestId' => $rpcServiceRequest->id]);

        return true;
    }
}
