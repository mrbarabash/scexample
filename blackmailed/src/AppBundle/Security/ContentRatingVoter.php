<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContentRatingVoter extends Voter
{
    const SUBJECT = 'ContentRating';

    const ATTRIBUTE_VOTE = 'vote';

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker     = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, $this->getAttributes())) {
            return false;
        }

        if ($subject != self::SUBJECT) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // Deny access if the user is not logged in.
            return false;
        }

        return $user->isEmailConfirmed()
            || $this->authorizationChecker->isGranted(WebsiteVoter::ATTRIBUTE_ACCESS, WebsiteVoter::SUBJECT);
    }

    /**
     * @return array
     */
    private function getAttributes(): array
    {
        return [
            self::ATTRIBUTE_VOTE,
        ];
    }
}
