<?php

namespace AppBundle\Security;

use AppBundle\Entity\ContentPermission;
use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use AppBundle\Service\Content\ContentPermissionService;
use EaPaysites\Entity\ContentPermission as StanContentPermission;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class WebsiteVoter extends Voter
{
    const SUBJECT = 'website';

    const ATTRIBUTE_ACCESS = 'access';

    /**
     * @var ContentPermissionService
     */
    private $contentPermissionService;

    /**
     * @param ContentPermissionService $contentPermissionService
     */
    public function __construct(ContentPermissionService $contentPermissionService)
    {
        $this->contentPermissionService = $contentPermissionService;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, $this->getAttributes())) {
            return false;
        }

        return $subject == self::SUBJECT;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // Deny access if the user is not logged in.
            return false;
        }

        switch ($attribute) {
            case self::ATTRIBUTE_ACCESS:
                return $this->canAccess($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param string $subject
     * @param User   $user
     *
     * @return bool
     */
    private function canAccess(string $subject, User $user): bool
    {
        $website = new Website();
        $website->setId(Website::BLACKMAILED);

        return $this->contentPermissionService->isAllowed(
            $user,
            $website,
            StanContentPermission::PERMISSION_WEBSITE_ACCESS
        );
    }

    /**
     * @return array
     */
    private function getAttributes(): array
    {
        return [
            self::ATTRIBUTE_ACCESS,
        ];
    }
}
