<?php

namespace AppBundle\Security;

use AppBundle\Entity\{
    Scene, User
};
use AppBundle\Service\Content\ContentPermissionService;
use EaPaysites\Entity\ContentPermission as StanContentPermission;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SceneVoter extends Voter
{
    const PERMISSION_VIEW_FULL = 'permission_view_full';

    const PERMISSION_VIEW_PROMO = 'permission_view_promo';

    /**
     * @var ContentPermissionService
     */
    private $contentPermissionService;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @param ContentPermissionService $permissionService
     * @param AuthorizationChecker     $authorizationChecker
     */
    public function __construct(ContentPermissionService $permissionService, AuthorizationChecker $authorizationChecker)
    {
        $this->contentPermissionService = $permissionService;
        $this->authorizationChecker     = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, $this->getAttributes())) {
            return false;
        }

        if (!$subject instanceof Scene) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var Scene $subject */
        $user = $token->getUser();

        switch ($attribute) {
            case self::PERMISSION_VIEW_PROMO:
                return $this->canViewPromo($subject, $user);
            case self::PERMISSION_VIEW_FULL:
                return $this->canViewFull($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param Scene $scene
     * @param       $user
     *
     * @return bool
     */
    private function canViewPromo(Scene $scene, $user): bool
    {
        return true;
    }

    /**
     * @param Scene $scene
     * @param       $user
     *
     * @return bool
     */
    private function canViewFull(Scene $scene, $user): bool
    {
        if (!$user instanceof User) {
            // Deny access if the user is not logged in.
            return false;
        }

        return $this->hasSceneAccess($scene, $user)
            || $this->authorizationChecker->isGranted(WebsiteVoter::ATTRIBUTE_ACCESS, WebsiteVoter::SUBJECT);
    }

    /**
     * @param Scene $scene
     * @param User  $user
     *
     * @return bool
     */
    private function hasSceneAccess(Scene $scene, User $user): bool
    {
        return $this->contentPermissionService->isAllowed(
            $user,
            $scene,
            StanContentPermission::PERMISSION_SCENE_VIEW_FULL
        );
    }

    /**
     * @return array
     */
    private function getAttributes(): array
    {
        return [
            self::PERMISSION_VIEW_FULL,
            self::PERMISSION_VIEW_PROMO,
        ];
    }
}
