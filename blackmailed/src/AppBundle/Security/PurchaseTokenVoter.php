<?php

namespace AppBundle\Security;

use AppBundle\Entity\{PurchaseToken, User};
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PurchaseTokenVoter extends Voter
{
    const USE = 'use';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, $this->getAttributes())) {
            return false;
        }

        if (!$subject instanceof PurchaseToken && $subject != PurchaseToken::staticGetOwnClassName()) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // Deny access if the user is not logged in.
            return false;
        }

        switch ($attribute) {
            case self::USE:
                return $this->canUse($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param      $subject
     * @param User $user
     *
     * @return bool
     */
    private function canUse($subject, User $user): bool
    {
        if (is_string($subject)) {
            return true;
        }

        /** @var PurchaseToken $subject */
        return $subject->getUser()->isEqual($user);
    }

    /**
     * @return array
     */
    private function getAttributes(): array
    {
        return [
            self::USE,
        ];
    }
}
