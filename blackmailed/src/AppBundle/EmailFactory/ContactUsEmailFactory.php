<?php

namespace AppBundle\EmailFactory;

use AppBundle\Entity\Website;
use EaPaysites\Service\Email\Email;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ContactUsEmailFactory
{
    const SUPPORT_TO_NAME = 'Blackmailed support';

    /**
     * @var string
     */
    private $supportEmail;

    /**
     * @var Request
     */
    private $request;

    /**
     * ContactUsEmailFactory constructor.
     *
     * @param string       $supportEmail
     * @param RequestStack $requestStack
     */
    public function __construct(string $supportEmail, RequestStack $requestStack)
    {
        $this->supportEmail = $supportEmail;
        $this->request      = $requestStack->getCurrentRequest();
    }

    /**
     * @param string $fromEmail
     * @param string $subject
     * @param string $description
     *
     * @return Email
     */
    public function factory(string $fromEmail, string $subject, string $description): Email
    {
        $email = new Email();

        $email->fromEmail = Website::EMAIL_FROM;
        $email->fromName  = Website::NAME;
        $email->toEmail   = $this->supportEmail;
        $email->toName    = self::SUPPORT_TO_NAME;
        $email->subject   = $this->getSubject();
        $email->content   = $this->getContent($fromEmail, $subject, $description);

        return $email;
    }

    /**
     * @return string
     */
    private function getSubject(): string
    {
        return sprintf("New support request at %s", date('m/d/Y H:i:s'));
    }

    /**
     * @param string $email
     * @param string $subject
     * @param string $description
     *
     * @return string
     */
    private function getContent(string $email, string $subject, string $description): string
    {
        $message = implode(
            "\n\n",
            [
                "New support request submitted at blackmailed.com.",
                "Date: %s",
                "From: %s",
                "IP: %s",
                "Subject: %s",
                "Description: %s"
            ]
        );

        return sprintf($message, date('m/d/Y H:i:s'), $email, $this->request->getClientIp(), $subject, $description);
    }
}
