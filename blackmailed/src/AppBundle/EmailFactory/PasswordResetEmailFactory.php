<?php

namespace AppBundle\EmailFactory;

use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use EaPaysites\Service\Email\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PasswordResetEmailFactory
 * @package AppBundle\EmailFactory
 */
class PasswordResetEmailFactory
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * PasswordResetEmailFactory constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param User   $user
     * @param string $token
     *
     * @return Email
     */
    public function factory(User $user, string $token): Email
    {
        $email = new Email();

        $email->fromEmail = Website::EMAIL_FROM;
        $email->fromName  = Website::NAME;
        $email->toEmail   = $user->getEmail();
        $email->toName    = $user->getName();
        $email->subject   = $this->getSubject();
        $email->content   = $this->getContent($token);
        $email->mimeType = 'text/html';

        return $email;
    }

    /**
     * @return string
     */
    private function getSubject(): string
    {
        return "Reset your password";
    }

    /**
     * @param string $token
     *
     * @return string
     */
    private function getContent(string $token): string
    {
        return sprintf(
            "<p>Hey there!</p>
<p>You recently requested to reset your password for Blackmailed.com. Please click the link below to reset it.</p>
<p>%s</p>

<p>If you're having trouble clicking the password reset link, copy and paste the URL into your web browser.<br>
If you did not request a password reset, please ignore this email.</p>

<p>Thanks,<br>
Blackmailed.com</p>",
            $this->buildUrl($token)
        );
    }

    /**
     * @param $token
     *
     * @return string
     */
    private function buildUrl($token): string
    {
        return $this->router->generate('userResetPassword', ['t' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
