<?php

namespace AppBundle\EmailFactory;

use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use EaPaysites\Service\Email\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SignupEmailFactory
 * @package AppBundle\EmailFactory
 */
class SignupEmailFactory
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * PasswordResetEmailFactory constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param User   $user
     * @param string $token
     *
     * @return Email
     */
    public function factory(User $user, string $token): Email
    {
        $email = new Email();

        $email->fromEmail    = Website::EMAIL_FROM;
        $email->fromName     = Website::NAME;
        $email->toEmail      = $user->getEmail();
        $email->toName       = $user->getName();
        $email->subject      = $this->getSubject();
        $email->mimeType     = 'text/html';
        $email->replacements = [
            '-activate-url-' => $this->buildUrl($token)
        ];
        $email->templateId   = '80206559-f5fa-46fc-bc51-ba83e1f5b476';

        return $email;
    }

    /**
     * @return string
     */
    private function getSubject(): string
    {
        return 'Welcome to Blackmailed';
    }

    /**
     * @param $token
     *
     * @return string
     */
    private function buildUrl($token): string
    {
        return $this->router->generate('userConfirmEmail', ['t' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
