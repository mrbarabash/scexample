<?php

namespace AppBundle\Form;

use AppBundle\Validator\Constraints\Adult;
use Beelab\Recaptcha2Bundle\Form\Type\RecaptchaType;
use Beelab\Recaptcha2Bundle\Validator\Constraints\Recaptcha2;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserSignupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder
                ->add('email', EmailType::class)
                ->add('name', TextType::class)
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'first_options'  => array('label' => 'Password'),
                    'second_options' => array('label' => 'Repeat Password'),
                    'constraints' => [
                        new NotBlank()
                    ]
                ])
                ->add('birthday', BirthdayType::class, [
                    'mapped' => false,
                    'widget' => 'text',
                    'constraints' => [
                        new Adult(),
                    ]
                ])
                ->add('product', ChoiceType::class, [
                    'choices' => $options['products'],
                    'choice_label' => 'title',
                    'choice_value' => 'id',
                    'expanded' => true,
                ])
                ->add('captcha', RecaptchaType::class, [
                    'constraints' => new Recaptcha2(),
                ])
                ->add('save', SubmitType::class, ['label' => 'Get access']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['products' => []]);
    }
}
