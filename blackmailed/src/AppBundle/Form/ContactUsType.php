<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactUsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'required'    => true,
                    'constraints' => [new NotBlank(), new Email(['checkMX' => true, 'checkHost' => true])]
                ]
            )
            ->add(
                'subject',
                TextType::class,
                ['required' => true, 'constraints' => [new NotBlank(), new Length(['min' => 3, 'max' => 255])]]
            )
            ->add(
                'description',
                TextareaType::class,
                ['required' => true, 'constraints' => [new NotBlank(), new Length(['min' => 0, 'max' => 10000])]]
            )
            ->add('save', SubmitType::class, ['label' => 'Submit']);
    }
}
