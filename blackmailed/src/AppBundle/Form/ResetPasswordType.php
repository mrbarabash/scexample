<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ResetPasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'password',
                RepeatedType::class,
                [
                    'type'           => PasswordType::class,
                    'first_name'     => 'password',
                    'second_name'    => 'confirm',
                    'first_options'  => ['label' => 'Password'],
                    'second_options' => ['label' => 'Repeat password'],
                    'constraints'    => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('save', SubmitType::class, ['label' => 'Change password']);
    }
}
