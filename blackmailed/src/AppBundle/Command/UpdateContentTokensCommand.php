<?php

namespace AppBundle\Command;

use AppBundle\Service\ContentCdn\ContentTokenService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateContentTokensCommand extends Command
{
    /**
     * @var ContentTokenService
     */
    private $contentTokenService;

    /**
     * UpdateCdnTokensCommand constructor.
     *
     * @param ContentTokenService $contentTokenService
     */
    public function __construct(ContentTokenService $contentTokenService = null)
    {
        $this->contentTokenService = $contentTokenService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:update-content-tokens')
             ->setDescription('Set fresh CDN tokens to scenes table')
             ->setHelp('Set fresh CDN tokens to scenes table');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('CDN Tokens Updater');

        $output->writeln(sprintf('[%s] Starting update...', date('c')));
        $this->contentTokenService->updateContentTokens();

        $output->writeln(sprintf('[%s] Successfully finished.', date('c')));
    }
}
