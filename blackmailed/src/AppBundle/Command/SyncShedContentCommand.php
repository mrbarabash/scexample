<?php

namespace AppBundle\Command;

use AppBundle\Service\ContentImporter\ShedImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncShedContentCommand extends Command
{
    /**
     * @var ShedImporter
     */
    private $shedImporter;

    /**
     * SyncShedContentCommand constructor.
     *
     * @param ShedImporter $shedImporter
     */
    public function __construct(ShedImporter $shedImporter)
    {
        $this->shedImporter = $shedImporter;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:sync-shed-content')
             ->setDescription('Download paysite content from SHED')
             ->setHelp('Download paysite content from SHED');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('SHED Content Importer');

        $output->writeln(sprintf('[%s] Starting import...', date('c')));
        $response = $this->shedImporter->import();

        if ($response->isError()) {
            $output->writeln(sprintf('[%s] Failed to import: "%s"', date('c'), $response->error->getMessage()));
        } else {
            $output->writeln(sprintf('[%s] Successfully finished.', date('c')));
        }
    }
}
