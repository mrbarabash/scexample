<?php

namespace AppBundle\Command;

use AppBundle\Service\ContentRating\AggregateRatingImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAggregateContentRatingsCommand extends Command
{
    /**
     * @var AggregateRatingImporter
     */
    private $aggregateRatingImporter;

    /**
     * UpdateAggregateContentRatings constructor.
     *
     * @param AggregateRatingImporter $aggregateRatingImporter
     */
    public function __construct(AggregateRatingImporter $aggregateRatingImporter)
    {
        $this->aggregateRatingImporter = $aggregateRatingImporter;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:update-aggregate-content-ratings')
             ->setDescription('Set current aggregate rating values to content')
             ->setHelp('Set current aggregate rating values to content');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Aggregate Content Rating Updater');

        $output->writeln(sprintf('[%s] Starting update...', date('c')));

        $this->aggregateRatingImporter->import();

        $output->writeln(sprintf('[%s] Successfully finished.', date('c')));
    }
}
