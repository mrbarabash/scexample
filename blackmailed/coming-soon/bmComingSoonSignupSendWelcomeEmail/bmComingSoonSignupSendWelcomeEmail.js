'use strict';

let validator = require('validator');
let AWS = require('aws-sdk');
let sendGridFactory = require('sendgrid');

let decryptedSendgridApiToken;

const bmListId = 1497148;
const webListId = 1560544;

function processEvent(event, context, callback) {
    let body;

    if (!event.body) {
        return callback(null, createResponse(400, 'Request body must not be empty'));
    }

    try {
        body = JSON.parse(event.body);
    } catch (e) {
        return callback(null, createResponse(400, 'Request format must be a valid JSON'));
    }

    let toEmail = body.email;

    if (!toEmail) {
        return callback(null, createResponse(400, 'Field "email" is required'));
    }

    if (!validator.isEmail(toEmail)) {
        return callback(null, createResponse(400, 'Field "email" is malformed'));
    }

    let sendGrid = sendGridFactory(decryptedSendgridApiToken);

    saveContact(sendGrid, toEmail)
        .then(function (response) {
            let contactId = response.body.persisted_recipients[0];

            return isRecipientInList(sendGrid, contactId)
                .then(function (recipientInList) {
                    if (!recipientInList) {
                        return addContactToList(sendGrid, contactId)
                            .then(function () {
                                return sendWelcomeEmail(sendGrid, toEmail);
                            });
                    }
                });
        })
        .then(function () {
            callback(null, createResponse(201, 'OK'));
        })
        .catch(function (error) {
            callback(error);
        });
}

function addContactToList(sendGrid, contactId) {
    return Promise.all([
        getBmListPromise(),
        getWebListPromise()
    ]);

    function getBmListPromise() {
        let request = sendGrid.emptyRequest({
            method: 'POST',
            path:   `/v3/contactdb/lists/${bmListId}/recipients/${contactId}`
        });

        return sendGrid.API(request)
            .then(function (response) {
                console.log('SG: added contact to BM list');
                // console.log('SG: add contact to BM list response', response);
            })
            .catch(function (error) {
                console.log('SG: failed to add contact to BM list. Error: ', error);

                throw error;
            });
    }

    function getWebListPromise() {
        let request = sendGrid.emptyRequest({
            method: 'POST',
            path:   `/v3/contactdb/lists/${webListId}/recipients/${contactId}`
        });

        return sendGrid.API(request)
            .then(function (response) {
                console.log('SG: added contact to Web list');
                // console.log('SG: add contact to Web list response', response);
            })
            .catch(function (error) {
                console.log('SG: failed to add contact to Web list. Error: ', error);

                throw error;
            });
    }
}

function isRecipientInList(sendGrid, contactId) {
    let request = sendGrid.emptyRequest({
        method: 'GET',
        path:   `/v3/contactdb/recipients/${contactId}/lists`
    });

    return sendGrid.API(request)
        .then(function (response) {
            console.log('SG: got lists recipient is on');
            // console.log('SG: got lists recipient is on response', response);

            return response &&
                response.body &&
                response.body.lists &&
                response.body.lists.find(function (list) {
                    return list.id === bmListId;
                });
        })
        .catch(function (error) {
            console.log('SG: failed to get lists recipient is on. Error: ', error);

            throw error;
        });
}

function sendWelcomeEmail(sendGrid, email) {
    let request = sendGrid.emptyRequest({
        method: 'POST',
        path:   '/v3/mail/send',
        body:   {
            personalizations: [
                {
                    to:            [
                        {
                            email: email
                        }
                    ],
                    substitutions: {
                        "-unsubscribe-": "<%asm_group_unsubscribe_raw_url%>"
                    }
                }
            ],
            asm:              {
                group_id: 3011
            },
            from:             {
                email: 'noreply@blackmailed.com',
                name:  'Blackmailed'
            },
            template_id:      '28f24929-3075-4aaa-98b4-5390d10aabe4',
            "mail_settings":  {
                "sandbox_mode": {
                    "enable": false
                }
            }
        }
    });

    return sendGrid.API(request)
        .then(function (response) {
            console.log('SG: sent welcome email');
            // console.log('SG: send email response', response);
        })
        .catch(function (error) {
            console.log('SG: failed to send welcome email. Error: ', error);

            throw error;
        });
}

function saveContact(sendGrid, email) {
    let request = sendGrid.emptyRequest();
    request.body = [
        {
            "email":          email,
            "source":         "blackmailed",
            "source_channel": "coming_soon"
        }
    ];
    request.method = 'POST';
    request.path = '/v3/contactdb/recipients';

    return sendGrid.API(request)
        .then(function (response) {
            console.log('SG: saved contact');
            // console.log('SG: save contact response', response);

            return response;
        })
        .catch(function (error) {
            console.log('SG: failed to save contact. Error: ', error);

            throw error;
        });
}

function createResponse(code, message) {
    return {
        statusCode: code,
        body:       JSON.stringify({
            "message": message
        }),
        headers:    {
            "Content-Type":                "application/json",
            "Access-Control-Allow-Origin": "*"
        }
    };
}

exports.handler = function (event, context, callback) {
    // decryptedSendgridApiToken = 'secret';
    // return processEvent(event, context, callback);

    let encryptedSendgridApiToken = process.env.SENDGRID_API_KEY;
    if (!encryptedSendgridApiToken) {
        return callback(new Error('Provide SENDGRID_API_KEY'));
    }

    if (decryptedSendgridApiToken) {
        processEvent(event, context, callback);
    } else {
        // Decrypt code should run once and variables stored outside of the function
        // handler so that these are decrypted once per container
        const kms = new AWS.KMS();
        kms.decrypt({CiphertextBlob: new Buffer(encryptedSendgridApiToken, 'base64')}, (err, data) => {
            if (err) {
                console.log('Decrypt error:', err);
                return callback(err);
            }
            decryptedSendgridApiToken = data.Plaintext.toString('ascii');
            processEvent(event, context, callback);
        });
    }
};
