var player_container = document.getElementById("player");
flowplayer(player_container, {
  brand: 'Evil Angel',
  key: '$789717647597974',
  clip: {
    sources: [
      {
        type: "video/webm",
          src:  "https://videocdn.evilangel.com/blackmailed/blackmailed-promo-8000kbps.webm"
      },
      {
        type: "video/mp4",
        src:  "https://videocdn.evilangel.com/blackmailed/blackmailed-promo-8000kbps.mp4"
      }
    ]
  }
});

function activatePlayer() {
  document.getElementById('player_cover').style.display = 'none';
  flowplayer().play();
}

function showErrorResponse(text) {
  document.getElementById('sign_for_updates').style.display = 'none';
  document.getElementById('submit_text').innerText = "Try again";
  document.getElementById('form_submit_holder').innerText = text;
  document.getElementById('form_submit_holder').style.display = 'block';
  // setTimeout(function() {
    document.getElementById('submit_text').style.visibility = 'visible';
    document.getElementById('submit_loader').style.display = 'none';
  // }, 2000);
}
function showSuccessResponse() {
  // setTimeout(function() {
    document.getElementById("submit_form").remove();
    document.getElementById("form_success").style.display = "block";
  // }, 2000);
}

var requestInFlight = false;
function submitUpdatesForm(e) {
  if (requestInFlight) {
    return false;
  }
  requestInFlight = true;
  var email = document.getElementById('email').value;
  if(!isValidEmail(email)) {
    showErrorResponse("Incorrect email format");
    requestInFlight = false;
  } else {
    document.getElementById('submit_text').style.visibility = 'hidden';
    document.getElementById('submit_loader').style.display = 'block';
    axios.post('https://hpefwscfbc.execute-api.us-west-2.amazonaws.com/test/coming-soon/signup', {
      'email': email
    }).then(function(response) {
      if(response.status === 201) {
        showSuccessResponse();
      } else if(response.status === 400) {
        showErrorResponse('Wrong email');
      } else {
        showErrorResponse('We encountered a problem. Try again.');
      }
      requestInFlight = false;
      console.log(response);
    })["catch"](function(error) {
      showErrorResponse('We encountered a problem. Try again.');
      requestInFlight = false;
    });
  }
  return false;
}

function isValidEmail(string) {
    var atSym = string.lastIndexOf("@");
    if (atSym < 1) { return false; } // no local-part
    if (atSym == string.length - 1) { return false; } // no domain
    if (atSym > 64) { return false; } // there may only be 64 octets in the local-part
    if (string.length - atSym > 255) { return false; } // there may only be 255 octets in the domain

    // Is the domain plausible?
    var lastDot = string.lastIndexOf(".");
    // Check if it is a dot-atom such as example.com
    if (lastDot > atSym + 1 && lastDot < string.length - 1) { return true; }
    // Check if could be a domain-literal.
    if (string.charAt(atSym + 1) == '[' && string.charAt(string.length - 1) == ']') { return true; }
    return false;
}