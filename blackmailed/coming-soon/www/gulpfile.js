var gulp = require('gulp');
var gulpif = require('gulp-if');
var less = require('gulp-less');
var sass = require('gulp-sass');
var uglifycss = require('gulp-uglifycss');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function () {
    return gulp.src([
        'sass/**/*.scss'])
        .pipe(gulpif(/[.]scss/, sass()))
        .pipe(uglifycss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('css/'));
});

gulp.task("watch_sass", function() {
  return gulp.watch("sass/**/*.scss", function() {
    gulp.src([
        'sass/**/*.scss'])
        .pipe(gulpif(/[.]scss/, sass()))
        .pipe(uglifycss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('css/'));
  })
});

gulp.task('default', ['sass']);
gulp.task('watch', ['watch_sass']);