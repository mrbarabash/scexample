# BM Coming Soon

## Lambda callbacks

### Prepare package
```
zip -r bmComingSoonSignupSendWelcomeEmail bmComingSoonSignupSendWelcomeEmail.js node_modules/
```

### Create function
```
aws lambda create-function \
--region us-west-2 \
--function-name bmComingSoonSignupSendWelcomeEmail \
--zip-file fileb://bmComingSoonSignupSendWelcomeEmail.zip \
--environment Variables={SENDGRID_API_KEY=REPLACE_WITH_ENCRYPTED_IN_LAMBDA_CONSOLE} \
--role arn:aws:iam::548355647590:role/AWSLambdaBasicExecutionRole \
--handler bmComingSoonSignupSendWelcomeEmail.handler \
--runtime nodejs6.10 \
--description "When user signs up, store email and send welcome message" \
--kms-key-arn "arn:aws:kms:us-west-2:548355647590:key/9e434baf-1a50-4cb3-99d4-580060c0fa00" \
--memory-size 128 \
--timeout 15 \
--tags project="blackmailed" \
--profile ea_maxivanov
```

### Create "dev" alias
```
aws lambda create-alias \
--region us-west-2 \
--function-name bmComingSoonSignupSendWelcomeEmail \
--description "dev environment alias" \
--function-version "\$LATEST" \
--name dev \
--profile ea_maxivanov
```

### Publish version for "prod" (it will create version 1 initially)
```
aws lambda publish-version \
--region us-west-2 \
--function-name bmComingSoonSignupSendWelcomeEmail \
--profile ea_maxivanov
```

### Create "prod" alias
```
aws lambda create-alias \
--region us-west-2 \
--function-name bmComingSoonSignupSendWelcomeEmail \
--description "prod environment alias" \
--function-version 1 \
--name prod \
--profile ea_maxivanov
```

### Update function
```
aws lambda update-function-code \
--region us-west-2 \
--function-name bmComingSoonSignupSendWelcomeEmail \
--zip-file fileb://bmComingSoonSignupSendWelcomeEmail.zip \
--profile ea_maxivanov
```

### Update alias (after new version published)
```
aws lambda update-alias \
--region us-west-2 \
--function-name bmComingSoonSignupSendWelcomeEmail \
--function-version 2 \
--name prod \
--profile ea_maxivanov
```

### Invoke function
```
aws lambda invoke \
--invocation-type RequestResponse \
--function-name bmComingSoonSignupSendWelcomeEmail \
--region us-west-2 \
--profile ea_maxivanov \
/dev/stdout
```

### Update function env vars
```
aws lambda update-function-configuration \
--function-name bmComingSoonSignupSendWelcomeEmail \
--region us-west-2 \
--environment Variables={SENDGRID_API_KEY=test2} \
--profile ea_maxivanov
```

### Add tags to function
```
aws lambda tag-resource \
--resource arn:aws:lambda:us-west-2:548355647590:function:bmComingSoonSignupSendWelcomeEmail \
--region us-west-2 \
--tags project="blackmailed" \
--profile ea_maxivanov
```

### List tags
```
aws lambda list-tags \
--resource arn:aws:lambda:us-west-2:548355647590:function:bmComingSoonSignupSendWelcomeEmail \
--region us-west-2 \
--profile ea_maxivanov
```

### Delete function
```
aws lambda delete-function \
--function-name bmComingSoonSignupSendWelcomeEmail \
--region us-west-2 \
--profile ea_maxivanov
```

## API Gateway

### Create API
```
aws apigateway create-rest-api \
--name 'BlackMailedApi' \
--description "Blackmailed API suite" \
--region us-west-2 \
--profile ea_maxivanov
```

### Get root resource ID
```
aws apigateway get-resources \
--rest-api-id hpefwscfbc \
--region us-west-2 \
--profile ea_maxivanov
```

### Create resource
```
aws apigateway create-resource \
--rest-api-id hpefwscfbc \
--parent-id 9ibf34zjv5 \
--path-part "coming-soon" \
--region us-west-2 \
--profile ea_maxivanov
```

### Create sub-resource
```
aws apigateway create-resource \
--rest-api-id hpefwscfbc \
--parent-id 08g2ms \
--path-part "signup" \
--region us-west-2 \
--profile ea_maxivanov
```

### Create method on sub-resource
```
aws apigateway put-method \
--rest-api-id hpefwscfbc \
--resource-id ov20u3 \
--http-method POST \
--authorization-type NONE \
--no-api-key-required \
--region us-west-2 \
--profile ea_maxivanov
```

### Create integration request on sub-resource
```
aws apigateway put-integration \
--rest-api-id hpefwscfbc \
--resource-id ov20u3 \
--http-method POST \
--integration-http-method POST \
--type AWS_PROXY \
--uri "arn:aws:apigateway:us-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:us-west-2:548355647590:function:bmComingSoonSignupSendWelcomeEmail/invocations" \
--region us-west-2 \
--profile ea_maxivanov
```

### Allow API Gateway to execute function
--function-name arn:aws:lambda:[region]:[account-id]:function:[name] 
--source-arn "arn:aws:execute-api:$region:$account_id:$api_rest_id
```
aws lambda add-permission \
--function-name "arn:aws:lambda:us-west-2:548355647590:function:bmComingSoonSignupSendWelcomeEmail" \
--source-arn "arn:aws:execute-api:us-west-2:548355647590:hpefwscfbc/*/*/*" \
--principal apigateway.amazonaws.com \
--statement-id 1 \
--action lambda:InvokeFunction \
--region us-west-2 \
--profile ea_maxivanov
```

### Enable CORS
Via Console enable endpoint CORS.

### Create deployment
```
aws apigateway create-deployment \
--rest-api-id hpefwscfbc \
--stage-name test \
--stage-description "Test stage" \
--description "First deployment" \
--region us-west-2 \
--profile ea_maxivanov
```

### Set throttle
```
aws apigateway update-stage \
--rest-api-id hpefwscfbc \
--stage-name test \
--patch-operations "op=replace,path=/*/*/throttling/burstLimit,value=4" "op=replace,path=/*/*/throttling/rateLimit,value=2" \
--region us-west-2 \
--profile ea_maxivanov
```

## Deploy

### Upload code
```
aws s3 cp source/ s3://www.blackmailed.com/ --recursive --include "*" --profile ea_maxivanov
```

### Reset cloudfront cache
```
aws cloudfront create-invalidation --distribution-id ECA7KANCE0NLR --paths "/*" --profile ea_maxivanov
```
