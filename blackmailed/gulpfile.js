(function () {
    'use strict';

    let gulp = require('gulp');
    let del = require('del');
    let gulpSourceMaps = require('gulp-sourcemaps');
    let gulpIf = require('gulp-if');
    let gulpSass = require('gulp-sass');
    let gulpConcat = require('gulp-concat');
    let gulpAutoprefixer = require('gulp-autoprefixer');
    let gulpCleanCss = require('gulp-clean-css');
    let argv = require('yargs').argv;
    let merge = require('merge-stream');
    let uglify = require('gulp-uglify');

    let isProductionBuild = !!argv.production;

    gulp.task('css:clean-dist', function () {
        return del(['web/dist/css/*']);
    });

    gulp.task('css:build', ['css:clean-dist'], function () {
        let sources = [
            {
                pattern:             [
                    'app/Resources/scss/main.scss'
                ],
                destinationFileName: 'app.css',
                destinationDir:      'web/dist/css'
            },
            {
                pattern:             [
                    'app/Resources/scss/flowplayer-default.css',
                    'app/Resources/scss/flowplayer.scss'
                ],
                destinationFileName: 'player.css',
                destinationDir:      'web/dist/css'
            },
            {
                pattern:             [
                    'app/Resources/scss/iframe.scss'
                ],
                destinationFileName: 'iframe.css',
                destinationDir:      'web/dist/css'
            }
        ];

        let tasks = sources.map(function (source) {
            return gulp.src(source.pattern)
            // disabled due to https://github.com/sass/libsass/issues/2312
            // .pipe(gulpSourceMaps.init())
                .pipe(gulpIf('*.scss', gulpSass({
                    outputStyle: 'nested', // libsass doesn't support expanded yet
                    precision:   10
                })).on('error', gulpSass.logError))
                .pipe(gulpConcat(source.destinationFileName))
                .pipe(gulpAutoprefixer({browsers: ['> 1%', 'last 2 versions']}))
                .pipe(gulpIf(isProductionBuild, gulpCleanCss()))
                // .pipe(gulpSourceMaps.write('.'))
                .pipe(gulp.dest(source.destinationDir));
        });

        return merge(tasks);
    });

    gulp.task('js:clean-dist', function () {
        return del(['web/dist/js/*']);
    });

    gulp.task('js:build', ['js:clean-dist'], function () {
        let destinationDir = 'web/dist/js';
        let sources = [
            {
                pattern:             [
                    'node_modules/js-cookie/src/js.cookie.js'
                ],
                destinationFileName: 'lib.js',
                mangle:              true
            },
            {
                pattern:             [
                    'app/Resources/js/common/*.js',
                ],
                destinationFileName: 'app.js',
                mangle:              true
            },
            {
                pattern:             [
                    'app/Resources/js/player/eaPlayer.js'
                ],
                destinationFileName: 'player.js',
                mangle:              true
            }
        ];

        let tasks = sources.map(function (source) {
            return gulp.src(source.pattern)
                .pipe(gulpSourceMaps.init())
                .pipe(gulpConcat(source.destinationFileName))
                .pipe(gulpIf(isProductionBuild, uglify({
                    compress: {
                        'drop_debugger': true
                    },
                    mangle:   source.mangle
                })))
                .pipe(gulpSourceMaps.write('.'))
                .pipe(gulp.dest(destinationDir));
        });

        return merge(tasks);
    });

    gulp.task('build', ['css:build', 'js:build']);

    gulp.task('watch', ['build'], function () {
        gulp.watch(
            [
                'app/Resources/scss/*.scss'
            ],
            ['css:build']
        );

        gulp.watch(
            [
                'app/Resources/js/**/*.js'
            ],
            ['js:build']
        );
    });
}());
