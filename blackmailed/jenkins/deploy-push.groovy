node {
  try {
    notifyBuild('STARTED')

    stage ('Checkout BM application code') {
        git branch: params.selectedBranch,
        credentialsId: '747ab74f-f046-47da-b5c4-1d1284296891',
        url: ' git@github.com-bm:adamgrayson31/blackmailed.git'
        sh 'echo "JOB: $JOB_NAME" > version.txt'
        sh 'echo "BUILD NUMBER: $BUILD_NUMBER" >> version.txt'
        sh 'echo "GIT REVISION:" >> version.txt'
        sh 'git log -n 1 >> version.txt'
        sh 'cp -p version.txt $WORKSPACE/app/version.txt'
    }


    stage ('Checkout paysites-shared application code') {
        dir ('paysites-shared') {
            git branch: 'master',
                credentialsId: '3a5206b4-e498-4402-ba87-6706511f8972',
                url: ' git@github.com-shared:adamgrayson31/paysites-shared.git'
        }
    }

    stage ('Copy parameters.yml') {
        sh 'cp -p /tmp/parameters-bm-stg.yml ./app/config/parameters.yml'
    }

    stage ('Build base images') {
        if (params.rebuildBaseImages) {
            def phpBaseImage = docker.build("blackmailed_bm-php-base:latest", "-f docker/php/Dockerfile ./docker/php")
            def phpBuildImage = docker.build("php-bm-build", "-f docker/php/dev/Dockerfile ./docker/php/dev")
            def phpImageLayer1 = docker.build("bm-php-layer1", "-f docker/php/prod/Dockerfile-logs ./docker/php/prod")
            def nginxImageLayer1 = docker.build("nginx-bm-layer1", "-f docker/nginx/Dockerfile ./docker/nginx ")
        } else {
            echo 'skipping base images rebuild'
        }
    }

    stage ('Composer install') {
        sh 'sudo rm -rf $WORKSPACE/vendor'
        sh 'docker run --net=host --rm -v $WORKSPACE:/var/www/html -v $WORKSPACE/paysites-shared:/var/paysites-shared php-bm-build /bin/bash -c "SYMFONY_ENV=prod composer clear-cache"'
        sh 'docker run --net=host --rm -v $WORKSPACE:/var/www/html -v $WORKSPACE/paysites-shared:/var/paysites-shared php-bm-build /bin/bash -c "SYMFONY_ENV=prod composer install --no-dev --optimize-autoloader"'
        sh 'sudo rm ./vendor/evilangel/paysites-shared'
        sh 'sudo ln -s ../../../paysites-shared ./vendor/evilangel/paysites-shared'
    }

    stage('npm install') {
        sh "npm install"
    }

    stage('gulp build') {
        sh "gulp build"
    }

    stage('calculate md5 hash') {

        def viewsDir = new File("$WORKSPACE/app/Resources/views")
        def fileText
        def ext = ".html.twig"

        sh "find $WORKSPACE/app/Resources/views -name '*.twig' -print > $WORKSPACE/LIST"

        def CSS_APP = 'dist/css/app.css'
        CSS_APP  = CSS_APP.replace("/", "\\/")
        def CSS_PLAYER = 'dist/css/player.css'
        CSS_PLAYER = CSS_PLAYER.replace("/", "\\/")
        def JS_APP = 'dist/js/app.js'
        JS_APP = JS_APP.replace("/", "\\/")
        def JS_PLAYER = 'dist/js/player.js'
        JS_PLAYER = JS_PLAYER.replace("/", "\\/")
        def JS_LIB = 'dist/js/lib.js'
        JS_LIB = JS_LIB.replace("/", "\\/")

        def CSS_APP_MD5 = 'dist/css/app.css?' + sh (
            script: "md5sum $WORKSPACE/web/dist/css/app.css|awk '{print \$1}'",
            returnStdout: true
        ).trim()

        CSS_APP_MD5 = CSS_APP_MD5.replace("/", "\\/")

        sh """CSS_APP=${CSS_APP}"""
        sh """CSS_APP_MD5=${CSS_APP_MD5}"""
        sh """cat $WORKSPACE/LIST|while read file; do sed -i "s/${CSS_APP}/${CSS_APP_MD5}/g" \$file; done;"""

        def CSS_PLAYER_MD5 = 'dist/css/player.css?' +sh (
            script: "md5sum $WORKSPACE/web/dist/css/player.css|awk '{print \$1}'",
            returnStdout: true
        ).trim()

        CSS_PLAYER_MD5 = CSS_PLAYER_MD5.replace("/", "\\/")

        sh """CSS_PLAYER=${CSS_PLAYER}"""
        sh """CSS_PLAYER_MD5=${CSS_PLAYER_MD5}"""
        sh """cat $WORKSPACE/LIST|while read file; do sed -i "s/${CSS_PLAYER}/${CSS_PLAYER_MD5}/g" \$file; done;"""

        def JS_APP_MD5 = 'dist/js/app.js?' + sh (
            script: "md5sum $WORKSPACE/web/dist/js/app.js|awk '{print \$1}'",
            returnStdout: true
        ).trim()

        JS_APP_MD5 = JS_APP_MD5.replace("/", "\\/")

        sh """JS_APP=${JS_APP}"""
        sh """JS_APP_MD5=${JS_APP_MD5}"""
        sh """cat $WORKSPACE/LIST|while read file; do sed -i "s/${JS_APP}/${JS_APP_MD5}/g" \$file; done;"""

        def JS_PLAYER_MD5 = 'dist/js/player.js?' + sh (
            script: "md5sum $WORKSPACE/web/dist/js/player.js|awk '{print \$1}'",
            returnStdout: true
        ).trim()

        JS_PLAYER_MD5 = JS_PLAYER_MD5.replace("/", "\\/")

        sh """JS_PLAYER=${JS_PLAYER}"""
        sh """JS_PLAYER_MD5=${JS_PLAYER_MD5}"""
        sh """cat $WORKSPACE/LIST|while read file; do sed -i "s/${JS_PLAYER}/${JS_PLAYER_MD5}/g" \$file; done;"""

        def JS_LIB_MD5 = 'dist/js/lib.js?' + sh (
            script: "md5sum $WORKSPACE/web/dist/js/lib.js|awk '{print \$1}'",
            returnStdout: true
        ).trim()

        JS_LIB_MD5 = JS_LIB_MD5.replace("/", "\\/")

        sh """JS_LIB=${JS_LIB}"""
        sh """JS_LIB_MD5=${JS_LIB_MD5}"""
        sh """cat $WORKSPACE/LIST|while read file; do sed -i "s/${JS_LIB}/${JS_LIB_MD5}/g" \$file; done;"""

    }


    stage ('Build nginx-bm container') {
        dir('docker/nginx/release') {
            sh 'rm -rf *'
            sh 'cp -rp $WORKSPACE/web .'
            sh 'cp -rp $WORKSPACE/app .'
            sh 'cp -rp $WORKSPACE/vendor .'
            sh 'cp -rp $WORKSPACE/bin .'
            sh 'cp -rp $WORKSPACE/var .'
            sh 'cp -rp $WORKSPACE/node_modules .'
            sh 'cp -rp $WORKSPACE/src .'
            sh 'cp -rp $WORKSPACE/paysites-shared .'
        }

        def nginxImage = docker.build("nginx-bm:${BUILD_NUMBER}", "--no-cache=true -f docker/nginx/Dockerfile-app ./docker/nginx ")

    }


    stage ('Build php-bm container') {
        dir('docker/php/prod/release') {
            sh 'rm -rf *'
            sh 'cp -rp $WORKSPACE/web .'
            sh 'cp -rp $WORKSPACE/app .'
            sh 'cp -rp $WORKSPACE/vendor .'
            sh 'cp -rp $WORKSPACE/bin .'
            sh 'cp -rp $WORKSPACE/var .'
            sh 'cp -rp $WORKSPACE/node_modules .'
            sh 'cp -rp $WORKSPACE/src .'
            sh 'cp -rp $WORKSPACE/paysites-shared .'
        }

        def phpImage = docker.build("php-bm:${BUILD_NUMBER}", "--no-cache=true -f docker/php/prod/Dockerfile ./docker/php/prod")

    }

    stage ('push nginx-bm to ECR') {
        docker.withRegistry("https://548355647590.dkr.ecr.us-west-2.amazonaws.com", "ecr:us-west-2:jenkins-aws") {
            docker.image('nginx-bm:${BUILD_NUMBER}').push('${BUILD_NUMBER}')
            docker.image('nginx-bm:${BUILD_NUMBER}').push('latest')
        }
    }

    stage ('push php-bm to ECR') {
        docker.withRegistry("https://548355647590.dkr.ecr.us-west-2.amazonaws.com", "ecr:us-west-2:jenkins-aws") {
            docker.image('php-bm:${BUILD_NUMBER}').push('${BUILD_NUMBER}')
            docker.image('php-bm:${BUILD_NUMBER}').push('latest')
        }
    }

    stage ('Cleanup') {
        sh 'sudo rm -rf $WORKSPACE/vendor'
        sh 'docker container prune -f || true'
        sh 'docker rmi -f nginx-bm:${BUILD_NUMBER} || true'
        sh 'docker rmi -f php-bm:${BUILD_NUMBER} || true'
        sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/php-bm:${BUILD_NUMBER} || true'
        sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/php-bm:latest || true'
        sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/nginx-bm:${BUILD_NUMBER} || true'
        sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/nginx-bm:latest || true'
    }

    stage('Deploy New Build To Kubernetes') {
        sh ("kubectl set image deployment/bm-nginx bm-nginx=548355647590.dkr.ecr.us-west-2.amazonaws.com/nginx-bm:${BUILD_NUMBER}")
        sh ("kubectl set image deployment/bm-php bm-php=548355647590.dkr.ecr.us-west-2.amazonaws.com/php-bm:${BUILD_NUMBER}")
    }

    stage ('Migration run') {
      sh "sleep 5"
      if (params.runMigration) {
        sh "kubectl get pods|grep bm-php|head -1|awk '{print \$1,\$3}' > /tmp/bm-php_pod-status"
        waitForBmPod()
        sh '''kubectl exec -it $(tail -1 /tmp/bm-php_pod-status|awk '{print $1}') -- /bin/bash -c "bin/console --no-interaction doctrine:migrations:migrate --env=prod"'''
      }
    }

  } catch (e) {
    // If there was an exception thrown, the build failed
    sh 'sudo rm -rf $WORKSPACE/vendor'
    sh 'docker container prune -f || true'
    sh 'docker rmi -f nginx-bm:${BUILD_NUMBER} || true'
    sh 'docker rmi -f php-bm:${BUILD_NUMBER} || true'
    sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/php-bm:${BUILD_NUMBER} || true'
    sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/php-bm:latest || true'
    sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/nginx-bm:${BUILD_NUMBER} || true'
    sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/nginx-bm:latest || true'
    currentBuild.result = "FAILED"
    throw e
  } finally {

    notifyBuild(currentBuild.result)
  }
}

def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus = buildStatus ?: 'SUCCESS'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"
  def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESS') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary)
}

def waitForBmPod() {
  sh '''while true; do status=$(tail -1 /tmp/bm-php_pod-status|awk '{print $2}'); if [ "$status" != "Running" ]; then echo "not running"; else echo "running"; break; fi; kubectl get pods|grep bm-php|head -1|awk '{print $1,$3}' > /tmp/bm-php_pod-status; sleep 5; done'''
  echo "Services are ready, continuing"
}
