<?php

namespace EaPaysites\Sqs\Client;

use JMS\Serializer\SerializerInterface;

class SqsClient implements SqsClientInterface
{
    /**
     * @var \Aws\Sqs\SqsClient
     */
    private $awsSqsClient;

    /**
     * @var array
     */
    private $sqsQueues;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * SqsClient constructor.
     *
     * @param \Aws\Sqs\SqsClient  $awsSqsClient
     * @param array               $sqsQueues
     * @param SerializerInterface $serializer
     */
    public function __construct(
        \Aws\Sqs\SqsClient $awsSqsClient,
        array $sqsQueues,
        SerializerInterface $serializer
    ) {
        $this->awsSqsClient = $awsSqsClient;
        $this->sqsQueues    = $sqsQueues;
        $this->serializer   = $serializer;
    }

    /**
     * @param string $queueId
     * @param        $message
     */
    public function sendMessage(string $queueId, $message)
    {
        $this->awsSqsClient->sendMessage(
            [
                'QueueUrl'    => $this->getQueueUrl($queueId),
                'MessageBody' => $this->serializer->serialize($message, 'json')
            ]
        );
    }

    /**
     * @param string $queueId
     *
     * @return array
     */
    public function receiveMessages(string $queueId): array
    {
        $result = $this->awsSqsClient->receiveMessage(
            [
                'QueueUrl' => $this->getQueueUrl($queueId),
            ]
        );

        if (!$result->hasKey('Messages')) {
            return [];
        }

        return $result->get('Messages');
    }

    /**
     * @param string $queueId
     * @param string $receiptHandle
     */
    public function deleteMessage(string $queueId, string $receiptHandle)
    {
        $this->awsSqsClient->deleteMessage(
            [
                'QueueUrl'      => $this->getQueueUrl($queueId),
                'ReceiptHandle' => $receiptHandle
            ]
        );
    }

    /**
     * @param string $queueId
     *
     * @return string
     */
    private function getQueueUrl(string $queueId): string
    {
        if (empty($this->sqsQueues[$queueId]['url'])) {
            throw new \InvalidArgumentException("Queue $queueId might be not defined");
        }

        return $this->sqsQueues[$queueId]['url'];
    }
}
