<?php

namespace EaPaysites\Sqs\Client;

interface SqsClientInterface
{
    /**
     * @param string $queueId
     * @param        $message
     *
     * @return mixed
     */
    public function sendMessage(string $queueId, $message);

    /**
     * @param string $queueId
     *
     * @return array
     */
    public function receiveMessages(string $queueId): array;

    /**
     * @param string $queueId
     * @param string $receiptHandle
     *
     * @return mixed
     */
    public function deleteMessage(string $queueId, string $receiptHandle);
}
