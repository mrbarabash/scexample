<?php

namespace EaPaysites\Sqs\Client;

class NullSqsClient implements SqsClientInterface
{
    /**
     * @param string $queueId
     * @param        $message
     */
    public function sendMessage(string $queueId, $message)
    {
    }

    /**
     * @param string $queueId
     *
     * @return array
     */
    public function receiveMessages(string $queueId): array
    {
        return [];
    }

    /**
     * @param string $queueId
     * @param string $receiptHandle
     */
    public function deleteMessage(string $queueId, string $receiptHandle)
    {
    }
}
