<?php

namespace EaPaysites\Sqs;

use EaPaysites\Sqs\Client\SqsClientInterface;

class SqsProducer
{
    /**
     * @var SqsClientInterface
     */
    private $sqsClient;

    /**
     * SqsProducer constructor.
     *
     * @param SqsClientInterface $sqsClient
     */
    public function __construct(SqsClientInterface $sqsClient)
    {
        $this->sqsClient = $sqsClient;
    }

    /**
     * @param string $queueId
     * @param        $message
     */
    public function send(string $queueId, $message)
    {
        $this->sqsClient->sendMessage($queueId, $message);
    }
}
