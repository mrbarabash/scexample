<?php

namespace EaPaysites\Sqs;

interface SqsConsumerInterface
{
    /**
     * @param array $message
     *
     * @return bool
     */
    public function execute(array $message): bool;
}
