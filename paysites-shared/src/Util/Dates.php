<?php

namespace EaPaysites\Util;

class Dates
{
    /**
     * @param \DateInterval $interval1
     * @param \DateInterval $interval2
     *
     * @return int -1 if 1st is smaller, 0 if equal, 1 if 2nd is smaller
     */
    public static function compareIntervals(\DateInterval $interval1, \DateInterval $interval2): int
    {
        $fakeStartDate1 = date_create();
        $fakeStartDate2 = clone $fakeStartDate1;
        $fakeEndDate1   = $fakeStartDate1->add($interval1);
        $fakeEndDate2   = $fakeStartDate2->add($interval2);

        if ($fakeEndDate1 < $fakeEndDate2) {
            return -1;
        } elseif ($fakeEndDate1->getTimestamp() == $fakeEndDate2->getTimestamp()) {
            return 0;
        }

        return 1;
    }
}
