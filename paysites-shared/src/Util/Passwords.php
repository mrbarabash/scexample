<?php

namespace EaPaysites\Util;

use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

class Passwords
{
    const BCRYPT_COST = 10;

    /**
     * @param string $password
     *
     * @return string
     */
    public static function encodePassword(string $password): string
    {
        $bCryptEncoder = new BCryptPasswordEncoder(self::BCRYPT_COST);
        $encoded       = $bCryptEncoder->encodePassword($password, null);

        return $encoded;
    }

    /**
     * @param string $encoded
     * @param string $raw
     *
     * @return bool
     */
    public static function isPasswordValid(string $encoded, string $raw): bool
    {
        $bCryptEncoder = new BCryptPasswordEncoder(self::BCRYPT_COST);

        return $bCryptEncoder->isPasswordValid($encoded, $raw, null);
    }

    /**
     * @return string
     */
    public static function generateToken(): string
    {
        $bytes             = random_bytes(64);
        $string            = base64_encode($bytes);
        $urlFriendlyString = strtr($string, '+/=', '._-');

        return $urlFriendlyString;
    }
}
