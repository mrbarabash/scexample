<?php

namespace EaPaysites\Util;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use EaPaysites\Service\ServiceResponseError;
use Particle\Validator\Exception\InvalidValueException;
use Respect\Validation\Validator;

class Validators
{
    /**
     * @param callable $cb
     * @param string   $message
     *
     * @return callable
     */
    public static function getDuplicateEntityRule(callable $cb, string $message): callable
    {
        return function ($value) use ($cb, $message) {
            if (!call_user_func($cb, $value)) {
                throw new InvalidValueException($message, 'Entity::DUPLICATE');
            }

            return true;
        };
    }

    /**
     * @param ObjectRepository $repo
     * @param string           $field
     * @param string           $message
     *
     * @return callable
     */
    public static function getEntityExistsRule(ObjectRepository $repo, string $field, string $message): callable
    {
        return function ($value) use ($repo, $field, $message) {
            if (!$repo->findOneBy([$field => $value])) {
                throw new InvalidValueException($message, 'Entity::NOT_FOUND');
            }

            return true;
        };
    }

    /**
     * @param EntityManagerInterface $em
     * @param string                 $contentTypeField
     * @param string                 $contentIdField
     * @param string                 $message
     *
     * @return callable
     */
    public static function getEntityExistsByDiscriminatorRule(
        EntityManagerInterface $em,
        string $contentTypeField,
        string $contentIdField,
        string $message
    ): callable {
        /** @noinspection PhpUnusedParameterInspection */
        return function ($value, $values) use ($em, $contentTypeField, $contentIdField, $message) {
            if (empty($values[$contentTypeField]) || empty($values[$contentIdField])) {
                throw new InvalidValueException($message, 'Entity::NOT_FOUND');
            }
            $contentType = $values[$contentTypeField];
            $contentId   = $values[$contentIdField];

            $entityClass = "AppBundle\\Entity\\{$contentType}";
            if (!class_exists($entityClass)) {
                throw new InvalidValueException($message, 'Entity::NOT_FOUND');
            }

            if ($em->getMetadataFactory()->isTransient($entityClass)) {
                throw new InvalidValueException($message, 'Entity::NOT_FOUND');
            }

            if (!$em->getRepository($entityClass)->find($contentId)) {
                throw new InvalidValueException($message, 'Entity::NOT_FOUND');
            }

            return true;
        };
    }

    /**
     * @return callable
     */
    public static function getGraphCharactersRule(): callable
    {
        return function ($value) {
            if (!Validator::prnt()->validate($value)) {
                throw new InvalidValueException('Input contains non-printable characters', 'String::NON_PRINTABLE');
            }

            return true;
        };
    }

    /**
     * @param array $errors
     *
     * @return ServiceResponseError
     */
    public static function getBadRequestServiceResponse($errors = []): ServiceResponseError
    {
        $error = new ServiceResponseError(
            ServiceResponseError::TYPE_BAD_REQUEST,
            'Bad request',
            $errors
        );

        return $error;
    }

    /**
     * @return callable
     */
    public static function getIpValidationRule(): callable
    {
        return function ($value) {
            if (!Validator::ip()->validate($value)) {
                throw new InvalidValueException('The IP value is incorrect', 'Ip::INCORRECT_VALUE');
            }

            return true;
        };
    }
}
