<?php

namespace EaPaysites\Util;

use Doctrine\Common\Collections\ArrayCollection;

class Arrays
{
    /**
     * @param ArrayCollection|array $entities
     * @param string                $key
     *
     * @return array
     */
    public static function getKeysToEntitiesArray($entities, string $key): array
    {
        $getter = 'get' . ucfirst($key);

        $keysToEntities = [];
        foreach ($entities as $entity) {
            if (is_array($entity)) {
                $key = $entity[$key];
            } else {
                $key = $entity->$getter();
            }

            $keysToEntities[$key] = $entity;
        }

        return $keysToEntities;
    }

    /**
     * @param ArrayCollection|array $entities
     * @param string                $key
     *
     * @return array
     */
    public static function getKeysArray($entities, string $key)
    {
        $keys   = [];
        $getter = 'get' . ucfirst($key);

        foreach ($entities as $entity) {
            if (is_array($entity)) {
                $keys[] = $entity[$key];
            } else {
                $keys[] = $entity->$getter();
            }
        }

        return $keys;
    }

    /**
     * @param array  $entities
     * @param string $key
     */
    public static function sortByKey(array $entities, string $key)
    {
        $getter = 'get' . ucfirst($key);

        uasort(
            $entities,
            function ($a, $b) use ($key, $getter) {
                if (is_array($a)) {
                    $keyA = $a[$key];
                    $keyB = $b[$key];
                } else {
                    $keyA = $a->$getter();
                    $keyB = $b->$getter();
                }

                return strcasecmp($keyA, $keyB);
            }
        );
    }

    /**
     * @param array  $entities
     * @param string $key
     * @param array  $keyValues
     *
     * @return array
     */
    public static function sortByValuesArray(array $entities, string $key, array $keyValues)
    {
        $keysToItems = self::getKeysToEntitiesArray($entities, $key);

        $sortedEntities = [];
        foreach ($keyValues as $keyValue) {
            if (isset($keysToItems[$keyValue])) {
                $sortedEntities[] = $keysToItems[$keyValue];
            }
        }

        return $sortedEntities;
    }
}
