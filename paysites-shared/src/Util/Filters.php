<?php

namespace EaPaysites\Util;

use Cocur\Slugify\Slugify;

class Filters
{
    /**
     * @param $email
     *
     * @return string
     */
    public static function getCanonicalEmail($email): string
    {
        return mb_strtolower($email);
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public static function slugify(string $text): string
    {
        $slugify = new Slugify();

        return $slugify->slugify($text);
    }
}
