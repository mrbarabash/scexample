<?php

namespace EaPaysites\Util;

use EaPaysites\Service\ServiceResponseError;
use EaPaysites\Service\ServiceResponseInterface;
use Particle\Validator\ValidationResult;

class Services
{
    /**
     * @param ServiceResponseInterface $response
     * @param ValidationResult         $validationResult
     */
    public static function setValidationError(ServiceResponseInterface $response, ValidationResult $validationResult)
    {
        $error = new ServiceResponseError(
            ServiceResponseError::TYPE_BAD_REQUEST,
            'Bad Request',
            $validationResult->getMessages()
        );
        $response->setError($error);
    }
}
