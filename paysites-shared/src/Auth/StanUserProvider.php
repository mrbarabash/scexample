<?php

namespace EaPaysites\Auth;

use AppBundle\Entity\Repository\UserRepository;
use Doctrine\Common\Util\Debug;
use EaPaysites\Entity\Repository\UserRepositoryInterface;
use EaPaysites\RpcTask;
use EaPaysites\Service\Customer\SignupServiceInterface;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\Rpc\Request\RpcRequest;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class StanUserProvider implements UserProviderInterface
{
    /**
     * @var RpcClientInterface
     */
    private $rpcClient;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var SignupServiceInterface
     */
    private $signupService;

    /**
     * StanUserProvider constructor.
     *
     * @param RpcClientInterface      $rpcClient
     * @param UserRepositoryInterface $userRepo
     * @param SignupServiceInterface  $signupService
     */
    public function __construct(
        RpcClientInterface $rpcClient,
        UserRepositoryInterface $userRepo,
        SignupServiceInterface $signupService
    ) {
        $this->rpcClient     = $rpcClient;
        $this->userRepo      = $userRepo;
        $this->signupService = $signupService;
    }

    /**
     * @param string $username
     *
     * @return null|UserInterface
     */
    public function loadUserByUsername($username): ?UserInterface
    {
        $stanRequest        = new RpcTask\Customer\GetByEmail\Request();
        $stanRequest->email = $username;

        $rpcRequest = new RpcRequest(
            'Customer\\GetByEmail', $stanRequest, RpcTask\Customer\GetByEmail\Response::class
        );

        /** @var RpcTask\Customer\GetByEmail\Response $stanResponse */
        $stanResponse = $this->rpcClient->send($rpcRequest);

        if ($stanResponse->isError()) {
            $errorSubType = $stanResponse->getError()->getSubType();
            if ($errorSubType === RpcTask\Customer\GetByEmail\Response::ERROR_SUBTYPE_CUSTOMER_NOT_FOUND) {
                throw new UsernameNotFoundException();
            }

            return null;
        }

        $user = $this->userRepo->find($stanResponse->customer['id']);
        if (!$user) {
            $user = $this->signupService->createFromStanCustomer($stanResponse->customer);
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        return $user;
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'AppBundle\\Entity\\User';
    }
}
