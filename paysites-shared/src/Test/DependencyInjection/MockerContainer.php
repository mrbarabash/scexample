<?php

namespace EaPaysites\Test\DependencyInjection;

use Symfony\Component\DependencyInjection\Container;

class MockerContainer extends Container
{
    /**
     * @var object[] $stubs
     */
    private $stubs = [];

    /**
     * @param $serviceId
     * @param $stub
     */
    public function stub($serviceId, $stub)
    {
        if (!$this->has($serviceId)) {
            throw new \InvalidArgumentException(sprintf('Cannot stub a non-existent service: "%s"', $serviceId));
        }

        $this->stubs[$serviceId] = $stub;
    }

    /**
     * @param string  $id
     * @param integer $invalidBehavior
     *
     * @return object
     */
    public function get($id, $invalidBehavior = self::EXCEPTION_ON_INVALID_REFERENCE)
    {
        if (array_key_exists($id, $this->stubs)) {
            return $this->stubs[$id];
        }

        return parent::get($id, $invalidBehavior);
    }

    /**
     * @param string $id
     *
     * @return boolean
     */
    public function has($id)
    {
        if (array_key_exists($id, $this->stubs)) {
            return true;
        }

        return parent::has($id);
    }

    /**
     * @return object[]
     */
    public function getStubs()
    {
        return $this->stubs;
    }
}
