<?php

namespace EaPaysites\RpcTask\PurchaseToken\Redeem;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $id;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $usedFor;
}
