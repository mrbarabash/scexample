<?php

namespace EaPaysites\RpcTask\PurchaseToken\Redeem;

use EaPaysites\Service\AbstractServiceResponse;
use JMS\Serializer\Annotation as Serializer;

class Response extends AbstractServiceResponse
{
    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $token;

    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $contentPermission;
}
