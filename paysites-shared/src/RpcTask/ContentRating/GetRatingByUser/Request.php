<?php

namespace EaPaysites\RpcTask\ContentRating\GetRatingByUser;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $userId;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contentType;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $contentId;
}
