<?php

namespace EaPaysites\RpcTask\ContentRating\GetRatingByUser;

use EaPaysites\Service\AbstractServiceResponse;
use JMS\Serializer\Annotation as Serializer;

class Response extends AbstractServiceResponse
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $value;
}
