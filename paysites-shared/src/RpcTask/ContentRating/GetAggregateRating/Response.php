<?php

namespace EaPaysites\RpcTask\ContentRating\GetAggregateRating;

use EaPaysites\Service\AbstractServiceResponse;
use JMS\Serializer\Annotation as Serializer;

class Response extends AbstractServiceResponse
{
    /**
     * @var int[]
     * @Serializer\Type("array<int,float>")
     */
    public $contentIdsToRatings;
}
