<?php

namespace EaPaysites\RpcTask\ContentRating\GetAggregateRating;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contentType;

    /**
     * @var int[]
     * @Serializer\Type("array<int>")
     */
    public $contentIds;
}
