<?php

namespace EaPaysites\RpcTask\PaymentGateway\GetPurchaseUrl;

use EaPaysites\Service\AbstractServiceResponse;
use JMS\Serializer\Annotation as Serializer;

class Response extends AbstractServiceResponse
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $url;
}
