<?php

namespace EaPaysites\RpcTask\PaymentGateway\GetPurchaseUrl;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $userId;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $productId;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $ip;

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public $contentId;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $locale;
}
