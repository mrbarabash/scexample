<?php

namespace EaPaysites\RpcTask\Product\GetList;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $website;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $ip;
}
