<?php

namespace EaPaysites\RpcTask\Product\GetList;

use EaPaysites\Service\AbstractServiceResponse;
use JMS\Serializer\Annotation as Serializer;

class Response extends AbstractServiceResponse
{
    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $products;
}
