<?php

namespace EaPaysites\RpcTask\ContentPermission\Sync;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $id;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $contentId;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contentType;

    /**
     * @var \DateTime
     * @Serializer\Type("DateTime")
     */
    public $grantedAt;

    /**
     * @var \DateTime
     * @Serializer\Type("DateTime")
     */
    public $expiresAt;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $permission;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $grantedVia;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $grantedViaType;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $userId;
}
