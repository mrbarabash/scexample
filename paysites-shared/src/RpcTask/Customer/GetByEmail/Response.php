<?php

namespace EaPaysites\RpcTask\Customer\GetByEmail;

use EaPaysites\Service\AbstractServiceResponse;
use JMS\Serializer\Annotation as Serializer;

class Response extends AbstractServiceResponse
{
    const ERROR_SUBTYPE_CUSTOMER_NOT_FOUND = 'customer_not_found';

    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $customer;
}
