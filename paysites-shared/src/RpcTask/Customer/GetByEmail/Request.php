<?php

namespace EaPaysites\RpcTask\Customer\GetByEmail;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $email;
}
