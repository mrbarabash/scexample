<?php

namespace EaPaysites\RpcTask\Customer\ConfirmEmail;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $customerId;
}
