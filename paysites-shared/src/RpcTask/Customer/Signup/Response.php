<?php

namespace EaPaysites\RpcTask\Customer\Signup;

use EaPaysites\Service\AbstractServiceResponse;
use JMS\Serializer\Annotation as Serializer;

class Response extends AbstractServiceResponse
{
    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $customer;

    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $purchaseToken;
}
