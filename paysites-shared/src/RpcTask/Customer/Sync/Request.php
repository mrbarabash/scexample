<?php

namespace EaPaysites\RpcTask\Customer\Sync;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class Request extends AbstractServiceRequest
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $name;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $email;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $emailCanonical;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $password;

    /**
     * @var \DateTime
     * @Serializer\Type("DateTime")
     */
    public $creationDateTime;
}
