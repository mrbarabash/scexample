<?php

namespace EaPaysites\RpcTask\Customer\ChangePassword;

use EaPaysites\Service\AbstractServiceResponse;
use JMS\Serializer\Annotation as Serializer;

class Response extends AbstractServiceResponse
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $passwordHash;
}
