<?php

namespace EaPaysites\Entity;

class Website extends AbstractEntity implements ContentPermissionableInterface
{
    const BLACKMAILED = 1;

    /**
     * @var int
     */
    private $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
}
