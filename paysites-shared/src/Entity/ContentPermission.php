<?php

namespace EaPaysites\Entity;

class ContentPermission
{
    const PERMISSION_SCENE_VIEW_FULL = 'scene_full_view';
    const PERMISSION_WEBSITE_ACCESS = 'website_access';
}
