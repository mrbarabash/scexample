<?php

namespace EaPaysites\Entity;

interface ContentPermissionableInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getOwnClassName(): string;
}
