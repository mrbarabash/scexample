<?php

namespace EaPaysites\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use EaPaysites\Entity\EntityInterface;

abstract class AbstractRepository extends EntityRepository
{
    /**
     * @param $id
     *
     * @return bool|EntityInterface
     */
    public function getReference($id)
    {
        return $this->getEntityManager()->getReference($this->getEntityName(), $id);
    }

    /**
     * @param EntityInterface $entity
     * @param bool            $flush
     */
    public function store(EntityInterface $entity, $flush = false)
    {
        $this->getEntityManager()->persist($entity);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param EntityInterface $entity
     * @param bool            $flush
     */
    public function remove(EntityInterface $entity, $flush = false)
    {
        $this->getEntityManager()->remove($entity);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param EntityInterface[] $entities
     * @param string            $key
     *
     * @return EntityInterface[]
     */
    protected function getKeyToEntityArray($entities, $key)
    {
        $keyToEntity = [];
        $getter      = 'get' . ucfirst($key);

        foreach ($entities as $entity) {
            $keyToEntity[$entity->$getter()] = $entity;
        }

        return $keyToEntity;
    }

    /**
     * @param string $field
     *
     * @return array
     */
    protected function getFieldValues(string $field): array
    {
        $qb   = $this->createQueryBuilder('e')
                     ->select('e.' . $field);
        $rows = $qb->getQuery()->getArrayResult();

        $values = array_map(
            function ($row) use ($field) {
                return $row[$field];
            },
            $rows
        );

        return $values;
    }
}
