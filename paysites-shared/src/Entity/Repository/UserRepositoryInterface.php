<?php

namespace EaPaysites\Entity\Repository;

interface UserRepositoryInterface
{
    /**
     * @param      $id
     * @param null $lockMode
     * @param null $lockVersion
     *
     * @return mixed
     */
    public function find($id, $lockMode = null, $lockVersion = null);
}
