<?php

namespace EaPaysites\Service;

interface ServiceResponseInterface
{
    /**
     * @return ServiceResponseError|null
     */
    public function getError(): ?ServiceResponseError;

    /**
     * @return bool
     */
    public function isError(): bool;

    /**
     * @param ServiceResponseError $error
     *
     * @return void
     */
    public function setError(ServiceResponseError $error);
}
