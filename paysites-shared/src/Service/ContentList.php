<?php

namespace EaPaysites\Service;

class ContentList
{
    /**
     * @var
     */
    private $list;

    /**
     * @var integer
     */
    private $totalCount;

    /**
     * @return mixed
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param $list
     */
    public function setList($list)
    {
        $this->list = $list;
    }

    /**
     * @return integer
     */
    public function getTotalCount(): ?integer
    {
        return $this->totalCount;
    }

    /**
     * @param integer $totalCount
     */
    public function setTotalCount(integer $totalCount)
    {
        $this->totalCount = $totalCount;
    }
}
