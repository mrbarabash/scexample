<?php

namespace EaPaysites\Service\Rpc\Client;

use EaPaysites\Service\Rpc\Request\RpcRequestInterface;
use EaPaysites\Service\ServiceResponseInterface;

interface RpcClientInterface
{
    /**
     * @param RpcRequestInterface $rpcRequest
     *
     * @return ServiceResponseInterface
     */
    public function send(RpcRequestInterface $rpcRequest): ServiceResponseInterface;
}
