<?php

namespace EaPaysites\Service\Rpc\Client;

use EaPaysites\Service\Rpc\Request\RpcRequestInterface;
use EaPaysites\Service\Rpc\RpcException;
use EaPaysites\Service\ServiceResponseError;
use EaPaysites\Service\ServiceResponseInterface;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

class HttpRpcClient implements RpcClientInterface
{
    const SERIALIZER = 'json';

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ClientInterface
     */
    private $httpTransport;

    /**
     * @var string
     */
    private $stanRpcAccessKey;

    /**
     * HttpRpcClient constructor.
     *
     * @param SerializerInterface $serializer
     * @param LoggerInterface     $logger
     * @param ClientInterface     $httpTransport
     * @param string              $stanRpcAccessKey
     */
    public function __construct(
        SerializerInterface $serializer,
        LoggerInterface $logger,
        ClientInterface $httpTransport,
        string $stanRpcAccessKey
    ) {
        $this->serializer       = $serializer;
        $this->logger           = $logger;
        $this->httpTransport    = $httpTransport;
        $this->stanRpcAccessKey = $stanRpcAccessKey;
    }

    /**
     * @param RpcRequestInterface $rpcRequest
     *
     * @return ServiceResponseInterface
     * @throws RpcException
     */
    public function send(RpcRequestInterface $rpcRequest): ServiceResponseInterface
    {
        $responseClass = $rpcRequest->getServiceResponseClass();

        try {
            $reply    = $this->sendMessage($rpcRequest);
            $response = $this->getResponse($reply, $responseClass);
        } catch (\Exception $e) {
            $this->logClientException($rpcRequest, $e);

            $response        = new $responseClass();
            $response->error = new ServiceResponseError(ServiceResponseError::TYPE_SERVER_ERROR);
        }

        return $response;
    }

    /**
     * @param RpcRequestInterface $rpcRequest
     * @param \Exception          $exception
     */
    private function logClientException(RpcRequestInterface $rpcRequest, \Exception $exception)
    {
        $message = sprintf(
            "RPC request failed. Task: %s. Request ID: %s. Exception: %s. Trace: %s",
            $rpcRequest->getTaskName(),
            $rpcRequest->getId(),
            $exception->getMessage(),
            $exception->getTraceAsString()
        );

        $this->logger->error($message);
    }

    /**
     * @param string $reply
     * @param string $responseClass
     *
     * @return ServiceResponseInterface
     */
    private function getResponse(string $reply, string $responseClass): ServiceResponseInterface
    {
        /** @var ServiceResponseInterface $response */
        $response = $this->serializer->deserialize(
            $reply,
            $responseClass,
            self::SERIALIZER
        );

        return $response;
    }

    /**
     * @param RpcRequestInterface $rpcRequest
     *
     * @return string
     */
    private function sendMessage(RpcRequestInterface $rpcRequest): string
    {
        $taskNameParts    = explode('\\', $rpcRequest->getTaskName());
        $taskNameParts[0] = lcfirst($taskNameParts[0]);
        $taskNameParts[1] = lcfirst($taskNameParts[1]);

        $taskNamePath = implode('/', $taskNameParts);

        $message = $this->serializer->serialize($rpcRequest->getServiceRequest(), self::SERIALIZER);

        $response = $this->httpTransport->request(
            'POST',
            $taskNamePath,
            [
                'body'    => $message,
                'headers' => [
                    'X-Stan-Access-Key' => $this->stanRpcAccessKey,
                    'X-Rpc-Request-Id'  => $rpcRequest->getId()
                ]
            ]
        );

        return (string)$response->getBody();
    }
}
