<?php

namespace EaPaysites\Service\Rpc\Client;

use EaPaysites\Service\Rpc\Request\RpcRequestInterface;
use EaPaysites\Service\ServiceResponseInterface;

class NullRpcClient implements RpcClientInterface
{
    /**
     * @param RpcRequestInterface $request
     *
     * @return ServiceResponseInterface
     */
    public function send(RpcRequestInterface $request): ServiceResponseInterface
    {
        $class = $request->getServiceResponseClass();

        return new $class;
    }
}
