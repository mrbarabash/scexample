<?php

namespace EaPaysites\Service\Rpc\Handler;

use Symfony\Component\DependencyInjection\ContainerInterface;

class RpcHandlerManager
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * RpcTaskManager constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $taskName
     *
     * @return RpcHandlerInterface
     */
    public function getHandler(string $taskName)
    {
        $handlerClass = $this->getHandlerClass($taskName);

        /** @var RpcHandlerInterface $handler */
        $handler = $this->container->get($handlerClass);

        return $handler;
    }

    /**
     * @param string $taskName
     *
     * @return bool
     */
    public function handlerExists(string $taskName): bool
    {
        $handlerClass = $this->getHandlerClass($taskName);

        return $this->container->has($handlerClass);
    }

    /**
     * @param string $taskName
     *
     * @return string
     */
    private function getHandlerClass(string $taskName): string
    {
        return sprintf("AppBundle\\RpcHandler\\%s", $taskName);
    }
}
