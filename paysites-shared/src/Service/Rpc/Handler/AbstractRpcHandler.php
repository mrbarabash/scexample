<?php

namespace EaPaysites\Service\Rpc\Handler;

use EaPaysites\Service\ServiceRequestInterface;

abstract class AbstractRpcHandler implements RpcHandlerInterface
{
    /**
     * @return string
     */
    public function getTaskName(): string
    {
        $fullClassNameParts = explode('\\', get_called_class());

        $taskNameParts = array_slice($fullClassNameParts, -2, 2);

        return implode('\\', $taskNameParts);
    }

    /**
     * @return string
     */
    public function getRequestClassName(): string
    {
        return 'EaPaysites\\RpcTask\\' . $this->getTaskName() . '\\Request';
    }

    /**
     * @param ServiceRequestInterface $rpcServiceRequest
     *
     * @return array
     */
    public function getLogSafeRequestData(ServiceRequestInterface $rpcServiceRequest): array
    {
        return $rpcServiceRequest->toArray();
    }
}
