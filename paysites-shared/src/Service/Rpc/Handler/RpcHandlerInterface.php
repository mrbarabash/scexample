<?php

namespace EaPaysites\Service\Rpc\Handler;

use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseInterface;

interface RpcHandlerInterface
{
    /**
     * @param ServiceRequestInterface $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface;

    /**
     * @param ServiceRequestInterface $rpcServiceRequest
     *
     * @return array
     */
    public function getLogSafeRequestData(ServiceRequestInterface $rpcServiceRequest): array;

    /**
     * @return string
     */
    public function getTaskName(): string;

    /**
     * @return string
     */
    public function getRequestClassName(): string;
}
