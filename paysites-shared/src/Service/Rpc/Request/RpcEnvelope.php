<?php

namespace EaPaysites\Service\Rpc\Request;

use EaPaysites\Service\ServiceRequestInterface;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\SerializerInterface;

class RpcEnvelope implements RpcEnvelopeInterface
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $taskName;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $requestSerialized;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $requestClass;

    /**
     * RpcEnvelope constructor.
     *
     * @param string $taskName
     * @param string $requestSerialized
     * @param string $requestClass
     */
    public function __construct(string $taskName, string $requestSerialized, string $requestClass)
    {
        $this->taskName          = $taskName;
        $this->requestSerialized = $requestSerialized;
        $this->requestClass      = $requestClass;
    }

    /**
     * @param string                  $taskName
     * @param ServiceRequestInterface $request
     * @param SerializerInterface     $serializer
     *
     * @return RpcEnvelope
     */
    public static function factory(string $taskName, ServiceRequestInterface $request, SerializerInterface $serializer)
    {
        return new self(
            $taskName,
            $serializer->serialize($request, 'json'),
            get_class($request)
        );
    }

    /**
     * @return string
     */
    public function getTaskName(): string
    {
        return $this->taskName;
    }

    /**
     * @param SerializerInterface $serializer
     *
     * @return ServiceRequestInterface
     */
    public function getRequest(SerializerInterface $serializer): ServiceRequestInterface
    {
        /** @var ServiceRequestInterface $request */
        $request = $serializer->deserialize($this->requestSerialized, $this->requestClass, 'json');

        return $request;
    }
}
