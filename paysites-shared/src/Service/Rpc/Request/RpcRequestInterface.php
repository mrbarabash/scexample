<?php

namespace EaPaysites\Service\Rpc\Request;

use EaPaysites\Service\ServiceRequestInterface;

interface RpcRequestInterface
{
    /**
     * @return string
     */
    public function getTaskName(): string;

    /**
     * @return ServiceRequestInterface
     */
    public function getServiceRequest(): ServiceRequestInterface;

    /**
     * @return string
     */
    public function getServiceResponseClass(): string;

    /**
     * @return string
     */
    public function getId(): string;
}
