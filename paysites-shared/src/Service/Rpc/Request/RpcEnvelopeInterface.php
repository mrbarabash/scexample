<?php

namespace EaPaysites\Service\Rpc\Request;

use EaPaysites\Service\ServiceRequestInterface;
use JMS\Serializer\SerializerInterface;

interface RpcEnvelopeInterface
{
    /**
     * @return string
     */
    public function getTaskName(): string;

    /**
     * @param SerializerInterface $serializer
     *
     * @return ServiceRequestInterface
     */
    public function getRequest(SerializerInterface $serializer): ServiceRequestInterface;
}
