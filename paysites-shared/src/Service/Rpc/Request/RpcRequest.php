<?php

namespace EaPaysites\Service\Rpc\Request;

use EaPaysites\Service\ServiceRequestInterface;

class RpcRequest implements RpcRequestInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $taskName;

    /**
     * @var ServiceRequestInterface
     */
    private $serviceRequest;

    /**
     * @var string
     */
    private $serviceResponseClass;

    /**
     * RpcRequest constructor.
     *
     * @param string                  $taskName
     * @param ServiceRequestInterface $serviceRequest
     * @param string                  $serviceResponseClass
     */
    public function __construct(string $taskName, ServiceRequestInterface $serviceRequest, string $serviceResponseClass)
    {
        $this->taskName             = $taskName;
        $this->serviceRequest       = $serviceRequest;
        $this->serviceResponseClass = $serviceResponseClass;

        $this->id = uniqid();
    }

    /**
     * @return ServiceRequestInterface
     */
    public function getServiceRequest(): ServiceRequestInterface
    {
        return $this->serviceRequest;
    }

    /**
     * @return string
     */
    public function getTaskName(): string
    {
        return $this->taskName;
    }

    /**
     * @return string
     */
    public function getServiceResponseClass(): string
    {
        return $this->serviceResponseClass;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
