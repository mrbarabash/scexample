<?php

namespace EaPaysites\Service;

interface ServiceRequestInterface
{
    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @param array $data
     *
     * @return void
     */
    public function populate(array $data);
}
