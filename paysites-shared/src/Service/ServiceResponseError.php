<?php

namespace EaPaysites\Service;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ServiceResponseError
 * @package EaPaysites\Service
 */
class ServiceResponseError
{
    const TYPE_BAD_REQUEST = 'bad_request';

    const TYPE_SERVER_ERROR = 'server_error';

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $type;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $subType;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $message;

    /**
     * @var array
     * @Serializer\Type("array")
     */
    private $data;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $requestId;

    /**
     * ServiceResponseError constructor.
     *
     * @param string      $type
     * @param string      $message
     * @param array       $data
     * @param string|null $requestId
     */
    public function __construct(string $type, string $message = '', array $data = [], string $requestId = null)
    {
        $this->type      = $type;
        $this->message   = $message;
        $this->data      = $data;
        $this->requestId = $requestId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSubType(): ?string
    {
        return $this->subType;
    }

    /**
     * @param string $subType
     */
    public function setSubType(string $subType)
    {
        $this->subType = $subType;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getRequestId(): ?string
    {
        return $this->requestId;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
