<?php

namespace EaPaysites\Service\Email;

use JMS\Serializer\Annotation as Serializer;

class Email
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fromEmail;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fromName;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $toEmail;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $toName;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $subject;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $content;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $mimeType = 'text/plain';

    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $replacements = [];

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $templateId;
}
