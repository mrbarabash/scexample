<?php

namespace EaPaysites\Service\Customer;

interface SignupServiceInterface
{
    /**
     * @param array $customer
     *
     * @return mixed
     */
    public function createFromStanCustomer(array $customer);
}