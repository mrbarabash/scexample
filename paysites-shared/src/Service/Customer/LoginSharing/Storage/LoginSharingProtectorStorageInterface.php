<?php

namespace EaPaysites\Service\Customer\LoginSharing\Storage;

interface LoginSharingProtectorStorageInterface
{
    /**
     * @param int       $userId
     * @param \DateTime $since
     *
     * @return int
     */
    public function getCountSince(int $userId, \DateTime $since): int;

    /**
     * @param int    $userId
     * @param string $ip
     *
     * @return void
     */
    public function addVisit(int $userId, string $ip);

    /**
     * @param int       $userId
     * @param \DateTime $until
     *
     * @return void
     */
    public function truncate(int $userId, \DateTime $until);

    /**
     * @param int       $userId
     * @param \DateTime $expire
     *
     * @return void
     */
    public function expire(int $userId, \DateTime $expire);

    /**
     * @param int $userId
     *
     * @return void
     */
    public function remove(int $userId);
}