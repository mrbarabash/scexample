<?php

namespace EaPaysites\Service\Customer\LoginSharing\Storage;

use Psr\Container\ContainerInterface;

class RedisLoginSharingProtectorStorage implements LoginSharingProtectorStorageInterface
{
    /**
     * @var \Redis
     */
    private $redis;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * RedisLoginSharingProtectorStorage constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param int       $userId
     * @param \DateTime $since
     *
     * @return int
     */
    public function getCountSince(int $userId, \DateTime $since): int
    {
        try {
            return $this->getClient()->zCount($this->getKey($userId), $since->getTimestamp(), '+inf');
        } catch (\RedisException $e) {
            throw new LoginSharingProtectorStorageException('Failed to query Redis: ' . $e->getMessage());
        }
    }

    /**
     * @param int    $userId
     * @param string $ip
     */
    public function addVisit(int $userId, string $ip)
    {
        try {
            $this->getClient()->zAdd($this->getKey($userId), time(), $ip);
        } catch (\RedisException $e) {
            throw new LoginSharingProtectorStorageException('Failed to query Redis: ' . $e->getMessage());
        }
    }

    /**
     * @param int       $userId
     * @param \DateTime $until
     */
    public function truncate(int $userId, \DateTime $until)
    {
        try {
            $this->getClient()->zRemRangeByScore($this->getKey($userId), '-inf', $until->getTimestamp());
        } catch (\RedisException $e) {
            throw new LoginSharingProtectorStorageException('Failed to query Redis: ' . $e->getMessage());
        }
    }

    /**
     * @param int       $userId
     * @param \DateTime $expire
     */
    public function expire(int $userId, \DateTime $expire)
    {
        try {
            $this->getClient()->expireAt($this->getKey($userId), $expire->getTimestamp());
        } catch (\RedisException $e) {
            throw new LoginSharingProtectorStorageException('Failed to query Redis: ' . $e->getMessage());
        }
    }

    /**
     * @param int $userId
     */
    public function remove(int $userId)
    {
        try {
            $this->getClient()->delete($this->getKey($userId));
        } catch (\RedisException $e) {
            throw new LoginSharingProtectorStorageException('Failed to query Redis: ' . $e->getMessage());
        }
    }

    /**
     * @return \Redis
     */
    private function getClient(): \Redis
    {
        if (!$this->redis) {
            /**
             * This is a hack to throw catchable redis connection exception.
             * If redis injected normally (non-catchable) exception will be thrown during Symfony container init.
             * https://github.com/snc/SncRedisBundle/issues/105
             */
            try {
                $this->redis = $this->container->get('snc_redis.default');
            } catch (\Exception $e) {
                throw new LoginSharingProtectorStorageException(
                    'Failed to connect to Redis server: ' . $e->getMessage()
                );
            }
        }

        return $this->redis;
    }

    /**
     * @param int $userId
     *
     * @return string
     */
    private function getKey(int $userId): string
    {
        return sprintf('bm:login_sharing:%s', $userId);
    }
}
