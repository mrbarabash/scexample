<?php

namespace EaPaysites\Service\Customer\LoginSharing;

use EaPaysites\Service\Customer\LoginSharing\Storage\LoginSharingProtectorStorageException;
use EaPaysites\Service\Customer\LoginSharing\Storage\LoginSharingProtectorStorageInterface;
use EaPaysites\Util\Dates;
use Psr\Log\LoggerInterface;

class LoginSharingProtector
{
    /**
     * @var array
     */
    private $rules;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var LoginSharingProtectorStorageInterface
     */
    private $loginSharingProtectorStorage;

    /**
     * LoginSharingProtector constructor.
     *
     * @param LoginSharingProtectorStorageInterface $loginSharingProtectorStorage
     * @param LoggerInterface                       $logger
     */
    public function __construct(
        LoginSharingProtectorStorageInterface $loginSharingProtectorStorage,
        LoggerInterface $logger
    ) {
        $this->loginSharingProtectorStorage = $loginSharingProtectorStorage;
        $this->logger                       = $logger;

        $this->rules = [
            ['count' => 2, 'interval' => new \DateInterval('PT10M')],
            ['count' => 4, 'interval' => new \DateInterval('PT6H')]
        ];
    }

    /**
     * @param int $userId
     *
     * @return bool
     */
    public function isAllowed(int $userId): bool
    {
        foreach ($this->rules as $rule) {
            $since = new \DateTime();
            $since->sub($rule['interval']);

            try {
                $isViolated = $this->loginSharingProtectorStorage->getCountSince($userId, $since) > $rule['count'];
            } catch (LoginSharingProtectorStorageException $e) {
                $this->logger->error('LoginSharingProtectorStorage failed', ['exception' => $e->getMessage()]);

                return true;
            }

            if ($isViolated) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param int    $userId
     * @param string $ip
     */
    public function trackVisit(int $userId, string $ip)
    {
        try {
            $this->loginSharingProtectorStorage->addVisit($userId, $ip);
        } catch (LoginSharingProtectorStorageException $e) {
            $this->logger->error('LoginSharingProtectorStorage failed', ['exception' => $e->getMessage()]);

            return;
        }

        $longestInterval = $this->getLongestRuleInterval();

        $truncateDate = new \DateTime();
        $truncateDate->sub($longestInterval);
        try {
            $this->loginSharingProtectorStorage->truncate($userId, $truncateDate);
        } catch (LoginSharingProtectorStorageException $e) {
            $this->logger->error('LoginSharingProtectorStorage failed', ['exception' => $e->getMessage()]);

            return;
        }

        $expireDate = new \DateTime();
        $expireDate->add($longestInterval);
        try {
            $this->loginSharingProtectorStorage->expire($userId, $expireDate);
        } catch (LoginSharingProtectorStorageException $e) {
            $this->logger->error('LoginSharingProtectorStorage failed', ['exception' => $e->getMessage()]);

            return;
        }
    }

    /**
     * @param int $userId
     */
    public function resetTracking(int $userId)
    {
        try {
            $this->loginSharingProtectorStorage->remove($userId);
        } catch (LoginSharingProtectorStorageException $e) {
            $this->logger->error('LoginSharingProtectorStorage failed', ['exception' => $e->getMessage()]);

            return;
        }
    }

    /**
     * @return \DateInterval|null
     */
    private function getLongestRuleInterval(): ?\DateInterval
    {
        $longestInterval = null;
        foreach ($this->rules as $rule) {
            if ($longestInterval === null) {
                $longestInterval = $rule['interval'];
                continue;
            }

            if (Dates::compareIntervals($longestInterval, $rule['interval']) === -1) {
                $longestInterval = $rule['interval'];
            }
        }

        return $longestInterval;
    }
}
