<?php

namespace EaPaysites\Service;

use JMS\Serializer\Annotation as Serializer;

abstract class AbstractServiceResponse implements ServiceResponseInterface
{
    /**
     * @var ServiceResponseError
     * @Serializer\Type("EaPaysites\Service\ServiceResponseError")
     */
    public $error;

    /**
     * @return ServiceResponseError|null
     */
    public function getError(): ?ServiceResponseError
    {
        return $this->error;
    }

    /**
     * @param ServiceResponseError $error
     *
     * @return void
     */
    public function setError(ServiceResponseError $error)
    {
        $this->error = $error;
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->error !== null;
    }
}
