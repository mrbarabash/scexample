<?php

namespace EaPaysites\Service;

class SortField
{
    const SORT_DIRECTION_ASC = 'asc';

    const SORT_DIRECTION_DESC = 'desc';

    /**
     * @var string
     */
    public $field;

    /**
     * @var string
     */
    public $direction = self::SORT_DIRECTION_ASC;

    /**
     * SortField constructor.
     *
     * @param string $field
     * @param string $direction
     */
    public function __construct(string $field, string $direction)
    {
        $this->field     = $field;
        $this->direction = $direction;
    }
}
