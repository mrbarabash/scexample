<?php

namespace EaPaysites\Service;

class AbstractServiceRequest implements ServiceRequestInterface
{
    /**
     * @return array
     */
    public function toArray(): array
    {
        $reflection = new \ReflectionClass($this);
        $properties = $reflection->getProperties(\ReflectionProperty::IS_PUBLIC);

        $data = [];
        foreach ($properties as $property) {
            $propertyName        = $property->getName();
            $data[$propertyName] = $this->$propertyName;
        }

        return $data;
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function populate(array $data)
    {
        foreach ($data as $name => $value) {
            if (property_exists($this, $name)) {
                $this->$name = $value;
            }
        }

        return $this;
    }
}
