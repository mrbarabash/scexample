<?php

namespace EaPaysites\Command;

use EaPaysites\Sqs\Client\SqsClientInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConsumeSqsQueueCommand extends Command
{
    /**
     * @var array
     */
    private $queues;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var SqsClientInterface
     */
    private $sqsClient;

    /**
     * ConsumeSqsQueueCommand constructor.
     *
     * @param SqsClientInterface $sqsClient
     * @param array              $sqsQueues
     * @param ContainerInterface $container
     */
    public function __construct(SqsClientInterface $sqsClient, array $sqsQueues, ContainerInterface $container)
    {
        $this->sqsClient = $sqsClient;
        $this->queues    = $sqsQueues;
        $this->container = $container;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:consume-sqs-queue')
             ->setDescription('Read and process messages from SQS queue')
             ->addArgument('queueId', InputArgument::REQUIRED, 'Provide queue ID (as defined in config.yml)')
             ->addArgument('count', InputArgument::OPTIONAL, 'How many messages to process (default: all)');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queueId = $input->getArgument('queueId');
        if (empty($this->queues[$queueId]['consumer'])) {
            throw new \InvalidArgumentException("Queue $queueId might be not defined in config.yml");
        }

        $output->writeln('SQS Queue Consumer: ' . $queueId);
        $output->writeln(sprintf('[%s] Starting consuming...', date('c')));

        $callbackService = $this->queues[$queueId]['consumer'];
        $callback        = [$this->container->get($callbackService), 'execute'];

        $count = $input->getArgument('count');
        $i     = 0;
        while (true) {
            $messages = $this->sqsClient->receiveMessages($queueId);

            foreach ($messages as $message) {
                $callResult = call_user_func($callback, $message);
                if ($callResult) {
                    $this->sqsClient->deleteMessage($queueId, $message['ReceiptHandle']);
                }
            }

            $i++;
            if ($count && $i >= $count) {
                break;
            } else {
                // @todo implement long polling
                echo $i . "\n";
                sleep(10);
            }
        }

        $output->writeln(sprintf('[%s] Finished consuming.', date('c')));
    }
}
