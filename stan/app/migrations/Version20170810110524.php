<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170810110524 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // Create Website table
        $this->addSql('CREATE TABLE websites (id INT NOT NULL, url VARCHAR(100) NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('INSERT INTO websites (`id`, `url`, `name`) VALUE (1, \'https://www.blackmailed.com\', \'blackmailed\')');

        // Create PurchaseToken table
        $this->addSql('CREATE TABLE purchase_tokens (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', website_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, kind VARCHAR(25) NOT NULL, used_for INT DEFAULT NULL, created_at DATETIME NOT NULL, used_at DATETIME DEFAULT NULL, INDEX IDX_713513DB18F45C82 (website_id), INDEX IDX_713513DB9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE purchase_tokens ADD CONSTRAINT FK_713513DB18F45C82 FOREIGN KEY (website_id) REFERENCES websites (id)');
        $this->addSql('ALTER TABLE purchase_tokens ADD CONSTRAINT FK_713513DB9395C3F3 FOREIGN KEY (customer_id) REFERENCES customers (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE purchase_tokens DROP FOREIGN KEY FK_713513DB9395C3F3');
        $this->addSql('ALTER TABLE purchase_tokens DROP FOREIGN KEY FK_713513DB18F45C82');
        $this->addSql('DROP TABLE purchase_tokens');
        $this->addSql('DROP TABLE websites');
    }
}
