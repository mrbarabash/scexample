<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170815182046 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE content_permissions (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, content_id INT NOT NULL, content_type VARCHAR(100) NOT NULL, granted_at DATETIME NOT NULL, expires_at DATETIME DEFAULT NULL, permission VARCHAR(50) NOT NULL, granted_via VARCHAR(100) NOT NULL, granted_via_type VARCHAR(100) NOT NULL, INDEX IDX_7EC766E99395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE content_permissions ADD CONSTRAINT FK_7EC766E99395C3F3 FOREIGN KEY (customer_id) REFERENCES customers (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE content_permissions');
    }
}
