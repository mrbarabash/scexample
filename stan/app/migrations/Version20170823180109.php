<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170823180109 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, status VARCHAR(25) NOT NULL, INDEX IDX_E52FFDEE9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_products (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, product_id INT DEFAULT NULL, content_id INT DEFAULT NULL, INDEX IDX_5242B8EB8D9F6D38 (order_id), INDEX IDX_5242B8EB4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payments (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, predicted_vat_id INT DEFAULT NULL, qualified_vat_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, amount DOUBLE PRECISION NOT NULL, gateway VARCHAR(100) NOT NULL, currency VARCHAR(3) NOT NULL, INDEX IDX_65D29B328D9F6D38 (order_id), UNIQUE INDEX UNIQ_65D29B324BE02983 (predicted_vat_id), UNIQUE INDEX UNIQ_65D29B329E214D64 (qualified_vat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment_transactions (id INT AUTO_INCREMENT NOT NULL, payment_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, status VARCHAR(25) NOT NULL, ip VARCHAR(40) NOT NULL, gateway_transaction_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_8C58AD564C3A3BB (payment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment_vat_rates (id INT AUTO_INCREMENT NOT NULL, payment_id INT DEFAULT NULL, rate_id INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_5DD133C74C3A3BB (payment_id), INDEX IDX_5DD133C7BC999F9F (rate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products (id INT AUTO_INCREMENT NOT NULL, website_id INT DEFAULT NULL, title VARCHAR(100) NOT NULL, description VARCHAR(255) NOT NULL, billing_description VARCHAR(255) NOT NULL, kind VARCHAR(100) NOT NULL, active TINYINT(1) NOT NULL, recurrent TINYINT(1) NOT NULL, recurrent_count INT DEFAULT NULL, recurrent_frequency VARCHAR(25) DEFAULT NULL, INDEX IDX_B3BA5A5A18F45C82 (website_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_prices (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, country VARCHAR(100) NOT NULL, price DOUBLE PRECISION NOT NULL, currency VARCHAR(3) NOT NULL, INDEX IDX_86B72CFD4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vat_rates (id INT AUTO_INCREMENT NOT NULL, country VARCHAR(2) NOT NULL, rate INT NOT NULL, created_at DATETIME NOT NULL, archived_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE9395C3F3 FOREIGN KEY (customer_id) REFERENCES customers (id)');
        $this->addSql('ALTER TABLE order_products ADD CONSTRAINT FK_5242B8EB8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE order_products ADD CONSTRAINT FK_5242B8EB4584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B328D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B324BE02983 FOREIGN KEY (predicted_vat_id) REFERENCES payment_vat_rates (id)');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B329E214D64 FOREIGN KEY (qualified_vat_id) REFERENCES payment_vat_rates (id)');
        $this->addSql('ALTER TABLE payment_transactions ADD CONSTRAINT FK_8C58AD564C3A3BB FOREIGN KEY (payment_id) REFERENCES payments (id)');
        $this->addSql('ALTER TABLE payment_vat_rates ADD CONSTRAINT FK_5DD133C74C3A3BB FOREIGN KEY (payment_id) REFERENCES payments (id)');
        $this->addSql('ALTER TABLE payment_vat_rates ADD CONSTRAINT FK_5DD133C7BC999F9F FOREIGN KEY (rate_id) REFERENCES vat_rates (id)');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5A18F45C82 FOREIGN KEY (website_id) REFERENCES websites (id)');
        $this->addSql('ALTER TABLE product_prices ADD CONSTRAINT FK_86B72CFD4584665A FOREIGN KEY (product_id) REFERENCES products (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_products DROP FOREIGN KEY FK_5242B8EB8D9F6D38');
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B328D9F6D38');
        $this->addSql('ALTER TABLE payment_transactions DROP FOREIGN KEY FK_8C58AD564C3A3BB');
        $this->addSql('ALTER TABLE payment_vat_rates DROP FOREIGN KEY FK_5DD133C74C3A3BB');
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B324BE02983');
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B329E214D64');
        $this->addSql('ALTER TABLE order_products DROP FOREIGN KEY FK_5242B8EB4584665A');
        $this->addSql('ALTER TABLE product_prices DROP FOREIGN KEY FK_86B72CFD4584665A');
        $this->addSql('ALTER TABLE payment_vat_rates DROP FOREIGN KEY FK_5DD133C7BC999F9F');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE order_products');
        $this->addSql('DROP TABLE payments');
        $this->addSql('DROP TABLE payment_transactions');
        $this->addSql('DROP TABLE payment_vat_rates');
        $this->addSql('DROP TABLE products');
        $this->addSql('DROP TABLE product_prices');
        $this->addSql('DROP TABLE vat_rates');
    }
}
