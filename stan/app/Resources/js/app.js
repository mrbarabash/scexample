$(function () {
    'use strict';

    var createForm = function (action, data) {
        var form = $('<form action="' + action + '" method="POST"></form>');
        for (var input in data) {
            if (data.hasOwnProperty(input)) {
                form.append('<input name="' + input + '" value="' + data[input] + '">');
            }
        }

        return form;
    };

    $('.stan-j-delete-button').each(function(i, linkDom) {
        $(linkDom).click(function (event) {
            event.preventDefault();

            if (!confirm('Delete?')) {
                return;
            }

            var form = createForm($(this).attr('href'), {
                _method: 'DELETE'
            }).hide();

            $('body').append(form); // Firefox requires form to be on the page to allow submission
            form.submit();
        });
    });
});
