#!/bin/bash

setfacl -dR -m u:www-data:rwX -m u:$(whoami):rwX var
setfacl -R -m u:www-data:rwX -m u:$(whoami):rwX var
php-fpm &

/var/awslogs/bin/awslogs-agent-launcher.sh &

wait
