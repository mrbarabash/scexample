#!/bin/bash
export TERM=xterm
setfacl -dR -m u:www-data:rwX -m u:$(whoami):rwX var
setfacl -R -m u:www-data:rwX -m u:$(whoami):rwX var

while true; do
    /var/www/bin/console app:consume-sqs-queue email_transactional --run-once --env=prod
    sleep 1
done

wait
