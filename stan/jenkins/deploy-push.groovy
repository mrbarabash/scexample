node {
  try {
    notifyBuild('STARTED')
    stage ('Checkout stan application code') {
        //sh 'mkdir -p /var/www/html'
        git branch: 'master',
        credentialsId: 'cb520cac-2900-494a-8678-a1eef8515e74',
        url: ' git@github.com-stan:adamgrayson31/stan.git'
        sh 'echo "JOB: $JOB_NAME" > version.txt'
        sh 'echo "BUILD NUMBER: $BUILD_NUMBER" >> version.txt'
        sh 'echo "GIT REVISION:" >> version.txt'
        sh 'git log -n 1 >> version.txt'
        sh 'cp -p version.txt $WORKSPACE/app/version.txt'
    }


    stage ('Checkout paysites-shared application code') {
        dir ('paysites-shared') {
            git branch: 'master',
                credentialsId: '3a5206b4-e498-4402-ba87-6706511f8972',
                url: ' git@github.com-shared:adamgrayson31/paysites-shared.git'
        }
    }

    stage ('Copy parameters.yml') {
        sh 'cp -p /tmp/parameters-stan-stg.yml ./app/config/parameters.yml'
    }

    stage ('Build base images') {
        if (params.rebuildBaseImages) {
            def phpBaseImage = docker.build("stan_stan-php-base:latest", "-f docker/php/Dockerfile ./docker/php")
            def phpBuildImage = docker.build("php-stan-build", "-f docker/php/dev/Dockerfile ./docker/php/dev")
            def phpImageLayer1 = docker.build("stan-php-layer1", "-f docker/php/prod/Dockerfile-logs ./docker/php/prod")
            def nginxImageLayer1 = docker.build("stan-nginx-layer1", "-f docker/nginx/Dockerfile ./docker/nginx ")
        } else {
            echo 'skipping base iamges rebuild'
        }
    }

    stage ('Composer install') {
        sh 'docker run --net=host --rm -v $WORKSPACE:/var/www/html -v $WORKSPACE/paysites-shared:/var/paysites-shared php-stan-build /bin/bash -c "SYMFONY_ENV=prod composer clear-cache"'
        sh 'docker run --net=host --rm -v $WORKSPACE:/var/www/html -v $WORKSPACE/paysites-shared:/var/paysites-shared php-stan-build /bin/bash -c "SYMFONY_ENV=prod composer install --no-dev --optimize-autoloader"'
        sh 'sudo rm ./vendor/evilangel/paysites-shared'
        sh 'sudo ln -s ../../../paysites-shared ./vendor/evilangel/paysites-shared'
    }

    stage('npm install') {
        sh "npm install"
    }

    stage('gulp build') {
        sh "gulp build"
    }

    stage ('Build stan-nginx container') {
        dir('docker/nginx/release') {
            sh 'sudo rm -rf *'
            sh 'sudo cp -rp $WORKSPACE/web .'
            sh 'sudo cp -rp $WORKSPACE/app .'
            sh 'sudo cp -rp $WORKSPACE/vendor .'
            sh 'sudo cp -rp $WORKSPACE/bin .'
            sh 'sudo cp -rp $WORKSPACE/var .'
            sh 'sudo cp -rp $WORKSPACE/node_modules .'
            sh 'sudo cp -rp $WORKSPACE/src .'
            sh 'sudo cp -rp $WORKSPACE/paysites-shared .'
            sh 'sudo chown -R 33:33 *'
        }


        def nginxImage = docker.build("stan-nginx:${BUILD_NUMBER}", "--no-cache=true -f docker/nginx/Dockerfile-app ./docker/nginx ")

    }


    stage ('Build stan-php container') {
        sh "sudo geoipupdate"
        sh 'sudo cp /usr/share/GeoIP/GeoLite2-Country.mmdb $WORKSPACE/var/'
        sh 'sudo cp /usr/share/GeoIP/GeoLite2-City.mmdb $WORKSPACE/var/'

        dir('docker/php/prod/release') {
            sh 'sudo rm -rf *'
            sh 'sudo cp -rp $WORKSPACE/web .'
            sh 'sudo cp -rp $WORKSPACE/app .'
            sh 'sudo cp -rp $WORKSPACE/vendor .'
            sh 'sudo cp -rp $WORKSPACE/bin .'
            sh 'sudo cp -rp $WORKSPACE/var .'
            sh 'sudo cp -rp $WORKSPACE/node_modules .'
            sh 'sudo cp -rp $WORKSPACE/src .'
            sh 'sudo cp -rp $WORKSPACE/paysites-shared .'
            sh 'sudo chown -R 33:33 *'
        }


        def phpImage = docker.build("stan-php:${BUILD_NUMBER}", "--no-cache=true -f docker/php/prod/Dockerfile ./docker/php/prod")
    }

    stage ('push stan-nginx to ECR') {
        docker.withRegistry("https://548355647590.dkr.ecr.us-west-2.amazonaws.com", "ecr:us-west-2:jenkins-aws") {
            docker.image('stan-nginx:${BUILD_NUMBER}').push('${BUILD_NUMBER}')
            docker.image('stan-nginx:${BUILD_NUMBER}').push('latest')
        }
    }

    stage ('push stan-php to ECR') {
        docker.withRegistry("https://548355647590.dkr.ecr.us-west-2.amazonaws.com", "ecr:us-west-2:jenkins-aws") {
            docker.image('stan-php:${BUILD_NUMBER}').push('${BUILD_NUMBER}')
            docker.image('stan-php:${BUILD_NUMBER}').push('latest')
        }
    }


    stage ('Cleanup') {
        sh 'sudo rm -rf $WORKSPACE/vendor'
        sh 'docker container prune -f || true'
        sh 'docker rmi -f stan-nginx:${BUILD_NUMBER} || true'
        sh 'docker rmi -f stan-php:${BUILD_NUMBER} || true'
        sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-php:${BUILD_NUMBER} || true'
        sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-nginx:${BUILD_NUMBER} || true'
        sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-php:latest || true'
        sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-nginx:latest || true'
    }

    stage('Deploy New Build To Kubernetes') {
        sh ("kubectl set image deployment/stan-consumer-email-transactional stan-consumer-email-transactional=548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-php:${BUILD_NUMBER}")
        sh ("kubectl set image deployment/stan-nginx stan-nginx=548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-nginx:${BUILD_NUMBER}")
        sh ("kubectl set image deployment/stan-php stan-php=548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-php:${BUILD_NUMBER}")
  }

    //stage ('Add updated mmdb file for GeoIP') {
    //  sleep 5
    //  sh "kubectl get pods|grep stan-php|head -1|awk {'print $1,$3}' > /tmp/stan-php_pod-status"
    //  waitForStanPod()

    //}

  } catch (e) {
    // If there was an exception thrown, the build failed
    sh 'sudo rm -rf $WORKSPACE/vendor'
    sh 'docker container prune -f || true'
    sh 'docker rmi -f stan-nginx:${BUILD_NUMBER} || true'
    sh 'docker rmi -f stan-php:${BUILD_NUMBER} || true'
    sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-php:${BUILD_NUMBER} || true'
    sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-php:latest || true'
    sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-nginx:${BUILD_NUMBER} || true'
    sh 'docker rmi -f 548355647590.dkr.ecr.us-west-2.amazonaws.com/stan-nginx:latest || true'
    currentBuild.result = "FAILED"
    throw e
  } finally {

    notifyBuild(currentBuild.result)
  }
}

def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus = buildStatus ?: 'SUCCESS'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"
  def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESS') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary)
}

def waitForStanPod() {
  sh '''while true; do status=$(tail -1 /tmp/bm-php_pod-status|awk '{print $2}'); if [ "$status" != "Running" ]; then echo "not running"; else echo "running"; break; fi; kubectl get pods|grep bm-php|head -1|awk '{print $1,$3}' > /tmp/bm-php_pod-status; sleep 5; done'''
  echo "Services are ready, continuing"
}
