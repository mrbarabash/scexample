<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProductRepository")
 */
class Product extends AbstractEntity
{
    const RECURRENT_FREQUENCY_MONTHLY = 1;
    const RECURRENT_FREQUENCY_QUARTERLY = 3;
    const RECURRENT_FREQUENCY_ANNUALLY = 12;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $billingDescription;

    /**
     * @var string
     *
     * @ORM\Column(length=100)
     */
    private $kind;

    /**
     * @var Website
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Website")
     */
    private $website;

    /**
     * @var ProductPrice[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductPrice", mappedBy="product", cascade={"persist", "remove"})
     */
    private $prices;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $recurrent = false;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $recurrentCount;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $recurrentFrequency;

    /**
     * @var OrderProduct[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrderProduct", mappedBy="product")
     */
    private $orders;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $validForDays;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getBillingDescription(): string
    {
        return $this->billingDescription;
    }

    /**
     * @param string $billingDescription
     */
    public function setBillingDescription(string $billingDescription)
    {
        $this->billingDescription = $billingDescription;
    }

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     */
    public function setKind(string $kind)
    {
        $this->kind = $kind;
    }

    /**
     * @return Website
     */
    public function getWebsite(): Website
    {
        return $this->website;
    }

    /**
     * @param Website $website
     */
    public function setWebsite(Website $website)
    {
        $this->website = $website;
    }

    /**
     * @return ProductPrice[]|ArrayCollection
     */
    public function getProductPrices()
    {
        return $this->prices;
    }

    /**
     * @param iterable $prices
     */
    public function setProductPrices(iterable $prices)
    {
        $this->prices = $prices;
    }

    /**
     * @param ProductPrice $price
     */
    public function addProductPrice(ProductPrice $price)
    {
        $this->prices->add($price);
    }

    /**
     * @param string $country
     *
     * @return ProductPrice|null
     */
    public function getProductPrice(string $country): ?ProductPrice
    {
        return $this->prices->filter(function (ProductPrice $price) use ($country) {
            return $price->getCountry() == $country;
        })->first() ?: null;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isRecurrent(): bool
    {
        return $this->recurrent;
    }

    /**
     * @param bool $recurrent
     */
    public function setRecurrent(bool $recurrent)
    {
        $this->recurrent = $recurrent;
    }

    /**
     * @return int
     */
    public function getRecurrentCount(): ?int
    {
        return $this->recurrentCount;
    }

    /**
     * @param int $recurrentCount
     */
    public function setRecurrentCount(int $recurrentCount)
    {
        $this->recurrentCount = $recurrentCount;
    }

    /**
     * @return int|null
     */
    public function getRecurrentFrequency(): ?int
    {
        return $this->recurrentFrequency;
    }

    /**
     * @param int|null $recurrentFrequency
     */
    public function setRecurrentFrequency(int $recurrentFrequency = null)
    {
        $this->recurrentFrequency = $recurrentFrequency;
    }

    /**
     * @return OrderProduct[]|ArrayCollection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param iterable $orders
     */
    public function setOrders(iterable $orders)
    {
        $this->orders = $orders;
    }

    /**
     * @param OrderProduct $order
     */
    public function addOrder(OrderProduct $order)
    {
        $this->orders->add($order);
    }

    /**
     * @return int|null
     */
    public function getValidForDays(): ?int
    {
        return $this->validForDays;
    }

    /**
     * @param int|null $validForDays
     */
    public function setValidForDays(int $validForDays = null)
    {
        $this->validForDays = $validForDays;
    }
}
