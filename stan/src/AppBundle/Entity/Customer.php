<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="customers")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CustomerRepository")
 * @UniqueEntity("emailCanonical")
 */
class Customer extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Assert\Length(
     *      min = 2,
     *      max = 100
     * )
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 100
     * )
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 100
     * )
     * @Assert\Email(
     *     message = "{{ value }} is not a valid email.",
     *     strict=true,
     *     checkMX = true
     * )
     * @ORM\Column(name="email_canonical", type="string", length=100, unique=true, nullable=false)
     */
    private $emailCanonical;

    /**
     * @var string
     *
     * @Assert\Length(max=72)
     */
    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @ORM\Column(name="creation_date_time", type="datetime", nullable=false)
     */
    private $creationDateTime;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $emailConfirmed = false;

    /**
     * @var PurchaseToken[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PurchaseToken", mappedBy="customer", cascade={"persist"})
     */
    private $purchaseTokens;

    /**
     * @var ContentPermission[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ContentPermission", mappedBy="customer")
     */
    private $contentPermissions;

    /**
     * @var Order[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order", mappedBy="customer")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $orders;

    public function __construct()
    {
        $this->purchaseTokens = new ArrayCollection();
        $this->contentPermissions = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name = null)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmailCanonical(): string
    {
        return $this->emailCanonical;
    }

    /**
     * @param string $email
     */
    public function setEmailCanonical(string $email)
    {
        $this->emailCanonical = $email;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword(string $plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreationDateTime(): ?\DateTime
    {
        return $this->creationDateTime;
    }

    /**
     * @param \DateTime $creationDateTime
     */
    public function setCreationDateTime(\DateTime $creationDateTime)
    {
        $this->creationDateTime = $creationDateTime;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }

    /**
     * @param bool $emailConfirmed
     */
    public function setEmailConfirmed(bool $emailConfirmed)
    {
        $this->emailConfirmed = $emailConfirmed;
    }

    /**
     * @return PurchaseToken[]|ArrayCollection
     */
    public function getPurchaseTokens()
    {
        return $this->purchaseTokens;
    }

    /**
     * @param iterable $purchaseTokens
     */
    public function setPurchaseTokens(iterable $purchaseTokens)
    {
        $this->purchaseTokens = $purchaseTokens;
    }

    /**
     * @param PurchaseToken $token
     */
    public function addPurchaseToken(PurchaseToken $token)
    {
        $this->purchaseTokens->add($token);
    }

    /**
     * @return ContentPermission[]|ArrayCollection
     */
    public function getContentPermissions()
    {
        return $this->contentPermissions;
    }

    /**
     * @param iterable $contentPermissions
     */
    public function setContentPermissions(iterable $contentPermissions)
    {
        $this->contentPermissions = $contentPermissions;
    }

    /**
     * @param ContentPermission $contentPermission
     */
    public function addContentPermission(ContentPermission $contentPermission)
    {
        $this->contentPermissions->add($contentPermission);
    }

    /**
     * @return Order[]|ArrayCollection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param iterable $orders
     */
    public function setOrders(iterable $orders)
    {
        $this->orders = $orders;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders->add($order);
    }
}
