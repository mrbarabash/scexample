<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Order extends AbstractEntity
{
    /**
     * Using as order number.
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Payment[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Payment", mappedBy="order", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $payments;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer", inversedBy="orders")
     */
    private $customer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var OrderProduct[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrderProduct", mappedBy="order", cascade={"persist", "remove"})
     */
    private $products;

    /**
     * @var string
     *
     * @ORM\Column(length=25)
     */
    private $status;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->status = Payment::PAYMENT_STATUS_NEW;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     *
     * @return float
     */
    public function getTotalCompletedAmount(\DateTime $from = null, \DateTime $to = null): float
    {
        $payments = $this->payments->filter(function (Payment $payment) {
            return $payment->isCompleted();
        });
        if ($from || $to) {
            $payments = $payments->filter(function (Payment $payment) use ($from, $to) {
                return !$payment->getTransactions($from, $to)->isEmpty();
            });
        }

        return array_reduce($payments->toArray(), function (float $total, Payment $payment) {
            return $total + $payment->getAmount();
        }, 0.0);
    }

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     *
     * @return float
     */
    public function getTotalVatAmount(\DateTime $from = null, \DateTime $to = null): float
    {
        $payments = $this->payments->filter(function (Payment $payment) {
            return $payment->isCompleted();
        });
        if ($from || $to) {
            $payments = $payments->filter(function (Payment $payment) use ($from, $to) {
                return !$payment->getTransactions($from, $to)->isEmpty();
            });
        }

        return array_reduce($payments->toArray(), function (float $total, Payment $payment) {
            return $total + ($payment->getAmount() * $payment->getQualifiedVat()->getRate() / 100);
        }, 0.0);
    }

    /**
     * @return Payment[]|ArrayCollection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param iterable $payments
     */
    public function setPayments(iterable $payments)
    {
        $this->payments = $payments;
    }

    /**
     * @param Payment $payment
     */
    public function addPayment(Payment $payment)
    {
        $this->payments->add($payment);
    }

    /**
     * @return Payment|null
     */
    public function getLastPayment(): ?Payment
    {
        return $this->payments->last() ?: null;
    }

    /**
     * @return OrderProduct[]|ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param iterable $products
     */
    public function setProducts(iterable $products)
    {
        $this->products = $products;
    }

    /**
     * @param OrderProduct $product
     */
    public function addProduct(OrderProduct $product)
    {
        $this->products->add($product);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    public function markAsCompleted()
    {
        // TODO Discuss
        $this->markOrderAs(Payment::PAYMENT_STATUS_COMPLETED);
    }

    public function markAsFailed()
    {
        // TODO Discuss
        $this->markOrderAs(Payment::PAYMENT_STATUS_FAILED);
    }

    /**
     * @internal
     *
     * @param string $status
     */
    private function markOrderAs(string $status)
    {
        $lastPayment = $this->getLastPayment();
        $lastTransaction = $lastPayment->getLastTransaction();
        if ($lastTransaction->isPending()) {
            $lastTransaction->setStatus($status);
        } else {
            $transaction = new PaymentTransaction();
            $transaction->setPayment($lastPayment);
            $transaction->setIp($lastTransaction->getIp());
            $transaction->setStatus($status);

            $lastPayment->addTransaction($transaction);
        }

        $this->setStatus($status);
    }
}
