<?php

namespace AppBundle\Entity\Factory;

use AppBundle\Entity\ContentPermission;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderProduct;
use AppBundle\Entity\Product;
use AppBundle\Entity\Website;
use EaPaysites\Entity\ContentPermission as StanContentPermission;

class ContentPermissionFactory
{
    /**
     * @param Order        $order
     * @param OrderProduct $orderProduct
     *
     * @return ContentPermission
     */
    public static function factory(Order $order, OrderProduct $orderProduct): ContentPermission
    {
        $contentPermission = new ContentPermission();

        $contentPermission->setCustomer($order->getCustomer());
        $contentPermission->setPermission(self::resolvePermission($orderProduct->getProduct()));
        $contentPermission->setContentId(self::resolveContentId($orderProduct));
        $contentPermission->setContentType($orderProduct->getProduct()->getKind());
        $contentPermission->setGrantedVia($order->getId());
        $contentPermission->setGrantedViaType($order->getOwnClassName());
        if ($validDaysCount = $orderProduct->getProduct()->getValidForDays()) {
            $contentPermission->setExpiresAt(new \DateTime(sprintf('+%d days', $validDaysCount)));
        }

        return $contentPermission;
    }

    /**
     * @param OrderProduct $orderProduct
     *
     * @return int
     */
    private static function resolveContentId(OrderProduct $orderProduct): int
    {
        switch ($orderProduct->getProduct()->getKind()) {
            case 'Scene':
                $contentId = $orderProduct->getContentId();
                break;
            case Website::staticGetOwnClassName():
                $contentId = $orderProduct->getProduct()->getWebsite()->getId();
                break;
            default:
                throw new \LogicException(
                    sprintf('There is no content id for the content type "%s".', $orderProduct->getProduct()->getKind())
                );
        }

        return $contentId;
    }

    /**
     * @param Product $product
     *
     * @return string
     */
    private static function resolvePermission(Product $product): string
    {
        switch ($product->getKind()) {
            case 'Scene':
                $permission = StanContentPermission::PERMISSION_SCENE_VIEW_FULL;
                break;
            case Website::staticGetOwnClassName():
                $permission = StanContentPermission::PERMISSION_WEBSITE_ACCESS;
                break;
            default:
                throw new \LogicException(
                    sprintf('There is no permission for the content type "%s".', $product->getKind())
                );
        }

        return $permission;
    }
}
