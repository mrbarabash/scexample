<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Customer;
use Doctrine\ORM\Query;
use EaPaysites\Entity\Repository\AbstractRepository;
use EaPaysites\Util\Filters;

/**
 * Class CustomerRepository
 * @package AppBundle\Entity\Repository
 */
class CustomerRepository extends AbstractRepository
{
    /**
     * @param string $email
     *
     * @return bool
     */
    public function isUniqueEmail(string $email): bool
    {
        $email = Filters::getCanonicalEmail($email);

        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT 1 FROM AppBundle:Customer c WHERE c.emailCanonical = :email'
            )
            ->setParameter('email', $email)
            ->setMaxResults(1);

        return count($query->getResult()) === 0;
    }

    public function findByEmail(string $email): ?Customer
    {
        $email = Filters::getCanonicalEmail($email);

        /** @var Customer $customer */
        $customer = $this->findOneBy(['emailCanonical' => $email]);

        return $customer;
    }

    /**
     * @param string $email
     *
     * @return string|null
     */
    public function getPasswordHashByEmail(string $email): ?string
    {
        $email = Filters::getCanonicalEmail($email);

        $qb = $this->createQueryBuilder('c')
            ->select('c.password')
            ->where('c.emailCanonical = :email')
            ->setParameter('email', $email);

        $result = $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_SCALAR);

        if (empty($result['password'])) {
            return null;
        }

        return $result['password'];
    }
}
