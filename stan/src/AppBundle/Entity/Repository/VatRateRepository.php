<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\VatRate;
use EaPaysites\Entity\Repository\AbstractRepository;

class VatRateRepository extends AbstractRepository
{
    /**
     * @param string $country
     *
     * @return VatRate|null
     */
    public function findActiveByCountry(string $country): ?VatRate
    {
        return $this->createQueryBuilder('vr')
            ->where('vr.country = :country')
            ->andWhere('vr.archivedAt IS NULL')
            ->setParameter('country', $country)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return VatRate
     */
    public function findFallbackVatRate(): VatRate
    {
        return $this->findActiveByCountry(VatRate::FALLBACK_COUNTRY_CODE);
    }
}
