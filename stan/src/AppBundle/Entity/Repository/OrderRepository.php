<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Payment;
use Doctrine\ORM\Query;
use EaPaysites\Entity\Repository\AbstractRepository;

class OrderRepository extends AbstractRepository
{
    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return Query
     */
    public function getVatReportQuery(\DateTime $dateFrom, \DateTime $dateTo)
    {
        return $this->createQueryBuilder('o')
            ->join('o.payments', 'p')
            ->join('p.transactions', 't')
            ->where('o.status = :status')
            ->andWhere('t.status = :status')
            ->andWhere('t.createdAt BETWEEN :from AND :to')
            ->setParameter('status', Payment::PAYMENT_STATUS_COMPLETED)
            ->setParameter('from', $dateFrom)
            ->setParameter('to', $dateTo)
            ->getQuery();
    }

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return Query
     */
    public function getVatCountryReportQuery(\DateTime $dateFrom, \DateTime $dateTo)
    {
        return $this->createQueryBuilder('o')
        ->select('vr.country as country, SUM(p.amount) as total, ROUND(SUM(p.amount * vr.rate / 100), 2) as vatTotal')
        ->join('o.payments', 'p')
        ->join('p.qualifiedVat', 'v')
        ->join('v.rate', 'vr')
        ->join('p.transactions', 't')
        ->groupBy('country')
        ->where('o.status = :status')
        ->andWhere('t.status = :status')
        ->andWhere('t.createdAt BETWEEN :from AND :to')
        ->setParameter('status', Payment::PAYMENT_STATUS_COMPLETED)
        ->setParameter('from', $dateFrom)
        ->setParameter('to', $dateTo)
        ->getQuery();
    }
}
