<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\User;
use EaPaysites\Entity\Repository\AbstractRepository;

/**
 * Class UserRepository
 * @package AppBundle\Entity\Repository
 */
class UserRepository extends AbstractRepository
{
    public function storeGoogleId(User $user, $googleId)
    {
        $user->setGoogleId($googleId);

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
    }
}
