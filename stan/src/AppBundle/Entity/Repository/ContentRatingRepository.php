<?php

namespace AppBundle\Entity\Repository;

use EaPaysites\Entity\Repository\AbstractRepository;

class ContentRatingRepository extends AbstractRepository
{
    /**
     * @param int    $customerId
     * @param string $contentType
     * @param int    $contentId
     * @param int    $value
     *
     * @return null|object
     */
    public function insertOrUpdate(int $customerId, string $contentType, int $contentId, int $value)
    {
        $conn  = $this->getEntityManager()->getConnection();
        $table = $this->getEntityManager()->getClassMetadata($this->getEntityName())->getTableName();

        $sql = "INSERT INTO {$conn->quoteIdentifier($table)} 
                    (customer_id, content_type, content_id, value, created_at, updated_at)
                VALUES (:customerId, :contentType, :contentId, :value, :createdAt, :createdAt)
                ON DUPLICATE KEY UPDATE
                    value = :value, updated_at = :updatedAt";

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'customerId'  => $customerId,
                'contentType' => $contentType,
                'contentId'   => $contentId,
                'value'       => $value,
                'createdAt'   => date('Y-m-d H:i:s'),
                'updatedAt'   => date('Y-m-d H:i:s')
            ]
        );

        $contentRating = $this->findOneBy(
            ['contentType' => $contentType, 'contentId' => $contentId, 'customer' => $customerId]
        );

        return $contentRating;
    }

    /**
     * @param string $contentType
     * @param array  $contentIds
     *
     * @return array
     */
    public function getAggregateRating(string $contentType, array $contentIds): array
    {
        $dql = "
            SELECT 
                cr.contentId,
                SUM(CASE WHEN cr.value > 0 THEN 1 ELSE 0 END) AS positiveVotes,
                COUNT(cr.id) AS totalVotes
            FROM AppBundle:ContentRating cr
            WHERE cr.contentType = :contentType AND cr.contentId IN (:contentIds)
            GROUP BY cr.contentId";

        $em    = $this->getEntityManager();
        $query = $em->createQuery($dql)
                    ->setParameter('contentType', $contentType)
                    ->setParameter('contentIds', $contentIds);

        $rows = $query->getArrayResult();

        $contentIdsToRatings = [];
        foreach ($rows as $row) {
            $contentIdsToRatings[$row['contentId']] = $row['positiveVotes'] / $row['totalVotes'];
        }

        return $contentIdsToRatings;
    }
}
