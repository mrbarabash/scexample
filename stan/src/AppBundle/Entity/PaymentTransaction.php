<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Table(name="payment_transactions")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PaymentTransactionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PaymentTransaction extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(length=25)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(length=40)
     */
    private $ip;

    /**
     * TODO Since doctrine/orm rename type to "json".
     *
     * @var array
     *
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $gatewayTransactionData;

    /**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment", inversedBy="transactions")
     */
    private $payment;

    public function __construct()
    {
        $this->status = Payment::PAYMENT_STATUS_NEW;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return array
     */
    public function getGatewayTransactionData(): array
    {
        return $this->gatewayTransactionData;
    }

    /**
     * @param array $gatewayTransactionData
     */
    public function setGatewayTransactionData(array $gatewayTransactionData)
    {
        $this->gatewayTransactionData = $gatewayTransactionData;
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->getStatus() == Payment::PAYMENT_STATUS_PENDING;
    }

    /**
     * @return bool
     */
    public function isFailed(): bool
    {
        return $this->getStatus() == Payment::PAYMENT_STATUS_FAILED;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->getStatus() == Payment::PAYMENT_STATUS_COMPLETED;
    }

    /**
     * @return bool
     */
    public function isCancelled(): bool
    {
        return $this->getStatus() == Payment::PAYMENT_STATUS_CANCELED;
    }
}
