<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 * @ORM\Table(name="sessions")
 * @ORM\Entity
 */
class Session
{
    /**
     * @var string
     * @ORM\Column(name="sess_id", type="string", length=128)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessId;

    /**
     * @var string
     * @ORM\Column(name="sess_data", type="blob")
     */
    private $sessData;

    /**
     * @var integer
     * @ORM\Column(name="sess_time", type="integer")
     */
    private $sessTime;

    /**
     * @var integer
     * @ORM\Column(name="sess_lifetime", type="integer")
     */
    private $sessLifetime;
}
