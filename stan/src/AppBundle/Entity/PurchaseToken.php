<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PurchaseTokenRepository")
 * @ORM\Table(name="purchase_tokens")
 * @ORM\HasLifecycleCallbacks
 */
class PurchaseToken extends AbstractEntity
{
    const KIND_SCENE = 'scene';
    const KIND_CATEGORY = 'category';

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var Website
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Website")
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(length=25)
     */
    private $kind;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $usedFor;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer", inversedBy="purchaseTokens")
     */
    private $customer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $usedAt;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return Website
     */
    public function getWebsite(): Website
    {
        return $this->website;
    }

    /**
     * @param Website $website
     */
    public function setWebsite(Website $website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     */
    public function setKind(string $kind)
    {
        $this->kind = $kind;
    }

    /**
     * @return int
     */
    public function getUsedFor(): ?int
    {
        return $this->usedFor;
    }

    /**
     * @param int|null $usedFor
     */
    public function setUsedFor(int $usedFor = null)
    {
        $this->usedFor = $usedFor;
    }

    /**
     * @return bool
     */
    public function isUsed(): bool
    {
        return (bool) $this->usedFor;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUsedAt(): ?\DateTime
    {
        return $this->usedAt;
    }

    /**
     * Call this method without argument to set current time.
     *
     * @param \DateTime $usedAt
     */
    public function setUsedAt(\DateTime $usedAt = null)
    {
        $this->usedAt = $usedAt ?: new \DateTime();
    }
}
