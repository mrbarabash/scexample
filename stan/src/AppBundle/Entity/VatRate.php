<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\VatRateRepository")
 * @ORM\Table(name="vat_rates")
 * @ORM\HasLifecycleCallbacks
 */
class VatRate extends AbstractEntity
{
    const FALLBACK_COUNTRY_CODE = '00';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Country code.
     *
     * @var string
     *
     * @ORM\Column(length=2)
     */
    private $country;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $rate;

    /**
     * @var |DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $archivedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     */
    public function setRate(int $rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime|null
     */
    public function getArchivedAt(): ?\DateTime
    {
        return $this->archivedAt;
    }

    /**
     * @param \DateTime|null $archivedAt
     */
    public function setArchivedAt(\DateTime $archivedAt = null)
    {
        $this->archivedAt = $archivedAt;
    }

    /**
     * @return bool
     */
    public function isArchived(): bool
    {
        return (bool) $this->getArchivedAt();
    }
}
