<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Table(name="content_permissions")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ContentPermissionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ContentPermission extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer", inversedBy="contentPermissions")
     */
    private $customer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $contentId;

    /**
     * @var string
     *
     * @ORM\Column(length=100)
     */
    private $contentType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $grantedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @var string
     *
     * @ORM\Column(length=50)
     */
    private $permission;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $grantedVia;

    /**
     * @var string
     *
     * @ORM\Column(length=100)
     */
    private $grantedViaType;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return int
     */
    public function getContentId(): int
    {
        return $this->contentId;
    }

    /**
     * @param int $contentId
     */
    public function setContentId(int $contentId)
    {
        $this->contentId = $contentId;
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     */
    public function setContentType(string $contentType)
    {
        $this->contentType = $contentType;
    }

    /**
     * @return \DateTime
     */
    public function getGrantedAt(): \DateTime
    {
        return $this->grantedAt;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setGrantedAtValue()
    {
        $this->grantedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt(): ?\DateTime
    {
        return $this->expiresAt;
    }

    /**
     * @param \DateTime|null $expiresAt
     */
    public function setExpiresAt(\DateTime $expiresAt = null)
    {
        $this->expiresAt = $expiresAt;
    }

    /**
     * @return string
     */
    public function getPermission(): string
    {
        return $this->permission;
    }

    /**
     * @param string $permission
     */
    public function setPermission(string $permission)
    {
        $this->permission = $permission;
    }

    /**
     * @return string
     */
    public function getGrantedVia(): string
    {
        return $this->grantedVia;
    }

    /**
     * @param string $grantedVia
     */
    public function setGrantedVia(string $grantedVia)
    {
        $this->grantedVia = $grantedVia;
    }

    /**
     * @return string
     */
    public function getGrantedViaType(): string
    {
        return $this->grantedViaType;
    }

    /**
     * @param string $grantedViaType
     */
    public function setGrantedViaType(string $grantedViaType)
    {
        $this->grantedViaType = $grantedViaType;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->getExpiresAt() && time() > $this->getExpiresAt()->getTimestamp();
    }
}
