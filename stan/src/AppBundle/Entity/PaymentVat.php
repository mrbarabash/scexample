<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Table(name="payment_vat_rates")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PaymentVatRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PaymentVat extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment")
     */
    private $payment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var VatRate
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\VatRate")
     */
    private $rate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->rate->getCountry();
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate->getRate();
    }

    /**
     * @return VatRate
     */
    public function getVatRate(): VatRate
    {
        return $this->rate;
    }

    /**
     * @param VatRate $vatRate
     */
    public function setVatRate(VatRate $vatRate)
    {
        $this->rate = $vatRate;
    }
}
