<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EaPaysites\Entity\AbstractEntity;

/**
 * @ORM\Table(name="payments")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PaymentRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Payment extends AbstractEntity
{
    const PAYMENT_GATEWAY_RG = 'rocket_gate';

    const PAYMENT_CURRENCY_USD = 'usd';

    const PAYMENT_STATUS_NEW = 'new';
    const PAYMENT_STATUS_PENDING = 'pending';
    const PAYMENT_STATUS_CANCELED = 'canceled';
    const PAYMENT_STATUS_COMPLETED = 'completed';
    const PAYMENT_STATUS_FAILED = 'failed';

    const PAYMENT_BILLING_METHOD_CC = 'cc';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order", inversedBy="payments")
     */
    private $order;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var PaymentVat|null
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\PaymentVat", cascade={"persist"})
     */
    private $qualifiedVat;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=7, scale=2)
     */
    private $amount;

    /**
     * @var PaymentTransaction[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PaymentTransaction", mappedBy="payment", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $transactions;

    /**
     * @var string
     *
     * @ORM\Column(length=100)
     */
    private $gateway = self::PAYMENT_GATEWAY_RG;

    /**
     * @var string
     *
     * @ORM\Column(length=3)
     */
    private $currency = self::PAYMENT_CURRENCY_USD;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return PaymentVat|null
     */
    public function getQualifiedVat(): ?PaymentVat
    {
        return $this->qualifiedVat;
    }

    /**
     * @param PaymentVat|null $qualifiedVat
     */
    public function setQualifiedVat(PaymentVat $qualifiedVat = null)
    {
        $this->qualifiedVat = $qualifiedVat;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     *
     * @return PaymentTransaction[]|ArrayCollection
     */
    public function getTransactions(\DateTime $from = null, \DateTime $to = null)
    {
        $transactions = $this->transactions;
        if ($from) {
            $transactions = $transactions->filter(function (PaymentTransaction $transaction) use ($from) {
                return $transaction->getCreatedAt()->getTimestamp() >= $from->getTimestamp();
            });
        }
        if ($to) {
            $transactions = $transactions->filter(function (PaymentTransaction $transaction) use ($to) {
                return $transaction->getCreatedAt()->getTimestamp() <= $to->getTimestamp();
            });
        }

        return $transactions;
    }

    /**
     * @param iterable $transactions
     */
    public function setTransactions(iterable $transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @param PaymentTransaction $transaction
     */
    public function addTransaction(PaymentTransaction $transaction)
    {
        $this->transactions->add($transaction);
    }

    /**
     * @return PaymentTransaction|null
     */
    public function getLastTransaction(): ?PaymentTransaction
    {
        return $this->transactions->last() ?: null;
    }

    /**
     * @return string
     */
    public function getGateway(): string
    {
        return $this->gateway;
    }

    /**
     * @param string $gateway
     */
    public function setGateway(string $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->getLastTransaction() ? $this->getLastTransaction()->getStatus() : Payment::PAYMENT_STATUS_NEW;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->getStatus() == self::PAYMENT_STATUS_COMPLETED;
    }
}
