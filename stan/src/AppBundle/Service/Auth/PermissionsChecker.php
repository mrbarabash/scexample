<?php

namespace AppBundle\Service\Auth;

use Sensio\Bundle\FrameworkExtraBundle\EventListener\ControllerListener;
use Sensio\Bundle\FrameworkExtraBundle\EventListener\SecurityListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PermissionsChecker
{
    /**
     * @var HttpKernelInterface
     */
    private $httpKernel;

    /**
     * @var ControllerListener
     */
    private $controllerListener;

    /**
     * @var SecurityListener
     */
    private $securityListener;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @param HttpKernelInterface $httpKernel
     * @param ControllerListener  $controllerListener
     * @param SecurityListener    $securityListener
     * @param TokenStorage        $tokenStorage
     */
    public function __construct(
        HttpKernelInterface $httpKernel,
        ControllerListener $controllerListener,
        SecurityListener $securityListener,
        TokenStorage $tokenStorage
    )
    {
        $this->httpKernel         = $httpKernel;
        $this->controllerListener = $controllerListener;
        $this->securityListener   = $securityListener;
        $this->tokenStorage       = $tokenStorage;
    }

    /**
     * @param string $controller
     * @param string $action
     * @param array  $params
     *
     * @return bool
     */
    public function check($controller, $action, $params = [])
    {
        $controllerClass = '\\AppBundle\\Controller\\' . ucfirst($controller) . 'Controller';
        if (!class_exists($controllerClass)) {
            throw new \InvalidArgumentException("Controller class not found");
        }

        $controllerObject = new $controllerClass();
        $actionName       = $action . 'Action';

        if (!is_callable([$controllerObject, $actionName])) {
            throw new \InvalidArgumentException("Method $actionName not found in $controllerClass");
        }

        $filterControllerEvent = new FilterControllerEvent(
            $this->httpKernel,
            [$controllerObject, $actionName],
            new Request($params),
            HttpKernelInterface::SUB_REQUEST
        );

        $this->controllerListener->onKernelController($filterControllerEvent);

        try {
            $this->securityListener->onKernelController($filterControllerEvent);

            return true;
        } catch (AccessDeniedException $e) {
            return false;
        }
    }
}
