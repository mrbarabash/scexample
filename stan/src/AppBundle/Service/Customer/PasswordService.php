<?php

namespace AppBundle\Service\Customer;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Repository\CustomerRepository;
use AppBundle\Service\Customer\Password\ChangeCustomerPasswordRequest;
use AppBundle\Service\Customer\Password\ChangeCustomerPasswordResponse;
use AppBundle\Service\Email\EmailSenderInterface;
use EaPaysites\Util\Passwords;
use EaPaysites\Util\Validators;
use Particle\Validator\ValidationResult;
use Particle\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PasswordService
{
    /**
     * @var CustomerRepository
     */
    protected $customerRepo;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var EmailSenderInterface
     */
    private $emailSender;

    /**
     * PasswordService constructor.
     *
     * @param CustomerRepository   $customerRepo
     * @param ValidatorInterface   $validator
     * @param EmailSenderInterface $emailSender
     */
    public function __construct(
        CustomerRepository $customerRepo,
        ValidatorInterface $validator,
        EmailSenderInterface $emailSender
    ) {
        $this->customerRepo = $customerRepo;
        $this->validator    = $validator;
        $this->emailSender  = $emailSender;
    }

    /**
     * @param ChangeCustomerPasswordRequest $serviceRequest
     *
     * @return ChangeCustomerPasswordResponse
     */
    public function changePassword(ChangeCustomerPasswordRequest $serviceRequest): ChangeCustomerPasswordResponse
    {
        $serviceResponse = new ChangeCustomerPasswordResponse();

        $validationResult = $this->validateNewPassword($serviceRequest);
        if (!$validationResult->isValid()) {
            $serviceResponse->error = Validators::getBadRequestServiceResponse(
                $validationResult->getMessages()
            );

            return $serviceResponse;
        }

        $passwordHash                  = $this->updatePassword($serviceRequest->customerId, $serviceRequest->password);
        $serviceResponse->passwordHash = $passwordHash;

        return $serviceResponse;
    }

    /**
     * @param ChangeCustomerPasswordRequest $serviceRequest
     *
     * @return ValidationResult
     */
    private function validateNewPassword(ChangeCustomerPasswordRequest $serviceRequest): ValidationResult
    {
        $v = new Validator();
        $v->required('password')
          ->lengthBetween(8, 256);

        return $v->validate($serviceRequest->toArray());
    }

    /**
     * @param int    $customerId
     * @param string $password
     *
     * @return string
     */
    private function updatePassword(int $customerId, string $password): string
    {
        /** @var Customer $customer */
        $customer = $this->customerRepo->find($customerId);

        $passwordHash = Passwords::encodePassword($password);
        $customer->setPassword($passwordHash);

        $this->customerRepo->store($customer, true);

        return $passwordHash;
    }
}
