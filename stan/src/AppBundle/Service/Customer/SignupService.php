<?php

namespace AppBundle\Service\Customer;

use AppBundle\Entity\Customer;
use AppBundle\Entity\PurchaseToken;
use AppBundle\Entity\Repository\{
    CustomerRepository, WebsiteRepository
};
use AppBundle\Entity\Website;
use AppBundle\Service\Customer\Signup\SignupRequest;
use AppBundle\Service\Customer\Signup\SignupResponse;
use AppBundle\Service\Email\EmailSenderInterface;
use EaPaysites\Util\Filters;
use EaPaysites\Util\Passwords;
use EaPaysites\Util\Services;
use EaPaysites\Util\Validators;
use Particle\Validator\ValidationResult;
use Particle\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SignupService
{
    /**
     * @var CustomerRepository
     */
    protected $customerRepo;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var WebsiteRepository
     */
    private $websiteRepo;

    /**
     * @var EmailSenderInterface
     */
    private $emailSender;

    /**
     * @param CustomerRepository   $customerRepo
     * @param WebsiteRepository    $websiteRepository
     * @param ValidatorInterface   $validator
     * @param EmailSenderInterface $emailSender
     */
    public function __construct(
        CustomerRepository $customerRepo,
        WebsiteRepository $websiteRepository,
        ValidatorInterface $validator,
        EmailSenderInterface $emailSender
    ) {
        $this->customerRepo = $customerRepo;
        $this->websiteRepo  = $websiteRepository;
        $this->validator    = $validator;
        $this->emailSender  = $emailSender;
    }

    /**
     * @param SignupRequest $serviceRequest
     *
     * @return SignupResponse
     */
    public function signup(SignupRequest $serviceRequest): SignupResponse
    {
        $serviceResponse = new SignupResponse();

        $validationResult = $this->validate($serviceRequest);
        if (!$validationResult->isValid()) {
            Services::setValidationError($serviceResponse, $validationResult);

            return $serviceResponse;
        }

        $serviceResponse->customer      = $this->createCustomer($serviceRequest);
        $serviceResponse->purchaseToken = $this->createFreeSceneToken($serviceRequest);
        $serviceResponse->purchaseToken->setCustomer($serviceResponse->customer);
        $serviceResponse->customer->addPurchaseToken($serviceResponse->purchaseToken);

        $this->customerRepo->store($serviceResponse->customer, true);

        return $serviceResponse;
    }

    /**
     * @param SignupRequest $signupRequest
     *
     * @return ValidationResult
     */
    private function validate(SignupRequest $signupRequest): ValidationResult
    {
        $v = new Validator();
        $v->required('email')
          ->email()
          ->lengthBetween(null, 100)
          ->callback(
              Validators::getDuplicateEntityRule(
                  [$this->customerRepo, 'isUniqueEmail'],
                  'User already exists'
              )
          );
        $v->required('password')
          ->lengthBetween(8, 256);
        $v->optional('name')
          ->lengthBetween(null, 128)
          ->callback(Validators::getGraphCharactersRule());

        return $v->validate(get_object_vars($signupRequest));
    }

    /**
     * @param SignupRequest $serviceRequest
     *
     * @return Customer
     */
    private function createCustomer(SignupRequest $serviceRequest): Customer
    {
        $customer = new Customer();

        $customer->setEmail($serviceRequest->email);

        $canonicalEmail = Filters::getCanonicalEmail($serviceRequest->email);
        $customer->setEmailCanonical($canonicalEmail);

        $passwordHash = Passwords::encodePassword($serviceRequest->password);
        $customer->setPassword($passwordHash);

        if (!empty($serviceRequest->name)) {
            $customer->setName($serviceRequest->name);
        }

        $customer->setCreationDateTime(new \DateTime());
        $customer->setEmailConfirmed(false);

        return $customer;
    }

    /**
     * @param SignupRequest $serviceRequest
     *
     * @return PurchaseToken
     */
    private function createFreeSceneToken(SignupRequest $serviceRequest): PurchaseToken
    {
        /** @var Website $website */
        $website = $this->websiteRepo->find($serviceRequest->website);

        $token = new PurchaseToken();
        $token->setKind('Scene'); // TODO Put Scene to PS to generate the name automatically?
        $token->setWebsite($website);

        return $token;
    }
}
