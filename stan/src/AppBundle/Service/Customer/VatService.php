<?php

namespace AppBundle\Service\Customer;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Entity\Repository\CustomerRepository;
use AppBundle\Entity\Repository\VatRateRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\VatRate;

class VatService
{
    /**
     * @var CustomerRepository
     */
    private $customerRepo;

    /**
     * @var VatRateRepository
     */
    private $vatRateRepo;

    /**
     * @param CustomerRepository $customerRepository
     * @param VatRateRepository $vatRateRepository
     */
    public function __construct(CustomerRepository $customerRepository, VatRateRepository $vatRateRepository)
    {
        $this->customerRepo = $customerRepository;
        $this->vatRateRepo = $vatRateRepository;
    }

    /**
     * @param User|string $customer
     * @param string      $country
     *
     * @return VatRate
     */
    public function getCustomerVatRate($customer, string $country): VatRate
    {
        $vat = null;
        /** @var Customer $customer */
        if ($customer = $this->customerRepo->find($customer)) {
            /** @var Order $lastOrder */
            $lastOrder = $customer->getOrders()->filter(function (Order $order) {
                return $order->getLastPayment() ? $order->getLastPayment()->isCompleted() : false;
            })->last();
            if ($lastOrder && $lastPayment = $lastOrder->getLastPayment()) {
                $vat = $lastPayment->getQualifiedVat()->getVatRate();
            }
        }
        if (!$vat) {
            /** @var VatRate $vat */
            $vat = $this->vatRateRepo->findActiveByCountry($country);
        }

        return $vat;
    }
}
