<?php

namespace AppBundle\Service\Customer\Password;

use EaPaysites\Service\AbstractServiceResponse;

class ChangeCustomerPasswordResponse extends AbstractServiceResponse
{
    /**
     * @var string
     */
    public $passwordHash;
}
