<?php

namespace AppBundle\Service\Customer\Password;

use EaPaysites\Service\AbstractServiceRequest;
use JMS\Serializer\Annotation as Serializer;

class ChangeCustomerPasswordRequest extends AbstractServiceRequest
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $customerId;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $password;
}
