<?php

namespace AppBundle\Service\Customer\Signup;

use AppBundle\Entity\Customer;
use AppBundle\Entity\PurchaseToken;
use EaPaysites\Service\AbstractServiceResponse;

class SignupResponse extends AbstractServiceResponse
{
    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var PurchaseToken
     */
    public $purchaseToken;
}
