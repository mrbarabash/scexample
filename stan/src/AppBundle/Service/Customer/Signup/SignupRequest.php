<?php

namespace AppBundle\Service\Customer\Signup;

use EaPaysites\Service\AbstractServiceRequest;

class SignupRequest extends AbstractServiceRequest
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $website;
}
