<?php

namespace AppBundle\Service\Customer;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Repository\CustomerRepository;
use EaPaysites\Service\Customer\Sync\CustomerSyncRequest;
use EaPaysites\Sqs\SqsProducer;

class EditService
{
    /**
     * @var CustomerRepository
     */
    private $customerRepo;

    private $sqsProducer;

    /**
     * EditService constructor.
     *
     * @param CustomerRepository $customerRepo
     */
    public function __construct(
        CustomerRepository $customerRepo,
        SqsProducer $sqsProducer
    ) {
        $this->customerRepo = $customerRepo;
        $this->sqsProducer  = $sqsProducer;
    }

    /**
     * @param Customer $customerOriginal
     * @param Customer $customer
     */
    public function edit(Customer $customerOriginal, Customer $customer)
    {
        $this->customerRepo->store($customer, true);

        $customerSyncRequest = new CustomerSyncRequest();
        $customerSyncRequest->populate($customer->toArray());

        $this->sqsProducer->send('sync_customer_bm', $customerSyncRequest);
    }
}
