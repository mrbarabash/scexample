<?php

namespace AppBundle\Service\ContentRating\RateContent;

use EaPaysites\Service\AbstractServiceRequest;

class RateContentRequest extends AbstractServiceRequest
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $contentType;

    /**
     * @var int
     */
    public $contentId;

    /**
     * @var int
     */
    public $value;
}
