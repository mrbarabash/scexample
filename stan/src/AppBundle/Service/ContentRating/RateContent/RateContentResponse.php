<?php

namespace AppBundle\Service\ContentRating\RateContent;

use AppBundle\Entity\ContentRating;
use EaPaysites\Service\AbstractServiceResponse;

class RateContentResponse extends AbstractServiceResponse
{
    /**
     * @var ContentRating
     */
    public $contentRating;
}
