<?php

namespace AppBundle\Service\ContentRating;

use AppBundle\Entity\Repository\ContentRatingRepository;
use AppBundle\Entity\Repository\CustomerRepository;
use AppBundle\Service\ContentRating;
use AppBundle\Service\ContentRating\RateContent\RateContentRequest;
use AppBundle\Service\ContentRating\RateContent\RateContentResponse;
use EaPaysites\Util\Services;
use EaPaysites\Util\Validators;
use Particle\Validator\ValidationResult;
use Particle\Validator\Validator;

class ContentRatingService
{
    /**
     * @var CustomerRepository
     */
    private $customerRepo;

    /**
     * @var ContentRatingRepository
     */
    private $contentRatingRepo;

    /**
     * ContentRatingService constructor.
     *
     * @param CustomerRepository      $userRepo
     * @param ContentRatingRepository $contentRatingRepo
     */
    public function __construct(CustomerRepository $userRepo, ContentRatingRepository $contentRatingRepo)
    {
        $this->customerRepo      = $userRepo;
        $this->contentRatingRepo = $contentRatingRepo;
    }

    /**
     * @param RateContentRequest $request
     *
     * @return RateContentResponse
     */
    public function rateContent(RateContentRequest $request): RateContentResponse
    {
        $response = new RateContentResponse();

        $validationResult = $this->validateRateContent($request);
        if (!$validationResult->isValid()) {
            Services::setValidationError($response, $validationResult);

            return $response;
        }

        $contentRating = $this->contentRatingRepo->insertOrUpdate(
            $request->userId,
            $request->contentType,
            $request->contentId,
            $request->value
        );

        $response->contentRating = $contentRating;

        return $response;
    }

    /**
     * @param GetRatingByUser\Request $request
     *
     * @return GetRatingByUser\Response
     */
    public function getRatingByUser(ContentRating\GetRatingByUser\Request $request
    ): ContentRating\GetRatingByUser\Response {
        $response = new ContentRating\GetRatingByUser\Response();

        $validationResult = $this->validateGetRatingByUser($request);
        if (!$validationResult->isValid()) {
            Services::setValidationError($response, $validationResult);

            return $response;
        }

        $contentRating = $this->contentRatingRepo->findOneBy(
            [
                'customer'    => $request->userId,
                'contentId'   => $request->contentId,
                'contentType' => $request->contentType
            ]
        );

        $response->contentRating = $contentRating;

        return $response;
    }

    /**
     * @param GetAggregateRating\Request $request
     *
     * @return GetAggregateRating\Response
     */
    public function getAggregateRating(ContentRating\GetAggregateRating\Request $request
    ): ContentRating\GetAggregateRating\Response {
        $response = new ContentRating\GetAggregateRating\Response();

        $validationResult = $this->validateGetAggregateRating($request);
        if (!$validationResult->isValid()) {
            Services::setValidationError($response, $validationResult);

            return $response;
        }

        $contentIdsToRatings           = $this->contentRatingRepo->getAggregateRating(
            $request->contentType,
            $request->contentIds
        );
        $response->contentIdsToRatings = $contentIdsToRatings;

        return $response;
    }

    /**
     * @param ContentRating\GetRatingByUser\Request $request
     *
     * @return ValidationResult
     */
    private function validateGetRatingByUser(ContentRating\GetRatingByUser\Request $request): ValidationResult
    {
        $v = new Validator();

        $this->addUserIdValidation($v);
        $this->addContentTypeValidation($v);
        $this->addContentIdValidation($v);

        return $v->validate(get_object_vars($request));
    }

    /**
     * @param ContentRating\GetAggregateRating\Request $request
     *
     * @return ValidationResult
     */
    private function validateGetAggregateRating(ContentRating\GetAggregateRating\Request $request): ValidationResult
    {
        $v = new Validator();

        $this->addContentTypeValidation($v);
        $v->required('contentIds')
          ->isArray();

        return $v->validate(get_object_vars($request));
    }

    /**
     * @param RateContentRequest $request
     *
     * @return ValidationResult
     */
    private function validateRateContent(RateContentRequest $request): ValidationResult
    {
        $v = new Validator();

        $this->addUserIdValidation($v);
        $this->addContentTypeValidation($v);
        $this->addContentIdValidation($v);
        $this->addValueValidation($v);

        return $v->validate(get_object_vars($request));
    }

    /**
     * @param Validator $v
     */
    private function addContentTypeValidation(Validator $v)
    {
        $v->required('contentType')
          ->lengthBetween(3, 100);
    }

    /**
     * @param Validator $v
     */
    private function addValueValidation(Validator $v)
    {
        $v->required('value')
          ->integer()
          ->inArray([-1, 1]);
    }

    /**
     * @param Validator $v
     */
    private function addContentIdValidation(Validator $v)
    {
        $v->required('contentId')
          ->greaterThan(0)
          ->integer();
    }

    /**
     * @param Validator $v
     */
    private function addUserIdValidation(Validator $v)
    {
        $v->required('userId')
          ->greaterThan(0)
          ->digits()
          ->callback(Validators::getEntityExistsRule($this->customerRepo, 'id', 'User not found'));
    }
}
