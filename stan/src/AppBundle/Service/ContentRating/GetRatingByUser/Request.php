<?php

namespace AppBundle\Service\ContentRating\GetRatingByUser;

use EaPaysites\Service\AbstractServiceRequest;

class Request extends AbstractServiceRequest
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $contentType;

    /**
     * @var int
     */
    public $contentId;
}
