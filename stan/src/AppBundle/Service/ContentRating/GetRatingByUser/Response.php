<?php

namespace AppBundle\Service\ContentRating\GetRatingByUser;

use AppBundle\Entity\ContentRating;
use EaPaysites\Service\AbstractServiceResponse;

class Response extends AbstractServiceResponse
{
    /**
     * @var ContentRating
     */
    public $contentRating;
}
