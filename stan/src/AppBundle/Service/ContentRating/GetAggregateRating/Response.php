<?php

namespace AppBundle\Service\ContentRating\GetAggregateRating;

use EaPaysites\Service\AbstractServiceResponse;

class Response extends AbstractServiceResponse
{
    /**
     * @var float[]
     */
    public $contentIdsToRatings;
}
