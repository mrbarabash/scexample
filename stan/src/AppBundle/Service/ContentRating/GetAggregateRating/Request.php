<?php

namespace AppBundle\Service\ContentRating\GetAggregateRating;

use EaPaysites\Service\AbstractServiceRequest;

class Request extends AbstractServiceRequest
{
    /**
     * @var string
     */
    public $contentType;

    /**
     * @var int[]
     */
    public $contentIds;
}
