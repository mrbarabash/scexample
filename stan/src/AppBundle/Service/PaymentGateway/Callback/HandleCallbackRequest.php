<?php

namespace AppBundle\Service\PaymentGateway\Callback;

use EaPaysites\Service\AbstractServiceRequest;

class HandleCallbackRequest extends AbstractServiceRequest
{
    /**
     * @var string
     */
    public $body;
}
