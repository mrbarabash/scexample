<?php

namespace AppBundle\Service\PaymentGateway\Callback;

use EaPaysites\Service\AbstractServiceResponse;

class HandleCallbackResponse extends AbstractServiceResponse
{
    /**
     * @var string|null
     */
    public $body;
}
