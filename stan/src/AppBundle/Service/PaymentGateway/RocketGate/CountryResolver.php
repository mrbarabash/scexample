<?php

namespace AppBundle\Service\PaymentGateway\RocketGate;

class CountryResolver
{
    /**
     * @var string
     */
    private $ipCountry;

    /**
     * @var string|null
     */
    private $customerCountry;

    /**
     * @var string|null
     */
    private $bankCountry;

    /**
     * @param string      $ipCountry
     * @param string|null $customerCountry
     * @param string|null $bankCountry
     */
    public function __construct(string $ipCountry, string $customerCountry = null, string $bankCountry = null)
    {
        $this->ipCountry = strtolower($ipCountry);
        $this->customerCountry = strtolower($customerCountry);
        $this->bankCountry = strtolower($bankCountry);
    }

    /**
     * @return string
     */
    public function getIpCountry(): string
    {
        return $this->ipCountry;
    }

    /**
     * @return null|string
     */
    public function getCustomerCountry(): ?string
    {
        return $this->customerCountry;
    }

    /**
     * @return null|string
     */
    public function getBankCountry(): ?string
    {
        return $this->bankCountry;
    }

    /**
     * @return string
     */
    public function resolveCountry(): string
    {
        $country = $this->ipCountry;
        if ($this->customerCountry || $this->bankCountry) {
            if ($this->customerCountry && !$this->bankCountry) {
                $country = $this->customerCountry;
            } elseif (!$this->customerCountry && $this->bankCountry) {
                $country = $this->bankCountry;
            } else {
                $countries = [$this->ipCountry, $this->customerCountry, $this->bankCountry];

                $countries = array_count_values($countries);
                if (count($countries) == 3) {
                    // All countries are different.
                    $country = $this->bankCountry;
                } else {
                    arsort($countries);
                    $countries = array_keys($countries);

                    $country = array_shift($countries);
                }
            }
        }

        return $country;
    }
}
