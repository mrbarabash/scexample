<?php

namespace AppBundle\Service\PaymentGateway\RocketGate;

class XmlRequest
{
    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @var int|null
     */
    private $errorCode;

    /**
     * @var string|null
     */
    private $errorMessage;

    /**
     * @var array
     */
    private $requiredParameters = ['customerID', 'invoiceID'];

    /**
     * @param string $content
     *
     * @return XmlRequest
     */
    public static function buildFromXml(string $content): self
    {
        $xmlRequest = new self;

        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        if (xml_parse_into_struct($parser, $content, $values, $index) != 0) {
            foreach ($values as $value) {
                if (array_key_exists('value', $value)) {
                    $xmlRequest->setParameter($value['tag'], $value['value']);
                }
            }
        }

        xml_parser_free($parser);

        return $xmlRequest;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getParameter(string $key)
    {
        return array_key_exists($key, $this->parameters) ? $this->parameters[$key] : null;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function setParameter(string $key, $value)
    {
        $this->parameters[$key] = trim($value);
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        $this->validate();

        return is_null($this->getErrorCode()) && is_null($this->getErrorMessage());
    }

    /**
     * @return int|null
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }

    /**
     * @return string|null
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    private function validate()
    {
        $this->validateXmlParameters();
        $this->validateRequiredParameters();
    }

    private function validateXmlParameters()
    {
        if (!$this->getParameters()) {
            $this->setValidationError(4, 'XmlRequestParser failed to parse');
        }
    }

    private function validateRequiredParameters()
    {
        foreach ($this->requiredParameters as $parameter) {
            if (!$this->getParameter($parameter)) {
                $this->setValidationError(4, $parameter.' is missing');

                break;
            }
        }
    }

    /**
     * @param int    $code
     * @param string $message
     */
    private function setValidationError(int $code, string $message)
    {
        if (!$this->errorCode && !$this->errorMessage) {
            $this->errorCode = $code;
            $this->errorMessage = $message;
        }
    }
}
