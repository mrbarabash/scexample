<?php

namespace AppBundle\Service\PaymentGateway\Success;

use EaPaysites\Service\AbstractServiceRequest;
use Symfony\Component\HttpFoundation\Request;

class HandleSuccessRequest extends AbstractServiceRequest
{
    /**
     * @var Request
     */
    public $request;
}
