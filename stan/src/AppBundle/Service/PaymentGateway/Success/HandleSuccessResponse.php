<?php

namespace AppBundle\Service\PaymentGateway\Success;

use EaPaysites\Service\AbstractServiceResponse;

class HandleSuccessResponse extends AbstractServiceResponse
{
    /**
     * @var string
     */
    public $redirectUrl;
}
