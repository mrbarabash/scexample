<?php

namespace AppBundle\Service\PaymentGateway;

use AppBundle\Entity\{Customer, Order};
use EaPaysites\Service\{AbstractServiceRequest, AbstractServiceResponse};

interface PaymentGatewayServiceInterface
{
    /**
     * @param Customer $customer
     * @param Order    $order
     * @param string   $locale
     *
     * @return string
     */
    public function getPurchaseUrl(Customer $customer, Order $order, string $locale): string;

    /**
     * @param AbstractServiceRequest $request
     *
     * @return AbstractServiceResponse
     */
    public function handleCallback(AbstractServiceRequest $request): AbstractServiceResponse;

    /**
     * @param AbstractServiceRequest $request
     *
     * @return AbstractServiceResponse
     */
    public function handleSuccess(AbstractServiceRequest $request): AbstractServiceResponse;

    /**
     * @param AbstractServiceRequest $request
     *
     * @return AbstractServiceResponse
     */
    public function handleFailure(AbstractServiceRequest $request): AbstractServiceResponse;
}
