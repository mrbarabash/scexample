<?php

namespace AppBundle\Service\PaymentGateway\Failure;

use EaPaysites\Service\AbstractServiceRequest;
use Symfony\Component\HttpFoundation\Request;

class HandleFailureRequest extends AbstractServiceRequest
{
    /**
     * @var Request
     */
    public $request;
}
