<?php

namespace AppBundle\Service\PaymentGateway\Failure;

use EaPaysites\Service\AbstractServiceResponse;

class HandleFailureResponse extends AbstractServiceResponse
{
    /**
     * @var string
     */
    public $redirectUrl;
}
