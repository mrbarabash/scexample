<?php

namespace AppBundle\Service\PaymentGateway;

use AppBundle\Entity\ContentPermission;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Factory\ContentPermissionFactory;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderProduct;
use AppBundle\Entity\Payment;
use AppBundle\Entity\PaymentTransaction;
use AppBundle\Entity\PaymentVat;
use AppBundle\Entity\Product;
use AppBundle\Entity\Repository\ContentPermissionRepository;
use AppBundle\Entity\Repository\OrderRepository;
use AppBundle\Entity\Repository\PaymentRepository;
use AppBundle\Entity\Repository\VatRateRepository;
use AppBundle\Service\GeoIpService;
use AppBundle\Service\PaymentGateway\Callback\{
    HandleCallbackRequest, HandleCallbackResponse
};
use AppBundle\Service\PaymentGateway\Failure\{
    HandleFailureRequest, HandleFailureResponse
};
use AppBundle\Service\PaymentGateway\RocketGate\CountryResolver;
use AppBundle\Service\PaymentGateway\RocketGate\XmlRequest;
use AppBundle\Service\PaymentGateway\Success\{
    HandleSuccessRequest, HandleSuccessResponse
};
use EaPaysites\Entity\Website;
use EaPaysites\RpcTask;
use EaPaysites\Service\{
    AbstractServiceRequest, AbstractServiceResponse, ServiceResponseError
};
use EaPaysites\Sqs\SqsProducer;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Router;

class RocketGateService implements PaymentGatewayServiceInterface
{
    /**
     * @var string
     */
    private $blackmailedDomain;

    /**
     * @var string
     */
    private $blackmailedSchema;

    /**
     * @var string
     */
    private $stanDomain;

    /**
     * @var string
     */
    private $stanSchema;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $sharedSecret;

    /**
     * @var int
     */
    private $merchantId;

    /**
     * @var string
     */
    private $currencyCode = 'USD';

    /**
     * @var string
     */
    private $billingMethod = 'cc';

    /**
     * @var string
     */
    private $avs;

    /**
     * @var string
     */
    private $scrub;

    /**
     * @var string
     */
    private $descriptor;

    /**
     * @var string
     */
    private $tosUrl;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var VatRateRepository
     */
    private $vatRateRepository;

    /**
     * @var ContentPermissionRepository
     */
    private $contentPermissionRepository;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var SqsProducer
     */
    private $sqsProducer;

    /**
     * @var GeoIpService
     */
    private $geoIpService;

    /**
     * @var string
     */
    private $iframeCssUrl;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param string                      $blackmailedDomain
     * @param string                      $blackmailedSchema
     * @param string                      $stanDomain
     * @param string                      $stanSchema
     * @param string                      $baseUrl
     * @param string                      $sharedSecret
     * @param string                      $merchantId
     * @param string                      $avs
     * @param string                      $scrub
     * @param string                      $descriptor
     * @param string                      $tosUrl
     * @param OrderRepository             $orderRepository
     * @param PaymentRepository           $paymentRepository
     * @param VatRateRepository           $vatRateRepository
     * @param ContentPermissionRepository $contentPermissionRepository
     * @param Router                      $router
     * @param SqsProducer                 $sqsProducer
     * @param GeoIpService                $geoIpService
     * @param string                      $iframeCssUrl
     */
    public function __construct(
        string $blackmailedDomain,
        string $blackmailedSchema,
        string $stanDomain,
        string $stanSchema,
        string $baseUrl,
        string $sharedSecret,
        string $merchantId,
        string $avs,
        string $scrub,
        string $descriptor,
        string $tosUrl,
        OrderRepository $orderRepository,
        PaymentRepository $paymentRepository,
        VatRateRepository $vatRateRepository,
        ContentPermissionRepository $contentPermissionRepository,
        Router $router,
        SqsProducer $sqsProducer,
        GeoIpService $geoIpService,
        string $iframeCssUrl,
        LoggerInterface $logger
    ) {
        $this->blackmailedDomain           = $blackmailedDomain;
        $this->blackmailedSchema           = $blackmailedSchema;
        $this->stanDomain                  = $stanDomain;
        $this->stanSchema                  = $stanSchema;
        $this->baseUrl                     = $baseUrl;
        $this->sharedSecret                = $sharedSecret;
        $this->merchantId                  = $merchantId;
        $this->avs                         = $avs;
        $this->scrub                       = $scrub;
        $this->descriptor                  = $descriptor;
        $this->tosUrl                      = $tosUrl;
        $this->orderRepository             = $orderRepository;
        $this->paymentRepository           = $paymentRepository;
        $this->vatRateRepository           = $vatRateRepository;
        $this->contentPermissionRepository = $contentPermissionRepository;
        $this->router                      = $router;
        $this->sqsProducer                 = $sqsProducer;
        $this->geoIpService                = $geoIpService;
        $this->iframeCssUrl                = $iframeCssUrl;
        $this->logger                      = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function getPurchaseUrl(Customer $customer, Order $order, string $locale): string
    {
        $slug       = '/hostedpage/servlet/HostedPagePurchase';
        $parameters = [
            'id'       => $customer->getId(),
            'merch'    => $this->merchantId,
            'amount'   => $order->getLastPayment()->getAmount(),
            'purchase' => 'yes',
            'currency' => $this->currencyCode,
            'method'   => $this->billingMethod,
            'avs'      => $this->avs,
            //            'scrub' => $this->scrub, // TODO Disable for DEV environment.
            'email'    => $customer->getEmail(),
            'invoice'  => $order->getId(),
            'lang'     => strtoupper($locale),
            'username' => $customer->getEmailCanonical(),
            'siteid'   => Website::BLACKMAILED,
            //            'descriptor' => $this->descriptor, // TODO Discuss with Ken when it would be available.
            'time'     => time(),
            'tos'      => $this->tosUrl,
            'success'  => $this->getStanFullDomain().$this->router->generate(
                'paymentGatewaySuccess',
                ['gateway' => 'rg']
            ),
            'fail'     => $this->getStanFullDomain().$this->router->generate(
                'paymentGatewayFailure',
                ['gateway' => 'rg']
            ),
            'style'    => $this->iframeCssUrl
        ];

        // TODO Handle it properly when multiple orders would possible.
        /** @var OrderProduct $orderProduct */
        $orderProduct = $order->getProducts()->first();
        if ($orderProduct->getProduct()->isRecurrent()) {
            $parameters['rebill-amount'] = $order->getLastPayment()->getAmount();
            $parameters['rebill-count']  = 1000000;
            $parameters['rebill-freq']   = $this->getBillingFrequencyParameter(
                $orderProduct->getProduct()->getRecurrentFrequency()
            );
        }

        $secretHashParameter = ['secret' => $this->sharedSecret];
        $sha1                = sha1(urldecode(http_build_query(array_merge($parameters, $secretHashParameter))), true);
        $parameters['hash']  = base64_encode($sha1);

        return $this->baseUrl . $slug . '?' . http_build_query($parameters);
    }

    /**
     * @param HandleCallbackRequest|AbstractServiceRequest $request
     *
     * @return HandleCallbackResponse|AbstractServiceResponse
     */
    public function handleCallback(AbstractServiceRequest $request): AbstractServiceResponse
    {
        $this->logger->debug('Receive request from RocketGate', ['request' => $request->body]);

        $response = new HandleCallbackResponse();

        $xmlRequest = XmlRequest::buildFromXml($request->body);
        if (!$xmlRequest->isValid()) {
            $response->body = $this->buildPostBackResponse(
                $xmlRequest->getErrorCode(),
                $xmlRequest->getErrorMessage()
            );

            $this->logger->error(
                'XML request is invalid',
                ['code' => $xmlRequest->getErrorCode(), 'message' => $xmlRequest->getErrorMessage()]
            );

            return $response;
        }

        /** @var Order $order */
        $order = $this->orderRepository->find($xmlRequest->getParameter('invoiceID'));
        if (!$order) {
            $response->body = $this->buildPostBackResponse(5, 'Cannot find Order');

            $this->logger->error('Cannot find order', ['order' => $xmlRequest->getParameter('invoiceID')]);

            return $response;
        }

        $payment     = $order->getLastPayment();
        $transaction = new PaymentTransaction();
        $transaction->setStatus(Payment::PAYMENT_STATUS_PENDING);
        $transaction->setIp($xmlRequest->getParameter('ipAddress'));
        $transaction->setGatewayTransactionData($xmlRequest->getParameters());
        $transaction->setPayment($payment);

        $payment->addTransaction($transaction);

        $ipCountry       = $this->geoIpService->getCountryCodeByIp($xmlRequest->getParameter('ipAddress'));
        $customerCountry = $xmlRequest->getParameter('customerCountry');
        $bankCountry     = $xmlRequest->getParameter('bankCountry');

        $country = $this->resolveCountry($ipCountry, $customerCountry, $bankCountry);

        $paymentVat = new PaymentVat();
        $paymentVat->setVatRate(
            $this->vatRateRepository->findActiveByCountry($country) ?: $this->vatRateRepository->findFallbackVatRate()
        );
        $paymentVat->setPayment($payment);

        $payment->setQualifiedVat($paymentVat);
        $this->paymentRepository->store($payment, true);

        $response->body = $this->buildPostBackResponse(0, 'Test Post Back Message');

        $this->logger->notice('RocketGate callback was successfully handled');

        return $response;
    }

    /**
     * @param HandleSuccessRequest|AbstractServiceRequest $request
     *
     * @return HandleSuccessResponse|AbstractServiceResponse
     */
    public function handleSuccess(AbstractServiceRequest $request): AbstractServiceResponse
    {
        $this->logger->debug('Receive request from RocketGate', ['request' => $request->request->query->all()]);

        $response = new HandleSuccessResponse();

        $flag       = $request->request->query->get('flag');
        $customerId = $request->request->query->get('id');
        $orderId    = $request->request->query->get('invoiceID');
        $hash       = $request->request->query->get('hash', '');

        if (!$this->isRocketGateHashValid(['flag' => $flag, 'id' => $customerId, 'invoiceID' => $orderId], $hash)) {
            $this->logger->debug('Received hash is invalid');

            $response->error = new ServiceResponseError(
                ServiceResponseError::TYPE_BAD_REQUEST,
                'RocketGate hash is not valid'
            );
        } else {
            try {
                $this->logger->debug('Trying to find the order', ['order' => $orderId]);

                /** @var Order $order */
                $order = $this->orderRepository->find($orderId);
                $this->completeOrder($order);

                $this->logger->debug('Order was found and marked as completed', ['order' => $orderId]);

                $this->createContentPermissions($order);

                $this->logger->debug('Content permissions were created', ['order' => $orderId]);
            } catch (\Throwable $exception) {
                $this->logger->debug(
                    'There is an issue during order completing and/or content permission creating',
                    ['order' => $orderId]
                );

                $response->error = new ServiceResponseError(
                    ServiceResponseError::TYPE_SERVER_ERROR,
                    'Cannot mark order completed and/or create content permission during RG success callback'
                );
            }
        }

        $redirectUrl = $this->getBlackmailedFullDomain().$this->router->generate(
            'blackmailed.payment_gateway_success',
            ['gateway' => 'rg']
        );

        if ($response->isError()) {
            $redirectUrl .= '?error=' . $response->getError()->getMessage();
        }

        $this->logger->debug('Generate redirect url', ['url' => $redirectUrl]);

        $response->redirectUrl = $redirectUrl;

        return $response;
    }

    /**
     * @param HandleFailureRequest|AbstractServiceRequest $request
     *
     * @return HandleFailureResponse|AbstractServiceResponse
     */
    public function handleFailure(AbstractServiceRequest $request): AbstractServiceResponse
    {
        $this->logger->debug('Receive request from RocketGate', ['request' => $request->request->query->all()]);

        $response = new HandleFailureResponse();

        $flag          = $request->request->query->get('flag');
        $customerId    = $request->request->query->get('id');
        $orderId       = $request->request->query->get('invoiceID');
        $errorCode     = $request->request->query->get('errcode');
        $errorCategory = $request->request->query->get('errcat');
        $hash          = $request->request->query->get('hash');

        $isRocketHashValid = $this->isRocketGateHashValid(
            [
                'flag'      => $flag,
                'errcat'    => $errorCategory,
                'errcode'   => $errorCode,
                'id'        => $customerId,
                'invoiceID' => $orderId,
            ],
            $hash
        );
        if (!$isRocketHashValid) {
            $this->logger->debug('Received hash is invalid');

            $response->error = new ServiceResponseError(
                ServiceResponseError::TYPE_BAD_REQUEST,
                'RocketGate hash is not valid'
            );
        } else {
            try {
                $this->logger->debug('Trying to find the order', ['order' => $orderId]);

                /** @var Order $order */
                $order = $this->orderRepository->find($orderId);
                $this->failOrder($order, true);

                $this->logger->debug('Order was found and marked as failed', ['order' => $orderId]);
            } catch (\Exception $exception) {
                $this->logger->debug('There is an issue during order failing', ['order' => $orderId]);

                $response->error = new ServiceResponseError(
                    ServiceResponseError::TYPE_SERVER_ERROR,
                    'Cannot mark order failed'
                );
            }
        }

        $redirectUrl = $this->getBlackmailedFullDomain().$this->router->generate(
            'blackmailed.payment_gateway_failure',
            ['gateway' => 'rg']
        );

        if ($response->isError()) {
            $redirectUrl .= '?error=' . $response->getError()->getMessage();
        }

        $this->logger->debug('Generate redirect url', ['url' => $redirectUrl]);

        $response->redirectUrl = $redirectUrl;

        return $response;
    }

    /**
     * @param array $parameters
     *
     * @return string
     */
    public function calculateRocketGateHash(array $parameters): string
    {
        return base64_encode(sha1(urldecode(http_build_query($parameters)), true));
    }

    /**
     * @param int         $code
     * @param string|null $message
     *
     * @return string
     */
    private function buildPostBackResponse(int $code, string $message = null): string
    {
        $response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $response .= "<Response>";
        $response .= "<results>" . $code . "</results>";
        if ($message != '') {
            $response .= "<message>" . $message . "</message>";
        }
        $response .= "</Response>\n";

        return $response;
    }

    /**
     * @param Order $order
     * @param bool  $flush
     */
    private function completeOrder(Order $order, bool $flush = false)
    {
        $order->markAsCompleted();
        $this->orderRepository->store($order, $flush);
    }

    /**
     * @param Order $order
     * @param bool  $flush
     */
    private function failOrder(Order $order, bool $flush = false)
    {
        // TODO Discuss should we do that? I think nope.
        $order->markAsFailed();
        $this->orderRepository->store($order, $flush);
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    private function createContentPermissions(Order $order): array
    {
        $contentPermissions = [];
        foreach ($order->getProducts() as $index => $orderProduct) {
            $contentPermission = ContentPermissionFactory::factory($order, $orderProduct);

            // Check if $orderProduct is the last.
            if ($index == ($order->getProducts()->count() - 1)) {
                $this->contentPermissionRepository->store($contentPermission, true);
            } else {
                $this->contentPermissionRepository->store($contentPermission);
            }

            $contentPermissions[] = $contentPermission;

            $this->sendContentPermissionToQueue($contentPermission);
        }

        return $contentPermissions;
    }

    /**
     * @param array  $urlArguments
     * @param string $hash
     *
     * @return bool
     */
    private function isRocketGateHashValid(array $urlArguments, string $hash): bool
    {
        return $hash == $this->calculateRocketGateHash(array_merge($urlArguments, ['secret' => $this->sharedSecret]));
    }

    /**
     * @param ContentPermission $contentPermission
     */
    private function sendContentPermissionToQueue(ContentPermission $contentPermission)
    {
        $syncRequest = new RpcTask\ContentPermission\Sync\Request();
        $syncRequest->populate($contentPermission->toArray());
        $syncRequest->userId = $contentPermission->getCustomer()->getId();

        $this->sqsProducer->send('sync_content_permissions_bm', $syncRequest);
    }

    /**
     * @param string      $ipCountry
     * @param string|null $customerCountry
     * @param string|null $bankCountry
     *
     * @return string
     */
    private function resolveCountry(
        string $ipCountry,
        string $customerCountry = null,
        string $bankCountry = null
    ): string {
        return (new CountryResolver($ipCountry, $customerCountry, $bankCountry))->resolveCountry();
    }

    /**
     * @param int $months
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    private function getBillingFrequencyParameter(int $months): string
    {
        switch ($months) {
            case Product::RECURRENT_FREQUENCY_MONTHLY:
                $parameter = 'MONTHLY';
                break;
            case Product::RECURRENT_FREQUENCY_QUARTERLY:
                $parameter = 'QUARTERLY';
                break;
            case Product::RECURRENT_FREQUENCY_ANNUALLY:
                $parameter = 'ANNUALLY';
                break;
            default:
                throw new \InvalidArgumentException(sprintf('"%d months" is not a valid parameter', $months));
        }

        return $parameter;
    }

    /**
     * @return string
     */
    private function getBlackmailedFullDomain(): string
    {
        return $this->blackmailedSchema . '://' . rtrim($this->blackmailedDomain, '/');
    }

    /**
     * @return string
     */
    private function getStanFullDomain(): string
    {
        return $this->stanSchema . '://' . rtrim($this->stanDomain, '/');
    }
}
