<?php

namespace AppBundle\Service\Email;

use EaPaysites\Service\Email\Email;
use SendGrid;

class EmailSender implements EmailSenderInterface
{
    const SENDGRID_SUCCESS_CODE = 202;

    /**
     * @var string
     */
    private $sendgridApiKey;

    /**
     * Sender constructor.
     *
     * @param string $sendgridApiKey
     */
    public function __construct(string $sendgridApiKey)
    {
        $this->sendgridApiKey = $sendgridApiKey;
    }

    /**
     * @param Email $email
     *
     * @return SendGrid\Response
     */
    public function send(Email $email)
    {
        $from    = new SendGrid\Email($email->fromName, $email->fromEmail);
        $to      = new SendGrid\Email($email->toName, $email->toEmail);
        $content = new SendGrid\Content($email->mimeType, $email->content ?: 'empty');
        $mail    = new SendGrid\Mail($from, $email->subject, $to, $content);

        if ($email->replacements) {
            /** @var SendGrid\Personalization $personalization */
            $personalization = $mail->personalization[0];

            foreach ($email->replacements as $id => $value) {
                $personalization->addSubstitution($id, $value);
            }
        }

        if ($email->templateId) {
            $mail->setTemplateId($email->templateId);
        }

        $sg = new \SendGrid($this->sendgridApiKey);

        /** @var SendGrid\Response $response */
        $response = $sg->client->mail()->send()->post($mail);

        if ($response->statusCode() !== 202) {
            $e = new SendFailedException();
            $e->setSendgridResponse($response);

            throw $e;
        }

        return $response;
    }
}
