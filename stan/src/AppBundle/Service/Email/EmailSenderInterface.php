<?php

namespace AppBundle\Service\Email;

use EaPaysites\Service\Email\Email;

interface EmailSenderInterface
{
    /**
     * @param Email $email
     *
     * @return mixed
     */
    public function send(Email $email);
}
