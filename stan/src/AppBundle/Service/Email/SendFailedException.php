<?php

namespace AppBundle\Service\Email;

use SendGrid;

class SendFailedException extends \RuntimeException
{
    /**
     * @var Sendgrid\Response;
     */
    private $sendgridResponse;

    /**
     * @return SendGrid\Response
     */
    public function getSendgridResponse(): SendGrid\Response
    {
        return $this->sendgridResponse;
    }

    /**
     * @param SendGrid\Response $sendgridResponse
     */
    public function setSendgridResponse(SendGrid\Response $sendgridResponse)
    {
        $this->sendgridResponse = $sendgridResponse;
    }
}
