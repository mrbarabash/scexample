<?php

namespace AppBundle\Service\Email;

use EaPaysites\Service\Email\Email;

class NullEmailSender implements EmailSenderInterface
{
    /**
     * @param Email $email
     *
     * @return mixed|void
     */
    public function send(Email $email)
    {
    }
}
