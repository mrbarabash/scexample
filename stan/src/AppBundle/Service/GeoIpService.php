<?php

namespace AppBundle\Service;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;

class GeoIpService
{
    const FALLBACK_COUNTRY_CODE = 'us';

    /**
     * @var Reader
     */
    private $geoIpReader;

    /**
     * @param Reader $reader
     */
    public function __construct(Reader $reader)
    {
        $this->geoIpReader = $reader;
    }

    /**
     * @param string $ip
     *
     * @return string
     */
    public function getCountryCodeByIp(string $ip): string
    {
        $code = null;
        try {
            $code = strtolower($this->geoIpReader->city($ip)->country->isoCode);
        } catch (AddressNotFoundException $exception) {}

        return $code ?: self::FALLBACK_COUNTRY_CODE;
    }
}
