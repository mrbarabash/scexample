<?php

namespace AppBundle\Twig;

use AppBundle\Service\Auth\PermissionsChecker;
use Symfony\Component\Intl\Intl;
use Symfony\Component\PropertyAccess\PropertyAccess;

class Extension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    const FALLBACK_COUNTRY_NAME = 'United States';

    /**
     * @var PermissionsChecker
     */
    protected $permissionsChecker;

    /**
     * @param PermissionsChecker $permissionsChecker
     */
    public function __construct(PermissionsChecker $permissionsChecker)
    {
        $this->permissionsChecker = $permissionsChecker;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('joinProperties', [$this, 'joinPropertiesFilter']),
//            new \Twig_SimpleFilter('price', [$this->priceFormatter, 'format']),
//            new \Twig_SimpleFilter('timeHhMm', [$this->dateTimeFormatter, 'timeHhMm']),
//            new \Twig_SimpleFilter('timeHhMmSs', [$this->dateTimeFormatter, 'timeHhMmSs']),
            new \Twig_SimpleFilter('htmlEntityDecode', 'html_entity_decode'),
            new \Twig_SimpleFilter('countryCode2Name', [$this, 'countryCodeToName']),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('checkAccess', [$this->permissionsChecker, 'check']),
//            new \Twig_SimpleFunction('entityUrl', [$this, 'entityUrl']),
            new \Twig_SimpleFunction('numberTitle', [$this, 'numberTitle']),
//            new \Twig_SimpleFunction('entityUrlByDataTypeAndId', [$this, 'entityUrlByDataTypeAndId']),
        ];
    }

    /**
     * @param int    $number
     * @param string $titleSingle
     * @param string $titleMultiple
     *
     * @return string
     */
    public function numberTitle($number, $titleSingle, $titleMultiple)
    {
        if ($number == 1) {
            return $number . ' ' . $titleSingle;
        } else {
            return $number . ' ' . $titleMultiple;
        }
    }

    /**
     * @param        $objects
     * @param        $property
     * @param string $separator
     *
     * @return string
     */
    public function joinPropertiesFilter($objects, $property, $separator = ', ')
    {
        $values = [];

        if (!$objects) {
            return '';
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($objects as $object) {
            $values[] = $accessor->getValue($object, $property);
        }

        return implode($separator, $values);
    }

    /**
     * @param string $code
     *
     * @return string
     */
    public function countryCodeToName(string $code): string
    {
        return Intl::getRegionBundle()->getCountryName(strtoupper($code)) ?: self::FALLBACK_COUNTRY_NAME;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'filters';
    }

    /**
     * @return array
     */
    public function getGlobals()
    {
        return [
            'flashMessageTypes' => [
//                ShedBundle::FLASH_ERROR,
//                ShedBundle::FLASH_SUCCESS,
//                ShedBundle::FLASH_WARNING
            ]
        ];
    }
}
