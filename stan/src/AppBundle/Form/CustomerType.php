<?php

namespace AppBundle\Form;

use AppBundle\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class CustomerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name');

        /** @var Customer $customer */
        $customer = $builder->getData();

        if (!$customer->getId()) {
            $builder
                ->add('email', EmailType::class)
                ->add('plainPassword', PasswordType::class, [
                    'constraints' => [
                        new NotBlank()
                    ]
                ]);
        }

        $builder
            ->add('save', SubmitType::class, ['label' => 'Save']);
    }
}
