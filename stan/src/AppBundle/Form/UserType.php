<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('email', EmailType::class)
            ->add('roles', ChoiceType::class, [
                'label' => 'Permissions',
                'required' => true,
                'expanded'      => true,
                'multiple'      => true,
                'choices'           => [
                    'Manage users' => User::ROLE_USERS,
                    'Manage customers' => User::ROLE_CUSTOMERS,
                ]
            ])
            ->add('save', SubmitType::class, ['label' => 'Save']);
    }
}
