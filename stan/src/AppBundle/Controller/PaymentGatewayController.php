<?php

namespace AppBundle\Controller;

use AppBundle\Service\PaymentGateway\Callback\HandleCallbackRequest;
use AppBundle\Service\PaymentGateway\Failure\HandleFailureRequest;
use AppBundle\Service\PaymentGateway\RocketGateService;
use AppBundle\Service\PaymentGateway\Success\HandleSuccessRequest;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/payment-gateway")
 */
class PaymentGatewayController extends AppController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/callback/{gateway}", requirements={"gateway": "\w+"}, name="paymentGatewayCallback")
     * @Method("POST")
     *
     * @param Request $request
     * @param string  $gateway
     *
     * @return Response
     */
    public function callbackAction(Request $request, string $gateway)
    {
        $this->logger->debug('Callback controller action', ['request' => $request->getContent()]);

        $this->ensureRocketGatePaymentGateway($gateway);

        $service = $this->get(RocketGateService::class);

        $this->logger->debug('Start handle callback request');

        $serviceRequest = new HandleCallbackRequest();
        $serviceRequest->body = $request->getContent();

        $this->logger->debug('Start handle callback request');

        $serviceResponse = $service->handleCallback($serviceRequest);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml');
        $response->headers->set('Cache-Control', 'no-cache');
        $response->setContent($serviceResponse->body);

        $this->logger->debug('Finish handle callback request', ['response' => $serviceResponse->body]);

        return $response;
    }

    /**
     * @Route("/callback/{gateway}/success", requirements={"gateway": "\w+"}, name="paymentGatewaySuccess")
     * @Method("GET")
     *
     * @param Request $request
     * @param string $gateway
     *
     * @return array
     */
    public function successAction(Request $request, string $gateway)
    {
        $this->logger->debug('Success controller action', ['request' => $request->getContent()]);

        $this->ensureRocketGatePaymentGateway($gateway);

        $service = $this->get(RocketGateService::class);

        $this->logger->debug('Start handle success request');

        $serviceRequest = new HandleSuccessRequest();
        $serviceRequest->request = $request;

        return $this->render(
            'paymentGateway/_iframe_redirect.html.twig',
            ['url' => $service->handleSuccess($serviceRequest)->redirectUrl]
        );
    }

    /**
     * @Route("/callback/{gateway}/failure", requirements={"gateway": "\w+"}, name="paymentGatewayFailure")
     * @Method("GET")
     *
     * @param Request $request
     * @param string $gateway
     *
     * @return array
     */
    public function failureAction(Request $request, string $gateway)
    {
        $this->logger->debug('Failure controller action', ['request' => $request->getContent()]);

        $this->ensureRocketGatePaymentGateway($gateway);

        $service = $this->get(RocketGateService::class);

        $this->logger->debug('Start handle failure request');

        $serviceRequest = new HandleFailureRequest();
        $serviceRequest->request = $request;

        return $this->render(
            'paymentGateway/_iframe_redirect.html.twig',
            ['url' => $service->handleFailure($serviceRequest)->redirectUrl]
        );
    }

    /**
     * @param string $gateway
     *
     * @throws NotFoundHttpException
     */
    private function ensureRocketGatePaymentGateway(string $gateway)
    {
        if ($gateway != 'rg') {
            throw $this->createNotFoundException();
        }
    }
}
