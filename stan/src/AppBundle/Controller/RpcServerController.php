<?php

namespace AppBundle\Controller;

use EaPaysites\Service\EmptyServiceResponse;
use EaPaysites\Service\Rpc\Handler\RpcHandlerManager;
use EaPaysites\Service\Rpc\RpcLogger;
use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseError;
use EaPaysites\Service\ServiceResponseInterface;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RpcServerController extends AppController
{
    /**
     * @Route("/rpc/execute/{taskPrefix}/{taskSuffix}", name="rpcExecute")
     * @Method({"POST"})
     * @param Request             $request
     * @param SerializerInterface $serializer
     * @param RpcHandlerManager   $rpcHandlerManager
     *
     * @return JsonResponse
     */
    public function executeAction(
        Request $request,
        SerializerInterface $serializer,
        RpcHandlerManager $rpcHandlerManager
    ) {
        $rpcAccessKey = $this->getParameter('stan_rpc_access_key');
        if ($request->headers->get('X-Stan-Access-Key') != $rpcAccessKey) {
            throw new AccessDeniedHttpException();
        }

        $taskName = $this->getTaskName($request);
        if (!$rpcHandlerManager->handlerExists($taskName)) {
            return $this->getJsonResponse($this->getTaskNotFoundResponse(), $serializer);
        }

        $handler = $rpcHandlerManager->getHandler($taskName);
        $this->log('notice', $request, sprintf('Received: %s', $handler->getTaskName()));

        try {
            /** @var ServiceRequestInterface $rpcServiceRequest */
            $rpcServiceRequest = $serializer->deserialize(
                $request->getContent(),
                $handler->getRequestClassName(),
                'json'
            );
            $this->log(
                'notice',
                $request,
                sprintf(
                    'Parsed: %s',
                    $serializer->serialize($handler->getLogSafeRequestData($rpcServiceRequest), 'json')
                )
            );
        } catch (\Exception $e) {
            $this->log('error', $request, sprintf('Parse failed: %s', $e->getMessage()));

            return $this->getJsonResponse($this->getMalformedRequestResponse($request), $serializer);
        }

        try {
            $rpcResponse = $handler->execute($rpcServiceRequest);
        } catch (\Exception $e) {
            $this->log(
                'error',
                $request,
                sprintf('Execution failed. Message: %s. Trace: %s', $e->getMessage(), $e->getTraceAsString())
            );

            return $this->getJsonResponse($this->getServerErrorResponse($request), $serializer);
        }

        $this->log(
            'notice',
            $request,
            sprintf('Replying. Error: %s', $serializer->serialize($rpcResponse->getError(), 'json'))
        );

        return $this->getJsonResponse($rpcResponse, $serializer);
    }

    /**
     * @param string  $level
     * @param Request $request
     * @param string  $message
     */
    private function log(string $level, Request $request, string $message)
    {
        $logger = $this->get('monolog.logger.rpc');

        $message = sprintf('Request %s. %s', $this->getRequestId($request), $message);

        $logger->log($level, $message);
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    private function getRequestId(Request $request): ?string
    {
        return $request->headers->get('X-Rpc-Request-Id');
    }

    /**
     * @param ServiceResponseInterface $rpcResponse
     * @param SerializerInterface      $serializer
     *
     * @return JsonResponse
     */
    private function getJsonResponse(
        ServiceResponseInterface $rpcResponse,
        SerializerInterface $serializer
    ): JsonResponse {
        $response = new JsonResponse();
        $response->setStatusCode($this->getResponseCode($rpcResponse));
        $response->setContent($serializer->serialize($rpcResponse, 'json'));

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return ServiceResponseInterface
     */
    private function getServerErrorResponse(Request $request): ServiceResponseInterface
    {
        $error = new ServiceResponseError(
            ServiceResponseError::TYPE_SERVER_ERROR, '', [], $this->getRequestId($request)
        );

        $rpcResponse        = new EmptyServiceResponse();
        $rpcResponse->error = $error;

        return $rpcResponse;
    }

    /**
     * @param Request $request
     *
     * @return ServiceResponseInterface
     */
    private function getMalformedRequestResponse(Request $request): ServiceResponseInterface
    {
        $error = new ServiceResponseError(
            ServiceResponseError::TYPE_BAD_REQUEST,
            'Bad format: deserialize failed',
            [],
            $this->getRequestId($request)
        );

        $rpcResponse        = new EmptyServiceResponse();
        $rpcResponse->error = $error;

        return $rpcResponse;
    }

    /**
     * @return ServiceResponseInterface
     */
    private function getTaskNotFoundResponse(): ServiceResponseInterface
    {
        $error = new ServiceResponseError(ServiceResponseError::TYPE_BAD_REQUEST, 'Task not found');

        $rpcResponse        = new EmptyServiceResponse();
        $rpcResponse->error = $error;

        return $rpcResponse;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    private function getTaskName(Request $request): string
    {
        return ucfirst($request->get('taskPrefix')) . '\\' . ucfirst($request->get('taskSuffix'));
    }

    /**
     * @param ServiceResponseInterface $serviceResponse
     *
     * @return int
     */
    private function getResponseCode(ServiceResponseInterface $serviceResponse): int
    {
        if ($serviceResponse->isError()) {
            switch ($serviceResponse->getError()->getType()) {
                case ServiceResponseError::TYPE_BAD_REQUEST:
                    return Response::HTTP_BAD_REQUEST;
                case ServiceResponseError::TYPE_SERVER_ERROR:
                default:
                    return Response::HTTP_INTERNAL_SERVER_ERROR;
            }
        }

        return Response::HTTP_OK;
    }
}
