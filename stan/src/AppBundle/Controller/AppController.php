<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AppController extends Controller
{
    /**
     * @param        $entityName
     * @param        $value
     * @param string $key
     *
     * @return object
     */
    public function findRequiredEntity($entityName, $value, $key = 'id')
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->find($entityName, $value);
        if (!$entity) {
            throw $this->createNotFoundException("$entityName not found by $key");
        }

        return $entity;
    }

    /**
     * @param string $type
     * @param string $message
     */
    public function addFlash($type, $message)
    {
        if (is_array($message)) {
            foreach ($message as $messageItem) {
                parent::addFlash($type, $messageItem);
            }
        } else {
            parent::addFlash($type, $message);
        }
    }
}
