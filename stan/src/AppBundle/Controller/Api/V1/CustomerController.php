<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\Repository\CustomerRepository;
use AppBundle\Service\Customer\SignupService;
use AppBundle\Util\Validators;
use Particle\Validator\Validator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends ApiController
{
    /**
     * @Route("/customers", name="apiV1CustomerSignup")
     * @Method({"POST"})
     *
     * @param Request            $request
     * @param CustomerRepository $customerRepository
     * @param SignupService      $signupService
     *
     * @return JsonResponse
     */
    public function signupAction(
        Request $request,
        CustomerRepository $customerRepository,
        SignupService $signupService
    )
    {
        $v = new Validator();
        $v->required('email')
            ->email()
            ->lengthBetween(null, 100)
            ->callback(Validators::getDuplicateEntityRule(
                [$customerRepository, 'isUniqueEmail'],
                'User already exists'
            ));
        $v->required('password')
            ->lengthBetween(8, 256);
        $v->optional('name')
            ->lengthBetween(null, 128)
            ->callback(Validators::getGraphCharactersRule());

        $result = $v->validate($request->request->all());
        if (!$result->isValid()) {
            return $this->getParamsErrorResponse($result->getMessages());
        }

        $customer = $signupService->signup(
            $request->request->get('email'),
            $request->request->get('password'),
            $request->request->get('name')
        );

        $response = new JsonResponse(
            [
                'data' => [
                    'id'             => $customer->getId(),
                    'email'          => $customer->getEmail(),
                    'emailCanonical' => $customer->getEmailCanonical(),
                    'password'       => $customer->getPassword(),
                    'name'           => $customer->getName()
                ]
            ],
            Response::HTTP_OK
        );

        return $response;
    }

    /**
     * @Route("/customers/login", name="apiV1CustomerLogin")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $v = new Validator();
        $v->required('email')
            ->lengthBetween(null, 100);
        $v->required('password')
            ->lengthBetween(8, 256);

        $result = $v->validate($request->request->all());
        if (!$result->isValid()) {
            return $this->getParamsErrorResponse($result->getMessages());
        }

        $loginService = $this->get('app.customer_login_service');
        $token        = $loginService->loginByEmailAndPassword(
            $request->request->get('email'),
            $request->request->get('password')
        );

        if (!$token) {
            $response = new JsonResponse(
                ['message' => 'User/password combination does not match'],
                Response::HTTP_UNAUTHORIZED
            );
        } else {
            $response = new JsonResponse(
                ['data' => $token],
                Response::HTTP_OK
            );
        }

        return $response;
    }
}
