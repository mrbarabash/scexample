<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Controller\AppController;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AppController
{
    /**
     * @param Request $request
     * @param array   $paramsConfig
     *
     * @return array
     */
    protected function validateParams(Request $request, array $paramsConfig)
    {
        $v = new \Valitron\Validator($request->request->all());
        $v->mapFieldsRules($paramsConfig);

        if (!$v->validate()) {
            return $v->errors();
        }

        return [];

        $errors = [];
        foreach ($paramsConfig as $field => $fieldRule) {



//            try {
//                /** @var Validator $validator */
//                if ($validator = $fieldRule['validate']) {
//                    $validator->assert($request->get($field));
//                }
//            } catch (NestedValidationException $e) {
//                $errors[$field] = $e->getMessages();
//            }
        }

        return $errors;
    }

    /**
     * @param Request $request
     * @param array   $paramsConfig
     *
     * @return array
     */
    protected function getParams(Request $request, array $paramsConfig)
    {
        $params = [];
        foreach ($paramsConfig as $field => $fieldRule) {
            $params[$field] = $request->get($field, isset($fieldRule['default']) ? $fieldRule['default'] : null);
        }

        return $params;
    }

    /**
     * @param $errors
     *
     * @return array|JsonResponse
     */
    protected function getParamsErrorResponse($errors)
    {
        $response = [
            'message'     => 'Bad Request',
            'fields' => $errors
        ];

        $response = new JsonResponse($response, Response::HTTP_BAD_REQUEST);

        return $response;
    }
}
