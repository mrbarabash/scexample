<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AppController
{
    /**
     * @Route("/users", name="userIndex")
     * @Method({"GET"})
     * @Security("has_role('ROLE_USERS')")
     */
    public function indexAction(Request $request, UserRepository $userRepo, Paginator $paginator)
    {
        $qb = $userRepo->createQueryBuilder('u');
        $query     = $qb->getQuery();

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            $request->query->get('perPage', 50),
            ['defaultSortFieldName' => 'u.id', 'defaultSortDirection' => 'asc']
        );

        $data = [
            'pagination' => $pagination,
            'menu'       => 'users',
        ];
//
//        if ($request->isXmlHttpRequest()) {
//            return $this->render(
//                'ShedBundle:User:index/list.html.twig',
//                $data
//            );
//        }

        return $this->render('user/index.html.twig', $data);
    }

    /**
     * @Route("/users/new", name="userNew")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USERS')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, UserRepository $userRepository)
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $userRepository->store($user, true);

            return $this->redirectToRoute('userIndex');
        }

        return $this->render('user/new.html.twig', [
            'form' => $form->createView(),
            'menu' => 'users'
        ]);
    }

    /**
     * @Route("/users/{id}/edit", name="userEdit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USERS')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, UserRepository $userRepository)
    {
        /** @var User $user */
        $user = $this->findRequiredEntity('AppBundle:User', $request->get('id'));

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $userRepository->store($user, true);

            return $this->redirectToRoute('userIndex');
        }

        return $this->render('user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'menu' => 'users'
        ]);
    }

    /**
     * @Route("/users/{id}", name="userDelete", requirements={"id" = "\d+"})
     * @Method({"DELETE"})
     * @Security("has_role('ROLE_USERS')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, UserRepository $userRepository)
    {
        /** @var User $user */
        $user = $this->findRequiredEntity('AppBundle:User', $request->get('id'));

        $userRepository->remove($user, true);

        return $this->redirectToRoute('userIndex');
    }
}
