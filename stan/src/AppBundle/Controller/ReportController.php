<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Repository\OrderRepository;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/reports")
 * @Security("has_role('ROLE_REPORTS')")
 */
class ReportController extends AppController
{
    /**
     * @Route(name="reportIndex")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('report/index.html.twig');
    }

    /**
     * @Route("/vat", name="reportVat")
     * @Method("GET")
     */
    public function vatReportAction(Request $request, OrderRepository $orderRepo, Paginator $paginator)
    {
        $dateFrom = new \DateTime($request->query->get('from', '-1 month'));
        $dateTo   = new \DateTime($request->query->get('to', 'now'));

        $pagination = $paginator->paginate(
            $orderRepo->getVatReportQuery($dateFrom, $dateTo),
            $request->query->get('page', 1),
            $request->query->get('perPage', 50),
            ['defaultSortFieldName' => 'o.id', 'defaultSortDirection' => 'asc']
        );

        return $this->render(
            'report/vatReport.html.twig',
            ['pagination' => $pagination, 'from' => $dateFrom, 'to' => $dateTo]
        );
    }

    /**
     * @Route("/country-vat", name="vatCountryReport")
     * @Method("GET")
     */
    public function vatCountryReportAction(Request $request, OrderRepository $orderRepo, Paginator $paginator)
    {
        $dateFrom = new \DateTime($request->query->get('from', '-1 month'));
        $dateTo   = new \DateTime($request->query->get('to', 'now'));

        $pagination = $paginator->paginate(
            $orderRepo->getVatCountryReportQuery($dateFrom, $dateTo),
            $request->query->get('page', 1),
            $request->query->get('perPage', 50),
            ['defaultSortFieldName' => 'country', 'defaultSortDirection' => 'asc']
        );

        return $this->render(
            'report/vatCountryReport.html.twig',
            ['pagination' => $pagination]
        );
    }
}
