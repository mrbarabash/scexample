<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use AppBundle\Form\CustomerType;
use AppBundle\Service\Customer\EditService;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends AppController
{
    /**
     * @Route("/customers", name="customerIndex")
     * @Method({"GET"})
     * @Security("has_role('ROLE_CUSTOMERS')")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var EntityRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Customer');

        $qb = $repo->createQueryBuilder('c');

        $query     = $qb->getQuery();
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            $request->query->get('perPage', 50),
            ['defaultSortFieldName' => 'c.id', 'defaultSortDirection' => 'asc']
        );

        $data = [
            'pagination' => $pagination,
            'menu'       => 'customers',
        ];
//
//        if ($request->isXmlHttpRequest()) {
//            return $this->render(
//                'ShedBundle:Customer:index/list.html.twig',
//                $data
//            );
//        }

        return $this->render('customer/index.html.twig', $data);
    }

    /**
     * @Route("/customers/new", name="customerNew")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_CUSTOMERS')")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $customer = new Customer();

        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $password = $this->get('security.password_encoder')
                             ->encodePassword($customer, $customer->getPlainPassword());
            $customer->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);

            $em->flush();

            return $this->redirectToRoute('customerIndex');
        }

        return $this->render(
            'customer/new.html.twig',
            [
                'form' => $form->createView(),
                'menu' => 'customers'
            ]
        );
    }

    /**
     * @Route("/customers/{id}/edit", name="customerEdit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_CUSTOMERS')")
     * @param Request     $request
     * @param EditService $editService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, EditService $editService)
    {
        /** @var Customer $customer */
        $customer         = $this->findRequiredEntity('AppBundle:Customer', $request->get('id'));
        $customerOriginal = clone($customer);

        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $editService->edit($customerOriginal, $customer);

            return $this->redirectToRoute('customerIndex');
        }

        return $this->render(
            'customer/edit.html.twig',
            [
                'form'     => $form->createView(),
                'customer' => $customer,
                'menu'     => 'customers'
            ]
        );
    }

    /**
     * @Route("/customers/{id}", name="customerDelete", requirements={"id" = "\d+"})
     * @Method({"DELETE"})
     * @Security("has_role('ROLE_CUSTOMERS')")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request)
    {
        $customer = $this->findRequiredEntity('AppBundle:Customer', $request->get('id'));

        $em = $this->getDoctrine()->getManager();
        $em->remove($customer);
        $em->flush();

        return $this->redirectToRoute('customerIndex');
    }
}
