<?php

namespace AppBundle\Consumer;

use AppBundle\Service\Email\EmailSenderInterface;
use AppBundle\Service\Email\SendFailedException;
use EaPaysites\Service\Email\Email;
use EaPaysites\Sqs\SqsConsumerInterface;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

class SendEmail implements SqsConsumerInterface
{
    /**
     * @var EmailSenderInterface
     */
    private $emailSender;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * SendEmail constructor.
     *
     * @param EmailSenderInterface $emailSender
     * @param LoggerInterface      $logger
     * @param SerializerInterface  $serializer
     */
    public function __construct(
        EmailSenderInterface $emailSender,
        LoggerInterface $logger,
        SerializerInterface $serializer
    ) {
        $this->emailSender = $emailSender;
        $this->logger      = $logger;
        $this->serializer  = $serializer;
    }

    /**
     * @param array $message
     *
     * @return bool
     */
    public function execute(array $message): bool
    {
        try {
            /** @var Email $email */
            $email = $this->serializer->deserialize($message['Body'], Email::class, 'json');
        } catch (\Exception $e) {
            $this->logger->error('Failed to deserialize message', ['message' => $message]);

            return false;
        }

        $this->logger->notice('Got email to send', ['email' => $email->toEmail, 'subject' => $email->subject]);

        try {
            $this->emailSender->send($email);
        } catch (SendFailedException $e) {
            $sendgridResponse = $e->getSendgridResponse();
            $this->logger->error(
                'Send email: Sendgrid failed',
                [
                    'exception'            => $e->getMessage(),
                    'sgResponseStatusCode' => $sendgridResponse->statusCode(),
                    'sgResponseBody'       => $sendgridResponse->body(),
                    'sgResponseHeaders'    => $sendgridResponse->headers(true)
                ]
            );

            return false;
        } catch (\Exception $e) {
            $this->logger->error(
                'Send email: unexpected error',
                ['exception' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return false;
        }

        $this->logger->notice('Email sent', ['email' => $email->toEmail, 'subject' => $email->subject]);

        return true;
    }
}
