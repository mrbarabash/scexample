<?php

namespace AppBundle;

use Respect\Validation\Validator;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    public function boot()
    {
        Validator::with('AppBundle\Validator');

        parent::boot();
    }
}
