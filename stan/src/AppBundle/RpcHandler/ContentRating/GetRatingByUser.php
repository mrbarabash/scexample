<?php

namespace AppBundle\RpcHandler\ContentRating;

use AppBundle\Service\ContentRating;
use AppBundle\Service\ContentRating\ContentRatingService;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Handler\AbstractRpcHandler;
use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseInterface;

class GetRatingByUser extends AbstractRpcHandler
{
    private $contentRatingService;

    public function __construct(ContentRatingService $contentRatingService)
    {
        $this->contentRatingService = $contentRatingService;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\ContentRating\GetRatingByUser\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $request = new ContentRating\GetRatingByUser\Request();
        $request->populate($rpcServiceRequest->toArray());

        $serviceResponse = $this->contentRatingService->getRatingByUser($request);

        $rpcServiceResponse = new RpcTask\ContentRating\GetRatingByUser\Response();
        if ($serviceResponse->isError()) {
            $rpcServiceResponse->error = $serviceResponse->getError();
        } else {
            $contentRating             = $serviceResponse->contentRating;
            $rpcServiceResponse->value = $contentRating ? $contentRating->getValue() : null;
        }

        return $rpcServiceResponse;
    }
}
