<?php

namespace AppBundle\RpcHandler\ContentRating;

use AppBundle\Service\ContentRating\ContentRatingService;
use AppBundle\Service\ContentRating\RateContent\RateContentRequest;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Handler\AbstractRpcHandler;
use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseInterface;

class RateContent extends AbstractRpcHandler
{
    private $contentRatingService;

    public function __construct(ContentRatingService $contentRatingService)
    {
        $this->contentRatingService = $contentRatingService;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\ContentRating\RateContent\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $request = new RateContentRequest();
        $request->populate($rpcServiceRequest->toArray());

        $serviceResponse = $this->contentRatingService->rateContent($request);

        $rpcServiceResponse = new RpcTask\ContentRating\RateContent\Response();
        if ($serviceResponse->isError()) {
            $rpcServiceResponse->error = $serviceResponse->getError();
        }

        return $rpcServiceResponse;
    }
}
