<?php

namespace AppBundle\RpcHandler\ContentRating;

use AppBundle\Service\ContentRating;
use AppBundle\Service\ContentRating\ContentRatingService;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Handler\AbstractRpcHandler;
use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseInterface;

class GetAggregateRating extends AbstractRpcHandler
{
    /**
     * @var ContentRatingService
     */
    private $contentRatingService;

    /**
     * GetAggregateRating constructor.
     *
     * @param ContentRatingService $contentRatingService
     */
    public function __construct(ContentRatingService $contentRatingService)
    {
        $this->contentRatingService = $contentRatingService;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\ContentRating\GetAggregateRating\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $request = new ContentRating\GetAggregateRating\Request();
        $request->populate($rpcServiceRequest->toArray());

        $serviceResponse = $this->contentRatingService->getAggregateRating($request);

        $rpcServiceResponse = new RpcTask\ContentRating\GetAggregateRating\Response();
        if ($serviceResponse->isError()) {
            $rpcServiceResponse->error = $serviceResponse->getError();
        } else {
            $rpcServiceResponse->contentIdsToRatings = $serviceResponse->contentIdsToRatings;
        }

        return $rpcServiceResponse;
    }
}
