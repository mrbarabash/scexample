<?php

namespace AppBundle\RpcHandler\PurchaseToken;

use AppBundle\Entity\{
    ContentPermission, PurchaseToken
};
use AppBundle\Entity\Repository\{
    ContentPermissionRepository, PurchaseTokenRepository
};
use EaPaysites\Entity\ContentPermission as EaContentPermission;
use EaPaysites\RpcTask;
use EaPaysites\Service\{
    Rpc\Handler\AbstractRpcHandler, ServiceRequestInterface, ServiceResponseError, ServiceResponseInterface
};
use EaPaysites\Util\{Services, Validators};
use Particle\Validator\{ValidationResult, Validator};

class Redeem extends AbstractRpcHandler
{
    /**
     * @var PurchaseTokenRepository
     */
    private $purchaseTokenRepo;

    /**
     * @var ContentPermissionRepository
     */
    private $contentPermissionRepo;

    /**
     * @param PurchaseTokenRepository     $purchaseTokenRepository
     * @param ContentPermissionRepository $contentPermissionRepository
     */
    public function __construct(
        PurchaseTokenRepository $purchaseTokenRepository,
        ContentPermissionRepository $contentPermissionRepository
    ) {
        $this->purchaseTokenRepo     = $purchaseTokenRepository;
        $this->contentPermissionRepo = $contentPermissionRepository;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\PurchaseToken\Redeem\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $rpcServiceResponse = new RpcTask\PurchaseToken\Redeem\Response();

        $validationResult = $this->validateRedeemRequest($rpcServiceRequest);
        if (!$validationResult->isValid()) {
            Services::setValidationError($rpcServiceResponse, $validationResult);

            return $rpcServiceResponse;
        }

        /** @var PurchaseToken $token */
        $token = $this->purchaseTokenRepo->find($rpcServiceRequest->id);

        $token->setUsedFor($rpcServiceRequest->usedFor);
        $token->setUsedAt();
        $contentPermission = $this->createContentPermission($token, EaContentPermission::PERMISSION_SCENE_VIEW_FULL);

        try {
            $this->contentPermissionRepo->store($contentPermission);
            $this->purchaseTokenRepo->store($token, true);

            $rpcServiceResponse->token             = $token->toArray();
            $rpcServiceResponse->contentPermission = $contentPermission->toArray();
        } catch (\Exception $exception) {
            $rpcServiceResponse->error = new ServiceResponseError(
                ServiceResponseError::TYPE_SERVER_ERROR,
                'Cannot save PurchaseToken in the db during redeem process.'
            );
        }

        return $rpcServiceResponse;
    }

    /**
     * @param PurchaseToken $token
     * @param string        $permission
     *
     * @return ContentPermission
     */
    private function createContentPermission(PurchaseToken $token, string $permission): ContentPermission
    {
        $contentPermission = new ContentPermission();
        $contentPermission->setCustomer($token->getCustomer());
        $contentPermission->setContentId($token->getUsedFor());
        $contentPermission->setContentType($token->getKind());
        $contentPermission->setPermission($permission);
        $contentPermission->setGrantedVia($token->getId());
        $contentPermission->setGrantedViaType($token->getOwnClassName());

        return $contentPermission;
    }

    /**
     * @param RpcTask\PurchaseToken\Redeem\Request $request
     *
     * @return ValidationResult
     */
    private function validateRedeemRequest(RpcTask\PurchaseToken\Redeem\Request $request): ValidationResult
    {
        $v = new Validator();

        $v->required('id')
            ->string()
            ->callback(Validators::getEntityExistsRule($this->purchaseTokenRepo, 'id', 'PurchaseToken not found'));
        $v->required('usedFor')
            ->digits()
            ->greaterThan(0);

        return $v->validate($request->toArray());
    }
}
