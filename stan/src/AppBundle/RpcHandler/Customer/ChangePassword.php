<?php

namespace AppBundle\RpcHandler\Customer;

use AppBundle\Service\Customer\Password\ChangeCustomerPasswordRequest;
use AppBundle\Service\Customer\PasswordService;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Handler\AbstractRpcHandler;
use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseInterface;

class ChangePassword extends AbstractRpcHandler
{
    /**
     * @var PasswordService
     */
    private $passwordService;

    /**
     * ChangePassword constructor.
     *
     * @param PasswordService $passwordService
     */
    public function __construct(PasswordService $passwordService)
    {
        $this->passwordService = $passwordService;
    }

    /**
     * @param ServiceRequestInterface $rpcServiceRequest
     *
     * @return array
     */
    public function getLogSafeRequestData(ServiceRequestInterface $rpcServiceRequest): array
    {
        $data             = $rpcServiceRequest->toArray();
        $data['password'] = 'xxx';

        return $data;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\Customer\ChangePassword\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $serviceRequest = new ChangeCustomerPasswordRequest();
        $serviceRequest->populate($rpcServiceRequest->toArray());

        $serviceResponse = $this->passwordService->changePassword($serviceRequest);

        $rpcServiceResponse = new RpcTask\Customer\ChangePassword\Response();
        if ($serviceResponse->isError()) {
            $rpcServiceResponse->error = $serviceResponse->getError();
        } else {
            $rpcServiceResponse->passwordHash = $serviceResponse->passwordHash;
        }

        return $rpcServiceResponse;
    }
}
