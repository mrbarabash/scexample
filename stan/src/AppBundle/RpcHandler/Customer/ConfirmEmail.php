<?php

namespace AppBundle\RpcHandler\Customer;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Repository\CustomerRepository;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Handler\AbstractRpcHandler;
use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseError;
use EaPaysites\Service\ServiceResponseInterface;

class ConfirmEmail extends AbstractRpcHandler
{
    /**
     * @var CustomerRepository
     */
    private $customerRepo;

    /**
     * ConfirmEmail constructor.
     *
     * @param CustomerRepository $customerRepo
     */
    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\Customer\ConfirmEmail\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $rpcServiceResponse = new RpcTask\Customer\ConfirmEmail\Response();

        /** @var Customer $customer */
        $customer = $this->customerRepo->find($rpcServiceRequest->customerId);
        if (!$customer) {
            $rpcServiceResponse->error = new ServiceResponseError(
                ServiceResponseError::TYPE_BAD_REQUEST,
                'Customer not found by ID'
            );

            return $rpcServiceResponse;
        }

        $customer->setEmailConfirmed(true);
        $this->customerRepo->store($customer, true);

        return $rpcServiceResponse;
    }
}
