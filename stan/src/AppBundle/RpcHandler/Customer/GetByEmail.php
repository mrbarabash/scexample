<?php

namespace AppBundle\RpcHandler\Customer;

use AppBundle\Entity\Repository\CustomerRepository;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Handler\AbstractRpcHandler;
use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseError;
use EaPaysites\Service\ServiceResponseInterface;

class GetByEmail extends AbstractRpcHandler
{
    /**
     * @var CustomerRepository
     */
    private $customerRepo;

    /**
     * GetCustomerByEmail constructor.
     *
     * @param CustomerRepository $customerRepo
     */
    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\Customer\GetByEmail\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        if (!$rpcServiceRequest->email) {
            return $this->getNotFoundResponse();
        }

        $customer = $this->customerRepo->findByEmail($rpcServiceRequest->email);
        if (!$customer) {
            return $this->getNotFoundResponse();
        }

        $stanResponse           = new RpcTask\Customer\GetByEmail\Response();
        $stanResponse->customer = $customer->toArray();

        return $stanResponse;
    }

    /**
     * @return RpcTask\Customer\GetByEmail\Response
     */
    private function getNotFoundResponse()
    {
        $stanResponse = new RpcTask\Customer\GetByEmail\Response();

        $error = new ServiceResponseError(ServiceResponseError::TYPE_BAD_REQUEST, 'Customer not found by email');
        $error->setSubType(RpcTask\Customer\GetByEmail\Response::ERROR_SUBTYPE_CUSTOMER_NOT_FOUND);

        $stanResponse->error = $error;

        return $stanResponse;
    }
}
