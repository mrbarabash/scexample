<?php

namespace AppBundle\RpcHandler\Customer;

use AppBundle\Service\Customer\Signup\SignupRequest;
use AppBundle\Service\Customer\SignupService;
use EaPaysites\RpcTask;
use EaPaysites\Service\Rpc\Handler\AbstractRpcHandler;
use EaPaysites\Service\ServiceRequestInterface;
use EaPaysites\Service\ServiceResponseInterface;

class Signup extends AbstractRpcHandler
{
    /**
     * @var SignupService
     */
    private $signupService;

    /**
     * Signup constructor.
     *
     * @param SignupService $signupService
     */
    public function __construct(SignupService $signupService)
    {
        $this->signupService = $signupService;
    }

    /**
     * @param ServiceRequestInterface $rpcServiceRequest
     *
     * @return array
     */
    public function getLogSafeRequestData(ServiceRequestInterface $rpcServiceRequest): array
    {
        $data = $rpcServiceRequest->toArray();
        $data['password'] = 'xxx';

        return $data;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\Customer\Signup\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $serviceRequest = new SignupRequest();
        $serviceRequest->populate($rpcServiceRequest->toArray());

        $serviceResponse = $this->signupService->signup($serviceRequest);

        $rpcServiceResponse = new RpcTask\Customer\Signup\Response();
        if ($serviceResponse->isError()) {
            $rpcServiceResponse->error = $serviceResponse->getError();
        } else {
            $rpcServiceResponse->customer      = $serviceResponse->customer->toArray();
            $rpcServiceResponse->purchaseToken = $serviceResponse->purchaseToken->toArray();
        }

        return $rpcServiceResponse;
    }
}
