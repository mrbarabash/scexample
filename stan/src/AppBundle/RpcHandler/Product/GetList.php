<?php

namespace AppBundle\RpcHandler\Product;

use AppBundle\Entity\Product;
use AppBundle\Entity\Repository\{
    ProductRepository, VatRateRepository, WebsiteRepository
};
use AppBundle\Service\GeoIpService;
use EaPaysites\RpcTask;
use EaPaysites\Service\{
    Rpc\Handler\AbstractRpcHandler, ServiceRequestInterface, ServiceResponseInterface
};
use EaPaysites\Util\{Services, Validators};
use Particle\Validator\{ValidationResult, Validator};

class GetList extends AbstractRpcHandler
{
    const FALLBACK_PRODUCT_PRICE_COUNTRY = 'us';

    /**
     * @var ProductRepository
     */
    private $productRepo;

    /**
     * @var GeoIpService
     */
    private $geoIpService;

    /**
     * @var WebsiteRepository
     */
    private $websiteRepo;

    /**
     * @var VatRateRepository
     */
    private $vatRateRepo;

    /**
     * @param ProductRepository $productRepository
     * @param WebsiteRepository $websiteRepository
     * @param GeoIpService      $geoIpService
     * @param VatRateRepository $vatRateRepository
     */
    public function __construct(
        ProductRepository $productRepository,
        WebsiteRepository $websiteRepository,
        GeoIpService $geoIpService,
        VatRateRepository $vatRateRepository
    ) {
        $this->productRepo = $productRepository;
        $this->websiteRepo = $websiteRepository;
        $this->geoIpService = $geoIpService;
        $this->vatRateRepo = $vatRateRepository;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\Product\GetList\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $rpcServiceResponse = new RpcTask\Product\GetList\Response();

        $validationResult = $this->validateGetListRequest($rpcServiceRequest);
        if (!$validationResult->isValid()) {
            Services::setValidationError($rpcServiceResponse, $validationResult);

            return $rpcServiceResponse;
        }

        $country = $this->geoIpService->getCountryCodeByIp($rpcServiceRequest->ip);

        $products = $this->productRepo->findBy([
            'website' => $rpcServiceRequest->website,
            'active' => true,
        ]);
        $vatRate = $this->vatRateRepo->findActiveByCountry($country);
        $products = array_map(function (Product $product) use ($country, $vatRate) {
            $arrayData = $product->toArray();
            $productPrice = $product->getProductPrice($country)
                ?: $product->getProductPrice(self::FALLBACK_PRODUCT_PRICE_COUNTRY);
            $arrayData['price'] = $productPrice->getPrice();
            $arrayData['currency'] = $productPrice->getCurrency();
            $arrayData['priceMonthly'] = $productPrice->getPriceMonthly();

            return $arrayData;
        }, $products);

        $rpcServiceResponse->products = $products;

        return $rpcServiceResponse;
    }

    /**
     * @param RpcTask\Product\GetList\Request $request
     *
     * @return ValidationResult
     */
    private function validateGetListRequest(RpcTask\Product\GetList\Request $request): ValidationResult
    {
        $v = new Validator();

        $v->required('website')
            ->greaterThan(0)
            ->digits()
            ->callback(Validators::getEntityExistsRule($this->websiteRepo, 'id', 'Website not found'));
        $v->required('ip')
            ->callback(Validators::getIpValidationRule());

        return $v->validate(get_object_vars($request));
    }
}
