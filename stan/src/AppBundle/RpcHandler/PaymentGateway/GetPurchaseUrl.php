<?php

namespace AppBundle\RpcHandler\PaymentGateway;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderProduct;
use AppBundle\Entity\Payment;
use AppBundle\Entity\Product;
use AppBundle\Entity\Repository\CustomerRepository;
use AppBundle\Entity\Repository\OrderRepository;
use AppBundle\Entity\Repository\ProductRepository;
use AppBundle\Service\GeoIpService;
use AppBundle\Service\PaymentGateway\PaymentGatewayServiceInterface;
use AppBundle\Service\PaymentGateway\RocketGateService;
use EaPaysites\RpcTask;
use EaPaysites\Service\{
    Rpc\Handler\AbstractRpcHandler, ServiceRequestInterface, ServiceResponseInterface
};
use EaPaysites\Util\Services;
use EaPaysites\Util\Validators;
use Particle\Validator\ValidationResult;
use Particle\Validator\Validator;

class GetPurchaseUrl extends AbstractRpcHandler
{
    /**
     * @var ProductRepository
     */
    private $productRepo;

    /**
     * @var CustomerRepository
     */
    private $customerRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var GeoIpService
     */
    private $geoIpService;

    /**
     * @var PaymentGatewayServiceInterface
     */
    private $paymentGatewayService;

    /**
     * @param ProductRepository  $productRepository
     * @param CustomerRepository $customerRepository
     * @param OrderRepository    $orderRepository
     * @param GeoIpService       $geoIpService
     * @param RocketGateService  $paymentGatewayService
     */
    public function __construct(
        ProductRepository $productRepository,
        CustomerRepository $customerRepository,
        OrderRepository $orderRepository,
        GeoIpService $geoIpService,
        RocketGateService $paymentGatewayService
    ) {
        $this->productRepo = $productRepository;
        $this->customerRepo = $customerRepository;
        $this->orderRepo = $orderRepository;
        $this->geoIpService = $geoIpService;
        $this->paymentGatewayService = $paymentGatewayService;
    }

    /**
     * @param ServiceRequestInterface|RpcTask\PaymentGateway\GetPurchaseUrl\Request $rpcServiceRequest
     *
     * @return ServiceResponseInterface
     */
    public function execute(ServiceRequestInterface $rpcServiceRequest): ServiceResponseInterface
    {
        $rpcServiceResponse = new RpcTask\PaymentGateway\GetPurchaseUrl\Response();

        $validationResult = $this->validateGetPurchaseUrlRequest($rpcServiceRequest);
        if (!$validationResult->isValid()) {
            Services::setValidationError($rpcServiceResponse, $validationResult);

            return $rpcServiceResponse;
        }

        /** @var Customer $customer */
        $customer = $this->customerRepo->find($rpcServiceRequest->userId);
        /** @var Product $product */
        $product = $this->productRepo->find($rpcServiceRequest->productId);
        $country = $this->geoIpService->getCountryCodeByIp($rpcServiceRequest->ip);

        /** @var Order $order */
        $order = $this->orderRepo->findOneBy([
            'status' => Payment::PAYMENT_STATUS_PENDING,
            'customer' => $customer,
        ]);
        if (!$order) {
            $order = $this->createOrder($customer, $product, $country, $rpcServiceRequest->contentId);
        }

        $rpcServiceResponse->url = $this->paymentGatewayService->getPurchaseUrl(
            $customer,
            $order,
            $rpcServiceRequest->locale
        );

        return $rpcServiceResponse;
    }

    /**
     * @param Customer $customer
     * @param Product  $product
     * @param string   $country
     * @param int|null $contentId
     *
     * @return Order
     */
    private function createOrder(
        Customer $customer,
        Product $product,
        string $country,
        int $contentId = null
    ): Order {
        $order = new Order();
        $order->setStatus(Payment::PAYMENT_STATUS_PENDING);
        $order->setCustomer($customer);

        $orderProduct = new OrderProduct();
        $orderProduct->setOrder($order);
        $orderProduct->setProduct($product);
        $orderProduct->setContentId($contentId);

        $order->addProduct($orderProduct);

        $payment = new Payment();
        $payment->setOrder($order);
        $payment->setAmount($product->getProductPrice($country)->getPrice());

        $order->addPayment($payment);

        $this->orderRepo->store($order, true);

        return $order;
    }

    /**
     * @param RpcTask\PaymentGateway\GetPurchaseUrl\Request $request
     *
     * @return ValidationResult
     */
    private function validateGetPurchaseUrlRequest(RpcTask\PaymentGateway\GetPurchaseUrl\Request $request): ValidationResult
     {
         $v = new Validator();

         $v->required('userId')
             ->greaterThan(0)
             ->digits()
             ->callback(Validators::getEntityExistsRule($this->customerRepo, 'id', 'Customer not found'));
         $v->required('productId')
             ->greaterThan(0)
             ->digits()
             ->callback(Validators::getEntityExistsRule($this->productRepo, 'id', 'Product not found'));
         $v->required('ip')
             ->callback(Validators::getIpValidationRule());
         $v->required('locale')
             ->string()
             ->length(2);

         return $v->validate(get_object_vars($request));
     }
}
