<?php

namespace Tests\AppBundle;

use AppBundle\Entity\ContentRating;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderProduct;
use AppBundle\Entity\Payment;
use AppBundle\Entity\PaymentTransaction;
use AppBundle\Entity\PaymentVat;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductPrice;
use AppBundle\Entity\PurchaseToken;
use AppBundle\Entity\VatRate;
use AppBundle\Entity\Website;
use EaPaysites\Entity\AbstractEntity;

class EntityFactory
{
    /**
     * @param array $data
     *
     * @return Customer
     */
    public function getCustomer(array $data = []): Customer
    {
        $data = array_merge(
            [
                'email'            => 'MaX@EvilAngel.com',
                'emailCanonical'   => 'max@evilangel.com',
                'name'             => 'Max',
                'password'         => '$2y$10$F1GavDg/FYn7VPt/0JQbrOHs06BAJoQQNfiCTXWPelT2Agwv7LIIy',
                // encoded 12345678
                'creationDateTime' => new \DateTime('2017-05-20 14:40:00')
            ],
            $data
        );

        /** @var Customer $entity */
        $entity = $this->createEntity('AppBundle\Entity\Customer', $data);

        return $entity;
    }

    /**
     * @param array $data
     *
     * @return ContentRating
     */
    public function getContentRating(array $data = []): ContentRating
    {
        $data = array_merge(
            [
            ],
            $data
        );

        /** @var ContentRating $entity */
        $entity = $this->createEntity(ContentRating::class, $data);

        return $entity;
    }

    /**
     * @param array $data
     *
     * @return Website
     */
    public function getWebsite(array $data = []): Website
    {
        $data = array_merge(
            [
                'id' => time(),
                'url'  => 'blackmailed.dev',
                'name' => 'Blackmailed',
            ],
            $data
        );

        /** @var Website $website */
        $website = $this->createEntity(Website::class, $data);

        return $website;
    }

    /**
     * @param array $data
     *
     * @return Product
     */
    public function getProduct(array $data = []): Product
    {
        $data = array_merge(
            [
                'title'              => 'Test Product',
                'description'        => 'Description',
                'billingDescription' => 'Billing Description',
                'kind'               => Website::staticGetOwnClassName(),
                'website'            => $this->getWebsite(),
                'active'             => true,
                'recurrent'          => false,
            ],
            $data
        );

        /** @var Product $product */
        $product = $this->createEntity(Product::class, $data);

        return $product;
    }

    /**
     * @param array $data
     *
     * @return ProductPrice
     */
    public function getProductPrice(array $data = []): ProductPrice
    {
        $data = array_merge(
            [
                'country'  => 'ru',
                'price'    => 10.00,
                'currency' => 'USD',
                'product'  => $this->getProduct(),
            ],
            $data
        );

        /** @var ProductPrice $productPrice */
        $productPrice = $this->createEntity(ProductPrice::class, $data);

        return $productPrice;
    }

    /**
     * @param array $data
     *
     * @return OrderProduct
     */
    public function getOrderProduct(array $data = []): OrderProduct
    {
        /** @var OrderProduct $orderProduct */
        $orderProduct = $this->createEntity(OrderProduct::class, $data);

        return $orderProduct;
    }

    /**
     * @param array $data
     *
     * @return VatRate
     */
    public function getVatRate(array $data = []): VatRate
    {
        $data = array_merge(
            [
                'country'   => 'ru',
                'rate'      => 13,
            ],
            $data
        );

        /** @var VatRate $vatRate */
        $vatRate = $this->createEntity(VatRate::class, $data);

        return $vatRate;
    }

    /**
     * @param array $data
     *
     * @return Order
     */
    public function getOrder(array $data = []): Order
    {
        $data = array_merge(
            [
                'customer' => $this->getCustomer(),
            ],
            $data
        );

        /** @var Order $order */
        $order = $this->createEntity(Order::class, $data);

        return $order;
    }

    /**
     * @param array $data
     *
     * @return Payment
     */
    public function getPayment(array $data = []): Payment
    {
        $data = array_merge(
            [
                'order' => $this->getOrder(),
                'amount' => 10.00,
            ],
            $data
        );

        /** @var Payment $payment */
        $payment = $this->createEntity(Payment::class, $data);

        return $payment;
    }

    /**
     * @return PurchaseToken
     */
    public function getPurchaseToken(array $data = []): PurchaseToken
    {
        $data = array_merge(
            [
                'website' => $this->getWebsite(),
                'kind' => 'Scene',
                'customer' => $this->getCustomer(),
            ],
            $data
        );

        /** @var PurchaseToken $token */
        $token = $this->createEntity(PurchaseToken::class, $data);

        return $token;
    }

    /**
     * @param array $data
     *
     * @return PaymentVat
     */
    public function getPaymentVat(array $data = []): PaymentVat
    {
        /** @var PaymentVat $paymentVat */
        $paymentVat = $this->createEntity(PaymentVat::class, $data);

        return $paymentVat;
    }

    /**
     * @param array $data
     *
     * @return PaymentTransaction
     */
    public function getPaymentTransaction(array $data = []): PaymentTransaction
    {
        $data = array_merge(
            [
                'status' => Payment::PAYMENT_STATUS_PENDING,
                'ip' => '127.0.0.1',
            ],
            $data
        );

        /** @var PaymentTransaction $paymentTransaction */
        $paymentTransaction = $this->createEntity(PaymentTransaction::class, $data);

        return $paymentTransaction;
    }

    protected function createEntity(string $class, array $data): AbstractEntity
    {
        /** @var AbstractEntity $entity */
        $entity = new $class();
        $entity->populate($data);

        if (isset($data['id'])) {
            $ref  = new \ReflectionObject($entity);
            $prop = $ref->getProperty('id');
            $prop->setAccessible(true);
            $prop->setValue($entity, $data['id']);
        }

        return $entity;
    }
}
