<?php

namespace Tests\AppBundle\Command\ConsumeSqsQueue;

use AppBundle\Service\Email\EmailSenderInterface;
use EaPaysites\RpcTask;
use EaPaysites\Sqs\Client\SqsClientInterface;
use Tests\AppBundle\CommandTestCase;

class SendEmailConsumerTest extends CommandTestCase
{
    public function testSendgridGetsCalled()
    {
        $messages = json_decode(
            '[{"MessageId":"f016be6f-7e75-4572-8681-227ded84b1fd","ReceiptHandle":"f016be6f-7e75-4572-8681-227ded84b1fd#688a5ddf-7fea-4605-826a-5afb955cafb4","MD5OfBody":"35f77c4f7c2b46c850088cba2a325411","Body":"{\"fromEmail\":\"noreply@blackmailed.com\",\"fromName\":\"Blackmailed\",\"toEmail\":\"max@evilangel.com\",\"subject\":\"Reset your password\",\"content\":\"To reset your password, follow this link: http:\\\/\\\/blackmailed.dev\\\/app_dev.php\\\/reset-password?t=CKKy12gcg02PZCGNV5ne1La6K39BGTjkVUNqgbBDVweTvhQchfgPDl67qdkMNKyD8rTI7F9zoWDj2vUgLHSx1w--\",\"mimeType\":\"text\\\/plain\",\"replacements\":[]}"}]',
            true
        );

        $sqsClientMock = \Mockery::mock(SqsClientInterface::class);
        $sqsClientMock->shouldReceive('receiveMessages')->once()->andReturn($messages);
        $sqsClientMock->shouldReceive('deleteMessage')->withArgs(
            ['email_transactional', 'f016be6f-7e75-4572-8681-227ded84b1fd#688a5ddf-7fea-4605-826a-5afb955cafb4']
        )->once()->andReturn($messages);
        $this->getContainer()->stub(SqsClientInterface::class, $sqsClientMock);

        $emailSenderMock = \Mockery::mock(EmailSenderInterface::class);
        $emailSenderMock->shouldReceive('send')->once();
        $this->getContainer()->stub(EmailSenderInterface::class, $emailSenderMock);

        $input = [
            'command' => 'app:consume-sqs-queue',
            'queueId' => 'email_transactional',
            '--run-once'   => 1
        ];

        $this->initAppTester();
        $this->appTester->run($input);

        $output = $this->appTester->getDisplay();

        $this->assertContains('Starting consuming...', $output);
        $this->assertContains('Finished consuming.', $output);
    }
}
