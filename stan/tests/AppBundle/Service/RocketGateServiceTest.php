<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\PaymentGateway\RocketGateService;
use Tests\AppBundle\AppTestCase;

class RocketGateServiceTest extends AppTestCase
{
    public function testBugWithRelativeParameterPaths()
    {
        $urlPrefix = 'http://stan.dev';

        $customer = $this->entityFactory->getCustomer();
        $order = $this->entityFactory->getOrder(['id' => time()]);
        $payment = $this->entityFactory->getPayment(['order' => $order]);
        $orderProduct = $this->entityFactory->getOrderProduct();
        $product = $this->entityFactory->getProduct();

        $orderProduct->setProduct($product);
        $orderProduct->setOrder($order);
        $order->addProduct($orderProduct);
        $order->addPayment($payment);

        $rocketGateService = $this->getContainer()->get(RocketGateService::class);
        $purchaseUrl = $rocketGateService->getPurchaseUrl($customer, $order, 'en');

        // Convert query string parameters to array.
        parse_str(parse_url($purchaseUrl)['query'], $queryParams);

        $this->assertStringStartsWith($urlPrefix, $queryParams['success']);
        $this->assertStringStartsWith($urlPrefix, $queryParams['fail']);
    }
}
