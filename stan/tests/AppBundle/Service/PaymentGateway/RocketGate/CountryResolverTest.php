<?php

namespace Tests\AppBundle\Service\PaymentGateway\RocketGate;

use AppBundle\Entity\VatRate;
use AppBundle\Service\PaymentGateway\RocketGate\CountryResolver;
use PHPUnit\Framework\TestCase;

class CountryResolverTest extends TestCase
{
    /**
     * @dataProvider resolveDataProvider
     *
     * @param string      $ipCountry
     * @param string      $assumedCountry
     * @param string|null $customerCountry
     * @param string|null $bankCountry
     */
    public function testResolve(
        string $ipCountry,
        string $assumedCountry,
        string $customerCountry = null,
        string $bankCountry = null
    ) {
        $this->assertEquals(
            $assumedCountry,
            (new CountryResolver($ipCountry, $customerCountry, $bankCountry))->resolveCountry()
        );
    }

    /**
     * @return array
     */
    public function resolveDataProvider(): array
    {
        return [
            ['us', 'us'],
            ['us', 'us', 'us'],
            ['us', 'ru', 'ru'],
            ['us', 'us', null, 'us'],
            ['us', 'ru', null, 'ru'],
            ['us', 'us', 'us', 'us'],
            ['us', 'us', 'ru', 'us'],
            ['us', 'us', 'us', 'ru'],
            ['us', 'ru', 'ru', 'ru'],
        ];
    }
}
