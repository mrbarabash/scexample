<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\GeoIpService;
use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use PHPUnit\Framework\TestCase;

class GeoIpServiceTest extends TestCase
{
    public function testFallbackCountry()
    {
        $readerMock = \Mockery::mock(Reader::class);
        $readerMock
            ->shouldReceive('city')
            ->once()
            ->andThrow(new AddressNotFoundException());

        $service = new GeoIpService($readerMock);

        $this->assertEquals(GeoIpService::FALLBACK_COUNTRY_CODE, $service->getCountryCodeByIp('127.0.0.1'));
    }
}
