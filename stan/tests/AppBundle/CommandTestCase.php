<?php

namespace Tests\AppBundle;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\ApplicationTester;

abstract class CommandTestCase extends AppTestCase
{
    /**
     * @var ApplicationTester
     */
    protected $appTester;

    protected function initAppTester()
    {
        $application = new Application(static::$kernel);
        $application->setAutoExit(false);

        $this->appTester = new ApplicationTester($application);
    }
}
