<?php

namespace Tests\AppBundle;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class ControllerTestCase extends AppTestCase
{
    /**
     * @var Client
     */
    protected $client = null;

    public function setUp()
    {
        parent::setUp();

        $this->client = static::$kernel->getContainer()->get('test.client');
    }

    /**
     * @param User|null $user
     */
    protected function logIn($user = null)
    {
        if ($user === null) {
            $user = 'admin';
        }

        /** @var Session $session */
        $session = $this->client->getContainer()->get('session');

        $firewall = 'main';
        $token    = new UsernamePasswordToken($user, null, $firewall, ['ROLE_SUPER_ADMIN']);
        $session->set('_security_' . $firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    protected function tearDown()
    {
        parent::tearDown();

        // knp paginator sets global $_GET... wtf?
        $_GET = [];
    }

    protected function printResponse()
    {
        echo "\n\n";
        print_r($this->client->getResponse()->getContent());
        echo "\n";
        print_r($this->client->getResponse()->getStatusCode());
        echo "\n\n";
    }

    /**
     * @return array
     */
    protected function getRpcAccessHeaders()
    {
        return ['HTTP_X-Stan-Access-Key' => 'some-secret-key'];
    }

    /**
     * @param string $path
     * @param        $data
     *
     * @return Crawler
     */
    protected function makeRpcRequest(string $path, $data): Crawler
    {
        return $this->client->request(
            'POST',
            '/rpc/execute/' . $path,
            [],
            [],
            $this->getRpcAccessHeaders(),
            json_encode($data)
        );
    }
}
