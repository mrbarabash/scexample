<?php

namespace Tests\AppBundle\Controller\RpcServer\Product;

use Tests\AppBundle\ControllerTestCase;

class GetListTest extends ControllerTestCase
{
    public function testRequestWithNonExistingWebsiteFails()
    {
        $this->makeRpcRequest('product/getList', ['website' => 0, 'ip' => '127.0.0.1']);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/website/Entity::NOT_FOUND');
    }

    /**
     * @dataProvider incorrectIpAddressDataProvider
     *
     * @param string $ip
     * @param string $exception
     */
    public function testRequestWithIncorrectIpAddressFails(string $ip, string $exception)
    {
        $website = $this->entityFactory->getWebsite();
        $this->em->persist($website);
        $this->em->flush();

        $this->makeRpcRequest('product/getList', ['website' => $website->getId(), 'ip' => $ip]);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/ip/' . $exception);
    }

    /**
     * @return array
     */
    public function incorrectIpAddressDataProvider(): array
    {
        return [
            [str_repeat('a', 6), 'Ip::INCORRECT_VALUE'],
            [str_repeat('a', 41), 'Ip::INCORRECT_VALUE'],
        ];
    }

    public function testRequestWithEmptyInputFails()
    {
        $this->makeRpcRequest('product/getList', ['website' => '', 'ip' => '']);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/website/Entity::NOT_FOUND');
        $this->assertJsonKey($response->getContent(), 'error/data/ip/NotEmpty::EMPTY_VALUE');
    }

    public function testNotExistingParametersReturnEmptyResponse()
    {
        $website1 = $this->entityFactory->getWebsite();
        $website2 = $this->entityFactory->getWebsite(['id' => 1]);
        $this->em->persist($website1);
        $this->em->persist($website2);

        $product      = $this->entityFactory->getProduct(['website' => $website1]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $product->addProductPrice($productPrice);

        $this->em->persist($product);
        $this->em->flush();

        $this->makeRpcRequest('product/getList', ['website' => $website2->getId(), 'ip' => '127.0.0.1']);
        $response = $this->client->getResponse();

        $this->assertTrue($response->isSuccessful());
        $this->assertEquals('{"products":[]}', $response->getContent());
    }

    public function testExistingParametersReturnProductList()
    {
        $this->mockGeoIpService();

        $website = $this->entityFactory->getWebsite();
        $this->em->persist($website);

        $product      = $this->entityFactory->getProduct(['website' => $website]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $product->addProductPrice($productPrice);

        $this->em->persist($product);
        $this->em->flush();

        $this->makeRpcRequest('product/getList', ['website' => $website->getId(), 'ip' => '127.0.0.1']);
        $response = $this->client->getResponse();

        $this->assertTrue($response->isSuccessful());
        $this->assertNotEmpty(json_decode($response->getContent(), true)['products']);
    }

    public function testBugWithNotFoundProductPrices()
    {
        $this->mockGeoIpService();

        $website = $this->entityFactory->getWebsite();
        $this->em->persist($website);

        $product      = $this->entityFactory->getProduct(['website' => $website]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product, 'country' => 'us']);
        $product->addProductPrice($productPrice);

        $this->em->persist($product);
        $this->em->flush();

        $this->makeRpcRequest('product/getList', ['website' => $website->getId(), 'ip' => '127.0.0.1']);
        $response = $this->client->getResponse();

        $this->assertTrue($response->isSuccessful());
        $this->assertNotEmpty(json_decode($response->getContent(), true)['products']);
    }
}
