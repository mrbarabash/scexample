<?php

namespace Tests\AppBundle\Controller\RpcServer\PurchaseToken;

use AppBundle\Entity\ContentPermission;
use EaPaysites\Entity\ContentPermission as StanContentPermission;
use Tests\AppBundle\ControllerTestCase;

class RedeemTest extends ControllerTestCase
{
    public function testNonExistsTokenFails()
    {
        $this->makeRpcRequest('purchaseToken/redeem', ['id' => time()]);
        $response = $this->client->getResponse();

        $this->assertTrue($response->isClientError());
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/id/Entity::NOT_FOUND');
    }

    public function testInvalidUsedForValueFails()
    {
        $this->makeRpcRequest('purchaseToken/redeem', ['usedFor' => 'test']);
        $response = $this->client->getResponse();

        $this->assertTrue($response->isClientError());
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/usedFor/GreaterThan::NOT_GREATER_THAN');
    }

    public function testEmptyValuesFails()
    {
        $this->makeRpcRequest('purchaseToken/redeem', []);
        $response = $this->client->getResponse();

        $this->assertTrue($response->isClientError());
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/id/NotEmpty::EMPTY_VALUE');
        $this->assertJsonKey($response->getContent(), 'error/data/usedFor/NotEmpty::EMPTY_VALUE');
    }

    public function testValidData()
    {
        $blackmailedSceneId = 1;

        $customer = $this->entityFactory->getCustomer();
        $website = $this->entityFactory->getWebsite();
        $token = $this->entityFactory->getPurchaseToken(['customer' => $customer, 'website' => $website]);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($token);
        $this->em->flush();

        $this->makeRpcRequest('purchaseToken/redeem', ['id' => $token->getId(), 'usedFor' => $blackmailedSceneId]);
        $response = $this->client->getResponse();

        $this->assertTrue($response->isSuccessful());

        $this->em->refresh($token);

        $this->assertEquals($blackmailedSceneId, $token->getUsedFor());
        $this->assertNotNull($token->getUsedAt());

        $contentPermission = $this->em->getRepository(ContentPermission::class)->findOneBy([
            'customer' => $customer,
            'contentId' => $blackmailedSceneId,
            'contentType' => $token->getKind(),
        ]);

        $this->assertNotNull($contentPermission);
        $this->assertEquals(StanContentPermission::PERMISSION_SCENE_VIEW_FULL, $contentPermission->getPermission());
        $this->assertEquals($token->getId(), $contentPermission->getGrantedVia());
        $this->assertEquals($token->getOwnClassName(), $contentPermission->getGrantedViaType());
        $this->assertNull($contentPermission->getExpiresAt());
    }
}
