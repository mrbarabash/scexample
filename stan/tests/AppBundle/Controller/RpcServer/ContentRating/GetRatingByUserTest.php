<?php

namespace Tests\AppBundle\Controller\RpcServer\ContentRating;

use Tests\AppBundle\ControllerTestCase;

class GetRatingByUserTest extends ControllerTestCase
{
    public function testRequestWithNonExistingUserFails()
    {
        $this->makeRpcRequest(
            'contentRating/getRatingByUser',
            ['userId' => '', 'contentId' => 123, 'contentType' => 'Scene']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/userId/Entity::NOT_FOUND');
    }

    public function testRequestWithEmptyInputFails()
    {
        $this->makeRpcRequest(
            'contentRating/getRatingByUser',
            ['userId' => '', 'contentId' => '', 'contentType' => '']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/userId/Entity::NOT_FOUND');
        $this->assertJsonKey($response->getContent(), 'error/data/contentId/GreaterThan::NOT_GREATER_THAN');
        $this->assertJsonKey($response->getContent(), 'error/data/contentType/NotEmpty::EMPTY_VALUE');
    }

    public function testExistingRatingReturnsValue()
    {
        $customer      = $this->entityFactory->getCustomer();
        $contentRating = $this->entityFactory->getContentRating(
            [
                'customer'    => $customer,
                'contentId'   => 222,
                'contentType' => 'Scene',
                'value'       => 1
            ]
        );

        $this->em->persist($customer);
        $this->em->persist($contentRating);
        $this->em->flush();

        $this->makeRpcRequest(
            'contentRating/getRatingByUser',
            ['userId' => $customer->getId(), 'contentId' => 222, 'contentType' => 'Scene']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonValue(1, $response->getContent(), 'value');
    }

    public function testNonExistingRatingReturnsNull()
    {
        $customer = $this->entityFactory->getCustomer();

        $this->em->persist($customer);
        $this->em->flush();

        $this->makeRpcRequest(
            'contentRating/getRatingByUser',
            ['userId' => $customer->getId(), 'contentId' => 222, 'contentType' => 'Scene']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{}', $response->getContent());
    }
}
