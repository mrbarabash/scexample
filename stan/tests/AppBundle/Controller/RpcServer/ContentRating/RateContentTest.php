<?php

namespace Tests\AppBundle\Controller\RpcServer\ContentRating;

use AppBundle\Entity\ContentRating;
use Tests\AppBundle\ControllerTestCase;

class RateContentTest extends ControllerTestCase
{
    public function testRequestWithNonExistingUserFails()
    {
        $this->makeRpcRequest(
            'contentRating/rateContent',
            ['userId' => 111, 'contentId' => 222, 'contentType' => 'Scene', 'value' => 1]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/userId/Entity::NOT_FOUND');
    }

    public function testRequestWithEmptyInputFails()
    {
        $this->makeRpcRequest(
            'contentRating/rateContent',
            ['userId' => '', 'contentId' => '', 'contentType' => '', 'value' => '']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/userId/Entity::NOT_FOUND');
        $this->assertJsonKey($response->getContent(), 'error/data/contentId/GreaterThan::NOT_GREATER_THAN');
        $this->assertJsonKey($response->getContent(), 'error/data/contentType/NotEmpty::EMPTY_VALUE');
        $this->assertJsonKey($response->getContent(), 'error/data/value/InArray::NOT_IN_ARRAY');
    }

    public function testInvalidValueFails()
    {
        $this->makeRpcRequest(
            'contentRating/rateContent',
            ['userId' => 111, 'contentId' => 222, 'contentType' => 'Scene', 'value' => 2]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/value/InArray::NOT_IN_ARRAY');
    }

    public function testUserRatesContentFirstTime()
    {
        $customer = $this->entityFactory->getCustomer();
        $this->em->persist($customer);
        $this->em->flush();

        $this->makeRpcRequest(
            'contentRating/rateContent',
            ['userId' => $customer->getId(), 'contentId' => 222, 'contentType' => 'Scene', 'value' => 1]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        /** @var ContentRating $contentRating */
        $contentRating = $this->em->getRepository(ContentRating::class)->findOneBy([]);
        $this->assertEquals($customer->getId(), $contentRating->getCustomer()->getId());
        $this->assertEquals(222, $contentRating->getContentId());
        $this->assertEquals('Scene', $contentRating->getContentType());
        $this->assertEquals(1, $contentRating->getValue());
        $this->assertNotNull($contentRating->getCreatedAt());
        $this->assertNotNull($contentRating->getUpdatedAt());
    }

    public function testUserUpdatesRating()
    {
        $customer      = $this->entityFactory->getCustomer();
        $contentRating = $this->entityFactory->getContentRating(
            [
                'customer'    => $customer,
                'contentId'   => 222,
                'contentType' => 'Scene',
                'value'       => 1
            ]
        );

        $this->em->persist($customer);
        $this->em->persist($contentRating);
        $this->em->flush();

        $this->makeRpcRequest(
            'contentRating/rateContent',
            ['userId' => $customer->getId(), 'contentId' => 222, 'contentType' => 'Scene', 'value' => -1]
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $this->em->refresh($contentRating);
        $this->assertEquals($customer->getId(), $contentRating->getCustomer()->getId());
        $this->assertEquals(222, $contentRating->getContentId());
        $this->assertEquals('Scene', $contentRating->getContentType());
        $this->assertEquals(-1, $contentRating->getValue());
    }
}
