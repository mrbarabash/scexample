<?php

namespace Tests\AppBundle\Controller\RpcServer\ContentRating;

use Tests\AppBundle\ControllerTestCase;

class GetAggregateRating extends ControllerTestCase
{
    public function testRatingsGroupCorrectly()
    {
        $customer1      = $this->entityFactory->getCustomer();
        $customer2      = $this->entityFactory->getCustomer(
            ['email' => 'max2@ea.com', 'emailCanonical' => 'max2@ea.com']
        );
        $contentRating1 = $this->entityFactory->getContentRating(
            [
                'customer'    => $customer1,
                'contentId'   => 222,
                'contentType' => 'Scene',
                'value'       => 1
            ]
        );
        $contentRating2 = $this->entityFactory->getContentRating(
            [
                'customer'    => $customer2,
                'contentId'   => 222,
                'contentType' => 'Scene',
                'value'       => -1
            ]
        );

        $this->em->persist($customer1);
        $this->em->persist($customer2);
        $this->em->persist($contentRating1);
        $this->em->persist($contentRating2);
        $this->em->flush();

        $this->makeRpcRequest('contentRating/getAggregateRating', ['contentIds' => [222], 'contentType' => 'Scene']);
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonValue(0.5, $response->getContent(), 'contentIdsToRatings/222');
    }

    public function testNonExistentContentIdIgnored()
    {
        $customer1      = $this->entityFactory->getCustomer();
        $contentRating1 = $this->entityFactory->getContentRating(
            [
                'customer'    => $customer1,
                'contentId'   => 222,
                'contentType' => 'Scene',
                'value'       => 1
            ]
        );

        $this->em->persist($customer1);
        $this->em->persist($contentRating1);
        $this->em->flush();

        $this->makeRpcRequest(
            'contentRating/getAggregateRating',
            ['contentIds' => [222, 333], 'contentType' => 'Scene']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);

        $this->assertEquals(['contentIdsToRatings' => ['222' => 1]], $json);
    }
}
