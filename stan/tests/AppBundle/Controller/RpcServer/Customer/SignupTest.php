<?php

namespace Tests\AppBundle\Controller\RpcServer\Customer;

use AppBundle\Entity\PurchaseToken;
use EaPaysites\Util\Passwords;
use Tests\AppBundle\ControllerTestCase;

class SignupTest extends ControllerTestCase
{
    public function testSignupEmptyEmail()
    {
        $this->makeRpcRequest('customer/signup', new \stdClass());
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');
        $this->assertJsonKey($response->getContent(), 'error/data/email/NotEmpty::EMPTY_VALUE');
        $this->assertJsonKey($response->getContent(), 'error/data/password/NotEmpty::EMPTY_VALUE');
    }

    public function testSignupLongEmail()
    {
        $this->makeRpcRequest('customer/signup', ['email' => str_repeat('x', 101)]);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertJsonKey($this->client->getResponse()->getContent(), 'error/data/email/LengthBetween::TOO_LONG');
    }

    public function testSignupWrongEmailFormat()
    {
        $this->makeRpcRequest('customer/signup', ['email' => 'asd@asf', 'password' => '12345678']);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertJsonKey($this->client->getResponse()->getContent(), 'error/data/email/Email::INVALID_VALUE');
    }

    public function testSignupDuplicateEmail()
    {
        $customer = $this->entityFactory->getCustomer();
        $this->em->persist($customer);
        $this->em->flush();

        $this->makeRpcRequest('customer/signup', ['email' => $customer->getEmail()]);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertJsonKey($this->client->getResponse()->getContent(), 'error/data/email/Entity::DUPLICATE');
    }

    public function testSignupShortPassword()
    {
        $this->makeRpcRequest('customer/signup', ['password' => str_repeat('x', 7)]);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertJsonKey(
            $this->client->getResponse()->getContent(),
            'error/data/password/LengthBetween::TOO_SHORT'
        );
    }

    public function testSignupLongPassword()
    {
        $this->makeRpcRequest('customer/signup', ['password' => str_repeat('x', 257)]);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertJsonKey($this->client->getResponse()->getContent(), 'error/data/password/LengthBetween::TOO_LONG');
    }

    public function testSignupLongName()
    {
        $this->makeRpcRequest('customer/signup', ['name' => str_repeat('x', 129)]);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertJsonKey($this->client->getResponse()->getContent(), 'error/data/name/LengthBetween::TOO_LONG');
    }

    public function testSignupNonPrintableName()
    {
        $this->makeRpcRequest('customer/signup', ['name' => 'GBPÂ ï¿½..Â the']);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertJsonKey($this->client->getResponse()->getContent(), 'error/data/name/String::NON_PRINTABLE');
    }

    public function testSignupValidData()
    {
        $website = $this->entityFactory->getWebsite();
        $this->em->persist($website);
        $this->em->flush();

        $this->makeRpcRequest(
            'customer/signup',
            [
                'email'    => 'Max@evilangel.com',
                'password' => '12345678',
                'name'     => 'Max',
                'website'  => $website->getId(),
            ]
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $json     = json_decode($this->client->getResponse()->getContent(), true);
        $customer = $this->em->getRepository('AppBundle:Customer')
                             ->findOneBy(['id' => $json['customer']['id']]);

        $this->assertNotNull($customer);
        $this->assertEquals('max@evilangel.com', $customer->getEmailCanonical());
        $this->assertEquals('Max@evilangel.com', $customer->getEmail());
        $this->assertTrue(Passwords::isPasswordValid($customer->getPassword(), '12345678'));
        $this->assertEquals('Max', $customer->getName());
        $this->assertInstanceOf('DateTime', $customer->getCreationDateTime());

        $this->assertEquals('max@evilangel.com', $json['customer']['emailCanonical']);
        $this->assertEquals('Max@evilangel.com', $json['customer']['email']);
        $this->assertEquals($customer->getPassword(), $json['customer']['password']);
        $this->assertEquals('Max', $json['customer']['name']);
    }

    public function testSignupCanonicalEmail()
    {
        $website = $this->entityFactory->getWebsite();
        $this->em->persist($website);
        $this->em->flush();

        $email = 'MaX@eviLangEl.coM';
        $this->makeRpcRequest(
            'customer/signup',
            [
                'email'    => $email,
                'password' => '12345678',
                'name'     => 'Max',
                'website'  => $website->getId(),
            ]
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $customer = $this->em->getRepository('AppBundle:Customer')
                             ->findByEmail('max@evilangel.com');

        $this->assertEquals($email, $customer->getEmail());
        $this->assertEquals('max@evilangel.com', $customer->getEmailCanonical());
    }

    public function testSignupDuplicateDifferentCase()
    {
        $customer = $this->entityFactory->getCustomer(['email' => 'max@evilangel.com']);
        $this->em->persist($customer);
        $this->em->flush();

        $this->makeRpcRequest('customer/signup', ['email' => 'MAX@evilangel.com']);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertJsonKey($this->client->getResponse()->getContent(), 'error/data/email/Entity::DUPLICATE');
    }

    public function testSignupCreatesPurchaseToken()
    {
        $website = $this->entityFactory->getWebsite();
        $this->em->persist($website);
        $this->em->flush();

        $this->makeRpcRequest(
            'customer/signup',
            [
                'email'    => 'customer@example.com',
                'password' => '12345678',
                'name'     => 'Customer',
                'website'  => $website->getId(),
            ]
        );
        $response = $this->client->getResponse();
        $decodedResponse = json_decode($response->getContent(), true);

        $purchaseToken = $this->em->getRepository(PurchaseToken::class)->findOneBy([
            'customer' => $decodedResponse['customer']['id'],
            'kind' => 'Scene',
            'website' => $website,
        ]);

        $this->assertNotNull($purchaseToken);
        $this->assertJsonKey($response->getContent(), 'purchaseToken/id');
    }
}
