<?php

namespace Tests\AppBundle\Controller\RpcServer\PaymentGateway;

use AppBundle\Entity\Order;
use AppBundle\Entity\OrderProduct;
use AppBundle\Entity\Payment;
use AppBundle\Service\Customer\VatService;
use AppBundle\Service\PaymentGateway\RocketGateService;
use Tests\AppBundle\ControllerTestCase;

class GetPurchaseUrlTest extends ControllerTestCase
{
    public function testRequestWithEmptyInputFails()
    {
        $this->makeRpcRequest(
            'paymentGateway/getPurchaseUrl',
            ['userId' => '', 'productId' => '', 'ip' => '', 'locale' => '']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');

        $this->assertJsonKey($response->getContent(), 'error/data/userId/GreaterThan::NOT_GREATER_THAN');
        $this->assertJsonKey($response->getContent(), 'error/data/userId/Entity::NOT_FOUND');
        $this->assertJsonKey($response->getContent(), 'error/data/productId/GreaterThan::NOT_GREATER_THAN');
        $this->assertJsonKey($response->getContent(), 'error/data/productId/Entity::NOT_FOUND');
        $this->assertJsonKey($response->getContent(), 'error/data/ip/NotEmpty::EMPTY_VALUE');
        $this->assertJsonKey($response->getContent(), 'error/data/locale/NotEmpty::EMPTY_VALUE');
    }

    public function testRequestWithNonExistingCustomerFails()
    {
        $this->makeRpcRequest('paymentGateway/getPurchaseUrl', ['userId' => time()]);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');

        $this->assertJsonKey($response->getContent(), 'error/data/userId/Entity::NOT_FOUND');
    }

    public function testRequestWithNonExistingProductFails()
    {
        $this->makeRpcRequest('paymentGateway/getPurchaseUrl', ['productId' => time()]);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');

        $this->assertJsonKey($response->getContent(), 'error/data/productId/Entity::NOT_FOUND');
    }

    /**
     * @dataProvider incorrectIpAddressDataProvider
     */
    public function testRequestWithIncorrectIpAddressFails(string $ip, string $exception)
    {
        $this->makeRpcRequest('paymentGateway/getPurchaseUrl', ['ip' => $ip]);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');

        $this->assertJsonKey($response->getContent(), 'error/data/ip/' . $exception);
    }

    public function testRequestWithIncorrectLocaleFails()
    {
        $this->makeRpcRequest('paymentGateway/getPurchaseUrl', ['locale' => 'e']);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertJsonValue('Bad Request', $response->getContent(), 'error/message');

        $this->assertJsonKey($response->getContent(), 'error/data/locale/Length::TOO_SHORT');
    }

    /**
     * @return array
     */
    public function incorrectIpAddressDataProvider(): array
    {
        return [
            [str_repeat('a', 6), 'Ip::INCORRECT_VALUE'],
            [str_repeat('a', 41), 'Ip::INCORRECT_VALUE'],
        ];
    }

    public function testGetPurchaseUrlWithCorrectData()
    {
        $this->mockGeoIpService();

        $customer     = $this->entityFactory->getCustomer();
        $website      = $this->entityFactory->getWebsite();
        $product      = $this->entityFactory->getProduct(['website' => $website]);
        $vatRate      = $this->entityFactory->getVatRate();
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $product->addProductPrice($productPrice);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($vatRate);
        $this->em->persist($product);
        $this->em->flush();

        $paymentGatewayServiceMock = \Mockery::mock(RocketGateService::class);
        $paymentGatewayServiceMock
            ->shouldReceive('getPurchaseUrl')
            ->once()
            ->andReturn('test');
        $this->getContainer()->stub(RocketGateService::class, $paymentGatewayServiceMock);

        $this->makeRpcRequest(
            'paymentGateway/getPurchaseUrl',
            [
                'userId'    => $customer->getId(),
                'productId' => $product->getId(),
                'ip'        => '127.0.0.1',
                'locale'    => 'en',
            ]
        );
        $response = $this->client->getResponse();

        $this->assertTrue($response->isSuccessful());
        $this->assertJsonValue('test', $response->getContent(), 'url');

        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findOneBy(['customer' => $customer]);

        $this->assertNotNull($order);
        $this->assertEquals(Payment::PAYMENT_STATUS_PENDING, $order->getStatus());
        $this->assertCount(1, $order->getProducts());
        $this->assertCount(1, $order->getPayments());

        $payment = $order->getLastPayment();

        $this->assertNotNull($payment);
        $this->assertEquals(Payment::PAYMENT_STATUS_NEW, $payment->getStatus());
        $this->assertCount(0, $payment->getTransactions());

        /** @var OrderProduct $orderProduct */
        $orderProduct = $order->getProducts()->first();

        $this->assertNotNull($orderProduct);
        $this->assertEquals($product->getId(), $orderProduct->getProduct()->getId());
        $this->assertNull($orderProduct->getContentId());
    }

    public function testGetPurchaseUrlForExistingOrderDoNotDuplicateOrder()
    {
        $customer     = $this->entityFactory->getCustomer();
        $order        = $this->entityFactory->getOrder(
            ['customer' => $customer, 'status' => Payment::PAYMENT_STATUS_PENDING]
        );
        $website      = $this->entityFactory->getWebsite();
        $product      = $this->entityFactory->getProduct(['website' => $website]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $product->addProductPrice($productPrice);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($product);
        $this->em->persist($order);
        $this->em->flush();

        $paymentGatewayServiceMock = \Mockery::mock(RocketGateService::class);
        $paymentGatewayServiceMock
            ->shouldReceive('getPurchaseUrl')
            ->once()
            ->withArgs([$customer, $order, 'en'])
            ->andReturn('test');
        $this->getContainer()->stub(RocketGateService::class, $paymentGatewayServiceMock);

        $vatServiceMock = \Mockery::mock(VatService::class);
        $vatServiceMock
            ->shouldReceive('getCustomerVatRate')
            ->never();
        $this->getContainer()->stub(VatService::class, $vatServiceMock);

        $this->makeRpcRequest(
            'paymentGateway/getPurchaseUrl',
            [
                'userId'    => $customer->getId(),
                'productId' => $product->getId(),
                'ip'        => '127.0.0.1',
                'locale'    => 'en',
            ]
        );
        $response = $this->client->getResponse();

        $this->assertTrue($response->isSuccessful());
        $this->assertJsonValue('test', $response->getContent(), 'url');

        $orders = $this->em->getRepository(Order::class)->findBy(['customer' => $customer]);

        $this->assertCount(1, $orders);
    }
}
