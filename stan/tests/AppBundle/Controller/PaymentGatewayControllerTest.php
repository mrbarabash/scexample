<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\ContentPermission;
use AppBundle\Entity\Payment;
use AppBundle\Entity\VatRate;
use AppBundle\Service\PaymentGateway\RocketGateService;
use EaPaysites\Entity\ContentPermission as StanContentPermission;
use EaPaysites\Sqs\Client\NullSqsClient;
use EaPaysites\Sqs\Client\SqsClientInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\AppBundle\ControllerTestCase;

class PaymentGatewayControllerTest extends ControllerTestCase
{
    private $invalidXmlRequest = '<?xml version="1.0" encoding="UTF-8"?><HostedPage><transactID>100015E577D72E9</transactID><invoiceID>1</invoiceID><customerID>1</customerID>';

    private $invalidXmlResponse = '<?xml version="1.0" encoding="UTF-8"?><Response><results>4</results><message>XmlRequestParser failed to parse</message></Response>';

    private $notExistsOrderIdXmlRequest = '<?xml version="1.0" encoding="UTF-8"?><HostedPage><transactID>100015E577D72E9</transactID><invoiceID>1</invoiceID><customerID>1</customerID></HostedPage>';

    private $notExistsOrderIdXmlResponse = '<?xml version="1.0" encoding="UTF-8"?><Response><results>5</results><message>Cannot find Order</message></Response>';

    private $validXmlRequestTemplate = '<?xml version="1.0" encoding="UTF-8"?><HostedPage><transactID>100015E577D72E9</transactID><invoiceID>{order}</invoiceID><customerID>1</customerID><ipAddress>{ip}</ipAddress><customerCountry>{customerCountry}</customerCountry><bankCountry>{bankCountry}</bankCountry></HostedPage>';

    private $validXmlResponse = '<?xml version="1.0" encoding="UTF-8"?><Response><results>0</results><message>Test Post Back Message</message></Response>';

    public function testOnlyRocketGateAllowed()
    {
        $this->client->request('POST', '/payment-gateway/callback/another_gateway');

        $this->assertTrue($this->client->getResponse()->isNotFound());
    }

    public function testInvalidXmlCallbackFailed()
    {
        $this->client->request('POST', '/payment-gateway/callback/rg', [], [], [], $this->invalidXmlRequest);
        $this->assertEquals($this->invalidXmlResponse, trim($this->client->getResponse()->getContent()));
    }

    public function testNotExistsOrderIdXmlCallbackFails()
    {
        $this->client->request('POST', '/payment-gateway/callback/rg', [], [], [], $this->notExistsOrderIdXmlRequest);
        $this->assertEquals($this->notExistsOrderIdXmlResponse, trim($this->client->getResponse()->getContent()));
    }

    public function testValidXmlCallbackPass()
    {
        $this->mockGeoIpService();

        $ip           = '127.0.0.1';
        $customer     = $this->entityFactory->getCustomer();
        $order        = $this->entityFactory->getOrder(
            ['customer' => $customer, 'status' => Payment::PAYMENT_STATUS_PENDING]
        );
        $website      = $this->entityFactory->getWebsite();
        $product      = $this->entityFactory->getProduct(['website' => $website]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $vatRateRu    = $this->entityFactory->getVatRate();
        $paymentVat   = $this->entityFactory->getPaymentVat();
        $payment      = $this->entityFactory->getPayment(['order' => $order, 'predictedVat' => $paymentVat]);

        $paymentVat->setPayment($payment);
        $paymentVat->setVatRate($vatRateRu);
        $product->addProductPrice($productPrice);
        $order->addPayment($payment);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($product);
        $this->em->persist($vatRateRu);
        $this->em->persist($paymentVat);
        $this->em->persist($order);
        $this->em->flush();

        $requestBody = str_replace(
            ['{order}', '{ip}', '{country}'],
            [$order->getId(), $ip, 'us'],
            $this->validXmlRequestTemplate
        );

        $this->client->request('POST', '/payment-gateway/callback/rg', [], [], [], $requestBody);
        $this->assertEquals($this->validXmlResponse, trim($this->client->getResponse()->getContent()));

        $this->em->refresh($order);

        $this->assertCount(1, $order->getPayments());
        $this->assertCount(1, $order->getLastPayment()->getTransactions());

        $transaction = $order->getLastPayment()->getLastTransaction();
        $this->assertEquals(Payment::PAYMENT_STATUS_PENDING, $transaction->getStatus());
        $this->assertEquals($ip, $transaction->getIp());
        $this->assertNotEmpty($transaction->getGatewayTransactionData());
        $this->assertNotEmpty($transaction->getPayment());
        $this->assertNotNull($order->getLastPayment()->getQualifiedVat());
        $this->assertEquals(VatRate::FALLBACK_COUNTRY_CODE, $order->getLastPayment()->getQualifiedVat()->getCountry());
    }

    /**
     * @dataProvider setQualifiedVatForDifferentCountriesDataProvider
     *
     * @param string      $ipCountry
     * @param string      $qualifiedVatCountry
     * @param string|null $customerCountry
     * @param string|null $bankCountry
     */
    public function testValidXmlCallbackWithDifferentCountriesSetCorrectQualifiedVat(
        string $ipCountry,
        string $qualifiedVatCountry,
        string $customerCountry = null,
        string $bankCountry = null
    ) {
        $this->mockGeoIpService();

        $ip                     = '127.0.0.1';
        $customer               = $this->entityFactory->getCustomer();
        $order                  = $this->entityFactory->getOrder(
            ['customer' => $customer, 'status' => Payment::PAYMENT_STATUS_PENDING]
        );
        $website                = $this->entityFactory->getWebsite();
        $product                = $this->entityFactory->getProduct(['website' => $website]);
        $productPrice           = $this->entityFactory->getProductPrice(['product' => $product]);
        $ipCountryVatRate       = $this->entityFactory->getVatRate(['country' => $ipCountry]);
        $customerCountryVatRate = $bankCountryVatRate = null;
        $paymentVat             = $this->entityFactory->getPaymentVat();
        if ($customerCountry) {
            if ($customerCountry != $ipCountry) {
                $customerCountryVatRate = $this->entityFactory->getVatRate(['country' => $customerCountry]);
                $this->em->persist($customerCountryVatRate);
            } else {
                $customerCountryVatRate = $ipCountryVatRate;
            }
        }
        if ($bankCountry) {
            if ($bankCountry == $customerCountry) {
                $bankCountryVatRate = $customerCountryVatRate;
            } elseif ($bankCountry == $ipCountry) {
                $bankCountryVatRate = $ipCountryVatRate;
            } else {
                $bankCountryVatRate = $this->entityFactory->getVatRate(['country' => $bankCountry]);
                $this->em->persist($bankCountryVatRate);
            }
        }

        $payment = $this->entityFactory->getPayment(['order' => $order, 'predictedVat' => $paymentVat]);
        $paymentVat->setPayment($payment);
        $paymentVat->setVatRate($ipCountryVatRate);
        $product->addProductPrice($productPrice);
        $order->addPayment($payment);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($product);
        $this->em->persist($ipCountryVatRate);
        $this->em->persist($paymentVat);
        $this->em->persist($order);
        $this->em->flush();

        $requestBody = str_replace(
            ['{order}', '{ip}', '{customerCountry}', '{bankCountry}'],
            [$order->getId(), $ip, $customerCountry ?? '', $bankCountry ?? ''],
            $this->validXmlRequestTemplate
        );

        $this->client->request('POST', '/payment-gateway/callback/rg', [], [], [], $requestBody);
        $this->assertEquals($this->validXmlResponse, trim($this->client->getResponse()->getContent()));

        $this->em->refresh($order);

        $this->assertCount(1, $order->getPayments());
        $this->assertCount(1, $order->getLastPayment()->getTransactions());

        $transaction = $order->getLastPayment()->getLastTransaction();
        $this->assertEquals(Payment::PAYMENT_STATUS_PENDING, $transaction->getStatus());
        $this->assertEquals($ip, $transaction->getIp());
        $this->assertNotEmpty($transaction->getGatewayTransactionData());
        $this->assertNotEmpty($transaction->getPayment());
        $this->assertNotNull($order->getLastPayment()->getQualifiedVat());
        $this->assertEquals($qualifiedVatCountry, $order->getLastPayment()->getQualifiedVat()->getCountry());
    }

    /**
     * @return array
     */
    public function setQualifiedVatForDifferentCountriesDataProvider(): array
    {
        return [
            ['ru', 'ru'],
            ['ru', 'ru', 'ru'],
            ['us', 'ru', 'ru'],
            ['us', 'us', null, 'us'],
            ['us', 'ru', null, 'ru'],
            ['us', 'us', 'us', 'us'],
            ['ru', 'us', 'us', 'us'],
            ['ru', 'ru', 'us', 'ru'],
            ['ru', 'ru', 'ru', 'ru'],
            ['', VatRate::FALLBACK_COUNTRY_CODE],
        ];
    }

    public function testInvalidHashSuccessHandleFails()
    {
        $this->client->request(
            'GET',
            '/payment-gateway/callback/rg/success',
            ['flag' => 123, 'id' => 1, 'invoiceId' => 1, 'hash' => 123]
        );

        $this->assertRedirectUrlParameterHasKey('error');
        $this->assertRedirectUrlParameterValue('error', 'RocketGate hash is not valid');
    }

    public function testInvalidOrderIdSuccessHandleFails()
    {
        $service = $this->getContainer()->get(RocketGateService::class);

        $this->client->request(
            'GET',
            '/payment-gateway/callback/rg/success',
            [
                'flag'      => 123,
                'id'        => 1,
                'invoiceID' => 1,
                'hash'      => $service->calculateRocketGateHash(
                    [
                        'flag'      => 123,
                        'id'        => 1,
                        'invoiceID' => 1,
                        'secret'    => $this->getContainer()->getParameter('rocket_gate.shared_secret'),
                    ]
                ),
            ]
        );

        $this->assertRedirectUrlParameterHasKey('error');
        $this->assertRedirectUrlParameterValue(
            'error',
            'Cannot mark order completed and/or create content permission during RG success callback'
        );
    }

    public function testSuccessHandle()
    {
        $sqsClientMock = \Mockery::mock(NullSqsClient::class);
        $sqsClientMock
            ->shouldReceive('sendMessage')
            ->once();
        $this->getContainer()->stub(SqsClientInterface::class, $sqsClientMock);

        $service = $this->getContainer()->get(RocketGateService::class);

        $customer     = $this->entityFactory->getCustomer();
        $order        = $this->entityFactory->getOrder(
            ['customer' => $customer, 'status' => Payment::PAYMENT_STATUS_PENDING]
        );
        $website      = $this->entityFactory->getWebsite();
        $product      = $this->entityFactory->getProduct(['website' => $website, 'validForDays' => 10]);
        $orderProduct = $this->entityFactory->getOrderProduct(['order' => $order, 'product' => $product]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $vatRateRu    = $this->entityFactory->getVatRate();
        $paymentVat   = $this->entityFactory->getPaymentVat();
        $payment      = $this->entityFactory->getPayment(['order' => $order, 'predictedVat' => $paymentVat]);
        $transaction  = $this->entityFactory->getPaymentTransaction(
            ['payment' => $payment, 'qualifiedVat' => $vatRateRu]
        );

        $payment->addTransaction($transaction);
        $paymentVat->setPayment($payment);
        $paymentVat->setVatRate($vatRateRu);
        $product->addProductPrice($productPrice);
        $order->addPayment($payment);
        $order->addProduct($orderProduct);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($product);
        $this->em->persist($vatRateRu);
        $this->em->persist($paymentVat);
        $this->em->persist($order);
        $this->em->flush();

        $this->client->request(
            'GET',
            '/payment-gateway/callback/rg/success',
            [
                'flag'      => 123,
                'id'        => $customer->getId(),
                'invoiceID' => $order->getId(),
                'hash'      => $service->calculateRocketGateHash(
                    [
                        'flag'      => 123,
                        'id'        => $customer->getId(),
                        'invoiceID' => $order->getId(),
                        'secret'    => $this->getContainer()->getParameter('rocket_gate.shared_secret'),
                    ]
                ),
            ]
        );

        $this->assertRedirectUrlParameterHasNotKey('error');

        $this->em->refresh($order);

        $this->assertCount(1, $order->getPayments());
        $this->assertCount(1, $order->getLastPayment()->getTransactions());
        $this->assertEquals(
            Payment::PAYMENT_STATUS_COMPLETED,
            $order->getLastPayment()->getLastTransaction()->getStatus()
        );

        $contentPermission = $this->em->getRepository(ContentPermission::class)->findOneBy(
            [
                'customer'    => $customer,
                'permission'  => StanContentPermission::PERMISSION_WEBSITE_ACCESS,
                'contentId'   => $website->getId(),
                'contentType' => $website->getOwnClassName(),
            ]
        );

        $this->assertNotNull($contentPermission);
        $this->assertEquals($order->getId(), $contentPermission->getGrantedVia());
        $this->assertEquals($order->getOwnClassName(), $contentPermission->getGrantedViaType());
        $this->assertNotNull($contentPermission->getExpiresAt());
    }

    public function testSuccessHandleWithNotPendingTransaction()
    {
        $service = $this->getContainer()->get(RocketGateService::class);

        $customer     = $this->entityFactory->getCustomer();
        $order        = $this->entityFactory->getOrder(
            ['customer' => $customer, 'status' => Payment::PAYMENT_STATUS_PENDING]
        );
        $website      = $this->entityFactory->getWebsite();
        $product      = $this->entityFactory->getProduct(['website' => $website]);
        $orderProduct = $this->entityFactory->getOrderProduct(['order' => $order, 'product' => $product]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $vatRateRu    = $this->entityFactory->getVatRate();
        $paymentVat   = $this->entityFactory->getPaymentVat();
        $payment      = $this->entityFactory->getPayment(['order' => $order, 'predictedVat' => $paymentVat]);
        $transaction  = $this->entityFactory->getPaymentTransaction(
            ['payment' => $payment, 'qualifiedVat' => $vatRateRu, 'status' => Payment::PAYMENT_STATUS_FAILED]
        );

        $payment->addTransaction($transaction);
        $paymentVat->setPayment($payment);
        $paymentVat->setVatRate($vatRateRu);
        $product->addProductPrice($productPrice);
        $order->addPayment($payment);
        $order->addProduct($orderProduct);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($product);
        $this->em->persist($vatRateRu);
        $this->em->persist($paymentVat);
        $this->em->persist($order);
        $this->em->flush();

        $this->client->request(
            'GET',
            '/payment-gateway/callback/rg/success',
            [
                'flag'      => 123,
                'id'        => $customer->getId(),
                'invoiceID' => $order->getId(),
                'hash'      => $service->calculateRocketGateHash(
                    [
                        'flag'      => 123,
                        'id'        => $customer->getId(),
                        'invoiceID' => $order->getId(),
                        'secret'    => $this->getContainer()->getParameter('rocket_gate.shared_secret'),
                    ]
                ),
            ]
        );

        $this->assertRedirectUrlParameterHasNotKey('error');
        $this->em->refresh($order);

        $this->assertCount(1, $order->getPayments());
        $this->assertCount(2, $order->getLastPayment()->getTransactions());

        $transaction = $order->getLastPayment()->getLastTransaction();
        $this->assertEquals(Payment::PAYMENT_STATUS_COMPLETED, $transaction->getStatus());
    }

    public function testFailureHandle()
    {
        $service = $this->getContainer()->get(RocketGateService::class);

        $customer     = $this->entityFactory->getCustomer();
        $order        = $this->entityFactory->getOrder(
            ['customer' => $customer, 'status' => Payment::PAYMENT_STATUS_PENDING]
        );
        $website      = $this->entityFactory->getWebsite();
        $product      = $this->entityFactory->getProduct(['website' => $website]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $vatRateRu    = $this->entityFactory->getVatRate();
        $paymentVat   = $this->entityFactory->getPaymentVat();
        $payment      = $this->entityFactory->getPayment(['order' => $order, 'predictedVat' => $paymentVat]);
        $transaction  = $this->entityFactory->getPaymentTransaction(
            ['payment' => $payment, 'qualifiedVat' => $vatRateRu]
        );

        $payment->addTransaction($transaction);
        $paymentVat->setPayment($payment);
        $paymentVat->setVatRate($vatRateRu);
        $product->addProductPrice($productPrice);
        $order->addPayment($payment);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($product);
        $this->em->persist($vatRateRu);
        $this->em->persist($paymentVat);
        $this->em->persist($order);
        $this->em->flush();

        $this->client->request(
            'GET',
            '/payment-gateway/callback/rg/failure',
            [
                'flag'      => 123,
                'errcat'    => 5,
                'errcode'   => 503,
                'id'        => $customer->getId(),
                'invoiceID' => $order->getId(),
                'hash'      => $service->calculateRocketGateHash(
                    [
                        'flag'      => 123,
                        'errcat'    => 5,
                        'errcode'   => 503,
                        'id'        => $customer->getId(),
                        'invoiceID' => $order->getId(),
                        'secret'    => $this->getContainer()->getParameter('rocket_gate.shared_secret'),
                    ]
                ),
            ]
        );

        $this->assertRedirectUrlParameterHasNotKey('error');

        $this->em->refresh($order);

        $this->assertCount(1, $order->getPayments());
        $this->assertCount(1, $order->getLastPayment()->getTransactions());
        $this->assertEquals(
            Payment::PAYMENT_STATUS_FAILED,
            $order->getLastPayment()->getLastTransaction()->getStatus()
        );
    }

    public function testFailureHandleWithNotPendingTransaction()
    {
        $service = $this->getContainer()->get(RocketGateService::class);

        $customer     = $this->entityFactory->getCustomer();
        $order        = $this->entityFactory->getOrder(
            ['customer' => $customer, 'status' => Payment::PAYMENT_STATUS_PENDING]
        );
        $website      = $this->entityFactory->getWebsite();
        $product      = $this->entityFactory->getProduct(['website' => $website]);
        $productPrice = $this->entityFactory->getProductPrice(['product' => $product]);
        $vatRateRu    = $this->entityFactory->getVatRate();
        $paymentVat   = $this->entityFactory->getPaymentVat();
        $payment      = $this->entityFactory->getPayment(['order' => $order, 'predictedVat' => $paymentVat]);
        $transaction  = $this->entityFactory->getPaymentTransaction(
            ['payment' => $payment, 'qualifiedVat' => $vatRateRu, 'status' => Payment::PAYMENT_STATUS_COMPLETED]
        );

        $payment->addTransaction($transaction);
        $paymentVat->setPayment($payment);
        $paymentVat->setVatRate($vatRateRu);
        $product->addProductPrice($productPrice);
        $order->addPayment($payment);

        $this->em->persist($customer);
        $this->em->persist($website);
        $this->em->persist($product);
        $this->em->persist($vatRateRu);
        $this->em->persist($paymentVat);
        $this->em->persist($order);
        $this->em->flush();

        $this->client->request(
            'GET',
            '/payment-gateway/callback/rg/failure',
            [
                'flag'      => 123,
                'errcat'    => 5,
                'errcode'   => 503,
                'id'        => $customer->getId(),
                'invoiceID' => $order->getId(),
                'hash'      => $service->calculateRocketGateHash(
                    [
                        'flag'      => 123,
                        'errcat'    => 5,
                        'errcode'   => 503,
                        'id'        => $customer->getId(),
                        'invoiceID' => $order->getId(),
                        'secret'    => $this->getContainer()->getParameter('rocket_gate.shared_secret'),
                    ]
                ),
            ]
        );

        $this->assertRedirectUrlParameterHasNotKey('error');

        $this->em->refresh($order);

        $this->assertCount(1, $order->getPayments());
        $this->assertCount(2, $order->getLastPayment()->getTransactions());

        $transaction = $order->getLastPayment()->getLastTransaction();
        $this->assertEquals(Payment::PAYMENT_STATUS_FAILED, $transaction->getStatus());
    }

    public function testBugWithRedirectUrlWhenHashIsInvalid()
    {
        $this->client->request(
            'GET',
            '/payment-gateway/callback/rg/failure',
            [
                'flag'      => 123,
                'hash'      => 123,
            ]
        );

        $this->assertRedirectUrlParameterHasKey('error');
        $this->assertRedirectUrlParameterValue('error', 'RocketGate hash is not valid');
    }

    private function assertRedirectUrlParameterHasKey(string $key)
    {
        $redirectUrl = $this->getRedirectUrl($this->client->getResponse());
        $request  = Request::create($redirectUrl);

        if (!$request->query->has($key)) {
            self::fail(sprintf('Key "%s" not found in the redirect url', $key));
        }

        return true;
    }

    private function assertRedirectUrlParameterHasNotKey(string $key)
    {
        $redirectUrl = $this->getRedirectUrl($this->client->getResponse());
        $request  = Request::create($redirectUrl);

        if ($request->query->has($key)) {
            self::fail(sprintf('Key "%s" not found in the redirect url', $key));
        }

        return true;
    }

    private function assertRedirectUrlParameterValue(string $key, string $value)
    {
        $this->assertRedirectUrlParameterHasKey($key);

        $redirectUrl = $this->getRedirectUrl($this->client->getResponse());
        $request  = Request::create($redirectUrl);
        if ($request->query->get($key) != $value) {
            self::fail(
                sprintf(
                    'Incorrect expected value "%s" for the key "%s". The actual value is: "%s"',
                    $value,
                    $key,
                    $request->query->get($key)
                )
            );
        }

        return true;
    }

    /**
     * @param Response $response
     *
     * @return string
     */
    private function getRedirectUrl(Response $response): string
    {
        preg_match('~parent\.window\.location\s=\s\'(?P<redirectUrl>[^)]+)\'~', $response->getContent(), $matches);

        if (!array_key_exists('redirectUrl', $matches)) {
            self::fail('There is no redirect response');
        }

        return $matches['redirectUrl'];
    }
}
