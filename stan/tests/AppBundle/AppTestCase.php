<?php

namespace Tests\AppBundle;

use AppBundle\Service\GeoIpService;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use EaPaysites\Service\Rpc\Client\RpcClientInterface;
use EaPaysites\Service\ServiceResponseInterface;
use EaPaysites\Test\DependencyInjection\MockerContainer;
use Mockery\Mock;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class AppTestCase extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Connection
     */
    protected $conn;

    /**
     * @var EntityFactory
     */
    protected $entityFactory;

    public function setUp()
    {
        parent::setUp();

        static::bootKernel();
        $this->setUpSqlDb();

        $this->entityFactory = new EntityFactory();
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->tearDownSqlDb();

        \Mockery::close();
    }

    /**
     * Assert value in json equals to expected
     *
     * @param        $expected
     * @param        $json
     * @param string $path
     */
    protected function assertJsonValue($expected, $json, $path = '')
    {
        self::assertJson($json, 'Not valid json');

        $json = json_decode($json, true);

        if (is_object($json)) {
            $json = (array)$json;
        }

        if (!is_array($json)) {
            self::fail('Path ' . $path . ' not found in JSON');
        }

        $path = trim($path, '/');

        if ($path !== '') {
            $pathParts = explode('/', $path);

            foreach ($pathParts as $pathPart) {
                if (is_object($json)) {
                    $json = (array)$json;
                }

                if (array_key_exists($pathPart, $json)) {
                    $json = $json[$pathPart];
                } else {
                    self::fail('Path ' . $path . ' not found in JSON');
                }
            }
        }

        $this->assertEquals($expected, $json);
    }

    /**
     * Assert JSON path exists
     *
     * @param string $json
     * @param string $path
     *
     * @return bool
     */
    protected function assertJsonKey(string $json, string $path): bool
    {
        self::assertJson($json, 'Not valid json');

        $json = json_decode($json, true);

        if (is_object($json)) {
            $json = (array)$json;
        }

        if (!is_array($json)) {
            self::fail('Path ' . $path . ' not found in JSON');
        }

        $path = trim($path, '/');

        if ($path !== '') {
            $pathParts = explode('/', $path);

            foreach ($pathParts as $pathPart) {
                if (is_object($json)) {
                    $json = (array)$json;
                }

                if (array_key_exists($pathPart, $json)) {
                    $json = $json[$pathPart];
                } else {
                    self::fail('Path ' . $path . ' not found in JSON');
                }
            }
        }

        return true;
    }

    /**
     * @return MockerContainer
     */
    protected function getContainer(): MockerContainer
    {
        /** @var MockerContainer $container */
        $container = static::$kernel->getContainer();

        return $container;
    }

    /**
     * @param callable                 $requestCompareCallback
     * @param ServiceResponseInterface $response
     */
    protected function mockRpcClient(callable $requestCompareCallback, ServiceResponseInterface $response)
    {
        /** @var Mock $rpcClientMock */
        $rpcClientMock = \Mockery::mock(RpcClientInterface::class);
        $rpcClientMock
            ->shouldReceive('send')
            ->once()
            ->withArgs($requestCompareCallback)
            ->andReturn($response);

        $this->getContainer()->stub(RpcClientInterface::class, $rpcClientMock);
    }

    /**
     * @param string $countryCode
     */
    protected function mockGeoIpService(string $countryCode = 'ru')
    {
        $mock = \Mockery::mock(GeoIpService::class);
        $mock
            ->shouldReceive('getCountryCodeByIp')
            ->once()
            ->withArgs(['127.0.0.1'])
            ->andReturn($countryCode);

        $this->getContainer()->stub(GeoIpService::class, $mock);
    }

    private function setUpSqlDb()
    {
        /** @var EntityManager $em */
        $this->em = static::$kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager');

        $conn = $this->em->getConnection();
        $conn->beginTransaction();
        $this->conn = clone $conn;
    }

    private function tearDownSqlDb()
    {
        if ($this->conn) {
            $this->conn->rollBack();
            $this->conn->close();
        }
    }
}
