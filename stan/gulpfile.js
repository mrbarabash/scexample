var gulp = require('gulp');
var del = require('del');
var gulpSourceMaps = require('gulp-sourcemaps');
var gulpIf = require('gulp-if');
var gulpSass = require('gulp-sass');
var gulpConcat = require('gulp-concat');
var gulpAutoprefixer = require('gulp-autoprefixer');
var gulpCleanCss = require('gulp-clean-css');
var argv = require('yargs').argv;
var merge = require('merge-stream');
var uglify = require('gulp-uglify');

var isProductionBuild = !!argv.production;

gulp.task('clean:dist', function () {
    'use strict';

    let sources = [
        'web/dist/css/*'
    ];

    return del(sources);
});

gulp.task('build:css', ['clean:dist'], function () {
    'use strict';

    let sources = [
        {
            pattern:             [
                'app/Resources/scss/bulma.scss'
            ],
            destinationFileName: 'lib.css',
            destinationDir:      'web/dist/css'
        },
        {
            pattern:             [
                'app/Resources/scss/components/*.scss'
            ],
            destinationFileName: 'app.css',
            destinationDir:      'web/dist/css'
        }
    ];

    let tasks = sources.map(function (source) {
        return gulp.src(source.pattern)
            // disabled due to https://github.com/sass/libsass/issues/2312
            // .pipe(gulpSourceMaps.init())
            .pipe(gulpIf('*.scss', gulpSass({
                outputStyle: 'nested', // libsass doesn't support expanded yet
                precision:   10
            })).on('error', gulpSass.logError))
            .pipe(gulpConcat(source.destinationFileName))
            .pipe(gulpAutoprefixer({browsers: ['> 1%', 'last 2 versions']}))
            .pipe(gulpIf(isProductionBuild, gulpCleanCss()))
            // .pipe(gulpSourceMaps.write('.'))
            .pipe(gulp.dest(source.destinationDir));
    });

    return merge(tasks);
});

gulp.task('build:js', function () {
    'use strict';

    let destinationDir = 'web/dist/js';
    let sources = [
        {
            pattern:             [
                'app/Resources/js/*.js',
            ],
            destinationFileName: 'app.js',
            mangle:              true
        }
    ];

    let tasks = sources.map(function (source) {
        return gulp.src(source.pattern)
            .pipe(gulpSourceMaps.init())
            .pipe(gulpConcat(source.destinationFileName))
            .pipe(gulpIf(isProductionBuild, uglify({
                compress: {
                    'drop_debugger': true
                },
                mangle:   source.mangle
            })))
            .pipe(gulpSourceMaps.write('.'))
            .pipe(gulp.dest(destinationDir));
    });

    return merge(tasks);
});

gulp.task('build', ['build:css', 'build:js']);

gulp.task('watch', ['build'], function () {
    'use strict';

    gulp.watch(
        [
            'app/Resources/scss/components/*.scss'
        ],
        ['build:css']
    );

    gulp.watch(
        [
            'app/Resources/js/*.js'
        ],
        ['build:js']
    );
});
