## Project for EvilAngel (adult company older than me)

Stack: PHP 7.2, Symfony 3.3, MySQL 5, Doctrine ORM, Redis, PHPUnit tests.

It's an adult website (*blackmailed*) with member area, but all content managed and delivered using by common system (*stan*) designed to serve other similar websites. Code-shared content placed in *paysites-shared*.